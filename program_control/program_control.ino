#include <DS3232RTC.h> // include library ic DS3232RTC
#include <Time.h> // --------- เป็นการดึง ฟังก์ขั่น ของเวลาเข้ามาใช้
#include <Wire.h> // ---------  ใช้สำหรับสื่อสาร ระหว่าง อุปกรณ์
#include <Streaming.h> // --------- ส่วนนี้ก็ใช้สำหรับ การสื่อสารระหว่าง อุปกรณ์
#include <EEPROM.h> // -----------  ดึงความสามารถ ของการอ่านเขียน รอม มาใช้

//===========================  Zone config Pin device ===========================================
int a = 7;  // 7-segment LED A
int b = 6;  // 7-segment LED B
int c = 3;  // 7-segment LED C
int d = 4;  // 7-segment LED D
int e = 5;  // 7-segment LED E
int f = 9;  // 7-segment LED F
int g = 8;  // 7-segment LED G
int dot = 2; // 7-segment LED DP
int sw1 = A0; // control ON or OFF 7-segment digit 1
int sw2 = A1; // control ON or OFF 7-segment digit 2
int sw3 = A2; // control ON or OFF 7-segment digit 3
int sw4 = A3; // control ON or OFF 7-segment digit 4
int plus = A4; //Button Plus
int minut = A5; //Button minus
int start = A6; //Button Start
int sett = A7; //Button set
int reset = A8; //Button reset
int switchrelay1 = 22; //pin control relay 1
int switchrelay2 = 23; //pin control relay 2
//===================================================================================================

int delayrelay = 0;  // ประการตัวแปรไว้ใช้เพื่อหน่วงเวลาการ สับ เปลี่ยนการทำงานระหว่าง รีเลย์
int count = 0; // ประกาศตัวแปรไว้สำหรับนับรอบ
String seg1,seg2,seg3,seg4; // ประกาศตัวแปล ไว้สำหรับ แสดงผลให้กับ 7-segment ที่ละหลัก

int countsett = 0; // ประกาศตัวแปรไว้สำหรับนับ รอบ หน่วงเวลาของการกดปุ่น set
int countplus = 0; // ประกาศตัวแปรไว้สำหรับนับ รอบ หน่วงเวลาของการกดปุ่น บวก
int countminut = 0; // ประกาศตัวแปรไว้สำหรับนับ รอบ หน่วงเวลาของการกดปุ่ม ลบ

void setup() {    
  Serial.begin(9600); // เรียกใช้การสื่อสารเป็น serial port
  setSyncProvider(RTC.get);  // เรียกใช้การสื่อสารกับ RTC Module 
  
  if(timeStatus() != timeSet)   // ตรวจสอบว่า สามารถ เชื่อมกับ RTC Module ได้หรือไม่
    Serial.println("Unable to sync with the RTC"); // แสดงผลออกทาง Serial port ในกรณี ที่ไม่สามารถเชื่อมต่อกับ RTC Module ได้
  else // ในกรณีที่สามารถตรอจ สอบได้และผ่าน 
    Serial.println("RTC has set the system time");  // แสดงผลออกทาง Serial port ในกรณี ที่สามารถเชื่อมต่อกับ RTC Module ได้

  // ==================================================  All pin device close =================================================  
  pinMode(a, OUTPUT); // บอกว่าให้ขา a ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(b, OUTPUT); // บอกว่าให้ขา b ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(c, OUTPUT); // บอกว่าให้ขา c ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(d, OUTPUT); // บอกว่าให้ขา d ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(e, OUTPUT); // บอกว่าให้ขา e ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(f, OUTPUT); // บอกว่าให้ขา f ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(g, OUTPUT); // บอกว่าให้ขา g ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(dot, OUTPUT); // บอกว่าให้ขา dot ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(sw1, OUTPUT);  // บอกว่าให้ขา sw1 ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(sw2, OUTPUT); // บอกว่าให้ขา sw2 ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(sw3, OUTPUT); // บอกว่าให้ขา sw3 ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(sw4, OUTPUT); // บอกว่าให้ขา sw4 ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(plus,INPUT); // บอกว่าให้ขา plus ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น INPUT เป็นการรอรับค่าการกดปุ่ม
  pinMode(minut,INPUT); // บอกว่าให้ขา minut ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น INPUT เป็นการรอรับค่าการกดปุ่ม
  pinMode(start,INPUT); // บอกว่าให้ขา start ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น INPUT เป็นการรอรับค่าการกดปุ่ม
  pinMode(sett,INPUT); // บอกว่าให้ขา sett ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น INPUT เป็นการรอรับค่าการกดปุ่ม
  pinMode(reset,INPUT); // บอกว่าให้ขา reset ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น INPUT เป็นการรอรับค่าการกดปุ่ม
  pinMode(switchrelay1,OUTPUT); // บอกว่าให้ขา switchrelay1 ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  pinMode(switchrelay2,OUTPUT); // บอกว่าให้ขา switchrelay2 ซึ่งเป็นตัวแปรที่ประกาศไว้ด้านบน ว่า เป็น OUTPUT
  
  digitalWrite(a, HIGH); // สั่งให้ขา a ดับ โดยการส่งค่า HIGH ไป
  digitalWrite(b, HIGH); // สั่งให้ขา b ดับ โดยการส่งค่า HIGH ไป
  digitalWrite(c, HIGH); // สั่งให้ขา c ดับ โดยการส่งค่า HIGH ไป
  digitalWrite(d, HIGH); // สั่งให้ขา d ดับ โดยการส่งค่า HIGH ไป
  digitalWrite(e, HIGH); // สั่งให้ขา e ดับ โดยการส่งค่า HIGH ไป
  digitalWrite(f, HIGH); // สั่งให้ขา f ดับ โดยการส่งค่า HIGH ไป
  digitalWrite(g, HIGH); // สั่งให้ขา g ดับ โดยการส่งค่า HIGH ไป
  digitalWrite(dot, HIGH); // สั่งให้ขา dow ดับ โดยการส่งค่า HIGH ไป
  digitalWrite(sw1, LOW); // สั่งให้ หลอดที่ 1 ดับ โดยการส่ง ค่า LOW ไป
  digitalWrite(sw2, LOW); // สั่งให้ หลอดที่ 2 ดับ โดยการส่ง ค่า LOW ไป
  digitalWrite(sw3, LOW); // สั่งให้ หลอดที่ 3 ดับ โดยการส่ง ค่า LOW ไป
  digitalWrite(sw4, LOW); // สั่งให้ หลอดที่ 4 ดับ โดยการส่ง ค่า LOW ไป
  digitalWrite(switchrelay1,LOW); // สั่งให้ relay1 ดับ โดยการส่งค่า LOW ไป
  digitalWrite(switchrelay2,LOW);  // สั่งให้ relay2 ดับ โดยการส่งค่า LOW ไป
  // ====================================================================================================================
}
void loop() {  
   // =====================================  program set time ===========================================================
   static time_t tLast;
   time_t t;
   tmElements_t tm;
   if (Serial.available() >= 12) {
        //note that the tmElements_t Year member is an offset from 1970,
        //but the RTC wants the last two digits of the calendar year.
        //use the convenience macros from Time.h to do the conversions.
        int y = Serial.parseInt();
        if (y >= 100 && y < 1000)
            Serial << F("Error: Year must be two digits or four digits!") << endl;
        else {
            if (y >= 1000)
                tm.Year = CalendarYrToTm(y);
            else    //(y < 100)
                tm.Year = y2kYearToTm(y);
            tm.Month = Serial.parseInt();
            tm.Day = Serial.parseInt();
            tm.Hour = Serial.parseInt();
            tm.Minute = Serial.parseInt();
            tm.Second = Serial.parseInt();
            t = makeTime(tm);
            RTC.write(tm);
            setTime(t);        
            Serial << F("RTC set to: ");
            printDateTime(t);
            Serial << endl;
            //dump any extraneous input
            while (Serial.available() > 0) Serial.read();
        }
    }
    //============================================================================================================
   // ==========================  show real time =================================================================
   count++; // เพิ่มค่า count ที่ละ 1
   int t1 = hour(); // t1 เท่ากับ ชั่วโมงที่อ่านได้จาก RTC โดยเก็บค่า เป็น int (ตัวเลข)
   int t2 = minute(); // t2 เท่ากับ นาทีที่อ่านได้จาก RTC โดยเก็บค่า เป็น int (ตัวเลข)
   digitalWrite(switchrelay1,LOW);  // สั่งให้ relay1 ดับ โดยการส่งค่า LOW ไป
   digitalWrite(switchrelay2,LOW);  // สั่งให้ relay2 ดับ โดยการส่งค่า LOW ไป
   
   on_sw("sw1"); // สั่งให้หลอดที่ 1 ติด
   print_segment(String(t1/10)); // ส่งตัวเลขออกไปแสดง
   delay(3); // หน่วงเวลา 3 มิลลิวินาที
   off_sw("sw1"); // สั่งให้หลอดที่ 1 ดับ
   delay(2); // หน่วงเวลา 2 มิลลิวินาที
   
   on_sw("sw2"); // สั่งให้หลอดที่ 2 ติด
   if(count == 50){ // ถ้า ตัวแปร count มีค่าเท่ากับ 50
     print_segment(String(t1%10)+"."); // แสดงผลออกที่ 7-segment
   } // จบการ if
   else{ // ถ้าไม่เข้ากรณีแรก ก็เข้า กรณี นี้
     print_segment(String(t1%10)); // แสดงผลออกที่ 7-segment
   } // จบคำสั่ง else
   delay(3); // หน่วงเวลา 3 มิลลิวินาที
   off_sw("sw2"); // สั่งให้หลอดที่ 2 ดับ
   delay(2); // หน่วงเวลา 2 มิลลิวินาที
   
   on_sw("sw3"); // สั่งให้หลอดที่ 3 ติด
   print_segment(String(t2/10)); // แสดงผลออก 7-segment
   delay(3); // หน่วงเวลา 3 มิลลิวินาที
   off_sw("sw3"); // สั่งให้หลอดที่ 3 ดับ
   delay(2); // หน่วงเวลา 2 มิลลิวินาที
   
   on_sw("sw4"); // สั่งให้หลอดที่ 4 ติด
   print_segment(String(t2%10)); // แสดงผลออก 7-segment
   delay(3); // หน่วงเวลา 3 มิลลิวินาที
   off_sw("sw4"); // สั่งให้หลอดที่ 4 ดับ
   delay(2); // หน่วงเวลา 2 มิลลิวินาที
   
   if(digitalRead(sett) == HIGH){ // ถ้ามีการกดปุ่น set
     while(digitalRead(sett) == HIGH); // ถ้ายังกดปุ่มค้างไว้ อยู่ แล้วยังไม่ปล่อย จะทำงานอยู่ตรงนี้ จนกว่าจะปล่อยปุ่ม set
     delay(100); // หน่วงเวลา 100 มิลลิวินาที
     countdown(); // ไปที่ฟังก์ ชั่น countdown
     if(digitalRead(sett) == HIGH){ // ในกรณี้ ที่มีการกลับมาจากการทำงานของ countdown จะทำการเช็คกการกดปุ่น set อีกที
       while(digitalRead(sett) == HIGH); // ถ้ายังกดปุ่ม set ค้างไว้อยู่ จะยังทำงานอยุ่ บันทัดนี้ตลอดเวลา
     } // จบการทำงาน if 
   } // จบการทำงาน if
   // ======= เมื่อ count = 50 จะทำการสแสดงผล ออกทาง Serial port ============================
   if(count == 50){
     Serial.print(hour());
     Serial.print(' ');
     Serial.print(minute());
     Serial.print(' ');
     Serial.print(second());
     Serial.print(' ');
     Serial.print(day());
     Serial.print(' ');
     Serial.print(month());
     Serial.print(' ');
     Serial.print(year());
     Serial.println(); 
     count = 0;
   }
   // ===================================================================================== 
  // ============================================================================================================  
}
void countdown(){
  seg1 = String(EEPROM.read(0)); // อ่านค่า จาก รอมตำแหน่งที่ 0 และเปลี่ยนเป็น String มาเป็นไว้ที่ตัวแปร ชื่อ seg1
  seg2 = String(EEPROM.read(1)); // อ่านค่า จาก รอมตำแหน่งที่ 1 และเปลี่ยนเป็น String มาเป็นไว้ที่ตัวแปร ชื่อ seg2
  seg3 = String(EEPROM.read(2)); // อ่านค่า จาก รอมตำแหน่งที่ 2 และเปลี่ยนเป็น String มาเป็นไว้ที่ตัวแปร ชื่อ seg3
  seg4 = String(EEPROM.read(3)); // อ่านค่า จาก รอมตำแหน่งที่ 3 และเปลี่ยนเป็น String มาเป็นไว้ที่ตัวแปร ชื่อ seg4
  int numseg1 = EEPROM.read(0); // อ่านค่าจาก รอมตำแหน่งที่ 0 ไม่ทำการเปลี่ยนค่าเป็น String และมาเก็บในตัวแปรชื่อ numseg1 ซึ่งเป็น integer เหมือนกัน
  int numseg2 = EEPROM.read(1); // อ่านค่าจาก รอมตำแหน่งที่ 1 ไม่ทำการเปลี่ยนค่าเป็น String และมาเก็บในตัวแปรชื่อ numseg2 ซึ่งเป็น integer เหมือนกัน
  int numseg3 = EEPROM.read(2); // อ่านค่าจาก รอมตำแหน่งที่ 2 ไม่ทำการเปลี่ยนค่าเป็น String และมาเก็บในตัวแปรชื่อ numseg3 ซึ่งเป็น integer เหมือนกัน
  int numseg4 = EEPROM.read(3); // อ่านค่าจาก รอมตำแหน่งที่ 3 ไม่ทำการเปลี่ยนค่าเป็น String และมาเก็บในตัวแปรชื่อ numseg4 ซึ่งเป็น integer เหมือนกัน
  while(true){ // LOOP นี้ทำงานไม่รู้จบ 
        if(digitalRead(start) == HIGH){ // ถ้ากดปุ่ม start
          while(digitalRead(start) == HIGH); // wait Logic Low for button start
          // =================================  countdown time ================================================================
          int s = -1; 
          delay(100);
          int checkrelay = 0;
          while(digitalRead(start) == LOW){ // ทำงานในกรณีที่เราไม่ได้กดปุ่มแล้ว คำสั่งแบบนี้ บอกว่า ในกรณี้ ที่เรากดปุ่ม นั้น มันจะหยุดทำงาน
            delayrelay++;
            if(checkrelay == 0){ 
              digitalWrite(switchrelay1,HIGH);
              digitalWrite(switchrelay2,LOW);
              Serial.println("print state relay 1");
            }
            else if(checkrelay == 1){
              digitalWrite(switchrelay1,LOW);
              digitalWrite(switchrelay2,HIGH);
              Serial.println("print state relay 2");
            }
            //===================================================================================================================
            if(delayrelay == 100){  // for delay relay when you want control motor between SLOW OR FAST. change number for delay.
            //===================================================================================================================
              delayrelay = 0;
              if(checkrelay == 0){
                checkrelay = 1;
              }
              else if(checkrelay == 1){
                checkrelay = 0;
              }
            }
            if(s != second()){
               s = second();
               
               numseg4--;
               if(numseg4 < 0){
                 numseg4 = 9;
                 numseg3--;
               }
               if(numseg3 < 0){
                 numseg3 = 5;
                 numseg4 = 9;
                 numseg2--;
               }
               if(numseg2 < 0){
                 numseg2 = 9;
                 numseg1--;
               }
               if(numseg1 < 0){
                 break;
               }
            }
             on_sw("sw1");
             print_segment(String(numseg1));
             delay(3);
             off_sw("sw1");
             delay(2);
               
             on_sw("sw2");
             print_segment(String(numseg2)+".");
             delay(3);
             off_sw("sw2");
             delay(2);
              
             on_sw("sw3");
             print_segment(String(numseg3));
             delay(3);
             off_sw("sw3");
             delay(2);
              
             on_sw("sw4");
             print_segment(String(numseg4));
             delay(3);
             off_sw("sw4");
             delay(2);
          }
          while(digitalRead(start) == HIGH); // ถ้ามีการกดปุ่ม start อยู่ จะทำการรอ จนกว่าจะปล่อย
          break; // หลังจากปล่อยปุ่ม start แล้ว ก็จะหยุดทำงาน
        }
       // ======================================================================================================
       if(digitalRead(sett) == HIGH){ // เข้าเงื่อนไข ในกรณี ที่กดปุ่ม set
         while(digitalRead(sett) == HIGH){ // รอจนกว่าจะปล่อย ปุ่ม set
	// ===========================  แสดง เวลา ส่วน count down ค้างไว้เหมือนเดิมก่อน =============================
           on_sw("sw1");
           print_segment(seg1);
           delay(3);
           off_sw("sw1");
           delay(2);
           
           on_sw("sw2");
           print_segment(seg2);
           delay(3);
           off_sw("sw2");
           delay(2);
           
           on_sw("sw3");
           print_segment(seg3);
           delay(3);
           off_sw("sw3");
           delay(2);
           
           on_sw("sw4");
           print_segment(seg4);
           delay(3);
           off_sw("sw4");
           delay(2);
           countsett++;
           if(countsett == 100){ // เป็นการตรวจเช็คว่า กดปุ่ม set ค้างไว้หรือไม่
             countsett = 0; // ทำให้ตัว set เป้น 0 เหมือนเดิม
             setminut(); // ไปที่ฟังก์ชั่น setminut set เวลานาที
             break; // รอ หยุดการทำงาน หลังออก จาก setnimut แล้ว
           }
         }
         countsett = 0;
         break;
       }
	// =====================================================================================
       if(digitalRead(reset) == HIGH){ // ถ้ามีการกดปุ่ม reset
         while(digitalRead(reset) == HIGH){ // ถ้ายังไม่ปล่อยปุ่ม reset จากยังคงรออยุ่ตรงนี้
	   // ======================  ทำการเคลียค่าของ เวลาที่บันทึกไว้ และก็ แสดงผล ออกมาด้วยพร้อมๆ ===============================
           seg1 = String(EEPROM.read(0));
           seg2 = String(EEPROM.read(1));
           seg3 = String(EEPROM.read(2));
           seg4 = String(EEPROM.read(3));
           on_sw("sw1");
           print_segment(seg1);
           delay(3);
           off_sw("sw1");
           delay(2);
           
           on_sw("sw2");
           print_segment(seg2);
           delay(3);
           off_sw("sw2");
           delay(2);
           
           on_sw("sw3");
           print_segment(seg3);
           delay(3);
           off_sw("sw3");
           delay(2);
           
           on_sw("sw4");
           print_segment(seg4);
           delay(3);
           off_sw("sw4");
           delay(2);
           
           EEPROM.write(0,0);
           EEPROM.write(1,0);
           EEPROM.write(2,0);
           EEPROM.write(3,0);
	  // ==================================================================================================================
         }
       }
       // =====================================  เป้นการแสดงผลออก segment เหมือนเดิม =============================================
       on_sw("sw1");
       print_segment(seg1);
       delay(3);
       off_sw("sw1");
       delay(2);
       
       on_sw("sw2");
       print_segment(seg2+".");
       delay(3);
       off_sw("sw2");
       delay(2);
       
       on_sw("sw3");
       print_segment(seg3);
       delay(3);
       off_sw("sw3");
       delay(2);
       
       on_sw("sw4");
       print_segment(seg4);
       delay(3);
       off_sw("sw4");
       delay(2);
	// =============================================================================================================
  }  
}
// ==================================  ส่วนนี้เป็นส่วนของการ set นาที ==============================================================
void setminut(){
   seg1 = String(EEPROM.read(0));
   seg2 = String(EEPROM.read(1));
   int numseg1 = EEPROM.read(0);
   int numseg2 = EEPROM.read(1);
   while(digitalRead(sett) == HIGH){
     on_sw("sw1");
     print_segment(seg1);
     delay(3);
     off_sw("sw1");
     delay(2);
             
     on_sw("sw2");
     print_segment(seg2);
     delay(3);
     off_sw("sw2");
     delay(2);
   }
   while(true){
       seg1 = String(EEPROM.read(0));
       seg2 = String(EEPROM.read(1));
       numseg1 = EEPROM.read(0);
       numseg2 = EEPROM.read(1);
       
       if(digitalRead(sett) == HIGH){
           while(digitalRead(sett) == HIGH)
           setsecond();
           break;
       }
       
       if(digitalRead(plus) == HIGH){
           while(digitalRead(plus) == HIGH){
             countplus++;
             on_sw("sw1");
             print_segment(seg1);
             delay(3);
             off_sw("sw1");
             delay(2);
             
             on_sw("sw2");
             print_segment(seg2);
             delay(3);
             off_sw("sw2");
             delay(2);
             if(countplus == 100){
               int countslow = 0;
               while(digitalRead(plus) == HIGH){
                 seg1 = String(EEPROM.read(0));
                 seg2 = String(EEPROM.read(1));
                 numseg1 = EEPROM.read(0);
                 numseg2 = EEPROM.read(1);
                 
                 on_sw("sw1");
                 print_segment(seg1);
                 delay(3);
                 off_sw("sw1");
                 delay(2);
                 
                 on_sw("sw2");
                 print_segment(seg2);
                 delay(3);
                 off_sw("sw2");
                 delay(2);
                 
                 countslow++;
                 if(countslow == 10){       
                   numseg2++;
                   EEPROM.write(1,numseg2);
                   if(numseg2 == 10){
                     numseg2 = 0;
                     numseg1++;
                     EEPROM.write(0,numseg1);
                     EEPROM.write(1,numseg2);
                   }
                   if(numseg1 == 10){
                     numseg1 = 0;
                     numseg2 = 0;
                     EEPROM.write(0,numseg1);
                     EEPROM.write(1,numseg2);
                   }
                   countslow = 0;
                 }
               }
             }
           }
           countplus = 0;
           numseg2++;
           EEPROM.write(1,numseg2);
           if(numseg2 >= 10){
             numseg2 = 0;
             numseg1++;
             EEPROM.write(0,numseg1);
             EEPROM.write(1,numseg2);
           }
           if(numseg1 >= 10){
             numseg1 = 0;
             numseg2 = 0;
             EEPROM.write(0,numseg1);
             EEPROM.write(1,numseg2);
           }
       }  
       
       if(digitalRead(minut) == HIGH){
           while(digitalRead(minut) == HIGH){
             countminut++;
             on_sw("sw1");
             print_segment(seg1);
             delay(3);
             off_sw("sw1");
             delay(2);
             
             on_sw("sw2");
             print_segment(seg2);
             delay(3);
             off_sw("sw2");
             delay(2);
             if(countminut == 100){
               int countslow = 0;
               while(digitalRead(minut) == HIGH){
                 seg1 = String(EEPROM.read(0));
                 seg2 = String(EEPROM.read(1));
                 numseg1 = EEPROM.read(0);
                 numseg2 = EEPROM.read(1);
                 
                 on_sw("sw1");
                 print_segment(seg1);
                 delay(3);
                 off_sw("sw1");
                 delay(2);
                 
                 on_sw("sw2");
                 print_segment(seg2);
                 delay(3);
                 off_sw("sw2");
                 delay(2);
                 
                 countslow++;
                 if(countslow == 10){       
                   numseg2--;
                   EEPROM.write(1,numseg2);
                   if(numseg2 < 0){
                     numseg2 = 9;
                     numseg1--;
                     EEPROM.write(0,numseg1);
                     EEPROM.write(1,numseg2);
                   }
                   if(numseg1 < 0){
                     numseg1 = 9;
                     numseg2 = 9;
                     EEPROM.write(0,numseg1);
                     EEPROM.write(1,numseg2);
                   }
                   countslow = 0;
                 }
               }
             }
           }
           countminut = 0;
           numseg2--;
           EEPROM.write(1,numseg2);
           if(numseg2 < 0){
             numseg2 = 9;
             numseg1--;
             EEPROM.write(0,numseg1);
             EEPROM.write(1,numseg2);
           }
           if(numseg1 < 0){
             numseg1 = 9;
             numseg2 = 9;
             EEPROM.write(0,numseg1);
             EEPROM.write(1,numseg2);
           }
       }
       
        if(digitalRead(reset) == HIGH){
         while(digitalRead(reset) == HIGH){
           seg1 = String(EEPROM.read(0));
           seg2 = String(EEPROM.read(1));
           
           on_sw("sw1");
           print_segment(seg1);
           delay(3);
           off_sw("sw1");
           delay(2);
           
           on_sw("sw2");
           print_segment(seg2);
           delay(3);
           off_sw("sw2");
           delay(2);
           
           EEPROM.write(0,0);
           EEPROM.write(1,0);
         }
       }
       
       on_sw("sw1");
       print_segment(seg1);
       delay(3);
       off_sw("sw1");
       delay(2);
       
       on_sw("sw2");
       print_segment(seg2);
       delay(3);
       off_sw("sw2");
       delay(2);
   }
}
//================================================================================================================
// ส่วนนี้เป็นส่วนของการ set วินาที =====================================================================================
void setsecond(){
  seg3 = String(EEPROM.read(2));
  seg4 = String(EEPROM.read(3));
  int numseg3 = EEPROM.read(2);
  int numseg4 = EEPROM.read(3);
  while(digitalRead(sett) == HIGH){
       on_sw("sw3");
       print_segment(seg3);
       delay(3);
       off_sw("sw3");
       delay(2);
       
       on_sw("sw4");
       print_segment(seg4);
       delay(3);
       off_sw("sw4");
       delay(2);
  }
  while(true){
      seg3 = String(EEPROM.read(2));
      seg4 = String(EEPROM.read(3));
      if(digitalRead(sett) == HIGH){
         while(digitalRead(sett) == HIGH);
         break;
       }
       
       if(digitalRead(plus) == HIGH){
           while(digitalRead(plus) == HIGH){
             countplus++;
             on_sw("sw3");
             print_segment(seg3);
             delay(3);
             off_sw("sw3");
             delay(2);
             
             on_sw("sw4");
             print_segment(seg4);
             delay(3);
             off_sw("sw4");
             delay(2);
             if(countplus == 100){
               int countslow = 0;
               while(digitalRead(plus) == HIGH){
                 seg3 = String(EEPROM.read(2));
                 seg4 = String(EEPROM.read(3));
                 numseg3 = EEPROM.read(2);
                 numseg4 = EEPROM.read(3);
                 
                 on_sw("sw3");
                 print_segment(seg3);
                 delay(3);
                 off_sw("sw3");
                 delay(2);
                 
                 on_sw("sw4");
                 print_segment(seg4);
                 delay(3);
                 off_sw("sw4");
                 delay(2);
                 
                 countslow++;
                 if(countslow == 10){       
                   numseg4++;
                   EEPROM.write(3,numseg4);
                   if(numseg4 == 10){
                     numseg4 = 0;
                     numseg3++;
                     EEPROM.write(2,numseg3);
                     EEPROM.write(3,numseg4);
                   }
                   if(numseg3 == 6){
                     numseg3 = 0;
                     numseg4 = 0;
                     EEPROM.write(2,numseg3);
                     EEPROM.write(3,numseg4);
                   }
                   countslow = 0;
                 }
               }
             }
           }
           countplus = 0;
           numseg4++;
           EEPROM.write(3,numseg4);
           if(numseg4 >= 10){
             numseg4 = 0;
             numseg3++;
             EEPROM.write(2,numseg3);
             EEPROM.write(3,numseg4);
           }
           if(numseg3 >= 6){
             numseg3 = 0;
             numseg4 = 0;
             EEPROM.write(2,numseg3);
             EEPROM.write(3,numseg4);
           }
       }  
       
       if(digitalRead(minut) == HIGH){
           while(digitalRead(minut) == HIGH){
             countminut++;
             on_sw("sw3");
             print_segment(seg3);
             delay(3);
             off_sw("sw3");
             delay(2);
             
             on_sw("sw4");
             print_segment(seg4);
             delay(3);
             off_sw("sw4");
             delay(2);
             if(countminut == 100){
               int countslow = 0;
               while(digitalRead(minut) == HIGH){
                 seg3 = String(EEPROM.read(2));
                 seg4 = String(EEPROM.read(3));
                 numseg3 = EEPROM.read(2);
                 numseg4 = EEPROM.read(3);
                 
                 on_sw("sw3");
                 print_segment(seg3);
                 delay(3);
                 off_sw("sw3");
                 delay(2);
                 
                 on_sw("sw4");
                 print_segment(seg4);
                 delay(3);
                 off_sw("sw4");
                 delay(2);
                 
                 countslow++;
                 if(countslow == 10){       
                   numseg4--;
                   EEPROM.write(3,numseg4);
                   if(numseg4 < 0){
                     numseg4 = 9;
                     numseg3--;
                     EEPROM.write(2,numseg3);
                     EEPROM.write(3,numseg4);
                   }
                   if(numseg3 < 0){
                     numseg3 = 5;
                     numseg4 = 9;
                     EEPROM.write(2,numseg3);
                     EEPROM.write(3,numseg4);
                   }
                   countslow = 0;
                 }
               }
             }
           }
           countminut = 0;
           numseg4--;
           EEPROM.write(3,numseg4);
           if(numseg4 < 0){
             numseg4 = 9;
             numseg3--;
             EEPROM.write(2,numseg3);
             EEPROM.write(3,numseg4);
           }
           if(numseg3 < 0){
             numseg3 = 5;
             numseg4 = 9;
             EEPROM.write(2,numseg3);
             EEPROM.write(3,numseg4);
           }
       }
       
        if(digitalRead(reset) == HIGH){
         while(digitalRead(reset) == HIGH){
           seg3 = String(EEPROM.read(2));
           seg4 = String(EEPROM.read(3));
           
           on_sw("sw3");
           print_segment(seg3);
           delay(3);
           off_sw("sw3");
           delay(2);
           
           on_sw("sw4");
           print_segment(seg4);
           delay(3);
           off_sw("sw4");
           delay(2);
           
           EEPROM.write(2,0);
           EEPROM.write(3,0);
         }
       }
       
       on_sw("sw3");
       print_segment(seg3);
       delay(3);
       off_sw("sw3");
       delay(2);
       
       on_sw("sw4");
       print_segment(seg4);
       delay(3);
       off_sw("sw4");
       delay(2);
  }
}
// ===================================================================================================================
// =================================  function program set time ======================================================
void printDateTime(time_t t)
{
    printDate(t);
    Serial << ' ';
    printTime(t);
}
void printDate(time_t t)
{
    printI00(day(t), 0);
    Serial << monthShortStr(month(t)) << _DEC(year(t));
}
void printTime(time_t t)
{
    printI00(hour(t), ':');
    printI00(minute(t), ':');
    printI00(second(t), ' ');
}
void printI00(int val, char delim)
{
    if (val < 10) Serial << '0';
    Serial << _DEC(val);
    if (delim > 0) Serial << delim;
    return;
}
// =====================================================================================================================
// ===================================== Show 7-segment 4 digit ========================================================
void print_segment(String num){
  if(num.equals("1")){
    digitalWrite(a, HIGH);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, HIGH);
    digitalWrite(e, HIGH);
    digitalWrite(f, HIGH);
    digitalWrite(g, HIGH);
    digitalWrite(dot, HIGH);
  }
  else if(num.equals("2")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, HIGH);
    digitalWrite(d, LOW);
    digitalWrite(e, LOW); 
    digitalWrite(f, HIGH);
    digitalWrite(g, LOW);
    digitalWrite(dot, HIGH);
  }
  else if(num.equals("3")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, HIGH);
    digitalWrite(f, HIGH);
    digitalWrite(g, LOW);
    digitalWrite(dot, HIGH);
  }
  else if(num.equals("4")){
    digitalWrite(a, HIGH);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, HIGH);
    digitalWrite(e, HIGH);
    digitalWrite(f, LOW);
    digitalWrite(g, LOW);
    digitalWrite(dot, HIGH);
  }
  else if(num.equals("5")){
    digitalWrite(a, LOW);
    digitalWrite(b, HIGH);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, HIGH);
    digitalWrite(f, LOW);
    digitalWrite(g, LOW);
    digitalWrite(dot, HIGH);
  }
  else if(num.equals("6")){
    digitalWrite(a, LOW);
    digitalWrite(b, HIGH);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, LOW);
    digitalWrite(f, LOW);
    digitalWrite(g, LOW);
    digitalWrite(dot, HIGH);
  }
  else if(num.equals("7")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, HIGH);
    digitalWrite(e, HIGH);
    digitalWrite(f, HIGH);
    digitalWrite(g, HIGH);
    digitalWrite(dot, HIGH);
  }
  else if(num.equals("8")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, LOW);
    digitalWrite(f, LOW);
    digitalWrite(g, LOW);
    digitalWrite(dot, HIGH);
  }
  else if(num.equals("9")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, HIGH);
    digitalWrite(f, LOW);
    digitalWrite(g, LOW);
    digitalWrite(dot, HIGH);
  }
  else if(num.equals("0")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, LOW);
    digitalWrite(f, LOW);
    digitalWrite(g, HIGH);
    digitalWrite(dot, HIGH);
  }
  
  if(num.equals("1.")){
    digitalWrite(a, HIGH);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, HIGH);
    digitalWrite(e, HIGH);
    digitalWrite(f, HIGH);
    digitalWrite(g, HIGH);
    digitalWrite(dot, LOW);
  }
  else if(num.equals("2.")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, HIGH);
    digitalWrite(d, LOW);
    digitalWrite(e, LOW); 
    digitalWrite(f, HIGH);
    digitalWrite(g, LOW);
    digitalWrite(dot, LOW);
  }
  else if(num.equals("3.")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, HIGH);
    digitalWrite(f, HIGH);
    digitalWrite(g, LOW);
    digitalWrite(dot, LOW);
  }
  else if(num.equals("4.")){
    digitalWrite(a, HIGH);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, HIGH);
    digitalWrite(e, HIGH);
    digitalWrite(f, LOW);
    digitalWrite(g, LOW);
    digitalWrite(dot, LOW);
  }
  else if(num.equals("5.")){
    digitalWrite(a, LOW);
    digitalWrite(b, HIGH);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, HIGH);
    digitalWrite(f, LOW);
    digitalWrite(g, LOW);
    digitalWrite(dot, LOW);
  }
  else if(num.equals("6.")){
    digitalWrite(a, LOW);
    digitalWrite(b, HIGH);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, LOW);
    digitalWrite(f, LOW);
    digitalWrite(g, LOW);
    digitalWrite(dot, LOW);
  }
  else if(num.equals("7.")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, HIGH);
    digitalWrite(e, HIGH);
    digitalWrite(f, HIGH);
    digitalWrite(g, HIGH);
    digitalWrite(dot, LOW);
  }
  else if(num.equals("8.")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, LOW);
    digitalWrite(f, LOW);
    digitalWrite(g, LOW);
    digitalWrite(dot, LOW);
  }
  else if(num.equals("9.")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, HIGH);
    digitalWrite(f, LOW);
    digitalWrite(g, LOW);
    digitalWrite(dot, LOW);
  }
  else if(num.equals("0.")){
    digitalWrite(a, LOW);
    digitalWrite(b, LOW);
    digitalWrite(c, LOW);
    digitalWrite(d, LOW);
    digitalWrite(e, LOW);
    digitalWrite(f, LOW);
    digitalWrite(g, HIGH);
    digitalWrite(dot, LOW);
  }
}
// =========================================================================================================================
// ====================================  on position 7-segment =============================================================
void on_sw(String s){
  if(s.equals("sw1")){
    digitalWrite(sw1, HIGH);
  }
  else if(s.equals("sw2")){
    digitalWrite(sw2, HIGH);
  }
  else if(s.equals("sw3")){
    digitalWrite(sw3, HIGH);
  }
  else if(s.equals("sw4")){
    digitalWrite(sw4, HIGH);
  }
}
// =============================================================================================================================
// ================================== off positino 7-segment ===================================================================
void off_sw(String s){
  if(s.equals("sw1")){
    digitalWrite(sw1, LOW);
  }
  else if(s.equals("sw2")){
    digitalWrite(sw2, LOW);
  }
  else if(s.equals("sw3")){
    digitalWrite(sw3, LOW);
  }
  else if(s.equals("sw4")){
    digitalWrite(sw4, LOW);
  }
}
// ==============================================================================================================================
