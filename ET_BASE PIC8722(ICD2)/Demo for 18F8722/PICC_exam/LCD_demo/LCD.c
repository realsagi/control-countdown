/*
Code Support     : Board ET-BASE PIC8722
Compiler         : PIC C Compiler Version 3.249
Micro Controller : PIC18F8722
OSC              : 40MHz
*/
/*                PIC18F8722         LCD Display
                   ---------         --------
             +5---|Vdd    H4|----11-|D4      |
                  |       H5|----12-|D5      |
           Gnd----|Vss    H6|----13-|D6      |
                  |       H7|----14-|D7      |
          24MHz---|Xtal   H3|-----6-|EN      |
              ----|Xtal   H1|-----4-|RS      |
                  |         |        --------
                  |         |
                  |         |
                  |         |
                   ---------

**********************************************************/
#include <18F8722.h>

#include "lcd_for_base8722.c"

#define TX1   PIN_C6
#define RX1   PIN_C7

#fuses   H4,NOLVP,NOWDT,NOPROTECT,NOSTVREN

#use delay (clock = 40000000)
#use rs232(baud = 9600, xmit = TX1, rcv = RX1)

//-------------------------------------------------------------------------
void main() {

  int16 Dat;


      set_tris_h(0B00000000);
      setup_comparator(NC_NC_NC_NC);   // Disable Comparator module
      SETUP_ADC_PORTS(NO_ANALOGS);     // Disable Analog to Digital module

      Output_low(PIN_H2);              // Write LCD

      delay_ms(1000);              // Wait for LCD Start-up

      lcd_init();
      delay_ms(100);

   While (1)
   {

    printf(lcd_putc,"\fET-BASE PIC8722\n");
    delay_ms(1000);
    printf(lcd_putc," * ETT CO.,LTD *");
    delay_ms(1000);

   }
}
