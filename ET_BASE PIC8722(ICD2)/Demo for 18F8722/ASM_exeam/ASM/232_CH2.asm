;**********************************************
; Test RS232 or RS422(Single node) 
; Hardware  : ET-BASE PIC18F8722
; Oscillator  40 MHz
; file name : 232_CH2.ASM
; Assembler : mpasm.exe 
; Programmer: Watcharin Kaolop
; Company   : ETT  CO.,LTD.
;**********************************************
; STEP to follow when test this program
;    1.Connect RS232 or RS422 cable between board and PC.
;    2.Download this program to CPU.
;    3.Run terminal program such as Procom plus,XTALK etc.
;    4.Set parameter Procom plus to 9600 baud,No parity,8 bits data and 1 bit. stop
;    5.Reset board ,after reset board you will see this message on PC.
;
;         "TEST PIC18F8720 ON RS-232 By ETT CO.,LTD."
;       
;    6.Press any key on keyboard, you will see the key that you pressed.
;**********************************************


     list p=18f8722                 ; list directive to define processor
     #include <p18f8722.inc>        ; processor specific variable definitions
;---------------------------------------------------------------------------------------
;  Device Configuration
;---------------------------------------------------------------------------------------
;code protect disabled
	CONFIG     CP0=OFF
;Oscillator H4 PLL.
	CONFIG     OSC=HSPLL
;Power Up Timer Enabled
	CONFIG     PWRT = ON            
;Watch Dog Timer disable
	CONFIG     WDT=OFF
;Low Voltage ICSP  Enabled
   	CONFIG     LVP = OFF            

;---------------------------------------------------------------------------------------
offset    EQU       0x20
temp      EQU       0x21
TEST	  EQU	    0x22
dt1       EQU       0x23
dt2       EQU       0x24
dt3       EQU       0x25

          ORG       0x0000

;************ initial *******************
init      clrf      TRISD
          movlw     .64           ; BAUD rate 9600
          movwf     SPBRG2
          clrf      TXSTA2          ; 8 bits data ,no,1 stop
          bsf       RCSTA2,SPEN     ; Asynchronous serial port enable
          bsf       RCSTA2,CREN     ; continuous receive
          bsf       TXSTA2,TXEN     ; Transmit enable
          bcf	    TXSTA2,BRGH	   ; HI SPEED
	  	  
;********** start to send **********************

new       clrf      offset         ; load offset of character table
	      call	    delay
start     movf      offset,w       
          call      TAB
          addlw     0              ; Character = 00 ?
          btfsc     STATUS,Z       ; Character = 00 ?
          goto      wait2          ; Yes , Z = 1
                                   ; No  , Z = 0
          movwf     TXREG2          ; Send recent data to TX 
wait1     lfsr      0,TXSTA2        ; 
          btfss     INDF0,1        ; check TRMT bit in TXSTA (FSR)
          goto      wait1          ; TXREG full  or TRMT = 0
          incf      offset,f       ; TXREG empty  or TRMT = 1
	      incf	    offset,f
          goto      start          ; Send again

;********** start to receive *******************          
wait2     btfss     PIR3,RC2IF      ; Check RCIF  bit in PIR1 register
          goto      wait2          ; RCREG empty or RCIF = 0
          movf      RCREG2,w        ; RCREG full or RCIF = 1
          movwf     TXREG2
          goto      wait2

;********* Tebel of message ********************  
TAB       addwf     PCL            ; Move offset to PC lower

	  DT	    0x0C,"TEST PIC18F8720 ON RS-232 Channel 2 By ETT CO.,LTD.",0XA,0XD,0X0

  
delay    movlw	    5
	     movwf	    dt1
sd3	     clrf      dt2
sd2      clrf      dt3
sd1      decfsz    dt3
         goto      sd1
         decfsz    dt2
         goto      sd2
	     decfsz    dt1
	     goto      sd3
         return

        END
