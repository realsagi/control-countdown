;********************************************                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ;********************************************
; Test  LED On Board
; Hardware  : ET-BASE PIC8722 
; OSC   10MHz or 40MHz
; Assembler : mpasm.exe
; Programmer: Watcharin Kaorop
; Company   : ETT  CO.,LTD.
;********************************************

     LIST P=18f8722
     include <p18f8722.inc>

;---------------------------------------------------------------------------------------
;  Device Configuration
;---------------------------------------------------------------------------------------
;code protect disabled
	CONFIG     CP0=OFF
;Oscillator H4 PLL.
	CONFIG     OSC=HSPLL
;Power Up Timer Enabled
	CONFIG     PWRT = ON            
;Watch Dog Timer disable
	CONFIG     WDT=OFF
;Low Voltage ICSP  Enabled
   	CONFIG     LVP = OFF            
;External Bus Data Wait Disabled
    CONFIG     WAIT = OFF             
;---------------------------------------------------------------------------------------

dt1         EQU       0x23
dt2         EQU       0x24
dt3         EQU       0x25

           ORG       0x0000

	    clrf	 TRISA	        ; port A is output
		clrf     TRISB          ; port B is output
	    clrf	 TRISC	        ; port C is output
		clrf     TRISD          ; port D is output
	    clrf	 TRISE	        ; port E is output
		clrf     TRISF			; port F is output
		clrf	 TRISG			; port G is output
		clrf	 TRISH			; port H is output
	    clrf	 TRISJ			; port J is output
	
	    movlw    0x0F
		movwf    ADCON1			; PORTA is digital I/O
		movwf    CMCON			; Comparator off

loop    movlw    0x55
		movwf    PORTA          ; Out data to PORTA
		movwf    PORTB          ; Out data to PORTB
		movwf    PORTC          ; Out data to PORTC
		movwf    PORTD          ; Out data to PORTD
		movwf    PORTE          ; Out data to PORTE
		movwf    PORTF          ; Out data to PORTF
		movwf    PORTG          ; Out data to PORTG
		movwf    PORTH          ; Out data to PORTH
		movwf    PORTJ          ; Out data to PORTJ
        call     delay

        movlw    0xAA
		movwf    PORTA          ; Out data to PORTA
		movwf    PORTB          ; Out data to PORTB
		movwf    PORTC          ; Out data to PORTC
		movwf    PORTD          ; Out data to PORTD
		movwf    PORTE          ; Out data to PORTE
		movwf    PORTF          ; Out data to PORTF
		movwf    PORTG          ; Out data to PORTG
		movwf    PORTH          ; Out data to PORTH
		movwf    PORTJ          ; Out data to PORTJ
        call     delay

        goto     loop

;***************************************
	
delay   movlw	  10
	    movwf	  dt1
sd3	    clrf      dt2
sd2     clrf      dt3
sd1     decfsz    dt3
        goto      sd1
        decfsz    dt2
        goto      sd2
	    decfsz    dt1
	    goto      sd3
        return

     end

