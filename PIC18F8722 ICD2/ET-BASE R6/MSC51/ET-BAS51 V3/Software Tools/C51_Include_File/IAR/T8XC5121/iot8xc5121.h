                                                                          
/*                      - iot8xc5121.H -

   Special header for the ATMEL t8xc5121 microcontroller.

*/

#pragma language=extended



/*===============================*/
/* Predefined SFR Byte Addresses */
/*===============================*/

sfr    P0        = 0x80;
sfr    P1        = 0x90;
sfr    P2        = 0xA0;
sfr    P3        = 0xB0;
sfr    PSW       = 0xD0;
sfr    ACC       = 0xE0;
sfr    B         = 0xF0;
sfr    SP        = 0x81;
sfr    DPL       = 0x82;
sfr    DPH       = 0x83;
sfr    PCON      = 0x87;
sfr    TCON      = 0x88;
sfr    TMOD      = 0x89;
sfr    TL0       = 0x8A;
sfr    TL1       = 0x8B;
sfr    TH0       = 0x8C;
sfr    TH1       = 0x8D;
sfr    AUXR      = 0x8E;
sfr    CKCON     = 0x8F;
sfr    CKCON0    = 0x8F;
sfr    SIOCON    = 0x91;
sfr    CKRL      = 0x97;
sfr    SCON      = 0x98;
sfr    SBUF      = 0x99;
sfr    BRL       = 0x9A;
sfr    BDRCON    = 0x9B;
sfr    AUXR1     = 0xA2;
sfr    WDTRST    = 0xA6;
sfr    WDTPRG    = 0xA7;
sfr    IEN0      = 0xA8;
sfr    SADDR     = 0xA9;
sfr    SCTBUF    = 0xAA;
sfr    SCRBUF    = 0xAA;
sfr    SCSR      = 0xAB;
sfr    SCCON     = 0xAC;
sfr    SCETU0    = 0xAC;
sfr    SCISR     = 0xAD;
sfr    SCETU1    = 0xAD;
sfr    SCIIR     = 0xAE;
sfr    SCIER     = 0xAE;
sfr    CKCON1    = 0xAF;
sfr    IEN1      = 0xB1;
sfr    IPL1      = 0xB2;
sfr    IPH1      = 0xB3;
sfr    SCWT0     = 0xB4;
sfr    SCGT0     = 0xB4;
sfr    SCWT1     = 0xB5;
sfr    SCGT1     = 0xB5;
sfr    SCWT2     = 0xB6;
sfr    SCICR     = 0xB6;
sfr    IPH0      = 0xB7;
sfr    IPL       = 0xB8;
sfr    IPL0      = 0xB8;
sfr    SADEN     = 0xB9;
sfr    ISEL      = 0xBA;
sfr    DCCKPS    = 0xBF;
sfr    RCON      = 0xD1;
sfr    LEDCON    = 0xF1;



/*==============================*/
/* Predefined SFR Bit Addresses */
/*==============================*/


/*========P1==========*/

bit   P1_0       = 0x90;
bit   P1_1       = 0x91;
bit   P1_2       = 0x92;
bit   P1_3       = 0x93;
bit   P1_4       = 0x94;
bit   P1_5       = 0x95;
bit   P1_6       = 0x96;
bit   P1_7       = 0x97;

/*========P2==========*/

bit   P2_0       = 0xA0;
bit   P2_1       = 0xA1;
bit   P2_2       = 0xA2;
bit   P2_3       = 0xA3;
bit   P2_4       = 0xA4;
bit   P2_5       = 0xA5;
bit   P2_6       = 0xA6;
bit   P2_7       = 0xA7;

/*========P3==========*/

bit   RXD        = 0xB0;
bit   TXD        = 0xB1;
bit   INT0       = 0xB2;
bit   INT1       = 0xB3;
bit   T0         = 0xB4;
bit   T1         = 0xB5;
bit   WR         = 0xB6;
bit   RD         = 0xB7;
bit   P3_0       = 0xB0;
bit   P3_1       = 0xB1;
bit   P3_2       = 0xB2;
bit   P3_3       = 0xB3;
bit   P3_4       = 0xB4;
bit   P3_5       = 0xB5;
bit   P3_6       = 0xB6;
bit   P3_7       = 0xB7;

/*========PSW=========*/

bit   P          = 0xD0;
bit   UD         = 0xD1;
bit   OV         = 0xD2;
bit   RS0        = 0xD3;
bit   RS1        = 0xD4;
bit   F0         = 0xD5;
bit   AC         = 0xD6;
bit   CY         = 0xD7;

/*========TCON========*/

bit   IT0        = 0x88;
bit   IE0        = 0x89;
bit   IT1        = 0x8A;
bit   IE1        = 0x8B;
bit   TR0        = 0x8C;
bit   TF0        = 0x8D;
bit   TR1        = 0x8E;
bit   TF1        = 0x8F;


/*========SCON========*/

bit   RI         = 0x98;
bit   TI         = 0x99;
bit   RB8        = 0x9A;
bit   TB8        = 0x9B;
bit   REN        = 0x9C;
bit   SM2        = 0x9D;
bit   SM1        = 0x9E;
bit   FE         = 0x9F;
bit   SM0        = 0x9F;

/*========IE==========*/

bit   EX0        = 0xA8;
bit   ET0        = 0xA9;
bit   EX1        = 0xAA;
bit   ET1        = 0xAB;
bit   ES         = 0xAC;
bit   EA         = 0xAF;

/*========IP==========*/

bit   PX0        = 0xB8;
bit   PT0        = 0xB9;
bit   PX1        = 0xBA;
bit   PT1        = 0xBB;
bit   PS         = 0xBC;



/*==============================*/
/* Interrupt Vector Definitions */
/*==============================*/

       
interrupt [0x03] void extern0_int (void);  /* Vector 0, External Interrupt 0      */
interrupt [0x0B] void timer0_int (void);   /* Vector 1, Timer 0 Overflow          */
interrupt [0x13] void extern1_int (void);  /* Vector 2, External Interrupt 1      */
interrupt [0x1B] void timer1_int (void);   /* Vector 3, Timer 1 Overflow          */
interrupt [0x23] void sio_ri_int (void);   /* Vector 4, Serial Port receive       */
interrupt [0x23] void sio_ti_int (void);   /* Vector 5, Serial Port transmit      */
interrupt [0x53] void sci_int (void);      /* Vector 6, Timer 2 interrupt         */



  
