                                                                          
/*                      - io8xc5122.h -

   Special header for the ATMEL t8xc5122 microcontroller.

*/

#pragma language=extended


/*----------------------------- C51 CORE -----------------------------------*/

sfr     SP      = 0x81;			//Stack Pointer
sfr     DPL     = 0x82;			//Data Pointer Low byte
sfr     DPH     = 0x83;			//Data Pointer High Byte

sfr     PSW     = 0xD0;			//Program Status Word
bit     CY      = 0xD7;		    //Carry Flag
bit     AC      = 0xD6;		    //Auxilary Carry Flag
bit     F0      = 0xD5;		    //Flag 0 available to the user for general purpose
bit     RS1     = 0xD4;		    //Register Bank Selector bit 1
bit     RS0     = 0xD3;		    //Register Bank Selector bit 0
bit     OV      = 0xD2;		    //Overflow Bit
bit     F1      = 0xD1;		    //General Purpose Flag
bit     P       = 0xD0;		    //Parity Bit

sfr     A       = 0xE0;			//Accumulator
sfr     ACC     = 0xE0;			//Accumulator
sfr     B       = 0xF0;			//B register

/*----------------------- SYSTEM CONFIGURATION -----------------------------*/

sfr     CKSEL   = 0x85;			//Clock Selection
sfr     PCON    = 0x87;			//Power Control
sfr     AUXR    = 0x8E;			//Auxilary Register 0
sfr     CKCON0  = 0x8F;			//Clock Control Register 0
sfr     CKRL    = 0x97;			//Clock Reload
sfr     AUXR1   = 0xA2;			//Auxilary Register 1
sfr     PLLCON  = 0xA3;			//Phase Lock Loop Control
sfr     PLLDIV  = 0xA4;			//Phase Lock Loop Divider
sfr     CKCON1  = 0xAF;			//Clock Control Register 0
sfr     RCON    = 0xD1;			//Memory Selection

/*---------------------------- INTERRUPTS ----------------------------------*/

sfr     ISEL    = 0xA1;			//Interrupt Enable Control

sfr     IEN0    = 0xA8;			//Interrupt Enable Control 0
bit     EA      = 0xAF;		    //Global Interrupts Enable/Disable Bit
bit     ES      = 0xAC;		    //Serial Port Interrupt Enable/Disable Bit
bit     ET1     = 0xAB;		    //Timer 1 Overflow Interrupt Enable/Disable Bit
bit     EX1     = 0xAA;		    //External Interrupt 1 Enable/Disable Bit
bit     ET0     = 0xA9;		    //Timer 0 Overflow Interrupt Enable/Disable Bit
bit     EX0     = 0xA8;		    //External Interrupt 0 Enable/Disable Bit

sfr     IPH0    = 0xB7;			//Interrupt Priority Control High 0

sfr     IPL0    = 0xB8;			//Interrupt Priority Control Low  0
bit     PSL     = 0xBC;		    //Serial Port Interrupt Priority LSB
bit     PT1L    = 0xBB;		    //Timer 1 Overflow Interrupt Priority LSB
bit     PX1L    = 0xBA;		    //External Interrupt 1 Priority LSB
bit     PT0L    = 0xB9;		    //Timer 0 Overflow Interrupt Priority LSB
bit     PX0L    = 0xB8;		    //External Interrupt 0 Priority LSB

sfr     IEN1    = 0xB1;			//Interrupt Enable Control 1
sfr     IPL1    = 0xB2;			//Interrupt Priority Control Low  1
sfr     IPH1    = 0xB3;			//Interrupt Priority Control High 1

/*------------------------------- PORTS -----------------------------------*/

sfr     P0      = 0x80;			//Port 0
bit     P0_7    = 0x87;
bit     P0_6    = 0x86;
bit     P0_5    = 0x85;
bit     P0_4    = 0x84;
bit     P0_3    = 0x83;
bit     P0_2    = 0x82;
bit     P0_1    = 0x81;
bit     P0_0    = 0x80;

sfr     PMOD1   = 0x84;			//Port Mode Register 1

sfr     P1      = 0x90;			//Port 1
bit     P1_7    = 0x97;
bit     P1_6    = 0x96;
bit     P1_2    = 0x92;
bit     CCLK1   = 0x97;		    //Alternate Card Clock
bit     CPRES   = 0x92;		    //Smart Card Presence

sfr     PMOD0   = 0x91;			//Port Mode Register 0

sfr     P2      = 0xA0;			//Port 2
bit     P2_7    = 0xA7;
bit     P2_6    = 0xA6;
bit     P2_5    = 0xA5;
bit     P2_4    = 0xA4;
bit     P2_3    = 0xA3;
bit     P2_2    = 0xA2;
bit     P2_1    = 0xA1;
bit     P2_0    = 0xA0;

sfr     P3      = 0xB0;			//Port 3
bit     P3_7    = 0xB7;
bit     P3_6    = 0xB6;
bit     P3_5    = 0xB5;
bit     P3_4    = 0xB4;
bit     P3_3    = 0xB3;
bit     P3_2    = 0xB2;
bit     P3_1    = 0xB1;
bit     P3_0    = 0xB0;
bit     T1      = 0xB5;		    //External Event 1
bit     T0      = 0xB4;		    //External Event 0
bit     INT1    = 0xB3;		    //External Interrupt 1
bit     INT0    = 0xB2;		    //External Interrupt 0
bit     TXD     = 0xB1;		    //UART Transmit Port
bit     RXD     = 0xB0;		    //UART Receive Port
bit     CRST1   = 0xB5;		    //Alternate Card Reset
bit     CIO1    = 0xB4;		    //Alternate Card I/O
bit     LED3    = 0xB7;		    //Led Number 3
bit     LED2    = 0xB6;		    //Led Number 2
bit     LED1    = 0xB4;		    //Led Number 1
bit     LED0    = 0xB2;		    //Led Number 0

sfr     P4      = 0xC0;			//Port 4
bit     P4_7    = 0xC7;
bit     P4_6    = 0xC6;
bit     P4_5    = 0xC5;
bit     P4_4    = 0xC4;
bit     P4_3    = 0xC3;
bit     P4_2    = 0xC2;
bit     P4_1    = 0xC1;
bit     P4_0    = 0xC0;
bit     LED5    = 0xC4;		    //Led Number 5
bit     LED4    = 0xC3;		    //Led Number 4

sfr     P5      = 0xE8;			//Port 5
bit     P5_7    = 0xEF;
bit     P5_6    = 0xEE;
bit     P5_5    = 0xED;
bit     P5_4    = 0xEC;
bit     P5_3    = 0xEB;
bit     P5_2    = 0xEA;
bit     P5_1    = 0xE9;
bit     P5_0    = 0xE8;
bit     KB7     = 0xEF;		    //Keyboard Input Line 7
bit     KB6     = 0xEE;		    //Keyboard Input Line 6
bit     KB5     = 0xED;		    //Keyboard Input Line 5
bit     KB4     = 0xEC;		    //Keyboard Input Line 4
bit     KB3     = 0xEB;		    //Keyboard Input Line 3
bit     KB2     = 0xEA;		    //Keyboard Input Line 2
bit     KB1     = 0xE9;		    //Keyboard Input Line 1
bit     KB0     = 0xE8;		    //Keyboard Input Line 0

/*------------------------------- TIMERS -----------------------------------*/

sfr     TCON    = 0x88;			//Timer-Counter 0 and 1 Control
bit     TF1     = 0x8F;		    //Timer 1 Overflow Flag
bit     TR1     = 0x8E;		    //Timer 1 Run Control Bit
bit     TF0     = 0x8D;		    //Timer 0 Overflow Flag
bit     TR0     = 0x8C;		    //Timer 0 Run Control Bit
bit     IE1     = 0x8B;		    //Interrupt 1 Edge Flag
bit     IT1     = 0x8A;		    //Interrupt 1 Type Control Bit
bit     IE0     = 0x89;		    //Interrupt 0 Edge Flag
bit     IT0     = 0x88;		    //Interrupt 0 Type Control Bit

sfr     TMOD    = 0x89;			//Timer-Counter 0 and 1 Configuration
sfr     TL0     = 0x8A;			//Timer-Counter 0 Low Byte
sfr     TL1     = 0x8B;			//Timer-Counter 1 Low Byte
sfr     TH0     = 0x8C;			//Timer-Counter 0 High Byte
sfr     TH1     = 0x8D;			//Timer-Counter 1 High Byte

/*------------------------------ WATCHDOG --------------------------------*/

sfr     WDTRST  = 0xA6;			//WatchDog Timer Reset
sfr     WDTPRG  = 0xA7;			//WatchDog Timer Program

/*--------------------------- USB CONTROLLER -----------------------------*/

sfr     UFNUML  = 0xBA;			//USB Frame Number Low
sfr     UFNUMH  = 0xBB;			//USB Frame Number Low
sfr     USBCON  = 0xBC;			//USB Global Control
sfr     USBINT  = 0xBD;			//USB Global Interrupt
sfr     USBIEN  = 0xBE;			//USB Global Interrupt Enable
sfr     UEPIEN  = 0xC2;			//USB Endpoint Interrupt Enable
sfr     USBADDR = 0xC6;			//USB Address
sfr     UEPNUM  = 0xC7;			//USB Endpoint Number
sfr     UEPSTAX = 0xCE;			//USB Endpoint X Status
sfr     UEPDATX = 0xCF;			//USB Endpoint X Fifo Data
sfr     UEPCONX = 0xD4;			//USB Endpoint X Control
sfr     UEPRST  = 0xD5;			//USB Endpoint Reset
sfr     UBYCTX  = 0xE2;			//USB Byte Counter Low     EPX)
sfr     UEPINT  = 0xF8;			//USB Endpoint Interrupt
bit     EP6INT  = 0xFE;		
bit     EP5INT  = 0xFD;		
bit     EP4INT  = 0xFC;		
bit     EP3INT  = 0xFB;		
bit     EP2INT  = 0xFA;		
bit     EP1INT  = 0xF9;		
bit     EP0INT  = 0xF8;		

/*------------------------------- SCIB ----------------------------------*/

sfr     SCTBUF  = 0xAA;			//Smart Card Transmit Buffer     Write Only)			
sfr     SCRBUF  = 0xAA;			//Smart Card Receive Buffer     read Only)
sfr     SCSR    = 0xAB;			//Smart Card Selection Register
sfr     SCCON   = 0xAC;			//Smart Card Interface Contacts
sfr     SCETU0  = 0xAC;			//Smart Card ETU Register 0
sfr     SCISR   = 0xAD;			//Smart Card UART Interface Status
sfr     SCETU1  = 0xAD;			//Smart Card ETU Register 1
sfr     SCIIR   = 0xAE;			//Smart Card UART Interrupt Identification
sfr     SCIER   = 0xAE;			//Smart Card UART Interrupt Enable
sfr     SCWT0   = 0xB4;			//Smart Card Character-Block Waiting Time Register 0
sfr     SCGT0   = 0xB4;			//Smart Card Transmit Guard Time Register 0
sfr     SCWT1   = 0xB5;			//Smart Card Character-Block Waiting Time Register 1
sfr     SCGT1   = 0xB5;			//Smart Card Transmit Guard Time Register 1
sfr     SCWT2   = 0xB6;			//Smart Card Character-Block Waiting Time Register 2
sfr     SCICR   = 0xB6;			//Smart Card Interface Control
sfr     DCCKPS  = 0xBF;			//DC/DC Converter Reload Register
sfr     SCWT3   = 0xC1;			//Smart Card Character-Block Waiting Time Register 3
sfr     SCICLK  = 0xC1;			//Smart Card Clock Prescaler

/*------------------------------- UART ---------------------------------*/

sfr     SCON    = 0x98;			//UART Interface Control
bit     SM0     = 0x9F;		    //Serial Port Mode Bit 0		
bit     FE      = 0x9F;		    //Framing error Bit
bit     SM1     = 0x9E;		    //Serial Port Mode Bit 1
bit     SM2     = 0x9D;		    //Serial Port Mode Bit 2
bit     REN     = 0x9C;		    //Reception Enable Bit
bit     TB8     = 0x9B;		    //Transmitter bit 8
bit     RB8     = 0x9A;		    //Receiver Bit 8
bit     TI      = 0x99;		    //Transmit Interrupt Flag
bit     RI      = 0x98;		    //Receive Interrupt Flag

sfr     SBUF    = 0x99;			//UART Data Buffer
sfr     SADDR   = 0xA9;			//Slave Address
sfr     SADEN   = 0xB9;			//Slave Address Mask

/*------------------------ BAUD RATE GENERATOR -------------------------*/

sfr     BRL     = 0x9A;			//Baud Rate Relaod
sfr     BDRCON  = 0x9B;			//Baud Rate Control

/*----------------------------- KEYBOARD -------------------------------*/

sfr     KBF     = 0x9E;			//Keyboard Flag Register
sfr     KBE     = 0x9D;			//Keyboard Input Enable Register
sfr     KBLS    = 0x9C;			//Keyboard Level Selector

/*-------------------------- SPI CONTROLLER ----------------------------*/

sfr     SPCON   = 0xC3;			//Serial Peripheral Control
sfr     SPSTA   = 0xC4;			//Serial Peripheral Status-Control
sfr     SPDAT   = 0xC5;			//Serial Peripheral Data

/*-------------------------------- LED ---------------------------------*/

sfr     LEDCON0 = 0xF1;			//LED control 0
sfr     LEDCON1 = 0xE1;			//LED control 1

/*------------------------------ PCMCIA --------------------------------*/

sfr     PCMADDR = 0xE4;			//PCMCIA Interface Address
sfr     PCMDATA = 0xE5;			//PCMCIA Interface Data
sfr     PCMTYPE = 0xE6;			//PCMCIA Interface Configuration
sfr     PCMINT  = 0xE7;			//PCMCIA Interface Interrupt Enable

/*------------------------------ DECODER -------------------------------*/

sfr     DECEN   = 0xE4;			//DECODER Interface Enable
sfr     DECPAG  = 0xE5;			//DECODER Interface Page
sfr     DECONF  = 0xE6;			//DECODER Interface Configuration

/*------------------------- EMULATION INTERFACE ------------------------*/

sfr     EMUCON  = 0xDF;			//EMULATION Interface Control
sfr     BKPCL   = 0xEF;			//Breakpoint PC Address Low
sfr     BKPCH   = 0xF7;			//Breakpoint PC Address High
sfr     ICON    = 0xFF;			//ICE Control Register

/*------------------------ INTERRUPTION VECTORS -----------------------*/

interrupt [0x03] void extern0_int (void);   // Vector 1, External Interrupt 0 
interrupt [0x0B] void timer0_int (void);    // Vector 2, Timer 0 Overflow     
interrupt [0x13] void extern1_int (void);   // Vector 3, External Interrupt 1 
interrupt [0x1B] void timer1_int (void);    // Vector 4, Timer 1 Overflow     
interrupt [0x23] void sio_ri_int (void);    // Vector 5, Serial Port receive  
interrupt [0x23] void sio_ti_int (void);    // Vector 5, Serial Port transmit 
interrupt [0x3B] void kbd_int (void);       // Vector 8, Keyboard Interface
interrupt [0x4B] void spi_int (void);       // Vector 10, SPI Interface
interrupt [0x53] void sci_int (void);       // Vector 11, Smart Card Interface
interrupt [0x63] void pcm_int (void);       // Vector 13, PCMCIA Interface
interrupt [0x6B] void uep_int (void);       // Vector 14, USB Endpoint 
interrupt [0x6B] void usb_int (void);       // Vector 14, USB Interface


