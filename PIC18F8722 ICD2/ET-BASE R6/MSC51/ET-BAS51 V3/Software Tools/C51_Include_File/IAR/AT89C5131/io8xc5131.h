/*                      - io8xc5131.H -

   Special header for the ATMEL AT8xC5131 microcontroller.

*/

#pragma language=extended



/*===============================*/
/* Predefined SFR Byte Addresses */
/*===============================*/

sfr    A         = 0xE0;
sfr    ACC       = 0xE0;
sfr    B         = 0xF0;
sfr    PSW       = 0xD0;
sfr    SP        = 0x81;
sfr    DPL       = 0x82;
sfr    DPH       = 0x83;
sfr    PCON      = 0x87;
sfr    AUXR      = 0x8E;
sfr    AUXR1     = 0xA2;

sfr    CKCON0    = 0x8F;
sfr    CKCON1    = 0xAF;
sfr    PLLCON    = 0xA3;
sfr    PLLDIV    = 0xA4;
sfr    IEN0      = 0xA8;
sfr    IPL0      = 0xB8;
sfr    IPH0      = 0xB7;
sfr    IEN1      = 0xB1;
sfr    IPL1      = 0xB2;
sfr    IPH1      = 0xB3;
sfr    P0        = 0x80;
sfr    P1        = 0x90;
sfr    P2        = 0xA0;
sfr    P3        = 0xB0;
sfr    P4        = 0xC0;
sfr    FCON      = 0xD1;
sfr    EECON     = 0xD2;
sfr    TCON      = 0x88;
sfr    TMOD      = 0x89;
sfr    TL0       = 0x8A;
sfr    TL1       = 0x8B;
sfr    TH0       = 0x8C;
sfr    TH1       = 0x8D;
sfr    WDTRST    = 0xA6;
sfr    WDTPRG    = 0xA7;
sfr    T2CON     = 0xC8;
sfr    T2MOD     = 0xC9;
sfr    RCAP2L    = 0xCA;
sfr    RCAP2H    = 0xCB;
sfr    TL2       = 0xCC;
sfr    TH2       = 0xCD;

sfr    USBCON    = 0xBC;
sfr    USBADDR   = 0xC6;
sfr    USBINT    = 0xBD;
sfr    USBIEN    = 0xBE;
sfr    UEPNUM    = 0xC7;
sfr    UEPCONX   = 0xD4;
sfr    UEPSTAX   = 0xCE;
sfr    UEPRST    = 0xD5;
sfr    UEPINT    = 0xF8;
sfr    UEPIEN    = 0xC2;
sfr    UEPDATX   = 0xCF;
sfr    UBYCTLX   = 0xE2;
sfr    UBYCTHX   = 0xE3;
sfr    UDPADDL   = 0xD6;
sfr    UDPADDH   = 0xD7;
sfr    UFNUML    = 0xBA;
sfr    UFNUMH    = 0xBB;

sfr    SCON      = 0x98;
sfr    SBUF      = 0x99;
sfr    SADDR     = 0xA9;
sfr    SADEN     = 0xB9;
sfr    SPCON     = 0xC3;
sfr    SPSTA     = 0xC4;
sfr    SPDAT     = 0xC5;
sfr    SSCON     = 0x93;
sfr    SSSTA     = 0x94;
sfr    SSCS      = 0x94;
sfr    SSDAT     = 0x95;
sfr    SSADR     = 0x96;

sfr    CCON      = 0xD8;
sfr    CMOD      = 0xD9;
sfr    CL        = 0xE9;
sfr    CH        = 0xF9;
sfr    CCAP0L    = 0xEA;
sfr    CCAP0H    = 0xFA;
sfr    CCAP1L    = 0xEB;
sfr    CCAP1H    = 0xFB;
sfr    CCAP2L    = 0xEC;
sfr    CCAP2H    = 0xFC;
sfr    CCAP3L    = 0xED;
sfr    CCAP3H    = 0xFD;
sfr    CCAP4L    = 0xEE;
sfr    CCAP4H    = 0xFE;
sfr    CCAPM0    = 0xDA;
sfr    CCAPM1    = 0xDB;
sfr    CCAPM2    = 0xDC;
sfr    CCAPM3    = 0xDD;
sfr    CCAPM4    = 0xDE;

sfr    LEDCON    = 0xF1;
sfr    BRL       = 0x9A;
sfr    BDRCON    = 0x9B;
sfr    KBLS      = 0x9C;
sfr    KBE       = 0x9D;
sfr    KBF       = 0x9E;


/*==============================*/
/* Predefined SFR Bit Addresses */
/*==============================*/


/*========PSW=========*/

bit   P          = 0xD0;
bit   F1         = 0xD1;
bit   OV         = 0xD2;
bit   RS0        = 0xD3;
bit   RS1        = 0xD4;
bit   F0         = 0xD5;
bit   AC         = 0xD6;
bit   CY         = 0xD7;

/*========IEN0========*/

bit   EX0        = 0xA8;
bit   ET0        = 0xA9;
bit   EX1        = 0xAA;
bit   ET1        = 0xAB;
bit   ES         = 0xAC;
bit   ET2        = 0xAD;
bit   EC         = 0xAE;
bit   EA         = 0xAF;

/*========IPL0========*/

bit   PX0L      = 0xB8;
bit   PT0L      = 0xB9;
bit   PX1L      = 0xBA;
bit   PT1L      = 0xBB;
bit   PSL       = 0xBC;
bit   PT2L      = 0xBD;
bit   PPCL      = 0xBE;

/*========P0==========*/

bit   P0_0       = 0x80;
bit   P0_1       = 0x81;
bit   P0_2       = 0x82;
bit   P0_3       = 0x83;
bit   P0_4       = 0x84;
bit   P0_5       = 0x85;
bit   P0_6       = 0x86;
bit   P0_7       = 0x87;

/*========P1==========*/

bit   P1_0       = 0x90;
bit   P1_1       = 0x91;
bit   P1_2       = 0x92;
bit   P1_3       = 0x93;
bit   P1_4       = 0x94;
bit   P1_5       = 0x95;
bit   P1_6       = 0x96;
bit   P1_7       = 0x97;

bit   KIN0       = 0x90;
bit   KIN1       = 0x91;
bit   KIN2       = 0x92;
bit   KIN3       = 0x93;
bit   KIN4       = 0x94;
bit   KIN5       = 0x95;
bit   KIN6       = 0x96;
bit   KIN7       = 0x97;

/*========P2==========*/

bit   P2_0       = 0xA0;
bit   P2_1       = 0xA1;
bit   P2_2       = 0xA2;
bit   P2_3       = 0xA3;
bit   P2_4       = 0xA4;
bit   P2_5       = 0xA5;
bit   P2_6       = 0xA6;
bit   P2_7       = 0xA7;

/*========P3==========*/

bit   RXD        = 0xB0;
bit   TXD        = 0xB1;
bit   INT0       = 0xB2;
bit   INT1       = 0xB3;
bit   T0         = 0xB4;
bit   T1         = 0xB5;
bit   WR         = 0xB6;
bit   RD         = 0xB7;

bit   P3_0       = 0xB0;
bit   P3_1       = 0xB1;
bit   P3_2       = 0xB2;
bit   P3_3       = 0xB3;
bit   P3_4       = 0xB4;
bit   P3_5       = 0xB5;
bit   P3_6       = 0xB6;
bit   P3_7       = 0xB7;

bit   LED0       = 0xB3;
bit   LED1       = 0xB5;
bit   LED2       = 0xB6;
bit   LED3       = 0xB7;


/*========P4==========*/

bit   P4_0       = 0xC0;
bit   P4_1       = 0xC1;

/*========TCON========*/

bit   IT0        = 0x88;
bit   IE0        = 0x89;
bit   IT1        = 0x8A;
bit   IE1        = 0x8B;
bit   TR0        = 0x8C;
bit   TF0        = 0x8D;
bit   TR1        = 0x8E;
bit   TF1        = 0x8F;

/*========UEPINT======*/

bit   EP0INT     = 0xF8;
bit   EP1INT     = 0xF9;
bit   EP2INT     = 0xFA;
bit   EP3INT     = 0xFB;
bit   EP4INT     = 0xFC;
bit   EP5INT     = 0xFD;
bit   EP6INT     = 0xFE;

/*========SCON========*/

bit   RI         = 0x98;
bit   TI         = 0x99;
bit   RB8        = 0x9A;
bit   TB8        = 0x9B;
bit   REN        = 0x9C;
bit   SM2        = 0x9D;
bit   SM1        = 0x9E;
bit   SM0        = 0x9F;

/*==============================*/
/* Interrupt Vector Definitions */
/*==============================*/

interrupt [0x03] void extern0_int (void);   /* Vector 0,  External Interrupt 0      */
interrupt [0x0B] void timer0_int (void);    /* Vector 1,  Timer 0 Overflow          */
interrupt [0x13] void extern1_int (void);   /* Vector 2,  External Interrupt 1      */
interrupt [0x1B] void timer1_int (void);    /* Vector 3,  Timer 1 Overflow          */
interrupt [0x23] void sio_ri_int (void);    /* Vector 4,  Serial Port receive       */
interrupt [0x23] void sio_ti_int (void);    /* Vector 4,  Serial Port transmit      */
interrupt [0x2B] void timer2_int (void);    /* Vector 5,  Timer 2 Overflow          */
interrupt [0x33] void pca_int (void);       /* Vector 6,  PCA interrupt             */
interrupt [0x3B] void keyboard_int (void);  /* Vector 7,  Keyboard interrupt        */
interrupt [0x43] void twi_int (void);       /* Vector 8,  TWI interrupt             */
interrupt [0x4B] void spi_spif_int (void);  /* Vector 9,  SPI interrupt SPIF        */
interrupt [0x4B] void spi_modf_int (void);  /* Vector 9,  SPI interrupt Mode fault  */
interrupt [0x6B] void usb_int (void);       /* Vector 13, USB interrupt             */
 
