/*******************************************/
/* Example Program For ET-BASE51 V3.0(ED2) */
/* MCU      : AT89C51ED2(XTAL=29.4912 MHz) */
/*          : Frequency Bus = 58.9824 MHz  */
/* Compiler : Keil C51 (V7.50)             */
/* Write By : Eakachai Makarn(ETT CO.,LTD.)*/
/*******************************************/
// Demo I2C EEPROM(24C32) 
// SCL = P3.7
// SDA = P3.6

/* Include  Section */
#include <reg52.h>					// Standard 8052 SFR : File
#include <stdio.h>                	// For printf I/O functions

/* AT89C51ED2 SFR */
sfr CKCON = 0x8F;					// Clock Control

/* Timer1 Baudrate (29.4912 MHz x 2 ) 
   Reload = 256 - [58.9824 MHz / (384 x Baud)]
   1200   = 0x80(256-128=128)
   2400   = 0xC0(256-94=192)
   4800   = 0xE0(256-32=224)
   9600   = 0xF0(256-16=240)
   19200  = 0xF8(256-8=248)
   38400  = 0xFC(256-4=252)   
*/	

sbit scl = P3^7;											// I2C SCL Signal
sbit sda = P3^6;    										// I2C SDA Signal

/* Pototype  Section */
void eep_write_byte(unsigned int,unsigned char);			// Write EEPROM 1 Byte
unsigned char eep_read_byte(unsigned int);					// Read EEPROM 1 Byte
void i2c_send_byte(unsigned char);							// Write Byte I2C
unsigned char i2c_get_byte(void); 							// Read Byte I2C
void delay_i2c(void);				               			// Short Delay For I2C Bus Interface
void delay(unsigned long);									// Delay Time Function(1..4294967295)

/*--------------------------------------------
The main C function.  Program execution Here 
---------------------------------------------*/
void main (void) 
{
    unsigned char dummy;

    CKCON = 0x01;											// Initial X2 Mode (58.9824 MHz)
	                  
	/* Initial MCS51 Serial Port */
    TMOD &=  0x0F;  										// Reset old Timer1 Mode Config
    TMOD |=  0x20;  										// Update Timer1 = 8 Bit Auto Reload             
    SCON  =  0x50;  										// Serial Port Mode 1 (N,8,1)
    ES    =  0;     										// Disable Serial Interupt
    ET1   =  0;     										// Disable Timer1 Interrupt
    PCON &=  0x7F;  										// SMOD1 = 0 (Disable Double Baudrate)                  
    TH1   =  0xF0;  										// Setup Timer1 Baudrate 9600BPS / 58.9824 MHz
    TL1   =  0xF0;
    TR1   =  1;     										// Start Timer1 Generate Baudrate                      
    TI    =  1;     										// Set TI to send First char of UART

  	scl = 1;												// Makesure I2C Stop Condition
  	sda = 1;
 	
  	printf ("Hello This is ET-BASE51 V3.0(ED2)\n\r\n\r");	// Display Start Menu
    delay(100);

	printf ("Start Test1 Write EEPROM:24C32 = A B C D\n\r");	
	eep_write_byte(0,0x41);									// "A"
	delay(1000);
  	eep_write_byte(1,0x42);									// "B"
	delay(1000);
	eep_write_byte(2,0x43);									// "C"
	delay(1000);
	eep_write_byte(3,0x44);									// "D"
	delay(1000);

	printf ("Start Read EEPROM Data : 24C32 = ");
	dummy = eep_read_byte(0);										
	printf("%c ",dummy);
	dummy = eep_read_byte(1);										
	printf("%c ",dummy);
	dummy = eep_read_byte(2);										
	printf("%c ",dummy);
	dummy = eep_read_byte(3);										
	printf("%c \n\r\n\r",dummy);

	printf ("Start Test2 Write EEPROM:24C32 = 0 1 2 3\n\r");	
	eep_write_byte(0,0x30);									// "0"
	delay(1000);
  	eep_write_byte(1,0x31);									// "1"
	delay(1000);
	eep_write_byte(2,0x32);									// "2"
	delay(1000);
	eep_write_byte(3,0x33);									// "3"
	delay(1000);

	printf ("Start Read EEPROM Data : 24C32 = ");
	dummy = eep_read_byte(0);										
	printf("%c ",dummy);
	dummy = eep_read_byte(1);										
	printf("%c ",dummy);
	dummy = eep_read_byte(2);										
	printf("%c ",dummy);
	dummy = eep_read_byte(3);										
	printf("%c \n\r",dummy);

  	while(1);
  	
}


/*******************************/
/* Write Data 1-Byte to EEPROM */
/*******************************/
void eep_write_byte(unsigned int eep_address,unsigned char eep_data)
{
   sda = 0;													// I2C Start Condition					
   scl = 0;
   delay_i2c();

   i2c_send_byte(0xA8);										// Send ID Code eeprom,Write (1010100+0)
   sda = 1;													// Release SDA
   scl = 1;   												// Start ACK Clock
   delay_i2c(); 
   while(sda) {}
   scl = 0;   												// End ACK Clock
   delay_i2c();

   i2c_send_byte(eep_address>>8);							// Send eeprom MSB Address
   sda = 1;													// Release SDA
   scl = 1;   												// Start ACK Clock
   delay_i2c();
   while(sda) {}
   scl = 0;   												// End ACK Clock
   delay_i2c(); 

   i2c_send_byte(eep_address&0xFF);							// Send eeprom LSB Address
   sda = 1;													// Release SDA
   scl = 1;   												// Start ACK Clock
   delay_i2c();
   while(sda) {}
   scl = 0;   												// End ACK Clock
   delay_i2c(); 

   i2c_send_byte(eep_data);									// Send eeprom Data
   sda = 1;													// Release SDA
   scl = 1;   												// Start ACK Clock
   delay_i2c();
   while(sda) {}				
   scl = 0;   												// End ACK Clock
   delay_i2c();
   sda = 0;													// Stop Bit(End of Data)

   scl = 1;													// I2C Stop Condition
   delay_i2c();
   sda = 1;
 
 return;
}
 

/********************************/
/* Read Data 1-Byte From EEPROM */
/********************************/
unsigned char eep_read_byte(unsigned int eep_address)
{
   unsigned char eep_data;									// Dummy Byte

   sda = 0;													// I2C Stat condition
   scl = 0;
   delay_i2c();
	
   i2c_send_byte(0xA8);   									// Send ID Code eeprom,Write (1010100+0)
   sda = 1;													// Release SDA
   scl = 1;   												// Start ACK Clock
   delay_i2c();
   while(sda) {}				
   scl = 0;   												// End ACK Clock
   delay_i2c(); 

   i2c_send_byte(eep_address>>8);							// Send eeprom MSB Address
   sda = 1;													// Release SDA
   scl = 1;   												// Start ACK Clock
   delay_i2c();
   while(sda) {}
   scl = 0;   												// End ACK Clock
   delay_i2c(); 

   i2c_send_byte(eep_address&0xFF);							// Send eeprom LSB Address
   sda = 1;													// Release SDA
   scl = 1;   												// Start ACK Clock
   delay_i2c();
   while(sda) {}
   scl = 0;   												// End ACK Clock
   delay_i2c(); 

   scl = 1;													// I2C Stop Condition
   delay_i2c();
   sda = 1;
	
   // New Start For Read //
   sda = 0;													// I2C Stat condition
   scl = 0;   
   delay_i2c();

   i2c_send_byte(0xA9);										// Send ID Code eeprom,Read (1010100+1)
   sda = 1;													// Release SDA
   scl = 1;   												// Start ACK Clock
   delay_i2c();
   while(sda) {}					
   scl = 0;   												// End ACK Clock
   delay_i2c(); 
   
   eep_data = i2c_get_byte();								// Read 1-Byte From eeprom

   sda = 1;													// Send Stop Bit (End of Read Data)
   scl = 1;  												// Start Stop Bit Clock
   delay_i2c();
   scl = 0;  												// End Stop Bit Clock
   delay_i2c();

   scl = 1;													// I2C Stop Condition
   delay_i2c();
   sda = 1;

   return eep_data;
}


/******************************/
/* Send Data 8 Bit to I2C Bus */
/******************************/
void i2c_send_byte(unsigned char i)
{
  char j;													// Bit Counter Send

  for(j = 0;j < 8;j++)										// 8-Bit Counter Send Data
  {
    if((i & 0x80) == 0x80) 									// Send MSB First 
      {sda = 1;}											// Send Data = 1
    else                   
      {sda = 0;}											// Send Data = 0

    scl = 1;  												// Release SDA
    delay_i2c();

    scl = 0;  												// Next Bit Send
    delay_i2c();

    i <<= 1;												// Shift Data For Send (MSB <- LSB)
  }
  
  return;
}


/*******************************/
/* Get Data 8 Bit From I2C Bus */
/*******************************/
unsigned char i2c_get_byte(void)
{
 unsigned char i;											// Result Byte Buffer
 char j;													// Bit Counter Read Data

 for(j = 0; j < 8; j++)										// 8-Bit Counter Read Data
 {
   i <<= 1;													// Shift Result Save (MSB <- LSB)

   sda = 1;  												// Release Data
   scl = 1;     											// Strobe Read SDA
   delay_i2c();

   if(sda == 1) 
     {
	   i |= 0x01;											// Save Bit Data = 1
	 }		
   else         
     {
	   i &= 0xFE;											// Save Bit Data = 0
	 }	
   
   scl = 0;     											// Next Bit Read
   delay_i2c();
 } 
  
 return i;
}

/**************************************/
/* Delay For I2C Bus Device Interface */
/**************************************/
void delay_i2c(void)
{
  unsigned char i; 
  i = 100; 													// Delay Counter
  while(i > 0) {i--;}										// Loop Decrease Counter	
  return;
}

/*******************************************/
/* Long Delay Time Function(1..4294967295) */
/*******************************************/
void delay(unsigned long i)
{
  while(i > 0) {i--;}										// Loop Decrease Counter	
  return;
}










