/*******************************************/
/* Example Program For ET-BASE51 V3.0(ED2) */
/* MCU      : AT89C51ED2(XTAL=29.4912 MHz) */
/*          : Frequency Bus = 58.9824 MHz  */
/* Compiler : Keil C51 (V7.50)             */
/* Write By : Eakachai Makarn(ETT CO.,LTD.)*/
/*******************************************/
// Demo RS232 Used Internal Baudrate

/* Include  Section */
#include <reg52.h>											// Standard 8052 SFR : File
#include <stdio.h>                							// For printf I/O functions

/* AT89C51ED2 SFR */
sfr CKCON0 = 0x8F;											// Clock Control
sfr CKRL   = 0x97;
sfr BRL    = 0x9A;
sfr BDRCON = 0x9B;

/* Baudrate 29.4912MHz (X2 Mode) Internal Baudrate
   Fper = 58.9824MHz
   SMOD = 0 = Normal Baudrate
   SPD  = 0 = Slow Baudrate
   BRL  = [256 - (((58982400 / 32) / 12) / Baud)]
   BRL  = [256 - ((58982400 / 384) / Baud)]
   BRL  = 256 - [58.9824 MHz / (384 x Baud)]

   BAUD_SLOW_1200 	= 0x80                         			// 1200  (58.9824 MHz)
   BAUD_SLOW_2400 	= 0xC0                         			// 2400  (58.9824 MHz)
   BAUD_SLOW_4800 	= 0xE0                         			// 4800  (58.9824 MHz)

   SMOD = 1 = Double Baudrate
   SPD  = 1 = Fast Baudrate
   BRL  = [256 - (((58982400 / 32) / 1) / Baud)]
   BRL  = [256 - ((58982400 / 32) / Baud)]
   BRL  = 256 - [58.9824 MHz / (32 x Baud)]

   BAUD_FAST_9600   = 0x40                         			// BRL = 256-192 = 136
   BAUD_FAST_19200  = 0xA0                         			// BRL = 256-96  = 160
   BAUD_FAST_38400  = 0xD0                         			// BRL = 256-48  = 208
   BAUD_FAST_57600  = 0xE0                         			// BRL = 256-32  = 224
   BAUD_FAST_115200 = 0xF0                         			// BRL = 256-16  = 240
   */
				
/*--------------------------------------------
 The main C function.  Program execution Here 
---------------------------------------------*/
void main (void) 
{   
	char uart_data;											// Char Buffer For UART   
    
	/* Start of Config AT89C51ED2 Start-Up */
    CKCON0 = 0x01;                     						// Select Clock = X2 Mode (6-Cycle)
    
    /* Start of Config AT89C51ED2:UART */
    SCON = 0x50;                     						// UART = Mode 1 (N,8,1)
    ES = 0;                             					// Disable UART Interupt
                    
    /* Select Generate Baudrate By Internal Baud */
    TCLK = 0;                           					// Disable Timer2 Generate TX Baudrate
    RCLK = 0;                           					// Disable Timer2 Generate RX Baudrate
    BDRCON |= 0x0C;                     					// TBCK:RBCK=1:1 = Used Internal Buad Generate
                    
    /* Setup Internal Baudrate Fast Mode */
    /* Support Baudrate : 9600,..,115200 */
    PCON |= 0x80;                     						// UART:SMOD = 1 (Enable Double Baudrate)
    BRL   = 0x40;             								// Setup UART Baudrate = 9600 (Fast Baud)
    BDRCON |= 0x02;                     					// SPD=1 = Fast Baudrate Generator
    BDRCON &= 0xFE;                     					// SRC=0 = Select Fosc to Baudrate
    BDRCON |= 0x10;                     					// BRR0=1 = Start Internal Baudrate
                    
    /* Setup Internal Baudrate Slow Mode */
    /* Support Baudrate : 1200,2400,4800 */
    //PCON &= 0x7F;                     					// UART:SMOD = 0 (Disable Double Baudrate)
    //BRL   = 0x80;        									// Setup Baudrate 1200BPS (Slow Baud)
    //BDRCON &= 0xFD;                   					// SPD=0 = Slow Baudrate Generator(Fosc/6)
    //BDRCON &= 0xFE;                   					// SRC=0 = Select Fosc to Baudrate
    //BDRCON |= 0x10;          								// BRR=1 = Start Internal Baud                                   

	TI    =  1;     										// Set TI to send First char of UART
    /* End of Config AT89C51ED2:UART */
					          
    /* Print String to UART */
    printf("Hello World From ET-BASE51 V3.0(ED2)\n\0");
	printf("Press Any Key For Test...RS232\n");

  	while(1)												// Loop Continue
  	{
	  uart_data = _getkey();								// Wait Receive Byte From UART
	  putchar(uart_data);									// Echo Data to UART
	}
	 
}
