;/***********************************/;
;/* Examples For : BASE51 ED2 V3.0  */;
;/* Controller   : AT89C51AC3       */;
;/* Run X-TAL    : 29.4912 MHz      */;
;/*              : Run X2 Mode      */;
;/* Assembler    : Cross-32 V4.0    */;
;/* File Name    : RS232-2.ASM      */;
;/* Write By     : Eakachai Makarn  */;
;/* Function     : Demo TX/RX RS232 */;
;/*              : Use Timer1 16Bit */;
;/*              : Generate Baudrate*/;
;/***********************************/;				

         			HOF  	"INT8"						; Intel HEX 8 Bit Output	
					CPU		"8051.TBL"					; MCS51 Assembler Code
	  				INCL	"89C51ED2.SFR"				; AT89C51ED2 SFR Register


; Timer2 Baudrate (29.4912 MHz x 2 ) 
; Reload = 65536 - [58.9824 MHz / (32 x Baud)]
; 1200   = 0xFA00(65536-1536=64000)
; 2400   = 0xFD00(65536-768=64768)
; 4800   = 0xFE80(65536-384=65152)
; 9600   = 0xFF40(65536-192=65344)
; 19200  = 0xFFA0(65536-96=65440)
; 38400  = 0xFFD0(65536-48=65488)
; 57600  = 0xFFE0(65536-32=65504)
; 115200 = 0xFFF0(65536-16=65520) 
			

                    ORG     20H
STACK:              DFS     30                          ; Stack 30 Byte

                    ORG     0000H                       ; Reset Vector of CPU
                    ;
                    ;/* Initial MCS51 Serial Port */;
MAIN:               MOV     SP,#STACK                   ; Initial Stack
					MOV     CKCON0,#00000001B           ; 6 Clock Mode(X2 Mode)
                    MOV     IE0,#00000000B              ; Initial Interrupt Control
                    ORL     AUXR,#00000001B             ; Inhibit ALE Signal
                    ;                    
                    ;/* Initial MCS51 Serial Port */;                                                     
                    MOV     SCON,#01010000B             ; Serial Port Mode 1 (N,8,1)
                    CLR     ES                          ; Disable Serial Interupt
					;
					;/* Initial Timer2 to Gen Baudrate */;
                    MOV     RCAP2H,#0FFH				; Initial Baud = 9600
                    MOV     RCAP2L,#40H
                    MOV     TH2,RCAP2H
                    MOV     TL2,RCAP2L
                    SETB    RCLK                        ; Enable RXD Baudrate
                    SETB    TCLK                        ; Enable TXD Baudrate
					CLR     ET2                         ; Disable Timer2 Interrupt 
                    SETB    TR2                         ; Start Timer2					
                    SETB    TR2                         ; Start Timer2 Generate Baudrate
                    ;
                    ;/* Start Send Data to RS232 */;
                    MOV     A,#0DH                      ; Enter 
                    LCALL   TX_BYTE
                    MOV     A,#0AH                      ; Line Feed
                    LCALL   TX_BYTE
                    ;
                    MOV     A,#"H"                      ; Sign-On = "Hello >"
                    LCALL   TX_BYTE
                    MOV     A,#"e"
                    LCALL   TX_BYTE
                    MOV     A,#"l"
                    LCALL   TX_BYTE
                    MOV     A,#"l"
                    LCALL   TX_BYTE
                    MOV     A,#"o"
                    LCALL   TX_BYTE
                    MOV     A,#" "
                    LCALL   TX_BYTE
                    MOV     A,#">"
                    LCALL   TX_BYTE
                    ;
LOOP:               LCALL   RX_BYTE                     ; Wait RS232 Receive
                    LCALL   TX_BYTE                     ; Echo Data
                    CJNE    A,#0DH,LOOP                 ; If Enter Auto Line Feed
                    MOV     A,#0AH
                    LCALL   TX_BYTE
                    SJMP    LOOP

;/*************************/;
;/* Send 1-Byte to RS-232 */;
;/* Input   : ACC         */;
;/* Output  : Serial Port */;
;/*************************/;
;
TX_BYTE:            MOV     SBUF,A                      ; Put Data to Send
                    JNB     TI,$                        ; Wait TX Data Ready
                    CLR     TI
                    RET

;/****************************/;
;/* Receive Data From RS-232 */;
;/* Input   :  Serial Port   */;
;/* Output  :  ACC           */;
;/****************************/;
;
RX_BYTE:            JNB     RI,RX_BYTE                  ; Wait RX Data Ready
                    CLR     RI
                    MOV     A,SBUF                      ; Get RX Data
                    RET

                    END

