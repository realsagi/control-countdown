                                                                          
/*                      - iot87c5112.h -

   Special header for the ATMEL AT87C5112 microcontroller.

*/

#pragma language=extended



/*===============================*/
/* Predefined SFR Byte Addresses */
/*===============================*/

sfr    A         = 0xE0;
sfr    ACC       = 0xE0;
sfr    B         = 0xF0;
sfr    SP        = 0x81;
sfr    DPL       = 0x82;
sfr    DPH       = 0x83;
sfr    PSW       = 0xD0;
sfr    PCON      = 0x87;
sfr    AUXR1     = 0xA2;
sfr    PFILT     = 0x84;
sfr    CONF      = 0xEF;
sfr    CKCON0    = 0x8F;
sfr    CKCON1    = 0xAF;
sfr    CKSEL     = 0x85;
sfr    CKRL      = 0x97;
sfr    OSCCON    = 0x86;
sfr    IEN0      = 0xA8;
sfr    IPL0      = 0xB8;
sfr    IPH0      = 0xB7;
sfr    IEN1      = 0xB1;
sfr    IPL1      = 0xB2;
sfr    IPH1      = 0xB3;
sfr    P1M1      = 0xD4;
sfr    P1M2      = 0xE2;
sfr    P3M1      = 0xD5;
sfr    P3M2      = 0xE4;
sfr    P4M1      = 0xDE;
sfr    P4M2      = 0xE5;
sfr    P0        = 0x80;
sfr    P1        = 0x90;
sfr    P2        = 0xA0;
sfr    P3        = 0xB0;
sfr    P4        = 0xC0;
sfr    TCON      = 0x88;
sfr    TMOD      = 0x89;
sfr    TL0       = 0x8A;
sfr    TL1       = 0x8B;
sfr    TH0       = 0x8C;
sfr    TH1       = 0x8D;
sfr    WDTRST    = 0xA6;
sfr    WDTPRG    = 0xA7;
sfr    SCON      = 0x98;
sfr    SBUF      = 0x99;
sfr    SADDR     = 0xA9;
sfr    SADEN     = 0xB9;
sfr    BRL       = 0x9A;
sfr    BDRCON    = 0x9B;
sfr    SPCON     = 0xC3;
sfr    SPSTA     = 0xC4;
sfr    SPDAT     = 0xC5;
sfr    ADCF      = 0xF6;
sfr    ADDL      = 0xF4;
sfr    ADDH      = 0xF5;
sfr    ADCON     = 0xF3;
sfr    ADCLK     = 0xF2;
sfr    CL        = 0xE9;
sfr    CH        = 0xF9;
sfr    CCON      = 0xD8;
sfr    CMOD      = 0xD9;
sfr    CCAPM4    = 0xDE;
sfr    CCAPM3    = 0xDD;
sfr    CCAPM2    = 0xDC;
sfr    CCAPM1    = 0xDB;
sfr    CCAPM0    = 0xDA;
sfr    CCAP4L    = 0xEE;
sfr    CCAP3L    = 0xED;
sfr    CCAP2L    = 0xEC;
sfr    CCAP1L    = 0xEB;
sfr    CCAP0L    = 0xEA;
sfr    CCAP4H    = 0xFE;
sfr    CCAP3H    = 0xFD;
sfr    CCAP2H    = 0xFC;
sfr    CCAP1H    = 0xFB;
sfr    CCAP0H    = 0xFA;


/*==============================*/
/* Predefined SFR Bit Addresses */
/*==============================*/


/*========PSW=========*/

bit   P          = 0xD0;
bit   F1         = 0xD1;
bit   OV         = 0xD2;
bit   RS0        = 0xD3;
bit   RS1        = 0xD4;
bit   F0         = 0xD5;
bit   AC         = 0xD6;
bit   CY         = 0xD7;

/*========IEN0========*/

bit   EX0        = 0xA8;
bit   ET0        = 0xA9;
bit   EX1        = 0xAA;
bit   ET1        = 0xAB;
bit   ES         = 0xAC;
bit   EC         = 0xAE;
bit   EA         = 0xAF;

/*========IPL0========*/

bit   PX0H       = 0xB8;
bit   PT0H       = 0xB9;
bit   PX1H       = 0xBA;
bit   PT1H       = 0xBB;
bit   PSH        = 0xBC;
bit   PPCH       = 0xBE;
bit   PX0L       = 0xB8;
bit   PT0L       = 0xB9;
bit   PX1L       = 0xBA;
bit   PT1L       = 0xBB;
bit   PSL        = 0xBC;
bit   PPCL       = 0xBE;

/*========P0==========*/

bit   P0_0       = 0x80;
bit   P0_1       = 0x81;
bit   P0_2       = 0x82;
bit   P0_3       = 0x83;
bit   P0_4       = 0x84;
bit   P0_5       = 0x85;
bit   P0_6       = 0x86;
bit   P0_7       = 0x87;

/*========P1==========*/

bit   P1_0       = 0x90;
bit   P1_1       = 0x91;
bit   P1_2       = 0x92;
bit   P1_3       = 0x93;
bit   P1_4       = 0x94;
bit   P1_5       = 0x95;
bit   P1_6       = 0x96;
bit   P1_7       = 0x97;

/*========P2==========*/

bit   P2_0       = 0xA0;
bit   P2_1       = 0xA1;
bit   P2_2       = 0xA2;
bit   P2_3       = 0xA3;
bit   P2_4       = 0xA4;
bit   P2_5       = 0xA5;
bit   P2_6       = 0xA6;
bit   P2_7       = 0xA7;

/*========P3==========*/

bit   RXD        = 0xB0;
bit   TXD        = 0xB1;
bit   INT0       = 0xB2;
bit   INT1       = 0xB3;
bit   T0         = 0xB4;
bit   T1         = 0xB5;
bit   WR         = 0xB6;
bit   RD         = 0xB7;
bit   P3_0       = 0xB0;
bit   P3_1       = 0xB1;
bit   P3_2       = 0xB2;
bit   P3_3       = 0xB3;
bit   P3_4       = 0xB4;
bit   P3_5       = 0xB5;
bit   P3_6       = 0xB6;
bit   P3_7       = 0xB7;

/*========P4==========*/

bit   MISO       = 0xC4;
bit   MOSI       = 0xC5;
bit   SCK        = 0xC6;
bit   SS_        = 0xC2;
bit   P4_0       = 0xC0;
bit   P4_1       = 0xC1;
bit   P4_2       = 0xC2;
bit   P4_3       = 0xC3;
bit   P4_4       = 0xC4;
bit   P4_5       = 0xC5;
bit   P4_6       = 0xC6;
bit   P4_7       = 0xC7;

/*========TCON========*/

bit   IT0        = 0x88;
bit   IE0        = 0x89;
bit   IT1        = 0x8A;
bit   IE1        = 0x8B;
bit   TR0        = 0x8C;
bit   TF0        = 0x8D;
bit   TR1        = 0x8E;
bit   TF1        = 0x8F;

/*========SCON========*/

bit   RI         = 0x98;
bit   TI         = 0x99;
bit   RB8        = 0x9A;
bit   TB8        = 0x9B;
bit   REN        = 0x9C;
bit   SM2        = 0x9D;
bit   SM1        = 0x9E;
bit   FE         = 0x9F;
bit   SM0        = 0x9F;

/*==============================*/
/* Interrupt Vector Definitions */
/*==============================*/



