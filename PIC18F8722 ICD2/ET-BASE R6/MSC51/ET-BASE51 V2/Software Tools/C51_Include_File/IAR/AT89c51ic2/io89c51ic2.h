/*                      - io89c51ic2.H -

   Special header for the ATMEL at89c51ic2 microcontroller.

*/

#pragma language=extended



/*===============================*/
/* Predefined SFR Byte Addresses */
/*===============================*/

sfr    P0        = 0x80;
sfr    P1        = 0x90;
sfr    P2        = 0xA0;
sfr    P3        = 0xB0;
sfr    P4        = 0xC0;
sfr    PSW       = 0xD0;
sfr    ACC       = 0xE0;
sfr    B         = 0xF0;
sfr    SP        = 0x81;
sfr    DPL       = 0x82;
sfr    DPH       = 0x83;
sfr    PCON      = 0x87;
sfr    CKCON0    = 0x8F;
sfr    CKCON1    = 0xAF;
sfr    TCON      = 0x88;
sfr    TMOD      = 0x89;
sfr    T2CON     = 0xC8;
sfr    T2MOD     = 0xC9;
sfr    TL0       = 0x8A;
sfr    TL1       = 0x8B;
sfr    TL2       = 0xCC;
sfr    TH0       = 0x8C;
sfr    TH1       = 0x8D;
sfr    TH2       = 0xCD;
sfr    RCAP2L    = 0xCA;
sfr    RCAP2H    = 0xCB;
sfr    WDTRST    = 0xA6;
sfr    WDTPRG    = 0xA7;
sfr    SCON      = 0x98;
sfr    SBUF      = 0x99;
sfr    SADEN     = 0xB9;
sfr    SADDR     = 0xA9;
sfr    BRL       = 0x9A;
sfr    BDRCON    = 0x9B;
sfr    IEN0      = 0xA8;
sfr    IEN1      = 0xB1;
sfr    IPH0      = 0xB7;
sfr    IPH1      = 0xB3;
sfr    IPL0      = 0xB8;
sfr    IPL1      = 0xB2;
sfr    CCON      = 0xD8;
sfr    CMOD      = 0xD9;
sfr    CH        = 0xF9;
sfr    CL        = 0xE9;
sfr    CCAP0H    = 0xFA;
sfr    CCAP0L    = 0xEA;
sfr    CCAPM0    = 0xDA;
sfr    CCAP1H    = 0xFB;
sfr    CCAP1L    = 0xEB;
sfr    CCAPM1    = 0xDB;
sfr    CCAP2H    = 0xFC;
sfr    CCAP2L    = 0xEC;
sfr    CCAPM2    = 0xDC;
sfr    CCAP3H    = 0xFD;
sfr    CCAP3L    = 0xED;
sfr    CCAPM3    = 0xDD;
sfr    CCAP4H    = 0xFE;
sfr    CCAP4L    = 0xEE;
sfr    CCAPM4    = 0xDE;
sfr    SSCON     = 0x93;
sfr    SSCS      = 0x94;
sfr    SSDAT     = 0x95;
sfr    SSADR     = 0x96;
sfr    CKSEL     = 0x85;
sfr    OSCCON    = 0x86;
sfr    CKRL      = 0x97;
sfr    KBLS      = 0x9C;
sfr    KBE       = 0x9D;
sfr    KBF       = 0x9E;
sfr    AUXR      = 0x8E;
sfr    AUXR1     = 0xA2;
sfr    FCON      = 0xD1;


/*==============================*/
/* Predefined SFR Bit Addresses */
/*==============================*/


/*========P0==========*/

bit   P0_0       = 0x80;
bit   P0_1       = 0x81;
bit   P0_2       = 0x82;
bit   P0_3       = 0x83;
bit   P0_4       = 0x84;
bit   P0_5       = 0x85;
bit   P0_6       = 0x86;
bit   P0_7       = 0x87;

/*========P1==========*/

bit   P1_0       = 0x90;
bit   P1_1       = 0x91;
bit   P1_2       = 0x92;
bit   P1_3       = 0x93;
bit   P1_4       = 0x94;
bit   P1_5       = 0x95;
bit   P1_6       = 0x96;
bit   P1_7       = 0x97;

/*========P2==========*/

bit   P2_0       = 0xA0;
bit   P2_1       = 0xA1;
bit   P2_2       = 0xA2;
bit   P2_3       = 0xA3;
bit   P2_4       = 0xA4;
bit   P2_5       = 0xA5;
bit   P2_6       = 0xA6;
bit   P2_7       = 0xA7;

/*========P3==========*/

bit   RXD        = 0xB0;
bit   TXD        = 0xB1;
bit   INT0       = 0xB2;
bit   INT1       = 0xB3;
bit   T0         = 0xB4;
bit   T1         = 0xB5;
bit   WR         = 0xB6;
bit   RD         = 0xB7;

/*========P4==========*/

bit   PI2_0      = 0xC0;
bit   PI2_1      = 0xC1;

/*========PSW=========*/

bit   P          = 0xD0;
bit   UD         = 0xD1;
bit   OV         = 0xD2;
bit   RS0        = 0xD3;
bit   RS1        = 0xD4;
bit   F0         = 0xD5;
bit   AC         = 0xD6;
bit   CY         = 0xD7;

/*========TCON========*/

bit   IT0        = 0x88;
bit   IE0        = 0x89;
bit   IT1        = 0x8A;
bit   IE1        = 0x8B;
bit   TR0        = 0x8C;
bit   TF0        = 0x8D;
bit   TR1        = 0x8E;
bit   TF1        = 0x8F;

/*========T2CON=======*/

bit   CP_RL2     = 0xC8;
bit   C_T2       = 0xC9;
bit   TR2        = 0xCA;
bit   EXEN2      = 0xCB;
bit   TCLK       = 0xCC;
bit   RCLK       = 0xCD;
bit   EXF2       = 0xCE;
bit   TF2        = 0xCF;

/*========SCON========*/

bit   RI         = 0x98;
bit   TI         = 0x99;
bit   RB8        = 0x9A;
bit   TB8        = 0x9B;
bit   REN        = 0x9C;
bit   SM2        = 0x9D;
bit   SM1        = 0x9E;
bit   SM0        = 0x9F;

/*========IEN0========*/

bit   EX0        = 0xA8;
bit   ET0        = 0xA9;
bit   EX1        = 0xAA;
bit   ET1        = 0xAB;
bit   ES         = 0xAC;
bit   ET2        = 0xAD;
bit   EC         = 0xAE;
bit   EA         = 0xAF;

/*========CCON========*/

bit   CCF0       = 0xD8;
bit   CCF1       = 0xD9;
bit   CCF2       = 0xDA;
bit   CCF3       = 0xDB;
bit   CCF4       = 0xDC;
bit   CR         = 0xDE;
bit   CF         = 0xDF;

/*==============================*/
/* Interrupt Vector Definitions */
/*==============================*/

interrupt [0x03] void extern0_int (void);  /* Vector 0, External Interrupt 0      */
interrupt [0x0B] void timer0_int (void);   /* Vector 1, Timer 0 Overflow          */
interrupt [0x13] void extern1_int (void);  /* Vector 2, External Interrupt 1      */
interrupt [0x1B] void timer1_int (void);   /* Vector 3, Timer 1 Overflow          */
interrupt [0x23] void sio_ri_int (void);   /* Vector 4, Serial Port receive       */
interrupt [0x23] void sio_ti_int (void);   /* Vector 5, Serial Port transmit      */
interrupt [0x2B] void timer2_int (void);   /* Vector 6, Timer 2 interrupt         */
interrupt [0x33] void pca_counter_int (void);  /* Vector 7, PCA counter interrupt     */
interrupt [0x33] void pca_module0_int (void);  /* Vector 8, PCA counter interrupt     */
interrupt [0x33] void pca_module1_int (void);  /* Vector 9, PCA counter interrupt     */
interrupt [0x33] void pca_module2_int (void);  /* Vector 10, PCA counter interrupt    */
interrupt [0x33] void pca_module3_int (void);  /* Vector 11, PCA counter interrupt    */
interrupt [0x33] void pca_module4_int (void);  /* Vector 12, PCA counter interrupt    */
interrupt [0x3b] void keyboard_key0_int (void);  /* Vector 13, Keyboard interrupt key0  */
interrupt [0x3b] void keyboard_key1_int (void);  /* Vector 14, Keyboard interrupt key1  */
interrupt [0x3b] void keyboard_key2_int (void);  /* Vector 15, Keyboard interrupt key2  */
interrupt [0x3b] void keyboard_key3_int (void);  /* Vector 16, Keyboard interrupt key3  */
interrupt [0x3b] void keyboard_key4_int (void);  /* Vector 17, Keyboard interrupt key4  */
interrupt [0x3b] void keyboard_key5_int (void);  /* Vector 18, Keyboard interrupt key5  */
interrupt [0x3b] void keyboard_key6_int (void);  /* Vector 19, Keyboard interrupt key6  */
interrupt [0x3b] void keyboard_key7_int (void);  /* Vector 20, Keyboard interrupt key7  */
interrupt [0x43] void i2c_int (void);      /* Vector 21, I2C interrupt            */
interrupt [0x4b] void spi_spif_int (void);  /* Vector 22, SPI interrupt SPIF       */
interrupt [0x4b] void spi_modf_int (void);  /* Vector 23, SPI interrupt Mode fault  */

  
