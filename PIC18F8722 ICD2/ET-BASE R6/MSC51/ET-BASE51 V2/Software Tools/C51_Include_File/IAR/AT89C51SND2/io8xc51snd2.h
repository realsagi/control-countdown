                                                                          
/*                      - iot8xc51snd2.H -

   Special header for the ATMEL at8xc51snd2c microcontroller.

*/

#pragma language=extended



/*===============================*/
/* Predefined SFR Byte Addresses */
/*===============================*/

sfr    A         = 0xE0;
sfr    ACC       = 0xE0;
sfr    B         = 0xF0;
sfr    PSW       = 0xD0;
sfr    SP        = 0x81;
sfr    DPL       = 0x82;
sfr    DPH       = 0x83;
sfr    PCON      = 0x87;
sfr    AUXR      = 0x8E;
sfr    AUXR1     = 0xA2;
sfr    NVERS     = 0xFB;
sfr    CKCON     = 0x8F;
sfr    PLLCON    = 0xE9;
sfr    PLLNDIV   = 0xEE;
sfr    PLLRDIV   = 0xEF;
sfr    IEN0      = 0xA8;
sfr    IPL0      = 0xB8;
sfr    IPH0      = 0xB7;
sfr    IEN1      = 0xB1;
sfr    IPL1      = 0xB2;
sfr    IPH1      = 0xB3;
sfr    P0        = 0x80;
sfr    AUXCON     = 0x90;
sfr    P2        = 0xA0;
sfr    P3        = 0xB0;
sfr    P4        = 0xC0;
sfr    FCON      = 0xD1;
sfr    TCON      = 0x88;
sfr    TMOD      = 0x89;
sfr    TL0       = 0x8A;
sfr    TL1       = 0x8B;
sfr    TH0       = 0x8C;
sfr    TH1       = 0x8D;
sfr    WDTRST    = 0xA6;
sfr    WDTPRG    = 0xA7;
sfr    MP3CON    = 0xAA;
sfr    MP3STA    = 0xC8;
sfr    MP3STA1   = 0xAF;
sfr    MP3DAT    = 0xAC;
sfr    MP3ANC    = 0xAD;
sfr    MP3VOL    = 0x9E;
sfr    MP3VOR    = 0x9F;
sfr    MP3BAS    = 0xB4;
sfr    MP3MED    = 0xB5;
sfr    MP3TRE    = 0xB6;
sfr    MP3CLK    = 0xEB;
sfr    AUDCON0   = 0x9A;
sfr    AUDCON1   = 0x9B;
sfr    AUDSTA    = 0x9C;
sfr    AUDDAT    = 0x9D;
sfr    AUDCLK    = 0xEC;
sfr    USBCON    = 0xBC;
sfr    USBADDR   = 0xC6;
sfr    USBINT    = 0xBD;
sfr    USBIEN    = 0xBE;
sfr    UEPNUM    = 0xC7;
sfr    UEPCONX   = 0xD4;
sfr    UEPSTAX   = 0xCE;
sfr    UEPRST    = 0xD5;
sfr    UEPINT    = 0xF8;
sfr    UEPIEN    = 0xC2;
sfr    UEPDATX   = 0xCF;
sfr    UBYCTX    = 0xE2;
sfr    UFNUML    = 0xBA;
sfr    UFNUMH    = 0xBB;
sfr    USBCLK    = 0xEA;
sfr    MMDAT     = 0xDC;
sfr    MMCMD     = 0xDD;
sfr    MMSTA     = 0xDE;
sfr    MMMSK     = 0xDF;
sfr    MMCON0    = 0xE4;
sfr    MMCON1    = 0xE5;
sfr    MMCON2    = 0xE6;
sfr    MMCLK     = 0xED;
sfr    MMINT     = 0xE7;
sfr    DAT16H    = 0xF9;
sfr    SCON      = 0x98;
sfr    SBUF      = 0x99;
sfr    SADDR     = 0xA9;
sfr    SADEN     = 0xB9;
sfr    BDRCON    = 0x92;
sfr    BRL       = 0x91;
sfr    SPCON     = 0xC3;
sfr    SPSTA     = 0xC4;
sfr    SPDAT     = 0xC5;
sfr    SSCON     = 0x93;
sfr    SSSTA     = 0x94;
sfr    SSDAT     = 0x95;
sfr    SSADR     = 0x96;
sfr    KBCON     = 0xA3;
sfr    KBSTA     = 0xA4;


/*==============================*/
/* Predefined SFR Bit Addresses */
/*==============================*/


/*========PSW=========*/

bit   P          = 0xD0;
bit   F1         = 0xD1;
bit   OV         = 0xD2;
bit   RS0        = 0xD3;
bit   RS1        = 0xD4;
bit   F0         = 0xD5;
bit   AC         = 0xD6;
bit   CY         = 0xD7;

/*========IEN0========*/

bit   EX0        = 0xA8;
bit   ET0        = 0xA9;
bit   EX1        = 0xAA;
bit   ET1        = 0xAB;
bit   ES         = 0xAC;
bit   EMP3       = 0xAD;
bit   EAUD       = 0xAE;
bit   EA         = 0xAF;

/*========IPL0========*/

bit   IPLX0      = 0xB8;
bit   IPLT0      = 0xB9;
bit   IPLX1      = 0xBA;
bit   IPLT1      = 0xBB;
bit   IPLS       = 0xBC;
bit   IPLMP3     = 0xBD;
bit   IPLAUD     = 0xBE;

/*========P0==========*/

bit   P0_0       = 0x80;
bit   P0_1       = 0x81;
bit   P0_2       = 0x82;
bit   P0_3       = 0x83;
bit   P0_4       = 0x84;
bit   P0_5       = 0x85;
bit   P0_6       = 0x86;
bit   P0_7       = 0x87;

/*========AUXCON==========*/

bit   KIN0       = 0x90;
bit   SCL        = 0x96;
bit   SDA        = 0x97;
bit   AUDCCS     = 0x91;
bit   AUDCCLK    = 0x92;
bit   AUDCDIN    = 0x93;
bit   AUDCDOUT   = 0x94;
bit   AUDCSMOD   = 0x95;

/*========P2==========*/

bit   P2_0       = 0xA0;
bit   P2_1       = 0xA1;
bit   P2_2       = 0xA2;
bit   P2_3       = 0xA3;
bit   P2_4       = 0xA4;
bit   P2_5       = 0xA5;
bit   P2_6       = 0xA6;
bit   P2_7       = 0xA7;

/*========P3==========*/

bit   RXD        = 0xB0;
bit   TXD        = 0xB1;
bit   INT0       = 0xB2;
bit   INT1       = 0xB3;
bit   T0         = 0xB4;
bit   T1         = 0xB5;
bit   WR         = 0xB6;
bit   RD         = 0xB7;
bit   P3_0       = 0xB0;
bit   P3_1       = 0xB1;
bit   P3_2       = 0xB2;
bit   P3_3       = 0xB3;
bit   P3_4       = 0xB4;
bit   P3_5       = 0xB5;
bit   P3_6       = 0xB6;
bit   P3_7       = 0xB7;

/*========P4==========*/

bit   MISO       = 0xC0;
bit   MOSI       = 0xC1;
bit   SCK        = 0xC2;
bit   SS_        = 0xC3;
bit   P4_0       = 0xC0;
bit   P4_1       = 0xC1;
bit   P4_2       = 0xC2;
bit   P4_3       = 0xC3;


/*========TCON========*/

bit   IT0        = 0x88;
bit   IE0        = 0x89;
bit   IT1        = 0x8A;
bit   IE1        = 0x8B;
bit   TR0        = 0x8C;
bit   TF0        = 0x8D;
bit   TR1        = 0x8E;
bit   TF1        = 0x8F;

/*========MP3STA======*/

bit   MPVER      = 0xC8;
bit   MPFS0      = 0xC9;
bit   MPFS1      = 0xCA;
bit   ERRCRC     = 0xCB;
bit   ERRSYN     = 0xCC;
bit   ERRLAY     = 0xCD;
bit   MPREQ      = 0xCE;
bit   MPANC      = 0xCF;

/*========UEPINT======*/

bit   EP0INT     = 0xF8;
bit   EP1INT     = 0xF9;
bit   EP2INT     = 0xFA;
bit   EP3INT     = 0xFB;

/*========SCON========*/

bit   RI         = 0x98;
bit   TI         = 0x99;
bit   RB8        = 0x9A;
bit   TB8        = 0x9B;
bit   REN        = 0x9C;
bit   SM2        = 0x9D;
bit   SM1        = 0x9E;
bit   FE         = 0x9F;
bit   SM0        = 0x9F;

/*==============================*/
/* Interrupt Vector Definitions */
/*==============================*/


interrupt [0x03] void extern0_int (void);     // External Interrupt 0
interrupt [0x0B] void timer0_int (void);      // Timer 0 Overflow
interrupt [0x13] void extern1_int (void);     // External Interrupt 1
interrupt [0x1B] void timer1_int (void);      // Timer 1 Overflow
interrupt [0x23] void sio_ri_int (void);      // Serial Port receive
interrupt [0x23] void sio_ti_int (void);      // Serial Port transmit
interrupt [0x43] void i2c_int (void);         // I2C interrupt
interrupt [0x4b] void spi_spif_int (void);    // SPI interrupt SPIF
interrupt [0x4b] void spi_modf_int (void);    // SPI interrupt Mode fault
interrupt [0x5b] void kbd_kin0_int (void);   // Keyboard interrupt key0
