$SAVE    
$NOLIST 


#define	P0      80H
#define	TCON	88H
;---  TCON Bits ---
#define	TF1     TCON.7
#define	TR1     TCON.6
#define	TF0     TCON.5
#define	TR0     TCON.4
#define	IE1     TCON.3
#define	IT1     TCON.2
#define	IE0     TCON.1
#define	IT0     TCON.0

#define	P1          90H

#define	SCON        98H
;--- SCON Bits ----
#define	SM0     SCON.7
#define	SM1     SCON.6
#define	SM2     SCON.5
#define	REN     SCON.4
#define	TB8     SCON.3
#define	RB8     SCON.2
#define	TI      SCON.1
#define	RI      SCON.0


#define	P2          0A0H
#define	IEN0        0A8H
;--- IEN0 Bits -----
#define	EA   	IENO.7
#define	EC   	IENO.6
#define	ET2  	IENO.5
#define	ES  	IENO.4
#define	ET1 	IENO.3
#define	EX1  	IENO.2
#define	ET0 	IENO.1
#define	EX0	IENO.0

#define	P3      0B0H
;--- P3 Bits -------
#define	RD      P3.7
#define	WR      P3.6
#define	T1      P3.5
#define	T0      P3.4
#define	INT1    P3.3
#define	INT0    P3.2
#define	TXD     P3.1
#define	RXD     P3.0

#define	P4      0C0H
#define	P5      0E8H


#define	IPL0	0B8H
;--- IPL0 Bits -----
#define	PPCL   	IPL0.6
#define	PT2L  	IPL0.5
#define	PSL  	IPL0.4
#define	PT1L 	IPL0.3
#define	PX1L  	IPL0.2
#define	PT0L 	IPL0.1
#define	PX0L	IPL0.0


#define	T2CON       0C8H
;--- T2CON bits ----
#define	TF2     T2CON.7
#define	EXF2    T2CON.6
#define	RCLK    T2CON.5
#define	TCLK    T2CON.4
#define	EXEN2   T2CON.3
#define	TR2     T2CON.2
#define	C_T2    T2CON.1
#define	CP_RL2  T2CON.0

#define	PSW         0D0H
;--- PSW bits ------
#define	CY      PSW.7
#define	AC      PSW.6
#define	F0      PSW.5
#define	RS1     PSW.4
#define	RS0     PSW.3
#define	OV      PSW.2
#define	P       PSW.0

#define	CCON		0D8H
;--- CCON bits -----
#define	CF	CCON.7
#define	CR	CCON.6
#define	CCF4  	CCON.4
#define	CCF3  	CCON.3
#define	CCF2  	CCON.2
#define	CCF1  	CCON.1
#define	CCF0  	CCON.0

#define	ACC         0E0H
#define	B           0F0H


#define	SP          81H
#define	DPL         82H
#define	DPH         83H
#define PCON        87H


#define	TMOD        	89H
#define	TL0         	8AH
#define	TL1         	8BH
#define	TH0         	8CH
#define	TH1         	8DH
#define	AUXR		08EH
#define	CKCON0		08Fh




#define	SBUF        99H
;-- Baud Rate generator
#define	BRL  		09AH
#define	BDRCON   	09BH
;--- Keyboard
#define	KBLS		09CH
#define	KBE		09DH
#define	KBF		09EH

;--- Watchdog timer
#define	WDTRST		0A6H
#define	WDTPRG 		0A7H 

#define	SADDR		0A9H
#define	CKCON1		0AFH


#define	IEN1		0B1H
#define	IPL1		0B2H
#define	IPH1		0B3
#define	IPH0		0B7H


#define	SADEN		0B9H


#define	T2MOD	    	0C9h
#define	RCAP2L      0CAH
#define	RCAP2H      0CBH
#define	TL2         0CCH
#define	TH2         0CDH
	
#define	CMOD		0D9H
#define	CCAPM0		0DAH
#define	CCAPM1		0DBH
#define	CCAPM2		0DCH
#define	CCAPM3		0DDH
#define	CCAPM4		0DEH


#define	CH		0F9H
#define	CCAP0H		0FAH
#define	CCAP1H		0FBH
#define	CCAP2H		0FCH
#define	CCAP3H		0FDH
#define CCAP4H		0FEH

#define	CL		0E9H
#define	CCAP0L		0EAH
#define	CCAP1L		0EBH
#define	CCAP2L		0ECH
#define	CCAP3L		0EDH
#define	CCAP4L		0EEH

; SPI 
#define	SPCON        0C3H
#define	SPSTA        0C4H
#define	SPDAT        0C5H



; TWI
#define	PI2	    	0F8h
#define	SSCON		093H
#define	SSCS		094H
#define	SSDAT		095H
#define	SSADR		096H
#define	PI2_O	PI2.0
#define	PI2_1	PI2.1

; Clock Control
#define	CKRL		097H
#define	OSCCON		086H
#define	CKSEL		085H

;MISC
#define	AUXR		08EH
#define	AUXR1		0A2H

; Flash control
#define	FCON   	    0D1H

;EEData
#define	EECON   	    0D2H


; Misc
#define	AUXR1       0A2H

$RESTORE