/*                      - io89c51rd2.H -

   Special header for the ATMEL t89c51rd2 microcontroller.

*/

#pragma language=extended



/*===============================*/
/* Predefined SFR Byte Addresses */
/*===============================*/

sfr    P0        = 0x80;
sfr    P1        = 0x90;
sfr    P2        = 0xA0;
sfr    P3        = 0xB0;
sfr    P4        = 0xC0;
sfr    P5        = 0xC7;
sfr    PSW       = 0xD0;
sfr    ACC       = 0xE0;
sfr    B         = 0xF0;
sfr    SP        = 0x81;
sfr    DPL       = 0x82;
sfr    DPH       = 0x83;
sfr    PCON      = 0x87;
sfr    CKCON     = 0x8F;
sfr    TCON      = 0x88;
sfr    TMOD      = 0x89;
sfr    T2CON     = 0xC8;
sfr    T2MOD     = 0xC9;
sfr    TL0       = 0x8A;
sfr    TL1       = 0x8B;
sfr    TL2       = 0xCC;
sfr    TH0       = 0x8C;
sfr    TH1       = 0x8D;
sfr    TH2       = 0xCD;
sfr    RCAP2L    = 0xCA;
sfr    RCAP2H    = 0xCB;
sfr    WDTRST    = 0xA6;
sfr    WDTPRG    = 0xA7;
sfr    SCON      = 0x98;
sfr    SBUF      = 0x99;
sfr    SADEN     = 0xB9;
sfr    SADDR     = 0xA9;
sfr    FCON      = 0xD1;
sfr    EECON     = 0xD2;
sfr    EETIM     = 0xD3;
sfr    AUXR      = 0x8E;
sfr    AUXR1     = 0xA2;
sfr    IE        = 0xA8;
sfr    IP        = 0xB8;
sfr    IPH       = 0xB7;
sfr    CCON      = 0xD8;
sfr    CMOD      = 0xD9;
sfr    CH        = 0xF9;
sfr    CL        = 0xE9;
sfr    CCAP0H    = 0xFA;
sfr    CCAP0L    = 0xEA;
sfr    CCAPM0    = 0xDA;
sfr    CCAP1H    = 0xFB;
sfr    CCAP1L    = 0xEB;
sfr    CCAPM1    = 0xDB;
sfr    CCAP2H    = 0xFC;
sfr    CCAP2L    = 0xEC;
sfr    CCAPM2    = 0xDC;
sfr    CCAP3H    = 0xFD;
sfr    CCAP3L    = 0xED;
sfr    CCAPM3    = 0xDD;
sfr    CCAP4H    = 0xFE;
sfr    CCAP4L    = 0xEE;
sfr    CCAPM4    = 0xDE;


/*==============================*/
/* Predefined SFR Bit Addresses */
/*==============================*/


/*========P1==========*/

bit   T2         = 0x90;
bit   T2EX       = 0x91;
bit   ECI        = 0x92;
bit   CEX0       = 0x93;
bit   CEX1       = 0x94;
bit   CEX2       = 0x95;
bit   CEX3       = 0x96;
bit   CEX4       = 0x97;

/*========P2==========*/

bit   P2_0       = 0xA0;
bit   P2_1       = 0xA1;
bit   P2_2       = 0xA2;
bit   P2_3       = 0xA3;
bit   P2_4       = 0xA4;
bit   P2_5       = 0xA5;
bit   P2_6       = 0xA6;
bit   P2_7       = 0xA7;

/*========P3==========*/

bit   RXD        = 0xB0;
bit   TXD        = 0xB1;
bit   INT0       = 0xB2;
bit   INT1       = 0xB3;
bit   T0         = 0xB4;
bit   T1         = 0xB5;
bit   WR         = 0xB6;
bit   RD         = 0xB7;

/*========PSW=========*/

bit   P          = 0xD0;
bit   UD         = 0xD1;
bit   OV         = 0xD2;
bit   RS0        = 0xD3;
bit   RS1        = 0xD4;
bit   F0         = 0xD5;
bit   AC         = 0xD6;
bit   CY         = 0xD7;

/*========TCON========*/

bit   IT0        = 0x88;
bit   IE0        = 0x89;
bit   IT1        = 0x8A;
bit   IE1        = 0x8B;
bit   TR0        = 0x8C;
bit   TF0        = 0x8D;
bit   TR1        = 0x8E;
bit   TF1        = 0x8F;

/*========T2CON=======*/

bit   CP_RL2     = 0xC8;
bit   C_T2       = 0xC9;
bit   TR2        = 0xCA;
bit   EXEN2      = 0xCB;
bit   TCLK       = 0xCC;
bit   RCLK       = 0xCD;
bit   EXF2       = 0xCE;
bit   TF2        = 0xCF;

/*========SCON========*/

bit   RI         = 0x98;
bit   TI         = 0x99;
bit   RB8        = 0x9A;
bit   TB8        = 0x9B;
bit   REN        = 0x9C;
bit   SM2        = 0x9D;
bit   SM1        = 0x9E;
bit   SM0        = 0x9F;

/*========IE==========*/

bit   EX0        = 0xA8;
bit   ET0        = 0xA9;
bit   EX1        = 0xAA;
bit   ET1        = 0xAB;
bit   ES         = 0xAC;
bit   ET2        = 0xAD;
bit   EC         = 0xAE;
bit   EA         = 0xAF;

/*========IP==========*/

bit   PX0        = 0xB8;
bit   PT0        = 0xB9;
bit   PX1        = 0xBA;
bit   PT1        = 0xBB;
bit   PS         = 0xBC;
bit   PT2        = 0xBD;
bit   PPC        = 0xBE;

/*========CCON========*/

bit   CCF0       = 0xD8;
bit   CCF1       = 0xD9;
bit   CCF2       = 0xDA;
bit   CCF3       = 0xDB;
bit   CCF4       = 0xDC;
bit   CR         = 0xDE;
bit   CF         = 0xDF;

/*==============================*/
/* Interrupt Vector Definitions */
/*==============================*/

interrupt [0x03] void extern0_int (void);  /* Vector 0, External Interrupt 0      */
interrupt [0x0B] void timer0_int (void);   /* Vector 1, Timer 0 Overflow          */
interrupt [0x13] void extern1_int (void);  /* Vector 2, External Interrupt 1      */
interrupt [0x1B] void timer1_int (void);   /* Vector 3, Timer 1 Overflow          */
interrupt [0x23] void sio_ri_int (void);   /* Vector 4, Serial Port receive       */
interrupt [0x23] void sio_ti_int (void);   /* Vector 5, Serial Port transmit      */
interrupt [0x2B] void timer2_int (void);   /* Vector 6, Timer 2 interrupt         */
interrupt [0x33] void pca_counter_int (void);  /* Vector 7, PCA counter interrupt     */
interrupt [0x33] void pca_module0_int (void);  /* Vector 8, PCA counter interrupt     */
interrupt [0x33] void pca_module1_int (void);  /* Vector 9, PCA counter interrupt     */
interrupt [0x33] void pca_module2_int (void);  /* Vector 10, PCA counter interrupt    */
interrupt [0x33] void pca_module3_int (void);  /* Vector 11, PCA counter interrupt    */
interrupt [0x33] void pca_module4_int (void);  /* Vector 12, PCA counter interrupt    */

  
