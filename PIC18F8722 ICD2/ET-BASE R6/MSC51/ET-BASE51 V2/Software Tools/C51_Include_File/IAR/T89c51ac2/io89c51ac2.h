/*                      - io89c51ac2.H -

   Special header for the Temic t89c51ac2 microcontroller.


*/

#pragma language=extended



/*===============================*/
/* Predefined SFR Byte Addresses */
/*===============================*/

sfr    ACC       = 0xE0;  /* Accumulator                                */
sfr    B         = 0xF0;  /* B Register                                 */
sfr    PSW       = 0xD0;  /* Program Status Word                        */
sfr    SP        = 0x81;  /* Stack Pointer LSB of SPX                   */
sfr    DPL       = 0x82;  /* Data Pointer Low byte LSB of DPTR          */
sfr    DPH       = 0x83;  /* Data Pointer High byte MSB of DPTR         */
sfr    P0        = 0x80;  /* Port 0                                     */
sfr    P1        = 0x90;  /* Port 1                                     */
sfr    P2        = 0xA0;  /* Port 2                                     */
sfr    P3        = 0xB0;  /* Port 3                                     */
sfr    P4        = 0xC0;  /* Port 4 (x2)                                */
sfr    TH0       = 0x8C;  /* Timer/Counter 0 High byte                  */
sfr    TL0       = 0x8A;  /* Timer/Counter 0 Low byte                   */
sfr    TH1       = 0x8D;  /* Timer/Counter 1 High byte                  */
sfr    TL1       = 0x8B;  /* Timer/Counter 1 Low byte                   */
sfr    TH2       = 0xCD;  /* Timer/Counter 2 High byte                  */
sfr    TL2       = 0xCC;  /* Timer/Counter 2 Low byte                   */
sfr    TCON      = 0x88;  /* Timer/Counter 0 and 1 control              */
sfr    TMOD      = 0x89;  /* Timer/Counter 0 and 1 Modes                */
sfr    T2CON     = 0xC8;  /* Timer/Counter 2 control                    */
sfr    T2MOD     = 0xC9;  /* Timer/Counter 2 Mode                       */
sfr    RCAP2H    = 0xCB;  /* Timer/Counter 2 Reload/Capture High byte   */
sfr    RCAP2L    = 0xCA;  /* Timer/Counter 2 Reload/Capture Low byte    */
sfr    WDTRST    = 0xA6;  /* WatchDog Timer Reset                       */
sfr    WDTPRG    = 0xA7;  /* WatchDog Timer Program                     */
sfr    SCON      = 0x98;  /* Serial Control                             */
sfr    SBUF      = 0x99;  /* Serial Data Buffer                         */
sfr    SADEN     = 0xB9;  /* Slave Address Mask                         */
sfr    SADDR     = 0xA9;  /* Slave Address                              */
sfr    CCON      = 0xD8;  /* PCA Timer/Counter Control                  */
sfr    CMOD      = 0xD9;  /* PCA Timer/Counter Mode                     */
sfr    CL        = 0xE9;  /* PCA Timer/Counter Low byte                 */
sfr    CH        = 0xF9;  /* PCA Timer/Counter High byte                */
sfr    CCAPM0    = 0xDA;  /* PCA Timer/Counter Mode 0                   */
sfr    CCAPM1    = 0xDB;  /* PCA Timer/Counter Mode 1                   */
sfr    CCAPM2    = 0xDC;  /* PCA Timer/Counter Mode 2                   */
sfr    CCAPM3    = 0xDD;  /* PCA Timer/Counter Mode 3                   */
sfr    CCAPM4    = 0xDE;  /* PCA Timer/Counter Mode 4                   */
sfr    CCAP0H    = 0xFA;  /* PCA Compare Capture Module 0 H             */
sfr    CCAP1H    = 0xFB;  /* PCA Compare Capture Module 1 H             */
sfr    CCAP2H    = 0xFC;  /* PCA Compare Capture Module 2 H             */
sfr    CCAP3H    = 0xFD;  /* PCA Compare Capture Module 3 H             */
sfr    CCAP4H    = 0xFE;  /* PCA Compare Capture Module 4 H             */
sfr    CCAP0L    = 0xEA;  /* PCA Compare Capture Module 0 L             */
sfr    CCAP1L    = 0xEB;  /* PCA Compare Capture Module 1 L             */
sfr    CCAP2L    = 0xEC;  /* PCA Compare Capture Module 2 L             */
sfr    CCAP3L    = 0xED;  /* PCA Compare Capture Module 3 L             */
sfr    CCAP4L    = 0xEE;  /* PCA Compare Capture Module 4 L             */
sfr    IEN0      = 0xA8;  /* Interrupt Enable Control 0                 */
sfr    IEN1      = 0xE8;  /* Interrupt Enable Control 1                 */
sfr    IPL0      = 0xB8;  /* Interrupt Priority Control Low 0           */
sfr    IPH0      = 0xB7;  /* Interrupt Priority Control High 0          */
sfr    IPL1      = 0xF8;  /* Interrupt Priority Control Low 1           */
sfr    IPH1      = 0xF7;  /* Interrupt Priority Control High1           */
sfr    ADCON     = 0xF3;  /* ADC Control                                */
sfr    ADCF      = 0xF6;  /* ADC Configuration                          */
sfr    ADCLK     = 0xF2;  /* ADC Clock                                  */
sfr    ADDH      = 0xF5;  /* ADC Data High byte                         */
sfr    ADDL      = 0xF4;  /* ADC Data Low byte                          */
sfr    PCON      = 0x87;  /* Power Control                              */
sfr    AUXR      = 0x8E;  /* Auxiliary Register 0                       */
sfr    AUXR1     = 0xA2;  /* Auxiliary Register 1                       */
sfr    CKCON     = 0x8F;  /* Clock Control                              */
sfr    FCON      = 0xD1;  /* FLASH Control                              */
sfr    EECON     = 0xD2;  /* EEPROM Contol                              */


/*==============================*/
/* Predefined SFR Bit Addresses */
/*==============================*/


/*========ACC=========*/

bit   ACC_7      = 0xE7;
bit   ACC_6      = 0xE6;
bit   ACC_5      = 0xE5;
bit   ACC_4      = 0xE4;
bit   ACC_3      = 0xE3;
bit   ACC_2      = 0xE2;
bit   ACC_1      = 0xE1;
bit   ACC_0      = 0xE0;

/*========B===========*/

bit   B7         = 0xF7;
bit   B6         = 0xF6;
bit   B5         = 0xF5;
bit   B4         = 0xF4;
bit   B3         = 0xF3;
bit   B2         = 0xF2;
bit   B1         = 0xF1;
bit   B0         = 0xF0;

/*========PSW=========*/

bit   CY         = 0xD7;
bit   ACF        = 0xD6;
bit   F0         = 0xD5;
bit   RS1        = 0xD4;
bit   RS0        = 0xD3;
bit   OV         = 0xD2;
bit   F1         = 0xD1;
bit   P          = 0xD0;


/*========P0==========*/

bit   P0_7       = 0x87;
bit   P0_6       = 0x86;
bit   P0_5       = 0x85;
bit   P0_4       = 0x84;
bit   P0_3       = 0x83;
bit   P0_2       = 0x82;
bit   P0_1       = 0x81;
bit   P0_0       = 0x80;

/*========P1==========*/

bit   P1_7       = 0x97;
bit   P1_6       = 0x96;
bit   P1_5       = 0x95;
bit   P1_4       = 0x94;
bit   P1_3       = 0x93;
bit   P1_2       = 0x92;
bit   P1_1       = 0x91;
bit   P1_0       = 0x90;

/*========P2==========*/

bit   P2_7       = 0xA7;
bit   P2_6       = 0xA6;
bit   P2_5       = 0xA5;
bit   P2_4       = 0xA4;
bit   P2_3       = 0xA3;
bit   P2_2       = 0xA2;
bit   P2_1       = 0xA1;
bit   P2_0       = 0xA0;

/*========P3==========*/

bit   P3_7       = 0xB7;
bit   P3_6       = 0xB6;
bit   P3_5       = 0xB5;
bit   P3_4       = 0xB4;
bit   P3_3       = 0xB3;
bit   P3_2       = 0xB2;
bit   P3_1       = 0xB1;
bit   P3_0       = 0xB0;

/*========P4==========*/

bit   P4_7       = 0xC7;
bit   P4_6       = 0xC6;
bit   P4_5       = 0xC5;
bit   P4_4       = 0xC4;
bit   P4_3       = 0xC3;
bit   P4_2       = 0xC2;
bit   P4_1       = 0xC1;
bit   P4_0       = 0xC0;


/*========TCON========*/

bit   TF1        = 0x8F;
bit   TR1        = 0x8E;
bit   TF0        = 0x8D;
bit   TR0        = 0x8C;
bit   IE1        = 0x8B;
bit   IT1        = 0x8A;
bit   IE0        = 0x89;
bit   IT0        = 0x88;

/*========T2CON=======*/

bit   TF2        = 0xCF;
bit   EXF2       = 0xCE;
bit   RCLK       = 0xCD;
bit   TCLK       = 0xCC;
bit   EXEN2      = 0xCB;
bit   TR2        = 0xCA;
bit   C_T2       = 0xC9;
bit   CP_RL2     = 0xC8;


/*========SCON========*/

bit   FE_SM0     = 0x9F;
bit   SM1        = 0x9E;
bit   SM2        = 0x9D;
bit   REN        = 0x9C;
bit   TB8        = 0x9B;
bit   RB8        = 0x9A;
bit   TI         = 0x99;
bit   RI         = 0x98;


/*========CCON========*/

bit   CF         = 0xDF;
bit   CR         = 0xDE;
bit   CCF4       = 0xDC;
bit   CCF3       = 0xDB;
bit   CCF2       = 0xDA;
bit   CCF1       = 0xD9;
bit   CCF0       = 0xD8;


/*========IEN0========*/

bit   EA         = 0xAF;
bit   AC         = 0xAE;
bit   ET2        = 0xAD;
bit   ES         = 0xAC;
bit   ET1        = 0xAB;
bit   EX1        = 0xAA;
bit   ET0        = 0xA9;
bit   EX0        = 0xA8;

/*========IEN1========*/

bit   EADC       = 0xE9;

/*========IPL0========*/

bit   PPC        = 0xBE;
bit   PT2        = 0xBD;
bit   PS         = 0xBC;
bit   PT1        = 0xBB;
bit   PX1        = 0xBA;
bit   PT0        = 0xB9;
bit   PX0        = 0xB8;

/*========IPL1========*/

bit   PADCL      = 0xF9;


/*==============================*/
/* Interrupt Vector Definitions */
/*==============================*/

interrupt [0x03] void extern0_int (void);  /* Vector 1, External Interrupt 0      */
interrupt [0x0B] void timer0_int (void);   /* Vector 2, Timer 0                   */
interrupt [0x13] void extern1_int (void);  /* Vector 3, External Interrupt 1      */
interrupt [0x1B] void timer1_int (void);   /* Vector 4, Timer 1                   */
interrupt [0x23] void sio_ri_int (void);   /* Vector 6, UART                      */
interrupt [0x23] void sio_ti_int (void);   /* Vector 6, UART                      */
interrupt [0x2B] void timer2_int (void);   /* Vector 7, Timer 2                   */
interrupt [0x33] void PCA_int (void);      /* Vector 5, PCA Interrupt             */
interrupt [0x43] void ADC_int (void);      /* Vector 8, ADC Interrupt             */

