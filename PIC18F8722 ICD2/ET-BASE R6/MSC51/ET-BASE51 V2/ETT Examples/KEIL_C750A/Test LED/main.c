/*******************************************/
/* Example Program for ET-BASE51 V2.0(ED2) */
/* MCU      : AT89C51ED2 (ATMEL)           */
/* XTAL 	: 29.4912 MHz)                 */
/* Compiler : Keil C51 (V7.50)             */
/* Write By : Eakachai Makarn(ETT CO.,LTD.)*/
/*******************************************/

/* include  section */
#include "at89c51xd2.h"				// AT89C51ED2 SFR : File
#include <stdio.h>                	// Prototype declarations for I/O functions

sbit led = P1^0;					// P1.0 = LED ON/OFF

/* pototype  section */
void delay(unsigned long);			// Delay Time Function(1..4294967295)

/*--------------------------------------------
The main C function.  Program execution Here 
---------------------------------------------*/
void main (void) 
{
   CKCON0 = 0x01;       			// 6 Clock Mode(X2 Mode)
   IE0    = 0x00;             		// Initial Interrupt Control
   AUXR  |= 0x01;             		// Inhibit ALE Signal

   while (1) 
   {      
     led = ~led;					// Toggle ON/OFF LED
     delay(100000);
   }
}

/*******************************************/
/* Long Delay Time Function(1..4294967295) */
/*******************************************/
void delay(unsigned long i)
{
  while(i > 0) {i--;}				// Loop Decrease Counter	
  return;
}

