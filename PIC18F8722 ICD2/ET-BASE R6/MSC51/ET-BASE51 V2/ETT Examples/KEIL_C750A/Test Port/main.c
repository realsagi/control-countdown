/*******************************************/
/* Example Program for ET-BASE51 V2.0(ED2) */
/* MCU      : AT89C51ED2 (ATMEL)           */
/* XTAL 	: 29.4912 MHz)                 */
/* Compiler : Keil C51 (V7.50)             */
/* Write By : Eakachai Makarn(ETT CO.,LTD.)*/
/*******************************************/

/* include  section */
#include "at89c51xd2.h"									// AT89C51ED2 SFR : File
#include <stdio.h>                						// Prototype declarations for I/O functions

/* pototype  section */
void delay(unsigned long);								// Delay Time Function(1..4294967295)

/*--------------------------------------------
The main C function.  Program execution Here 
---------------------------------------------*/
void main (void) 
{
  unsigned char j;										// Data Output Counter

  CKCON0 = 0x01;       									// 6 Clock Mode(X2 Mode)
  IE0    = 0x00;             							// Initial Interrupt Control
  AUXR  |= 0x01;             							// Inhibit ALE Signal

  while (1) 											// Loop Continue 
     {   
       // Shift Left Px[7..0]                 							
       for (j=0x01; j< 0x80; j<<=1)  					// Blink LED Px.7 <- Px.0
           {   
             P0 = 0xff-j;                         		// Output to LED Port(Toggle Active=0) 
             P1 = 0xff-j;                         		// Output to LED Port(Toggle Active=0) 
             P2 = 0xff-j;                         		// Output to LED Port(Toggle Active=0) 
             P3 = 0xff-j;                         		// Output to LED Port(Toggle Active=0) 
             delay(100000);
           }

	   // Shift Right Px[7..0]
       for (j=0x80; j> 0x01; j>>=1)  					// Blink LED Px.7 -> Px.0
           {   
 			 P0 = 0xff-j;                         		// Output to LED Port(Toggle Active=0) 
             P1 = 0xff-j;                         		// Output to LED Port(Toggle Active=0) 
             P2 = 0xff-j;                         		// Output to LED Port(Toggle Active=0) 
             P3 = 0xff-j;                         		// Output to LED Port(Toggle Active=0)              
             delay(100000);
           }
  	  }
}

/*******************************************/
/* Long Delay Time Function(1..4294967295) */
/*******************************************/
void delay(unsigned long i)
{
  while(i > 0) {i--;}									// Loop Decrease Counter	
  return;
}

