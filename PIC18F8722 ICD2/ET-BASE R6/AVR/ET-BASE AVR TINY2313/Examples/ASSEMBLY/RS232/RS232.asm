;***********************************************************
;* Examples Program For : ET-BASE AVR TINY2313             *
;* Target MCU  : Atmel ATTINY2313                          *
;* Frequency   : X-TAL : 7.3728 MHz                        *
;* Compiler    : AVR Studio 4.12 (AVR Assembler 2)         *
;* Last Update : 25/August/2006                            *
;* Description : RS232 Echo                                *
;*             : 9600,N,8,1                                *
;***********************************************************

.include "tn2313def.inc"

            .ORG   $0000 
             RJMP   RESET                ;Reset Handle


.def	    TEMP  = R16   

RESET:      
			LDI     TEMP,LOW(RAMEND)
            OUT     SPL,TEMP            ;Init Stack Pointer Low     

            LDI     TEMP,high(RAMEND)
            OUT     SPL+1,TEMP          ;Init Stack Pointer High

            CLI                         ;clear golbal interrupt
            CBI     UCR,RXCIE           ;Clesr rx interrupt
            CBI     UCR,TXCIE           ;Clesr tx interrupt
            CBI     UCR,UDRIE           ;Clesr data empty interrupt
            CBI     UCR,CHR9            ;Send 8 data 

            LDI     TEMP,47             ;Baud 9600 at 7.3728 MHz X-TAL
            OUT     UBRR,TEMP            
  
            SBI     UCR,TXEN            ;Set pin tx as serial TX 
            SBI     UCR,RXEN            ;Set pin rx as serial RX

            LDI     ZH,high(TEXT_TAB*2)
            LDI     ZL,low(TEXT_TAB*2)
XX:         
			LPM
            LDI     R16,0X00
            CP      R0,R16
            BREQ    YY 
            MOV     R20,R0
            RCALL   TX232
            ADIW    ZL,1
            RJMP    XX

YY:         
			RCALL   RX232
            RCALL   TX232 
            RJMP    YY
            
           
TX232:      
			SBIS    USR,UDRE           ;Wait until UDRE = 1
            RJMP    TX232 
            OUT     UDR,R20            ;Send data to serial  
            RET 
            
RX232:      
			SBIS    USR,RXC            ;Wait until have data in buffer
            RJMP    RX232
            IN      R20,UDR            ;Read data from buffer
            RET

TEXT_TAB:   
			.DB     0X0C,"Hello This is ET-BASE AVR TINY2313...Test RS232",0x0A,0x0D
			.DB     "Please enter any key to echo ",0x0A,0x0D,0x0A,0x0D,0






        
                   
                            
