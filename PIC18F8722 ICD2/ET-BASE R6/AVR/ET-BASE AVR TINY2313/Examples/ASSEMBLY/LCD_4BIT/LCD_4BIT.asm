;***********************************************************
;* Examples Program For : ET-BASE TINY2313                 *
;* Target MCU  : Atmel ATTINY2313                          *
;* Frequency   : X-TAL : 7.3728 MHz                        *
;* Compiler    : AVR Studio 4.12 (AVR Assembler 2)         *
;* Last Update : 28/June/2006                              *
;* Description : Example Use Character LCD 16x2 4-bit      *
;***********************************************************

;Connect LCD Module to ET-CLCD PORT  
;Lcdpin : D4 = PB0 , D5 = PB1 , D6 = PB2 , D7 = PB3 , EN = PB4 , RS = PB6

.include "tn2313def.inc"

;/******************************
; Define Register
;/******************************

.def		COUNTER1  =  R16
.def		COUNTER2  =  R17
.def		COUNTER3  =  R18
.def        TEMP      =  R19
.def        TEMP1     =  R20
.def		COLUMN	  =  R21

.equ        PORT_LCD  =  PORTB
.equ        DDR_LCD   =  DDRB 
.equ        RS_LCD    =  6
.equ        EN_LCD    =  4 

;/******************************
; Main Program
;/******************************
.CSEG
			.ORG   	0 
    		RJMP  	RESET				 	;Reset Handle

RESET:        
			LDI      TEMP,LOW(RAMEND)
            OUT      SPL,TEMP            	;Init Stack Pointer Low     
            LDI      TEMP,HIGH(RAMEND)
            OUT      SPL+1,TEMP          	;Init Stack Pointer High

            LDI      TEMP,0X5F           
            OUT      DDR_LCD,TEMP

            RCALL    DELAY30ms              ;Delay for start
            RCALL    INIT_LCD            	;Initial  LCD

LOOP:
			LDI      TEMP,0x80           	;Line 1   
            RCALL    WR_INS
            LDI      ZL,LOW(TEXT1*2)     	
            LDI      ZH,HIGH(TEXT1*2)
			RCALL    SHOW_LCD


            LDI      TEMP,0xC0            	;Line 2     
            RCALL    WR_INS 
			LDI      ZL,LOW(TEXT2*2)
            LDI      ZH,HIGH(TEXT2*2)
			RCALL    SHOW_LCD
            
			RCALL    DELAY1s

			LDI      TEMP,0x80             	;Line 1   
            RCALL    WR_INS
			LDI      ZL,LOW(TEXT3*2)     	
            LDI      ZH,HIGH(TEXT3*2)
			RCALL    SHOW_LCD


            LDI      TEMP,0xC0              ;Line 2     
            RCALL    WR_INS 
			LDI      ZL,LOW(TEXT4*2)
            LDI      ZH,HIGH(TEXT4*2)
			RCALL    SHOW_LCD

			RCALL    DELAY1s

			RJMP     LOOP

SHOW_LCD:             
			LPM
            TST      R0
            BREQ     END 
            MOV      TEMP,R0
            RCALL    WR_LCD
            ADIW     ZL,0X01
            RJMP     SHOW_LCD
END:             
			RET

;****************************************************
;*                WRITE DATA TO LCD                 *
;****************************************************
WR_LCD:           
			PUSH     TEMP
		    SWAP     TEMP
            ANDI     TEMP,0x0F
            SBI      PORT_LCD,RS_LCD
            IN       COLUMN,PORT_LCD
            ANDI     COLUMN,0xF0
            OR       TEMP,COLUMN
            OUT      PORT_LCD,TEMP
            RCALL    ENA_LCD
            POP      TEMP
            ANDI     TEMP,0x0F
            IN       COLUMN,PORT_LCD
            ANDI     COLUMN,0xF0
            OR       TEMP,COLUMN
            OUT      PORT_LCD,TEMP
            RCALL    ENA_LCD
            RET

;****************************************************
;*               WRITE INSTRUCTION LCD              *
;****************************************************
WR_INS:       
			PUSH     TEMP
            SWAP     TEMP
            ANDI     TEMP,0x0F
            CBI      PORT_LCD,RS_LCD 
            IN       COLUMN,PORT_LCD
			SER		 TEMP1
			OUT 	 DDR_LCD,TEMP1
            ANDI     COLUMN,0xF0
            OR       TEMP,COLUMN
            OUT      PORT_LCD,TEMP
            RCALL    ENA_LCD
            POP      TEMP
            ANDI     TEMP,0x0F
            IN       COLUMN,PORT_LCD
            ANDI     COLUMN,0xF0
            OR       TEMP,COLUMN
            OUT      PORT_LCD,TEMP 
            RCALL    ENA_LCD
            RET

;***************************************************
;*                  Initial LCD                    *
;***************************************************
INIT_LCD:   
            CBI      PORT_LCD,EN_LCD      
			LDI      TEMP,0x33       
            RCALL    WR_INS
            LDI      TEMP,0x32
            RCALL    WR_INS
            LDI      TEMP,0x28
            RCALL    WR_INS
            LDI      TEMP,0x0C
            RCALL    WR_INS
            LDI      TEMP,0x06
            RCALL    WR_INS
            LDI      TEMP,0x01
            RCALL    WR_INS
            RET

;****************************************************
;*                   ENABLE LCD                     *
;****************************************************
ENA_LCD:           
			SBI      PORT_LCD,EN_LCD
			RCALL    DELAY1ms
            CBI      PORT_LCD,EN_LCD
			RCALL    DELAY1ms
            RET

;****************************************************
;*                   DELAY TIME                     *
;****************************************************
DELAY1ms:	
			PUSH	COUNTER1
			PUSH	COUNTER2
			LDI    	COUNTER1,8
DELAY1ms_1: 
			LDI    	COUNTER2,250
DELAY1ms_2: 
			NOP
			DEC    	COUNTER2
            BRNE   	DELAY1ms_2
            DEC    	COUNTER1
            BRNE   	DELAY1ms_1
            POP	    COUNTER2
			POP	    COUNTER1
			RET          

DELAY30ms:	
			PUSH	COUNTER1
			PUSH	COUNTER2
			LDI    	COUNTER1,240
DELAY30ms_1:   	
			LDI    	COUNTER2,250
DELAY30ms_2:   	
			NOP
			DEC    	COUNTER2
            BRNE   	DELAY30ms_2
            DEC    	COUNTER1
            BRNE   	DELAY30ms_1
            POP	    COUNTER2
			POP	    COUNTER1
			RET          

DELAY200ms:	
			PUSH	COUNTER3
			LDI	    COUNTER3,200
DELAY200ms_1:	
			RCALL	DELAY1ms
			DEC	    COUNTER3
			BRNE	DELAY200ms_1
			POP	    COUNTER3
			RET

DELAY1s:	
			PUSH	COUNTER3
			LDI	    COUNTER3,30
DELAY1s_1:	
			RCALL	DELAY30ms
			DEC	    COUNTER3
			BRNE	DELAY1s_1
			POP	    COUNTER3
			RET

;***************************************************
;* Code segment                                    *
;***************************************************

TEXT1:		.DB	"ET-BASE AVR..... ",0
TEXT2:		.DB	"BASE ON TINY2313 ",0
TEXT3:      .DB "7.4 MIPS Execute ",0
TEXT4:      .DB "BY..ETT CO.,LTD. ",0

                







