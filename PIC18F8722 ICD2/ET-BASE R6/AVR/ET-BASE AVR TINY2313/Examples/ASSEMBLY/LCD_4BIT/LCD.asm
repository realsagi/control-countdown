;*******************************************************
;* Examples Program For "ET-AVR STAMP ATmega64" Board  *
;* Target MCU  : Atmel ATmega64                        *
;* Frequency   : X-TAL : 16 MHz                        *
;* Compiler    : AVR Studio 4.12 (AVR Assembler 2)     *
;* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)       *
;* Last Update : 4/February/2006                       *
;* Description : Example Use Character LCD 16x2        *
;*             : 4-bit Mode                            *
;*******************************************************

;Connect LCD Module to ET-CLCD PORT  
;Lcdpin : D4 = Porte.4 , D5 = Porte.5 , D6 = Porte.6 , D7 = Porte.7 , EN = Porte.3 , RS = Porte.2

.include "m64def.inc"			

;/******************************
; Define Register
;/******************************
.def		COUNTER1  	= R16
.def		COUNTER2	= R17
.def		COUNTER3	= R18
.def		TEMP1		= R19
.def		TEMP		= R20
.def		DATA		= R21
.equ		PORT_LCD	= PORTE
.equ		DDR_LCD		= DDRE
.equ		PIN_LCD		= PINE
.equ		EN_LCD		= 3
.equ		RES_LCD		= 2

;/******************************
; Main Program
;/******************************
.CSEG
			.ORG   	0 
    		RJMP  	RESET					;Reset Handle

RESET:      LDI		TEMP,LOW(RAMEND)		;Init Stack Pointer  
            OUT		SPL,TEMP          		   
            LDI		TEMP,HIGH(RAMEND)
            OUT		SPH,TEMP 
			       	
;/*******************************************
; Initialize LCD before send any data to it.
;/*******************************************
MAIN:		RCALL	DELAY_15ms
			SER		TEMP
			OUT		DDR_LCD,TEMP
			LDI		TEMP,0b00110000
			OUT		PORT_LCD,TEMP
			LDI		COUNTER3,3
LOOP_INIT:	SBI		PORT_LCD,EN_LCD
	     	CBI		PORT_LCD,EN_LCD
			RCALL	DELAY_1ms
			DEC		COUNTER3
			BRNE	LOOP_INIT
			CBI		PORT_LCD,4
			SBI		PORT_LCD,EN_LCD
			CBI		PORT_LCD,EN_LCD
			RCALL	DELAY_1ms
            RCALL   INIT_LCD          		;Init LCD

;/**************************************
; Display First line from Left to Right
;/**************************************
START_DISPLAY:	
			LDI		DATA,0x06				;Increment address mode
			RCALL	WR_INS
			LDI     DATA,0x80				;DDRAM address 00h
	        RCALL   WR_INS  
	     	LDI     ZH,HIGH(TEXT1*2)		
	     	LDI     ZL,LOW(TEXT1*2)			;Init pointer		
DISPLAY_TO_RIGHT:
	     	LPM								;Get Data
	     	TST		R0
			BREQ	END_TO_RIGHT
			MOV     DATA,R0     		
	     	RCALL	WR_DATA
			RCALL	DELAY_200ms
			ADIW    ZL,1					;Increment Z-pointer
			BRNE	DISPLAY_TO_RIGHT
END_TO_RIGHT:

;/*****************************************
; Display Second line from Right to Left
;/*****************************************	
			LDI		DATA,0x04				;Decrement Address mode
			RCALL	WR_INS
			LDI		DATA,0xCF				;DDRAM address 4F
			RCALL	WR_INS
			LDI		ZH,HIGH(TEXT2*2)
			LDI		ZL,LOW(TEXT2*2)			;Init pointer
			ADIW	ZL,15					;Increment ZL
DISPLAY_TO_LEFT:
			LPM								;Get Data
	     	TST		R0
			BREQ	END_TO_LEFT
			MOV		DATA,R0
			RCALL	WR_DATA
			RCALL	DELAY_200ms
	     	SBIW    ZL,1					;Decrement Z-pointer
			BRNE	DISPLAY_TO_LEFT
END_TO_LEFT:

			RCALL	DELAY_1s
			RCALL	DISPLAY_ON_OFF

;/**********************************
; Shift Left all 2 lines,16 times
;/**********************************
			LDI		COUNTER3,16
SHIFT_LEFT:	LDI		DATA,0x18				;Shift Left
			RCALL	WR_INS
			RCALL	DELAY_200ms
			DEC		COUNTER3
			BRNE	SHIFT_LEFT
			RCALL	DELAY_1s

;/**********************************
; Shift Right all 2 lines ,32 times
;/**********************************

SHIFT_RIGHT:	
			LDI		COUNTER3,2
SFR1:		PUSH	COUNTER3
			LDI		COUNTER3,16
SFR2:		LDI		DATA,0x1C				;Shift right
			RCALL	WR_INS
			RCALL	DELAY_200ms
			DEC		COUNTER3
			BRNE	SFR2
			RCALL	DELAY_1s
			POP		COUNTER3
			DEC		COUNTER3			
			BRNE	SFR1
			RCALL	INIT_LCD				;Init LCD
			RJMP	START_DISPLAY			;Begin again
	
;/*****************************
; Sub Routine Display ON / OFF
;/*****************************
DISPLAY_ON:	LDI		DATA,0x0C
			RCALL	WR_INS
			RET

DISPLAY_OFF:	
			LDI		DATA,0x08
			RCALL	WR_INS
			RET

CLEAR_DISPLAY:	
			LDI		DATA,0x01
			RCALL	WR_INS
			RET

DISPLAY_ON_OFF:
			LDI		COUNTER3,5
SUB_ON_OFF:	RCALL	DISPLAY_OFF
			RCALL	DELAY_200ms
			RCALL	DISPLAY_ON
			RCALL	DELAY_200ms
			DEC		COUNTER3
			BRNE	SUB_ON_OFF
			RCALL	DELAY_1s
			RET

;************************************
; Initial LCD
;************************************

INIT_LCD:	LDI     DATA,0x28             	;4 bit,2 lines,5x7 dots
            RCALL   WR_INS                       
            LDI     DATA,0x0C				;ON Display,OFF Cursor
            RCALL   WR_INS
            LDI     DATA,0x06              	;Increment DDRAM Address by 1
            RCALL   WR_INS 
            LDI     DATA,0x01				;Reset LCD
            RCALL   WR_INS 
            RET

WR_INS:		CBI		PORT_LCD,RES_LCD
			RCALL	DELAY_1ms
			RCALL	WR_DAT
			RET

WR_DATA:	SBI		PORT_LCD,RES_LCD
			RCALL	DELAY_1ms
			RCALL	WR_DAT
			RET

;*************************************
; Write Command and Data 8 bit 
;*************************************
WR_DAT:		LDI     COUNTER1,2
	     	MOV     TEMP,DATA				;Reserve Data        
WR_DAT1:    PUSH	TEMP
			ANDI    DATA,0xF0				;Clear 4 bit low
			CLR		TEMP1
			OUT		DDR_LCD,TEMP1
            IN      TEMP,PIN_LCD
			SER		TEMP1
			OUT		DDR_LCD,TEMP1
            ANDI    TEMP,0x0F				;Clear 4 bit high
            OR      DATA,TEMP                             
            OUT     PORT_LCD,DATA           ;Send Data out     
	     	RCALL   ENABLE_LCD				;LCD receive 4 bit Data 
            DEC     COUNTER1
			BREQ	END_WR
			POP		TEMP		
            SWAP    TEMP					;Next 4 bit Data LOW 
	     	MOV     DATA,TEMP
			RJMP	WR_DAT1	     	
END_WR:		POP		TEMP
			RET
	
;*********************************
; Enable LCD                               
; Rising edge to Enable LCD pin
;*********************************  
ENABLE_LCD: SBI     PORT_LCD,EN_LCD     	;Rising edge
            RCALL   DELAY_1ms
            CBI     PORT_LCD,EN_LCD			;Falling edge
	     	RCALL   DELAY_1ms
            RET

;*********************************
; Delay time
;*********************************
DELAY_1ms:	PUSH	COUNTER1
			PUSH	COUNTER2
			LDI		COUNTER1,16
DELAY_1ms_1: 
			LDI 	COUNTER2,250
DELAY_1ms_2: 
			NOP
			DEC 	COUNTER2
            BRNE   	DELAY_1ms_2
            DEC    	COUNTER1
            BRNE   	DELAY_1ms_1
            POP		COUNTER2
			POP		COUNTER1
			RET          

DELAY_15ms:	PUSH	COUNTER1
			PUSH	COUNTER2
			LDI		COUNTER1,240
DELAY_15ms_1: 
			LDI 	COUNTER2,250
DELAY_15ms_2: 
			NOP
			DEC 	COUNTER2
            BRNE   	DELAY_15ms_2
            DEC    	COUNTER1
            BRNE   	DELAY_15ms_1
            POP		COUNTER2
			POP		COUNTER1
			RET          

DELAY_200ms:	
			PUSH	COUNTER3
			LDI		COUNTER3,200
DELAY_200ms_1:	
			RCALL	DELAY_1ms
			DEC		COUNTER3
			BRNE	DELAY_200ms_1
			POP		COUNTER3
			RET

DELAY_1s:	PUSH	COUNTER3
			LDI		COUNTER3,66
DELAY_1s_1:	RCALL	DELAY_15ms
			DEC		COUNTER3
			BRNE	DELAY_1s_1
			POP		COUNTER3
			RET

;/******************
; Code segment
;/******************

TEXT1:		.DB	"WELCOME TO ETT",0
TEXT2:		.DB	"BY AVR-STAMP !!!",0

