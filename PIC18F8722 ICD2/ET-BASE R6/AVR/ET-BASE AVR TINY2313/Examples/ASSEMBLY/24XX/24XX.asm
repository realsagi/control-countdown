;***********************************************************
;* Examples Program For : ET-BASE AVR TINY2313             *
;* Target MCU  : Atmel ATTINY2313                          *
;* Frequency   : X-TAL : 7.3728 MHz                        *
;* Compiler    : AVR Studio 4.12 (AVR Assembler 2)         *
;* Last Update : 25/August/2006                            *
;* Description : EEPROM (24XX) TO RS232     	           *
;*             : 9600,N,8,1                                * 
;***********************************************************

.include "tn2313def.inc"

                .ORG    $0000 
                RJMP   RESET_VEC           ;Reset Handle

.equ       PORT_EE   =  PORTB
.equ       DDR_EE    =  DDRB
.equ       PIN_EE    =  PINB
.equ       BIT_SCL   =  7
.equ       BIT_SDA   =  5

.def       TEMP      =  R19
.def       TEMP1     =  R20
       

RESET_VEC:        
				LDI      TEMP,LOW(RAMEND)
                OUT      SPL,TEMP            ;Init Stack Pointer Low     
                LDI      TEMP,HIGH(RAMEND)
                OUT      SPL+1,TEMP          ;Init Stack Pointer High

                RCALL    DELAY               ;Delay for start

                CLI                          ;Clear golbal interrupt
                CBI      UCR,RXCIE           ;Clesr rx interrupt
                CBI      UCR,TXCIE           ;Clesr tx interrupt
                CBI      UCR,UDRIE           ;Clesr data empty interrupt
                CBI      UCR,CHR9            ;Send 8 data 

                LDI      R17,47
                OUT      UBRR,r17            ;Baud rate 9600 at 7.3728 MHz X-TAL 
  
                SBI      UCR,TXEN            ;Set pin tx as serial TX 
                SBI      UCR,RXEN            ;Set pin rx as serial RX

                SBI      DDR_EE,BIT_SDA
                SBI      DDR_EE,BIT_SCL 
                SBI      PORT_EE,BIT_SDA
                SBI      PORT_EE,BIT_SCL

                LDI      TEMP,0X0C
                RCALL    TX232

                LDI      ZH,high(TEXT_TAB*2)
                LDI      ZL,low(TEXT_TAB*2)
XX:               
				LPM
                LDI      R16,0X00
                CP       R0,R16
                BREQ     YY 
                MOV      TEMP,R0
                RCALL    TX232
                ADIW     ZL,1
                RJMP     XX

YY:               
				LDI      TEMP,0X0A
                RCALL    TX232
                LDI      TEMP,0X0D
                RCALL    TX232

                LDI      ZH,high(TEXT_TAB1*2)
                LDI      ZL,low(TEXT_TAB1*2)
AA:               
				LPM
                LDI      R16,0X00
                CP       R0,R16
                BREQ     BB 
                MOV      TEMP,R0
                RCALL    TX232
                ADIW     ZL,1
                RJMP     AA

BB:               
				LDI      TEMP,0X0A
                RCALL    TX232
                LDI      TEMP,0X0D
                RCALL    TX232

                LDI      ZH,high(TEXT_TAB2*2)
                LDI      ZL,low(TEXT_TAB2*2)
CC:               
				LPM
                LDI      R16,0X00
                CP       R0,R16
                BREQ     DD 
                MOV      TEMP,R0
                RCALL    TX232
                ADIW     ZL,1
                RJMP     CC

DD:               
				RCALL    WRITE_BYTE
                RCALL    RANDOM_READ
                PUSH     TEMP
                SWAP     TEMP
                ANDI     TEMP,0X0F
                RCALL    HEXASC
                RCALL    TX232 
                POP      TEMP
                ANDI     TEMP,0X0F
                RCALL    HEXASC
                RCALL    TX232
WAIT:             
				RJMP     WAIT

;****************************************************
;*                  WRITE BYTE                      *
;****************************************************
WRITE_BYTE:       
				CBI      PORT_EE,BIT_SDA      ;Start condition
                CBI      PORT_EE,BIT_SCL
                LDI      TEMP,0XA8            ;Write command
                RCALL    TX_BYTE
                RCALL    ACKNOW               ;Wait acknowledge
                BRCS     WRITE_BYTE 

                LDI      TEMP,0X00            ;Address high
                RCALL    TX_BYTE
                RCALL    ACKNOW               ;Wait acknowledge
                BRCS     WRITE_BYTE

                LDI      TEMP,0X00            ;Address low
                RCALL    TX_BYTE
                RCALL    ACKNOW
                BRCS     WRITE_BYTE

                LDI      TEMP,0X55            ;Data
                RCALL    TX_BYTE
                RCALL    ACKNOW               ;Wait acknowledge
                BRCS     WRITE_BYTE

                CBI      PORT_EE,BIT_SDA

                SBI      PORT_EE,BIT_SCL      ;Stop condition
                SBI      PORT_EE,BIT_SDA
                RCALL    DELAY
                RET
;*****************************************************
;*                   RANDOM READ                     *
;*****************************************************
RANDOM_READ:      
				CBI      PORT_EE,BIT_SDA      ;Start condition
                CBI      PORT_EE,BIT_SCL
                LDI      TEMP,0XA8            ;Write command
                RCALL    TX_BYTE
                RCALL    ACKNOW               ;Wait acknowledge
                BRCS     RANDOM_READ 

                LDI      TEMP,0X00            ;Address high
                RCALL    TX_BYTE
                RCALL    ACKNOW               ;Wait acknowledge
                BRCS     RANDOM_READ

                LDI      TEMP,0X00            ;Address low
                RCALL    TX_BYTE
                RCALL    ACKNOW
                BRCS     RANDOM_READ

                SBI      PORT_EE,BIT_SCL      ;Stop condition
                SBI      PORT_EE,BIT_SDA

                CBI      PORT_EE,BIT_SDA
                CBI      PORT_EE,BIT_SCL

                LDI      TEMP,0XA9
                RCALL    TX_BYTE
                RCALL    ACKNOW
                BRCS     RANDOM_READ

                RCALL    RX_BYTE

                SBI      PORT_EE,BIT_SCL
                SBI      PORT_EE,BIT_SCL

                CBI      PORT_EE,BIT_SDA

                SBI      PORT_EE,BIT_SCL       ;Stop condition
                SBI      PORT_EE,BIT_SDA
                RET

;*****************************************************
;*             CHECK BIT ACKNOWLEDGE BIT             *
;CARRY = 1: FAIL                                     *
;CARRY = 0: PASS                                     *
;*****************************************************
ACKNOW:           
				CBI      DDR_EE,BIT_SDA        ;BIT_SDA input port
                SBI      PORT_EE,BIT_SDA
                SEC
                SBI      PORT_EE,BIT_SCL          
                SBIS     PIN_EE,BIT_SDA
                CLC
                CBI      PORT_EE,BIT_SCL
                SBI      DDR_EE,BIT_SDA        ;BIT_SDA output port
                RET

;****************************************************
;*               TX 1 BITE TO 24256                 *
;****************************************************
TX_BYTE:          
				LDI      TEMP1,0X08
TX_BYTE1:         
				SBI      PORT_EE,BIT_SDA
                LSL      TEMP
                BRCS     TX_BYTE2
                CBI      PORT_EE,BIT_SDA
TX_BYTE2:         
				SBI      PORT_EE,BIT_SCL
                CBI      PORT_EE,BIT_SCL
                DEC      TEMP1
                BRNE     TX_BYTE1
                RET
				
;****************************************************
;*               RX 1 BYTE FROM 24256               *
;****************************************************
RX_BYTE:          
				CBI      DDR_EE,BIT_SDA
                SBI      PORT_EE,BIT_SDA
                LDI      TEMP,0X00
                LDI      TEMP1,0X08
RX_BYTE1:         
				SBI      PORT_EE,BIT_SCL
                SEC
                SBIS     PIN_EE,BIT_SDA
                CLC
                ROL      TEMP
                CBI      PORT_EE,BIT_SCL
                DEC      TEMP1
                BRNE     RX_BYTE1
                SBI      DDR_EE,BIT_SDA
                RET

;***************************************************
;*                DELAY TIME FOR START             *
;***************************************************
DELAY:            
				PUSH     R26
                PUSH     R27
                LDI      R27,0XFF
DEL2:             
				LDI      R26,0XFF
DEL1:             
				DEC      R26
                BRNE     DEL1
                DEC      R27
                BRNE     DEL2
                POP      R27
                POP      R26
                RET

HEXASC:           
				LDI      ZL,LOW(ASC_TAB*2)
                LDI      ZH,HIGH(ASC_TAB*2)
                ADD      ZL,TEMP
                LDI      TEMP,0
                ADC      ZH,TEMP
                LPM
                MOV      TEMP,R0
                RET

ASC_TAB:          
				.DB     0x30,0x31,0x32,0x33,0x34,0x35
                .DB     0x36,0x37,0x38,0x39,0x41,0x42
                .DB     0x43,0x44,0x45,0x46
                       
TX232:            
				SBIS    USR,UDRE            ;Wait until UDRE = 1
                RJMP    TX232 
                OUT     UDR,TEMP            ;Send data to serial  
                RET 
                              
TEXT_TAB:         
				.DB     "TEST EEPROM (24XX) ",0x00
TEXT_TAB1:        
				.DB     "WRITE = 55 ",0x00
TEXT_TAB2:        
				.DB     "READ  =",0x00







