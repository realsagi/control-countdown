;*******************************************************
;* Examples Program For "ET-AVR STAMP ATmega64" Board  *
;* Target MCU  : Atmel ATmega64                        *
;* Frequency   : X-TAL : 16 MHz                        *
;* Compiler    : AVR Studio 4.12 (AVR Assembler 2)     *
;* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)       *
;* Last Update : 4/February/2006                       *
;* Description : Example LED Blink on Portb.0          *
;*******************************************************

;Connect PB0 to LED1

.include "m64def.inc"			
		
;************************
; Define Register
;************************
.def		COUNTER1  	= R16
.def		COUNTER2	= R17
.def		COUNTER3	= R18
.def		TEMP		= R19

;***********************
; Define I/O Port,Pin
;***********************
.equ		LED	=	0

;*******************
; Main Program
;*******************
.CSEG
            	.ORG   	0 
    			RJMP  	RESET					;Reset Handle

RESET:      	LDI		TEMP,LOW(RAMEND)		;Initial Stack Pointer 
            	OUT		SPL,TEMP          		    
            	LDI		TEMP,HIGH(RAMEND)
            	OUT		SPH,TEMP
			   
		    	SBI 	DDRB,LED				;Config Portb.0 as output

MAIN:			SBI 	PORTB,LED
				RCALL 	DELAY_200ms
				CBI 	PORTB,LED
				RCALL 	DELAY_200MS
				RJMP 	MAIN


;/******************
; Delay time
;/******************
DELAY_1ms:		LDI    	COUNTER1,16
DELAY_1ms_1: 	LDI    	COUNTER2,250
DELAY_1ms_2: 	NOP
				DEC		COUNTER2           
            	BRNE   	DELAY_1ms_2
            	DEC 	COUNTER1
            	BRNE   	DELAY_1ms_1
				RET         

DELAY_200ms:	LDI		COUNTER3,200
DELAY_200ms_1:	RCALL	DELAY_1ms
				DEC		COUNTER3
				BRNE	DELAY_200ms_1
				RET
           	 
