;***********************************************************
;* Examples Program For : ET-BASE AVR TINY2313             *
;* Target MCU  : Atmel ATTINY2313                          *
;* Frequency   : X-TAL : 7.3728 MHz                        *
;* Compiler    : AVR Studio 4.12 (AVR Assembler 2)         *
;* Last Update : 25/August/2006                            *
;* Description : Test LED Blink on Portd.6                 *
;***********************************************************
 
.include "tn2313def.inc"

            .ORG   $0000 
             RJMP  RESET		      	;Reset Handle

.def	    TEMP  = R16

;***********************
; Define I/O Port,Pin
;***********************
.equ		LED	=	6


RESET:      
			LDI     TEMP,LOW(RAMEND)
            OUT     SPL,TEMP          	;Init Stack Pointer Low     

            LDI     TEMP,HIGH(RAMEND)
            OUT     SPL+1,TEMP        	;Init Stack Pointer High

              
		    SBI 	DDRD,LED		    ;Config Portd.6 as output

MAIN:			
			SBI 	PORTD,LED
			RCALL 	DELAY
			CBI 	PORTD,LED
			RCALL 	DELAY
			RJMP 	MAIN

;**********************************         
;*         Delay program          *  
;**********************************   
DELAY:     
			LDI     R22,0x05
DLY1:      
			LDI     R23,0xFF
DLY2:      
			LDI     R24,0xFF
DLY3:      
			DEC     R24
           	BRNE    DLY3  
           	DEC     R23
           	BRNE    DLY2
           	DEC     R22
           	BRNE    DLY1
           	RET   
