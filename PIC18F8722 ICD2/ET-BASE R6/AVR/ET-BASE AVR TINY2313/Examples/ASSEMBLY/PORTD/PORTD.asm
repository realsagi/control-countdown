;***********************************************************
;* Examples Program For : ET-BASE AVR TINY2313             *
;* Target MCU  : Atmel ATTINY2313                          *
;* Frequency   : X-TAL : 7.3728 MHz                        *
;* Compiler    : AVR Studio 4.12 (AVR Assembler 2)         *
;* Last Update : 25/August/2006                            *
;* Description : Test PORTD                                *
;***********************************************************
 
.include "tn2313def.inc"

            .ORG   $0000 
             RJMP  RESET		      	;Reset Handle

.def	    TEMP  = R16

RESET:      
			LDI     TEMP,LOW(RAMEND)
            OUT     SPL,TEMP          	;Init Stack Pointer Low     

            LDI     TEMP,HIGH(RAMEND)
            OUT     SPL+1,TEMP        	;Init Stack Pointer High

            SER		TEMP
		    OUT		DDRD,TEMP		  	;Config Portc as output
	
			LDI		TEMP,0xFB			; 11111011

ROTATE_LEFT:	
			OUT	    PORTD,TEMP
			RCALL	DELAY
			CPI		TEMP,0x7F			; 01111111
		    BREQ	ROTATE_RIGHT
			SEC							; cy = 1
			ROL		TEMP				; cy <- bit7...bit0 <- cy
			RJMP	ROTATE_LEFT
					
ROTATE_RIGHT:	
			OUT		PORTD,TEMP
			RCALL	DELAY				
			CPI		TEMP,0xFB			; 11111011
			BREQ	ROTATE_LEFT
			SEC							; cy = 1
			ROR		TEMP				; cy -> cit7...bit0 -> cy
			RJMP	ROTATE_RIGHT

;**********************************         
;*         Delay program          *  
;**********************************   
DELAY:     
			LDI     R22,0x05
DLY1:      
			LDI     R23,0xFF
DLY2:      
			LDI     R24,0xFF
DLY3:      
			DEC     R24
           	BRNE    DLY3  
           	DEC     R23
           	BRNE    DLY2
           	DEC     R22
           	BRNE    DLY1
           	RET   
