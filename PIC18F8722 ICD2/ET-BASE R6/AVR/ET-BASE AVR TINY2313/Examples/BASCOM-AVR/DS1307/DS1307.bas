'***********************************************************
'* Examples Program For "ET-BASE AVR TINY2313" Board       *
'* Target MCU  : Atmel ATtiny2313                          *
'* Frequency   : X-TAL : 7.3728 MHz                        *
'* Compiler    : BASCOM-AVR 1.11.7.9                       *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)           *
'* Last Update : 10/July/2006                              *
'* Description : Example Use I2C Connect DS1307            *
'*             : Setup RS232 = 9600,N,8,1                  *
'***********************************************************
'SCL = PB7(I2C-SCL)
'SDA = PB5(I2C-SDA)
'Address DS1307 = 1101000x
'Display Result to Serial Port UART(9600 bps)

$regfile = "ATtiny2313.dat"                                 'ATtiny2313 MCU
$baud = 9600                                                'Serial Port 9600 bps
$crystal = 7372800                                          'XTAL = 7.3728 MHz

Config Scl = Portb.7
Config Sda = Portb.5

Declare Sub Write_rtc_ds1307(byval Adres As Byte , Byval Value As Byte)
Declare Sub Read_rtc_ds1307(byval Adres As Byte , Value As Byte)

Const Id_write_ds1307 = &B11010000                          'Slave Write Address
Const Id_read_ds1307 = &B11010001                           'Slave Read Address

Dim Adres As Byte , Value As Byte                           'Dim byte

Call Write_rtc_ds1307(2 , 0)
Call Write_rtc_ds1307(1 , 0)
Call Write_rtc_ds1307(0 , 0)

Printbin $0c                                                'Clear Display
Print "Hello This is ET-BASE AVR TINY2313"
Print "Demo Test RTC:DS1307 Polling Read"
Print                                                       'Blank Line

Do
   Call Read_rtc_ds1307(2 , Value) : Print Bcd(value) ; ":" ;
   Call Read_rtc_ds1307(1 , Value) : Print Bcd(value) ; ":" ;
   Call Read_rtc_ds1307(0 , Value) : Print Bcd(value) ; "" ;
   Printbin $0d                                             'Send Cariage Return
Loop

Sub Write_rtc_ds1307(byval Adres As Byte , Byval Value As Byte)
    I2cstart                                                'Start Condition
    I2cwbyte Id_write_ds1307                                'Slave Address
    I2cwbyte Adres                                          'Address of DS1307
    I2cwbyte Value                                          'Value to Write
    I2cstop                                                 'Stop Condition
End Sub

Sub Read_rtc_ds1307(byval Adres As Byte , Value As Byte)
   I2cstart                                                 'Start Condition
   I2cwbyte Id_write_ds1307                                 'Slave Address
   I2cwbyte Adres                                           'Address of DS1307
   I2cstart                                                 'Repeated Start
   I2cwbyte Id_read_ds1307                                  'Slave Address (read)
   I2crbyte Value , 9                                       'Read byte
   I2cstop                                                  'Stop Condition
End Sub