'***********************************************************
'* Examples Program For "ET-BASE AVR TINY2313" Board       *
'* Target MCU  : Atmel ATtiny2313                          *
'* Frequency   : X-TAL : 7.3728 MHz                        *
'* Compiler    : BASCOM-AVR 1.11.7.9                       *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)           *
'* Last Update : 10/July/2006                              *
'* Description : Example LED Moving on PORTB[0..7]         *
'***********************************************************
'Used ET-TEST 10P/OUT
'Connect PB0-PB7 to LED

$regfile = "ATtiny2313.dat"                                 'ATtiny2313 MCU
$crystal = 7372800                                          'XTAL = 7.3728 MHz

Config Portb = Output                                       'Config Portb as output

Dim B As Byte                                               'Declare Variable

B = &B11111110
While B > 0
   Do
      Portb = B
      Waitms 200                                            'Delay 200ms
      Rotate B , Left , 1                                   'Rotate Left
   Loop Until B = &B01111111

   Do
      Portb = B
      Waitms 200                                            'Delay 200ms
      Rotate B , Right , 1                                  'Rotate Right
   Loop Until B = &B11111110
Wend
End                                                         'End Program