'***********************************************************
'* Examples Program For "ET-BASE AVR TINY2313" Board       *
'* Target MCU  : Atmel ATtiny2313                          *
'* Frequency   : X-TAL : 7.3728 MHz                        *
'* Compiler    : BASCOM-AVR 1.11.7.9                       *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)           *
'* Last Update : 10/July/2006                              *
'* Description : Example LED Moving on PORTD[2..5]         *
'***********************************************************
'Connect PD2-PD5 to LED

$regfile = "ATtiny2313.dat"                                 'ATtiny2313 MCU
$crystal = 7372800                                          'XTAL = 7.3728 MHz

Config Portd.2 = Output                                     'Config Portd.2-Portd.5 as output
Config Portd.3 = Output
Config Portd.4 = Output
Config Portd.5 = Output

Dim B As Byte                                               'Declare Variable

B = &B11111011
While B > 0
   Do
      Portd = B
      Waitms 200                                            'Delay 200ms
      Rotate B , Left , 1                                   'Rotate Left
   Loop Until B = &B11011111

   Do
      Portd = B
      Waitms 200                                            'Delay 200ms
      Rotate B , Right , 1                                  'Rotate Right
   Loop Until B = &B11111011
Wend
End                                                         'End Program