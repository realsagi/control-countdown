'***********************************************************
'* Examples Program For "ET-BASE AVR TINY2313" Board       *
'* Target MCU  : Atmel ATtiny2313                          *
'* Frequency   : X-TAL : 7.3728 MHz                        *
'* Compiler    : BASCOM-AVR 1.11.7.9                       *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)           *
'* Last Update : 10/July/2006                              *
'* Description : RS232 Demo Send/Receive                   *
'*             : Setup RS232 = 9600,N,8,1                  *
'***********************************************************
'Display Result to Serial Port UART(9600 bps)

$regfile = "ATtiny2313.dat"                                 'ATtiny2313 MCU
$baud = 9600                                                'Serial Port 9600 bps
$crystal = 7372800                                          'XTAL = 7.3728 MHz

Printbin $0c                                                'Clear Display
Print "Hello This is ET-BASE AVR TINY2313...Test RS232"
Print "Please enter any key to echo data"
Print                                                       'New Line

Dim I As Byte
   Do
      I = Waitkey()                                         'Wait for Character
      Print Chr(i) ;                                        'Print Character
   Loop
End