'***********************************************************
'* Examples Program For "ET-BASE AVR TINY2313" Board       *
'* Target MCU  : Atmel ATtiny2313                          *
'* Frequency   : X-TAL : 7.3728 MHz                        *
'* Compiler    : BASCOM-AVR 1.11.7.9                       *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)           *
'* Last Update : 10/July/2006                              *
'* Description : Example Use I2C Connect 24LC32            *
'*             : Setup RS232 = 9600,N,8,1                  *
'***********************************************************
'SCL = PB7
'SDA = PB5
'Device ID = 1010[100]X
'Display Result to Serial Port UART0(9600 bps)

$regfile = "ATtiny2313.dat"                                 'ATtiny2313 MCU
$baud = 9600                                                'Serial Port 9600 bps
$crystal = 7372800                                          'XTAL = 7.3728 MHz

Config Scl = Portb.7
Config Sda = Portb.5

Config I2cdelay = 50

Declare Sub Write_eeprom(byval H As Byte , Byval L As Byte , Byval Value As Byte)
Declare Sub Read_eeprom(byval H As Byte , Byval L As Byte , Value As Byte)

Dim Value As Byte , Dat As Byte , Addressw As Byte , Addressr As Byte
Dim Adrh As Byte , Adrl As Byte

Addressw = &B10101000                                       'ID Write
Addressr = &B10101001                                       'ID Read

Printbin $0c                                                'Clear Display
Print "Hello This is ET-BASE AVR TINY2313"
Print "Test Read/Write EEPROM 2432/64/128/256/512/515"
Print "Press any key to write data or ENTER key to read old data"
Print                                                       'Blank Line

   Dat = Waitkey()
   Print Chr(dat) ;
Do
   If Dat <> &HD Then
   Adrh = 0
   Adrl = 0
      Call Write_eeprom(adrh , Adrl , Dat)                  'Write Data to EEPROM

   Do
      Dat = Waitkey()
      Print Chr(dat) ;
      Adrl = Adrl + 1
      If Adrl = 0 Then
         Adrh = Adrh + 1
      End If

      Call Write_eeprom(adrh , Adrl , Dat)                  'Write Data to EEPROM
  Loop Until Dat = &HD
  Print
  Print
  End If

  Adrh = 0
  Adrl = 0
  Do
   Call Read_eeprom(adrh , Adrl , Value)                    'Read Data from EEPROM
   Print Chr(value) ;
   Adrl = Adrl + 1
      If Adrl = 0 Then
         Adrh = Adrh + 1
      End If
  Loop Until Value = &HD
   Print
   Dat = Waitkey()
   Print Chr(dat) ;
Loop Until Dat = 27
End                                                         'End Program

'sample of writing a byte to EEPROM 24XX

Sub Write_eeprom(byval H As Byte , Byval L As Byte , Byval Value As Byte)
    I2cstart                                                'Start Condition
    I2cwbyte Addressw                                       'Slave address
    I2cwbyte H                                              'Address of EEPROM
    I2cwbyte L
    I2cwbyte Value                                          'Value to write
    I2cstop                                                 'Stop condition
    Waitms 10                                               'Wait for 10 milliseconds
End Sub

Sub Read_eeprom(byval H As Byte , Byval L As Byte , Value As Byte)
   I2cstart                                                 'Start Condition
   I2cwbyte Addressw                                        'Slave Address
   I2cwbyte H                                               'Address of EEPROM
   I2cwbyte L
   I2cstart                                                 'Repeated start
   I2cwbyte Addressr                                        'Slave address (read)
   I2crbyte Value , Nack                                    'Read byte
   I2cstop                                                  'Stop condition
End Sub