'***********************************************************
'* Examples Program For "ET-BASE AVR TINY2313" Board       *
'* Target MCU  : Atmel ATtiny2313                          *
'* Frequency   : X-TAL : 7.3728 MHz                        *
'* Compiler    : BASCOM-AVR 1.11.7.9                       *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)           *
'* Last Update : 10/July/2006                              *
'* Description : Example LED Blink on PD6                  *
'***********************************************************

$regfile = "ATtiny2313.dat"                                 'ATtiny2313 MCU
$crystal = 7372800                                          'XTAL = 7.3728 MHz

Config Portd.6 = Output                                     'Config PD6 as output

   Do
      Portd.6 = Not Portd.6                                 'Convert PD6
      Waitms 200                                            'Wait 200 ms
   Loop
   End