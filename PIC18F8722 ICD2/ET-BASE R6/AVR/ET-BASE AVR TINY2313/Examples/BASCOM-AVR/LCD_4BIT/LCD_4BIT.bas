'***********************************************************
'* Examples Program For "ET-BASE AVR TINY2313" Board       *
'* Target MCU  : Atmel ATtiny2313                          *
'* Frequency   : X-TAL : 7.3728 MHz                        *
'* Compiler    : BASCOM-AVR 1.11.7.9                       *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)           *
'* Last Update : 10/July/2006                              *
'* Description : Example Use Character LCD 16x2            *
'*             : 4-bit Mode                                *
'***********************************************************
'Connect  LCD Module to ET-CLCD PORT
'Lcdpin : D4 = PB0 , D5 = PB1 , D6 = PB2 , D7 = PB3 , EN = PB4 , RS = PB6

$regfile = "ATtiny2313.dat"                                 'ATtiny2313 MCU
$crystal = 7372800                                          'XTAL = 7.3728 MHz

'Configure LCD Screen
Config Lcdbus = 4                                           '4-bit Mode
Config Lcd = 16 * 2                                         '16x2 LCD display
Config Lcdpin = Pin , Db4 = Portb.0 , Db5 = Portb.1 , Db6 = Portb.2 , Db7 = Portb.3 , E = Portb.4 , Rs = Portb.6

   Do
       Cls                                                  'Clear the LCD display
       Lcd "ET-BASE AVR....."                               'Display this at the top line
       Lowerline                                            'Select the lower line
       Lcd "BASE ON TINY2313"                               'Display this at the lower line
       Wait 1                                               'Wait for 1 Second

       Cls
       Lcd "7.4 MIPS Execute"
       Lowerline
       Lcd "BY..ETT CO.,LTD."
       Wait 1
   Loop
End
