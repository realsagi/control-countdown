//***************************************************/;
//* Hardware    : ET-BASE AVR TINY2313              */;
//* CPU         : ATMEL-ATtiny2313                  */;
//* X-TAL       : 7.3728 MHz                        */;
//* Complier    : CodeVisionAVR V1.24.8d            */;
//* Last Update : 10-07-2006 (ETT CO.,LTD)          */;
//*             : WWW.ETT.CO.TH                     */;
//* Description : Example LED Moving on PORTD[2..5] */;
//***************************************************/;
//* CodeVisionAVR Complier Option Setting           */;
//* Chip type           : ATtiny2313V               */;
//* Clock frequency     : 7.3728 MHz                */;
//* Memory model        : Tiny                      */;
//* External SRAM size  : 0                         */;
//* Data Stack size     : 32                        */;
//***************************************************/;
//Connect PD2-PD5 to LED
// CodeVisionAVR C Compiler
// (C) 1998-2003 Pavel Haiduc, HP InfoTech S.R.L.
// I/O registers definitions for the ATtiny2313
#pragma used+
sfrb DIDR=1;
sfrb UBRRH=2;
sfrb UCSRC=3;
sfrb ACSR=8;
sfrb UBRRL=9;
sfrb UCSRB=0xa;
sfrb UCSRA=0xb;
sfrb UDR=0xc;
sfrb USICR=0xd;
sfrb USISR=0xe;
sfrb USIDR=0xf;
sfrb PIND=0x10;
sfrb DDRD=0x11;
sfrb PORTD=0x12;
sfrb GPIOR0=0x13;
sfrb GPIOR1=0x14;
sfrb GPIOR2=0x15;
sfrb PINB=0x16;
sfrb DDRB=0x17;
sfrb PORTB=0x18;
sfrb PINA=0x19;
sfrb DDRA=0x1a;
sfrb PORTA=0x1b;
sfrb EECR=0x1c;
sfrb EEDR=0x1d;
sfrb EEAR=0x1e;
sfrb PCMSK=0x20;
sfrb WDTCR=0x21;
sfrb TCCR1C=0x22;
sfrb GTCCR=0x23;
sfrb ICR1L=0x24;
sfrb ICR1H=0x25;
sfrw ICR1=0x24;   // 16 bit access
sfrb CLKPR=0x26;
sfrb OCR1BL=0x28;
sfrb OCR1BH=0x29;
sfrw OCR1B=0x28;   // 16 bit access
sfrb OCR1AL=0x2a;
sfrb OCR1AH=0x2b;
sfrw OCR1A=0x2a;   // 16 bit access
sfrb TCNT1L=0x2c;
sfrb TCNT1H=0x2d;
sfrw TCNT1=0x2c;  // 16 bit access
sfrb TCCR1B=0x2e;
sfrb TCCR1A=0x2f;
sfrb TCCR0A=0x30;
sfrb OSCCAL=0x31;
sfrb TCNT0=0x32;
sfrb TCCR0B=0x33;
sfrb MCUSR=0x34;
sfrb MCUCR=0x35;
sfrb OCR0A=0x36;
sfrb SPMCSR=0x37;
sfrb TIFR=0x38;
sfrb TIMSK=0x39;
sfrb EIFR=0x3a;
sfrb GIMSK=0x3b;
sfrb OCR0B=0x3c;
sfrb SPL=0x3d;
sfrb SREG=0x3f;
#pragma used-
// Interrupt vectors definitions
// for compatibility with the interrupt vector names from Atmel's datasheet
// CodeVisionAVR C Compiler
// (C) 1998-2000 Pavel Haiduc, HP InfoTech S.R.L.
#pragma used+
void delay_us(unsigned int n);
void delay_ms(unsigned int n);
#pragma used-
void main(void)
{
  unsigned char LED; 	   		            // LED Output Status Buffer
    DDRD = 0x3C; 		   		            // PD2-PD5 = Output
   // Loop Test Output Port 
  while(1)													// Loop Continue
  {    	
    // Shift Left
    for (LED = 0x04; LED < 0x20; LED <<= 1)	    // Shift Left (Right <- Left)
    { 
      				       PORTD = ~LED;                                 // Active Output (Toggle ON LED)  
      delay_ms(200);				    // Display Delay	    
    }  	 
        // Shift Right
    for (LED = 0x20; LED > 0x04; LED >>= 1)	    // Shift Right (Right -> Left)
    { 
      PORTD = ~LED;                                 // Active Output (Toggle ON LED)  
      delay_ms(200);				    // Display Delay
    }  	  
	      } 
   }	
