//***************************************************/;
//* Hardware    : ET-BASE AVR TINY2313              */;
//* CPU         : ATMEL-ATtiny2313                  */;
//* X-TAL       : 7.3728 MHz                        */;
//* Complier    : CodeVisionAVR V1.24.8d            */;
//* Last Update : 10-07-2006 (ETT CO.,LTD)          */;
//*             : WWW.ETT.CO.TH                     */;
//* Description : Example LED Moving on PORTD[2..5] */;
//***************************************************/;
//* CodeVisionAVR Complier Option Setting           */;
//* Chip type           : ATtiny2313V               */;
//* Clock frequency     : 7.3728 MHz                */;
//* Memory model        : Tiny                      */;
//* External SRAM size  : 0                         */;
//* Data Stack size     : 32                        */;
//***************************************************/;
//Connect PD2-PD5 to LED

#include <TINY2313.h>                               // ATtiny2313 MCU
#include <delay.h>                                  // Delay functions

void main(void)
{
  unsigned char LED; 	   		            // LED Output Status Buffer
  
  DDRD = 0x3C; 		   		            // PD2-PD5 = Output
 
  // Loop Test Output Port 
  while(1)													// Loop Continue
  {    	
    // Shift Left
    for (LED = 0x04; LED < 0x20; LED <<= 1)	    // Shift Left (Right <- Left)
    { 
      				 
      PORTD = ~LED;                                 // Active Output (Toggle ON LED)  
      delay_ms(200);				    // Display Delay	    
    }  	 
    
    // Shift Right
    for (LED = 0x20; LED > 0x04; LED >>= 1)	    // Shift Right (Right -> Left)
    { 
      PORTD = ~LED;                                 // Active Output (Toggle ON LED)  
      delay_ms(200);				    // Display Delay
    }  	  
	    
  } 
   
}	

