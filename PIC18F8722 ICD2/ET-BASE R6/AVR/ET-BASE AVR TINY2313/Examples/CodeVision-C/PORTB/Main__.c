//***************************************************/;
//* Hardware    : ET-BASE AVR TINY2313              */;
//* CPU         : ATMEL-ATtiny2313                  */;
//* X-TAL       : 7.3728 MHz                        */;
//* Complier    : CodeVisionAVR V1.24.8d            */;
//* Last Update : 10-07-2006 (ETT CO.,LTD)          */;
//*             : WWW.ETT.CO.TH                     */;
//* Description : Example LED Moving on PORTB[0..7] */;
//***************************************************/;
//* CodeVisionAVR Complier Option Setting           */;
//* Chip type           : ATtiny2313V               */;
//* Clock frequency     : 7.3728 MHz                */;
//* Memory model        : Tiny                      */;
//* External SRAM size  : 0                         */;
//* Data Stack size     : 32                        */;
//***************************************************/;
//Used ET-TEST 10P/OUT
//Connect PB0-PB7 to LED

#include <TINY2313.h>                               // ATtiny2313 MCU
#include <delay.h>                                  // Delay functions

void main(void)
{
  unsigned char LED; 	   		            // LED Output Status Buffer
  
  DDRB = 0xFF; 		   		            // PortB = Output
 
  // Loop Test Output Port 
  while(1)													// Loop Continue
  {    	
    // Shift Left
    for (LED = 0x01; LED < 0x80; LED <<= 1)	    // Shift Left (Right <- Left)
    { 
      				 
      PORTB = ~LED;                                 // Active Output (Toggle ON LED)  
      delay_ms(200);				    // Display Delay	    
    }  	 
    
    // Shift Right
    for (LED = 0x80; LED > 0x01; LED >>= 1)	    // Shift Right (Right -> Left)
    { 
      PORTB = ~LED;                                 // Active Output (Toggle ON LED)  
      delay_ms(200);				    // Display Delay
    }  	  
	    
  } 
   
}	

