//***************************************************/;
//* Hardware    : ET-BASE AVR TINY2313              */;
//* CPU         : ATMEL-ATtiny2313                  */;
//* X-TAL       : 7.3728 MHz                        */;
//* Complier    : CodeVisionAVR V1.24.8d            */;
//* Last Update : 10-07-2006 (ETT CO.,LTD)           */;
//*             : WWW.ETT.CO.TH                     */;
//* Description : Example LED Blink on PD6          */;
//***************************************************/;
//* CodeVisionAVR Complier Option Setting           */;
//* Chip type           : ATtiny2313V               */;
//* Clock frequency     : 7.3728 MHz                */;
//* Memory model        : Tiny                      */;
//* External SRAM size  : 0                         */;
//* Data Stack size     : 32                        */;
//***************************************************/;

#include <TINY2313.h>                            // ATtiny2313 MCU
#include <delay.h>                               // Delay functions 

void main(void)
{  

  DDRD=0x40;                                     // PD6 = Output

  // Loop Blink LED on PD6 
  while (1)
  {
    PORTD |= 0x40;			         // PD6 = 1 (OFF LED)
    delay_ms(200);	   		         // Display LED Delay
    PORTD &= 0xAF;                               // PD6 = 0 (ON LED)	
    delay_ms(200);                               // Display LED Delay                                             
  }
}


