//***************************************************/;
//* Hardware    : ET-BASE AVR TINY2313              */;
//* CPU         : ATMEL-ATtiny2313                  */;
//* X-TAL       : 7.3728 MHz                        */;
//* Complier    : CodeVisionAVR V1.24.8d            */;
//* Last Update : 10-07-2006 (ETT CO.,LTD)          */;
//*             : WWW.ETT.CO.TH                     */;
//* Description : RS232 Demo Send/Receive           */;
//*             : Setup RS232 = 9600,N,8,1          */;
//***************************************************/;
//* CodeVisionAVR Complier Option Setting           */;
//* Chip type           : ATtiny2313V               */;
//* Clock frequency     : 7.3728 MHz                */;
//* Memory model        : Tiny                      */;
//* External SRAM size  : 0                         */;
//* Data Stack size     : 32                        */;
//***************************************************/;
//Display Result to Serial Port UART(9600 bps)

#include <TINY2313.h>                                               // ATtiny2313 MCU
#include <stdio.h>                                                  // Standard Input/Output functions

void main(void)
{
  char uart_data;                                                   // RS232 Buffer
  
  // USART initialization
  // Communication Parameters: 8 Data, 1 Stop, No Parity
  // USART Receiver: On
  // USART Transmitter: On
  // USART Mode: Asynchronous
  // USART Baud rate: 9600
  UCSRA=0x00;
  UCSRB=0x18;
  UCSRC=0x06;
  UBRRH=0x00;
  UBRRL=0x2F;

  printf("\fHello This is ET-BASE AVR TINY2313...Test RS232\n\r");   // Call printf Function
  printf("Please enter any key to echo data\n\n\r");
  while (1)                                                          // Loop Continue
  {
    uart_data = getchar();                                           // Wait RS232 Receive
    putchar(uart_data);                                              // Echo data
  }
}
