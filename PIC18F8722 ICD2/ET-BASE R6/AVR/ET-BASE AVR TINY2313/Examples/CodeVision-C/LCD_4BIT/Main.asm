
;CodeVisionAVR C Compiler V1.24.8d Professional
;(C) Copyright 1998-2006 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type              : ATtiny2313V
;Clock frequency        : 7.372800 MHz
;Memory model           : Tiny
;Optimize for           : Size
;(s)printf features     : int, width
;(s)scanf features      : int, width
;External SRAM size     : 0
;Data Stack size        : 32 byte(s)
;Heap size              : 0 byte(s)
;Promote char to int    : No
;char is unsigned       : Yes
;8 bit enums            : Yes
;Word align FLASH struct: No
;Enhanced core instructions    : On
;Automatic register allocation : On

	#pragma AVRPART ADMIN PART_NAME ATtiny2313V
	#pragma AVRPART MEMORY PROG_FLASH 2048
	#pragma AVRPART MEMORY EEPROM 128
	#pragma AVRPART MEMORY INT_SRAM SIZE 128
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x60

	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU WDTCR=0x21
	.EQU MCUSR=0x34
	.EQU MCUCR=0x35
	.EQU SPL=0x3D
	.EQU SREG=0x3F
	.EQU GPIOR0=0x13
	.EQU GPIOR1=0x14
	.EQU GPIOR2=0x15

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __se_bit=0x20
	.EQU __sm_mask=0x50
	.EQU __sm_powerdown=0x10
	.EQU __sm_standby=0x40

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM
	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM
	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM
	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM
	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM
	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM
	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+@1
	ANDI R30,LOW(@2)
	STS  @0+@1,R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+@1
	ANDI R30,LOW(@2)
	STS  @0+@1,R30
	LDS  R30,@0+@1+1
	ANDI R30,HIGH(@2)
	STS  @0+@1+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+@1
	ORI  R30,LOW(@2)
	STS  @0+@1,R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+@1
	ORI  R30,LOW(@2)
	STS  @0+@1,R30
	LDS  R30,@0+@1+1
	ORI  R30,HIGH(@2)
	STS  @0+@1+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __CLRD1S
	LDI  R30,0
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+@1)
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+@1)
	LDI  R31,HIGH(@0+@1)
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+@1)
	LDI  R31,HIGH(2*@0+@1)
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+@1)
	LDI  R31,HIGH(2*@0+@1)
	LDI  R22,BYTE3(2*@0+@1)
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+@1)
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+@1)
	LDI  R27,HIGH(@0+@1)
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+@2)
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+@3)
	LDI  R@1,HIGH(@2+@3)
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+@3)
	LDI  R@1,HIGH(@2*2+@3)
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+@1
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+@1
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+@1
	LDS  R31,@0+@1+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+@1
	LDS  R31,@0+@1+1
	LDS  R22,@0+@1+2
	LDS  R23,@0+@1+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+@2
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+@3
	LDS  R@1,@2+@3+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+@1
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+@1
	LDS  R27,@0+@1+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+@1
	LDS  R27,@0+@1+1
	LDS  R24,@0+@1+2
	LDS  R25,@0+@1+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+@1,R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+@1,R30
	STS  @0+@1+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+@1,R30
	STS  @0+@1+1,R31
	STS  @0+@1+2,R22
	STS  @0+@1+3,R23
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+@1,R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+@1,R@2
	STS  @0+@1+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+@1
	LDS  R31,@0+@1+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+@1)
	LDI  R31,HIGH(2*@0+@1)
	RCALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+@1)
	LDI  R27,HIGH(@0+@1)
	RCALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	SUBI R26,-@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	SUBI R26,-@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	SUBI R26,-@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	SUBI R26,-@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	SUBI R26,-@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	SUBI R26,-@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOV  R26,R@0
	SUBI R26,-@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOV  R26,R@0
	SUBI R26,-@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOV  R26,R@0
	SUBI R26,-@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOV  R26,R@0
	SUBI R26,-@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOV  R26,R@0
	SUBI R26,-@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOV  R26,R@0
	SUBI R26,-@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	SUBI R26,-@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	SUBI R26,-@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	SUBI R26,-@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	SUBI R26,-@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	SUBI R26,-@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	SUBI R26,-@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	CLR  R0
	ST   Z+,R0
	ST   Z,R0
	.ENDM

	.MACRO __CLRD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	CLR  R0
	ST   Z+,R0
	ST   Z+,R0
	ST   Z+,R0
	ST   Z,R0
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R@1
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.CSEG
	.ORG 0

	.INCLUDE "Main.vec"
	.INCLUDE "Main.inc"

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30
	OUT  MCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	IN   R26,MCUSR
	CBR  R26,8
	OUT  MCUSR,R26
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;CLEAR R2-R14
	LDI  R24,13
	LDI  R26,2
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(0x80)
	LDI  R26,0x60
__CLEAR_SRAM:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;GPIOR0-GPIOR2 INITIALIZATION
	LDI  R30,__GPIOR0_INIT
	OUT  GPIOR0,R30
	LDI  R30,__GPIOR1_INIT
	OUT  GPIOR1,R30
	LDI  R30,__GPIOR2_INIT
	OUT  GPIOR2,R30

;STACK POINTER INITIALIZATION
	LDI  R30,LOW(0xDF)
	OUT  SPL,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(0x80)
	LDI  R29,HIGH(0x80)

	RJMP _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x80
;       1 //***************************************************/;
;       2 //* Hardware    : ET-BASE AVR TINY2313              */;
;       3 //* CPU         : ATMEL-ATtiny2313                  */;
;       4 //* X-TAL       : 7.3728 MHz                        */;
;       5 //* Complier    : CodeVisionAVR V1.24.8d            */;
;       6 //* Last Update : 10-07-2006 (ETT CO.,LTD)          */;
;       7 //*             : WWW.ETT.CO.TH                     */;
;       8 //* Description : Example LCD CHAR 16X2             */;
;       9 //***************************************************/;
;      10 //* CodeVisionAVR Complier Option Setting           */;
;      11 //* Chip type           : ATtiny2313V               */;
;      12 //* Clock frequency     : 7.3728 MHz                */;
;      13 //* Memory model        : Tiny                      */;
;      14 //* External SRAM size  : 0                         */;
;      15 //* Data Stack size     : 32                        */;
;      16 //***************************************************/;
;      17 //*Connect LCD Module to ET-CLCD PORT  
;      18 //*Lcdpin : D4 = PB0 , D5 = PB1 , D6 = PB2 , D7 = PB3 , EN = PB4 , RS = PB6
;      19 
;      20 #include <TINY2313.h>                   // ATtiny2313 MCU
;      21 #include <delay.h>                      // Delay functions
;      22 #include <stdio.h>                      // Standard Input/Output functions
;      23 
;      24 char lcdbuf[16+1];			// LCD Display Buffer
_lcdbuf:
	.BYTE 0x11
;      25 
;      26 /* pototype  section */
;      27 void init_lcd(void);			// Initial Character LCD(4-Bit Interface)
;      28 void gotolcd(unsigned char);		// Set Cursor LCD
;      29 void write_ins(unsigned char);      	// Write Instruction LCD
;      30 void write_data(unsigned char);		// Write Data LCD
;      31 void printlcd(void);			// Display Message LCD
;      32 
;      33 
;      34 /*--------------------------------------------
;      35 The main C function.  Program execution Here 
;      36 ---------------------------------------------*/
;      37 void main(void)
;      38 {

	.CSEG
_main:
;      39   DDRB=0xFF;                                                    // PORTB as output
	LDI  R30,LOW(255)
	OUT  0x17,R30
;      40   
;      41   delay_ms(30);							// Power-on Delay
	LDI  R30,LOW(30)
	LDI  R31,HIGH(30)
	RCALL SUBOPT_0x0
	RCALL _delay_ms
;      42   init_lcd();							// Initial LCD
	RCALL _init_lcd
;      43    
;      44   while(1)
_0x3:
;      45   {
;      46     gotolcd(0);							// Set Cursor Line-1
	RCALL SUBOPT_0x1
;      47     sprintf(lcdbuf,"ET-BASE AVR....."); // Display Line-1
	__POINTW1FN _0,0
	RCALL SUBOPT_0x2
;      48     printlcd();
;      49     gotolcd(0x40);						// Set Cursor Line-2
	RCALL SUBOPT_0x3
;      50     sprintf(lcdbuf,"BASE ON TINY2313"); // Display Line-2
	__POINTW1FN _0,17
	RCALL SUBOPT_0x2
;      51     printlcd();	
;      52     delay_ms(1000);						// Display Delay
	RCALL SUBOPT_0x4
;      53 
;      54     gotolcd(0);							// Set Cursor Line-1
	RCALL SUBOPT_0x1
;      55     sprintf(lcdbuf,"7.4 MIPS Execute"); // Display Line-1  
	__POINTW1FN _0,34
	RCALL SUBOPT_0x2
;      56     printlcd();
;      57     gotolcd(0x40);						// Set Cursor Line-2
	RCALL SUBOPT_0x3
;      58     sprintf(lcdbuf,"BY..ETT CO.,LTD."); // Display Line-2
	__POINTW1FN _0,51
	RCALL SUBOPT_0x2
;      59     printlcd();	
;      60     delay_ms(1000);						// Display Delay
	RCALL SUBOPT_0x4
;      61   }  
	RJMP _0x3
;      62 }
_0x6:
	RJMP _0x6
;      63 
;      64 /*******************************/
;      65 /* Initial LCD 4-Bit Interface */
;      66 /*******************************/
;      67 void init_lcd(void)
;      68 {
_init_lcd:
;      69   PORTB &= 0b11101111;			// Start LCD Control   EN=0  (PB4)
	RCALL SUBOPT_0x5
;      70   delay_ms(1);				// Wait LCD Ready
;      71   
;      72   write_ins(0x33);			// Initial (Set DL=1 3 Time, Reset DL=0 1 Time)
	LDI  R30,LOW(51)
	RCALL SUBOPT_0x6
;      73   write_ins(0x32);  
	LDI  R30,LOW(50)
	RCALL SUBOPT_0x6
;      74   write_ins(0x28);  			// Function Set (DL=0 4-Bit,N=1 2 Line,F=0 5X7)
	LDI  R30,LOW(40)
	RCALL SUBOPT_0x6
;      75   write_ins(0x0C);  			// Display on/off Control (Entry Display,Cursor off,Cursor not Blink)
	LDI  R30,LOW(12)
	RCALL SUBOPT_0x6
;      76   write_ins(0x06);  			// Entry Mode Set (I/D=1 Increment,S=0 Cursor Shift)
	LDI  R30,LOW(6)
	RCALL SUBOPT_0x6
;      77   write_ins(0x01);  			// Clear Display  (Clear Display,Set DD RAM Address=0)
	LDI  R30,LOW(1)
	RCALL SUBOPT_0x6
;      78   delay_ms(1);			        // Wait Initial Complete
	RCALL SUBOPT_0x7
;      79   return;
	RET
;      80 }
;      81 
;      82 /******************/
;      83 /* Set LCD Cursor */
;      84 /******************/
;      85 void gotolcd(unsigned char i)
;      86 {
_gotolcd:
;      87   i |= 0x80;				// Set DD-RAM Address Command
;	i -> Y+0
	LD   R30,Y
	ORI  R30,0x80
	ST   Y,R30
;      88   write_ins(i);  
	RCALL SUBOPT_0x6
;      89   return;
	RJMP _0x68
;      90 }
;      91 
;      92 /****************************/
;      93 /* Write Instruction to LCD */
;      94 /****************************/
;      95 void write_ins(unsigned char i)
;      96 {
_write_ins:
;      97   PORTB &= 0b10111111;			// Instruction Select RS=0(PB6)
;	i -> Y+0
	CBI  0x18,6
;      98 
;      99   PORTB &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
	RCALL SUBOPT_0x8
;     100   PORTB |= (i>>4) & 0x0F;        	// Strobe High Nibble Command
	RCALL SUBOPT_0x9
;     101   PORTB |= 0b00010000;    		// Enable ON    EN=1(PB4)
;     102   delay_ms(1);
;     103   PORTB &= 0b11101111;   		// Enable OFF   EN=0(PB4)
	CBI  0x18,4
;     104   
;     105   PORTB &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
	RCALL SUBOPT_0x8
;     106   PORTB |=  i & 0x0F;    		// Strobe Low Nibble Command
	RCALL SUBOPT_0xA
;     107   PORTB |= 0b00010000;    		// Enable ON    EN=1(PB4)
;     108   delay_ms(1);
;     109   PORTB &= 0b11101111;   		// Enable OFF   EN=0(PB4)
	RCALL SUBOPT_0x5
;     110   delay_ms(1);				// Wait LCD Busy
;     111 
;     112   return;
	RJMP _0x68
;     113 }
;     114 
;     115 
;     116 /****************************/
;     117 /* Write Data(ASCII) to LCD */
;     118 /****************************/
;     119 void write_data(unsigned char i)
;     120 {
_write_data:
;     121   PORTB |= 0b01000000;			// Instruction Select  RS=1(PB6)
;	i -> Y+0
	SBI  0x18,6
;     122 
;     123   PORTB &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
	RCALL SUBOPT_0x8
;     124   PORTB |= (i>>4) & 0x0F;         	// Strobe High Nibble Command
	RCALL SUBOPT_0x9
;     125   PORTB |= 0b00010000;    		// Enable ON    EN=1(PB4)
;     126   delay_ms(1);
;     127   PORTB &= 0b11101111;   		// Enable OFF   EN=0(PB4)
	CBI  0x18,4
;     128   
;     129   PORTB &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
	RCALL SUBOPT_0x8
;     130   PORTB |= i & 0x0F;   			// Strobe Low Nibble Command
	RCALL SUBOPT_0xA
;     131   PORTB |= 0b00010000;    		// Enable ON    EN=1(PB4)
;     132   delay_ms(1);
;     133   PORTB &= 0b11101111;   		// Enable OFF   EN=0(PB4) 
	RCALL SUBOPT_0x5
;     134   delay_ms(1);				// Wait LCD Busy
;     135 
;     136   return;						
_0x68:
	ADIW R28,1
	RET
;     137 
;     138 }
;     139 
;     140 /****************************/
;     141 /* Print Data(ASCII) to LCD */
;     142 /****************************/
;     143 void printlcd(void)
;     144 {
_printlcd:
;     145   char *p;
;     146  
;     147   p = lcdbuf;
	ST   -Y,R16
;	*p -> R16
	__POINTBRM 16,_lcdbuf
;     148  
;     149   do 					// Get ASCII & Write to LCD Until null
_0x8:
;     150   {
;     151     write_data(*p); 			// Write ASCII to LCD
	MOV  R26,R16
	LD   R30,X
	ST   -Y,R30
	RCALL _write_data
;     152     p++;				// Next ASCII
	SUBI R16,-1
;     153   }
;     154   while(*p != '\0');		        // End of ASCII (null)
	MOV  R26,R16
	LD   R30,X
	CPI  R30,0
	BRNE _0x8
;     155  
;     156   return;
	LD   R16,Y+
	RET
;     157 }
;     158 
;     159 
;     160 

_getchar:
     sbis usr,rxc
     rjmp _getchar
     in   r30,udr
	RET
_putchar:
     sbis usr,udre
     rjmp _putchar
     ld   r30,y
     out  udr,r30
	ADIW R28,1
	RET
__put_G2:
	LD   R26,Y
	LD   R30,X
	CPI  R30,0
	BREQ _0xA
	SUBI R30,-LOW(1)
	ST   X,R30
	SUBI R30,LOW(1)
	LDD  R26,Y+1
	STD  Z+0,R26
	RJMP _0xB
_0xA:
	LDD  R30,Y+1
	ST   -Y,R30
	RCALL _putchar
_0xB:
	ADIW R28,2
	RET
__print_G2:
	SBIW R28,6
	RCALL __SAVELOCR6
	LDI  R16,0
_0xC:
	LDD  R30,Y+14
	LDD  R31,Y+14+1
	ADIW R30,1
	STD  Y+14,R30
	STD  Y+14+1,R31
	SBIW R30,1
	LPM  R30,Z
	MOV  R19,R30
	CPI  R30,0
	BRNE PC+2
	RJMP _0xE
	MOV  R30,R16
	CPI  R30,0
	BRNE _0x12
	CPI  R19,37
	BRNE _0x13
	LDI  R16,LOW(1)
	RJMP _0x14
_0x13:
	RCALL SUBOPT_0xB
_0x14:
	RJMP _0x11
_0x12:
	CPI  R30,LOW(0x1)
	BRNE _0x15
	CPI  R19,37
	BRNE _0x16
	RCALL SUBOPT_0xB
	RJMP _0x69
_0x16:
	LDI  R16,LOW(2)
	LDI  R21,LOW(0)
	LDI  R17,LOW(0)
	CPI  R19,45
	BRNE _0x17
	LDI  R17,LOW(1)
	RJMP _0x11
_0x17:
	CPI  R19,43
	BRNE _0x18
	LDI  R21,LOW(43)
	RJMP _0x11
_0x18:
	CPI  R19,32
	BRNE _0x19
	LDI  R21,LOW(32)
	RJMP _0x11
_0x19:
	RJMP _0x1A
_0x15:
	CPI  R30,LOW(0x2)
	BRNE _0x1B
_0x1A:
	LDI  R20,LOW(0)
	LDI  R16,LOW(3)
	CPI  R19,48
	BRNE _0x1C
	ORI  R17,LOW(128)
	RJMP _0x11
_0x1C:
	RJMP _0x1D
_0x1B:
	CPI  R30,LOW(0x3)
	BREQ PC+2
	RJMP _0x11
_0x1D:
	CPI  R19,48
	BRLO _0x20
	CPI  R19,58
	BRLO _0x21
_0x20:
	RJMP _0x1F
_0x21:
	MOV  R26,R20
	LDI  R30,LOW(10)
	RCALL __MULB12U
	MOV  R20,R30
	MOV  R30,R19
	SUBI R30,LOW(48)
	ADD  R20,R30
	RJMP _0x11
_0x1F:
	MOV  R30,R19
	CPI  R30,LOW(0x63)
	BRNE _0x25
	RCALL SUBOPT_0xC
	RCALL SUBOPT_0xD
	RJMP _0x26
_0x25:
	CPI  R30,LOW(0x73)
	BRNE _0x28
	RCALL SUBOPT_0xC
	STD  Y+6,R30
	ST   -Y,R30
	RCALL _strlen
	MOV  R16,R30
	RJMP _0x29
_0x28:
	CPI  R30,LOW(0x70)
	BRNE _0x2B
	RCALL SUBOPT_0xE
	RCALL SUBOPT_0xF
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	RCALL SUBOPT_0x0
	RCALL _strlenf
	MOV  R16,R30
	ORI  R17,LOW(8)
_0x29:
	ORI  R17,LOW(2)
	ANDI R17,LOW(127)
	LDI  R18,LOW(0)
	RJMP _0x2C
_0x2B:
	CPI  R30,LOW(0x64)
	BREQ _0x2F
	CPI  R30,LOW(0x69)
	BRNE _0x30
_0x2F:
	ORI  R17,LOW(4)
	RJMP _0x31
_0x30:
	CPI  R30,LOW(0x75)
	BRNE _0x32
_0x31:
	LDI  R30,LOW(_tbl10_G2*2)
	LDI  R31,HIGH(_tbl10_G2*2)
	RCALL SUBOPT_0xF
	LDI  R16,LOW(5)
	RJMP _0x33
_0x32:
	CPI  R30,LOW(0x58)
	BRNE _0x35
	ORI  R17,LOW(8)
	RJMP _0x36
_0x35:
	CPI  R30,LOW(0x78)
	BREQ PC+2
	RJMP _0x67
_0x36:
	LDI  R30,LOW(_tbl16_G2*2)
	LDI  R31,HIGH(_tbl16_G2*2)
	RCALL SUBOPT_0xF
	LDI  R16,LOW(4)
_0x33:
	SBRS R17,2
	RJMP _0x38
	RCALL SUBOPT_0xE
	RCALL SUBOPT_0x10
	LDD  R26,Y+10
	LDD  R27,Y+10+1
	SBIW R26,0
	BRGE _0x39
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	RCALL __ANEGW1
	RCALL SUBOPT_0x10
	LDI  R21,LOW(45)
_0x39:
	CPI  R21,0
	BREQ _0x3A
	SUBI R16,-LOW(1)
	RJMP _0x3B
_0x3A:
	ANDI R17,LOW(251)
_0x3B:
	RJMP _0x3C
_0x38:
	RCALL SUBOPT_0xE
	RCALL SUBOPT_0x10
_0x3C:
_0x2C:
	SBRC R17,0
	RJMP _0x3D
_0x3E:
	CP   R16,R20
	BRSH _0x40
	SBRS R17,7
	RJMP _0x41
	SBRS R17,2
	RJMP _0x42
	ANDI R17,LOW(251)
	MOV  R19,R21
	SUBI R16,LOW(1)
	RJMP _0x43
_0x42:
	LDI  R19,LOW(48)
_0x43:
	RJMP _0x44
_0x41:
	LDI  R19,LOW(32)
_0x44:
	RCALL SUBOPT_0xB
	SUBI R20,LOW(1)
	RJMP _0x3E
_0x40:
_0x3D:
	MOV  R18,R16
	SBRS R17,1
	RJMP _0x45
_0x46:
	CPI  R18,0
	BREQ _0x48
	SBRS R17,3
	RJMP _0x49
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	ADIW R30,1
	RCALL SUBOPT_0xF
	SBIW R30,1
	LPM  R30,Z
	RJMP _0x6A
_0x49:
	LDD  R26,Y+6
	LD   R30,X+
	STD  Y+6,R26
_0x6A:
	ST   -Y,R30
	LDD  R30,Y+13
	ST   -Y,R30
	RCALL __put_G2
	CPI  R20,0
	BREQ _0x4B
	SUBI R20,LOW(1)
_0x4B:
	SUBI R18,LOW(1)
	RJMP _0x46
_0x48:
	RJMP _0x4C
_0x45:
_0x4E:
	LDI  R19,LOW(48)
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	ADIW R30,2
	RCALL SUBOPT_0xF
	SBIW R30,2
	RCALL __GETW1PF
	STD  Y+8,R30
	STD  Y+8+1,R31
_0x50:
	LDD  R30,Y+8
	LDD  R31,Y+8+1
	LDD  R26,Y+10
	LDD  R27,Y+10+1
	CP   R26,R30
	CPC  R27,R31
	BRLO _0x52
	SUBI R19,-LOW(1)
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	LDD  R30,Y+10
	LDD  R31,Y+10+1
	SUB  R30,R26
	SBC  R31,R27
	RCALL SUBOPT_0x10
	RJMP _0x50
_0x52:
	CPI  R19,58
	BRLO _0x53
	SBRS R17,3
	RJMP _0x54
	SUBI R19,-LOW(7)
	RJMP _0x55
_0x54:
	SUBI R19,-LOW(39)
_0x55:
_0x53:
	SBRC R17,4
	RJMP _0x57
	CPI  R19,49
	BRSH _0x59
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,1
	BRNE _0x58
_0x59:
	RJMP _0x6B
_0x58:
	CP   R20,R18
	BRLO _0x5D
	SBRS R17,0
	RJMP _0x5E
_0x5D:
	RJMP _0x5C
_0x5E:
	LDI  R19,LOW(32)
	SBRS R17,7
	RJMP _0x5F
	LDI  R19,LOW(48)
_0x6B:
	ORI  R17,LOW(16)
	SBRS R17,2
	RJMP _0x60
	ANDI R17,LOW(251)
	ST   -Y,R21
	LDD  R30,Y+13
	ST   -Y,R30
	RCALL __put_G2
	CPI  R20,0
	BREQ _0x61
	SUBI R20,LOW(1)
_0x61:
_0x60:
_0x5F:
_0x57:
	RCALL SUBOPT_0xB
	CPI  R20,0
	BREQ _0x62
	SUBI R20,LOW(1)
_0x62:
_0x5C:
	SUBI R18,LOW(1)
	LDD  R26,Y+8
	LDD  R27,Y+8+1
	SBIW R26,2
	BRLO _0x4F
	RJMP _0x4E
_0x4F:
_0x4C:
	SBRS R17,0
	RJMP _0x63
_0x64:
	CPI  R20,0
	BREQ _0x66
	SUBI R20,LOW(1)
	LDI  R30,LOW(32)
	RCALL SUBOPT_0xD
	RJMP _0x64
_0x66:
_0x63:
_0x67:
_0x26:
_0x69:
	LDI  R16,LOW(0)
_0x11:
	RJMP _0xC
_0xE:
	RCALL __LOADLOCR6
	ADIW R28,16
	RET
_sprintf:
	PUSH R15
	MOV  R15,R24
	SBIW R28,1
	ST   -Y,R16
	MOV  R26,R28
	SUBI R26,-(2)
	ADD  R26,R15
	MOV  R16,R26
	MOV  R26,R28
	SUBI R26,-(4)
	ADD  R26,R15
	LD   R30,X
	STD  Y+1,R30
	MOV  R26,R28
	SUBI R26,-(2)
	ADD  R26,R15
	RCALL __GETW1P
	RCALL SUBOPT_0x0
	ST   -Y,R16
	MOV  R30,R28
	SUBI R30,-(4)
	ST   -Y,R30
	RCALL __print_G2
	LDD  R26,Y+1
	LDI  R30,LOW(0)
	ST   X,R30
	LDD  R16,Y+0
	ADIW R28,2
	POP  R15
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 17 TIMES, CODE SIZE REDUCTION:14 WORDS
SUBOPT_0x0:
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x1:
	LDI  R30,LOW(0)
	ST   -Y,R30
	RCALL _gotolcd
	LDI  R30,LOW(_lcdbuf)
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x2:
	RCALL SUBOPT_0x0
	LDI  R24,0
	RCALL _sprintf
	ADIW R28,3
	RJMP _printlcd

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x3:
	LDI  R30,LOW(64)
	ST   -Y,R30
	RCALL _gotolcd
	LDI  R30,LOW(_lcdbuf)
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x4:
	LDI  R30,LOW(1000)
	LDI  R31,HIGH(1000)
	RCALL SUBOPT_0x0
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x5:
	CBI  0x18,4
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	RCALL SUBOPT_0x0
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x6:
	ST   -Y,R30
	RJMP _write_ins

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x7:
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	RCALL SUBOPT_0x0
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x8:
	IN   R30,0x18
	ANDI R30,LOW(0xF0)
	OUT  0x18,R30
	IN   R30,0x18
	MOV  R26,R30
	LD   R30,Y
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x9:
	SWAP R30
	ANDI R30,LOW(0xF)
	OR   R30,R26
	OUT  0x18,R30
	SBI  0x18,4
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0xA:
	ANDI R30,LOW(0xF)
	OR   R30,R26
	OUT  0x18,R30
	SBI  0x18,4
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0xB:
	ST   -Y,R19
	LDD  R30,Y+13
	ST   -Y,R30
	RJMP __put_G2

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0xC:
	LDD  R26,Y+13
	SUBI R26,LOW(4)
	STD  Y+13,R26
	SUBI R26,-LOW(4)
	LD   R30,X
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xD:
	ST   -Y,R30
	LDD  R30,Y+13
	ST   -Y,R30
	RJMP __put_G2

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0xE:
	LDD  R26,Y+13
	SUBI R26,LOW(4)
	STD  Y+13,R26
	SUBI R26,-LOW(4)
	RCALL __GETW1P
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0xF:
	STD  Y+6,R30
	STD  Y+6+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x10:
	STD  Y+10,R30
	STD  Y+10+1,R31
	RET

_strlen:
	clr  r27
	ld   r26,y+
	clr  r30
__strlen0:
	ld   r22,x+
	tst  r22
	breq __strlen1
	inc  r30
	rjmp __strlen0
__strlen1:
	ret

_strlenf:
	clr  r26
	clr  r27
	ld   r30,y+
	ld   r31,y+
__strlenf0:
	lpm  r0,z+
	tst  r0
	breq __strlenf1
	adiw r26,1
	rjmp __strlenf0
__strlenf1:
	movw r30,r26
	ret

_delay_ms:
	ld   r30,y+
	ld   r31,y+
	adiw r30,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0x733
	wdr
	sbiw r30,1
	brne __delay_ms0
__delay_ms1:
	ret

__ANEGW1:
	COM  R30
	COM  R31
	ADIW R30,1
	RET

__MULB12U:
	MOV  R0,R26
	SUB  R26,R26
	LDI  R27,9
	RJMP __MULB12U1
__MULB12U3:
	BRCC __MULB12U2
	ADD  R26,R0
__MULB12U2:
	LSR  R26
__MULB12U1:
	ROR  R30
	DEC  R27
	BRNE __MULB12U3
	RET

__GETW1P:
	LD   R30,X+
	LD   R31,X
	DEC  R26
	RET

__GETW1PF:
	LPM  R0,Z+
	LPM  R31,Z
	MOV  R30,R0
	RET

__SAVELOCR6:
	ST   -Y,R21
__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR6:
	LDD  R21,Y+5
__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

;END OF CODE MARKER
__END_OF_CODE:
