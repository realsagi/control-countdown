//***************************************************/;
//* Hardware    : ET-BASE AVR TINY2313              */;
//* CPU         : ATMEL-ATtiny2313                  */;
//* X-TAL       : 7.3728 MHz                        */;
//* Complier    : CodeVisionAVR V1.24.8d            */;
//* Last Update : 10-07-2006 (ETT CO.,LTD)          */;
//*             : WWW.ETT.CO.TH                     */;
//* Description : Example LCD CHAR 16X2             */;
//***************************************************/;
//* CodeVisionAVR Complier Option Setting           */;
//* Chip type           : ATtiny2313V               */;
//* Clock frequency     : 7.3728 MHz                */;
//* Memory model        : Tiny                      */;
//* External SRAM size  : 0                         */;
//* Data Stack size     : 32                        */;
//***************************************************/;
//*Connect LCD Module to ET-CLCD PORT  
//*Lcdpin : D4 = PB0 , D5 = PB1 , D6 = PB2 , D7 = PB3 , EN = PB4 , RS = PB6
// CodeVisionAVR C Compiler
// (C) 1998-2003 Pavel Haiduc, HP InfoTech S.R.L.
// I/O registers definitions for the ATtiny2313
#pragma used+
sfrb DIDR=1;
sfrb UBRRH=2;
sfrb UCSRC=3;
sfrb ACSR=8;
sfrb UBRRL=9;
sfrb UCSRB=0xa;
sfrb UCSRA=0xb;
sfrb UDR=0xc;
sfrb USICR=0xd;
sfrb USISR=0xe;
sfrb USIDR=0xf;
sfrb PIND=0x10;
sfrb DDRD=0x11;
sfrb PORTD=0x12;
sfrb GPIOR0=0x13;
sfrb GPIOR1=0x14;
sfrb GPIOR2=0x15;
sfrb PINB=0x16;
sfrb DDRB=0x17;
sfrb PORTB=0x18;
sfrb PINA=0x19;
sfrb DDRA=0x1a;
sfrb PORTA=0x1b;
sfrb EECR=0x1c;
sfrb EEDR=0x1d;
sfrb EEAR=0x1e;
sfrb PCMSK=0x20;
sfrb WDTCR=0x21;
sfrb TCCR1C=0x22;
sfrb GTCCR=0x23;
sfrb ICR1L=0x24;
sfrb ICR1H=0x25;
sfrw ICR1=0x24;   // 16 bit access
sfrb CLKPR=0x26;
sfrb OCR1BL=0x28;
sfrb OCR1BH=0x29;
sfrw OCR1B=0x28;   // 16 bit access
sfrb OCR1AL=0x2a;
sfrb OCR1AH=0x2b;
sfrw OCR1A=0x2a;   // 16 bit access
sfrb TCNT1L=0x2c;
sfrb TCNT1H=0x2d;
sfrw TCNT1=0x2c;  // 16 bit access
sfrb TCCR1B=0x2e;
sfrb TCCR1A=0x2f;
sfrb TCCR0A=0x30;
sfrb OSCCAL=0x31;
sfrb TCNT0=0x32;
sfrb TCCR0B=0x33;
sfrb MCUSR=0x34;
sfrb MCUCR=0x35;
sfrb OCR0A=0x36;
sfrb SPMCSR=0x37;
sfrb TIFR=0x38;
sfrb TIMSK=0x39;
sfrb EIFR=0x3a;
sfrb GIMSK=0x3b;
sfrb OCR0B=0x3c;
sfrb SPL=0x3d;
sfrb SREG=0x3f;
#pragma used-
// Interrupt vectors definitions
// for compatibility with the interrupt vector names from Atmel's datasheet
// CodeVisionAVR C Compiler
// (C) 1998-2000 Pavel Haiduc, HP InfoTech S.R.L.
#pragma used+
void delay_us(unsigned int n);
void delay_ms(unsigned int n);
#pragma used-
// CodeVisionAVR C Compiler
// (C) 1998-2003 Pavel Haiduc, HP InfoTech S.R.L.
// Prototypes for standard I/O functions
// CodeVisionAVR C Compiler
// (C) 1998-2002 Pavel Haiduc, HP InfoTech S.R.L.
// Variable length argument list macros
typedef char *va_list;
#pragma used+
char getchar(void);
void putchar(char c);
void puts(char *str);
void putsf(char flash *str);
char *gets(char *str,unsigned char len);
void printf(char flash *fmtstr,...);
void sprintf(char *str, char flash *fmtstr,...);
void vprintf (char flash * fmtstr, va_list argptr);
void vsprintf (char *str, char flash * fmtstr, va_list argptr);
signed char scanf(char flash *fmtstr,...);
signed char sscanf(char *str, char flash *fmtstr,...);
                                               #pragma used-
#pragma library stdio.lib
char lcdbuf[16+1];			// LCD Display Buffer
/* pototype  section */
void init_lcd(void);			// Initial Character LCD(4-Bit Interface)
void gotolcd(unsigned char);		// Set Cursor LCD
void write_ins(unsigned char);      	// Write Instruction LCD
void write_data(unsigned char);		// Write Data LCD
void printlcd(void);			// Display Message LCD
/*--------------------------------------------
The main C function.  Program execution Here 
---------------------------------------------*/
void main(void)
{
  DDRB=0xFF;                                                    // PORTB as output
    delay_ms(30);							// Power-on Delay
  init_lcd();							// Initial LCD
     while(1)
  {
    gotolcd(0);							// Set Cursor Line-1
    sprintf(lcdbuf,"ET-BASE AVR....."); // Display Line-1
    printlcd();
    gotolcd(0x40);						// Set Cursor Line-2
    sprintf(lcdbuf,"BASE ON TINY2313"); // Display Line-2
    printlcd();	
    delay_ms(1000);						// Display Delay
    gotolcd(0);							// Set Cursor Line-1
    sprintf(lcdbuf,"7.4 MIPS Execute"); // Display Line-1  
    printlcd();
    gotolcd(0x40);						// Set Cursor Line-2
    sprintf(lcdbuf,"BY..ETT CO.,LTD."); // Display Line-2
    printlcd();	
    delay_ms(1000);						// Display Delay
  }  
}
/*******************************/
/* Initial LCD 4-Bit Interface */
/*******************************/
void init_lcd(void)
{
  PORTB &= 0b11101111;			// Start LCD Control   EN=0  (PB4)
  delay_ms(1);				// Wait LCD Ready
    write_ins(0x33);			// Initial (Set DL=1 3 Time, Reset DL=0 1 Time)
  write_ins(0x32);  
  write_ins(0x28);  			// Function Set (DL=0 4-Bit,N=1 2 Line,F=0 5X7)
  write_ins(0x0C);  			// Display on/off Control (Entry Display,Cursor off,Cursor not Blink)
  write_ins(0x06);  			// Entry Mode Set (I/D=1 Increment,S=0 Cursor Shift)
  write_ins(0x01);  			// Clear Display  (Clear Display,Set DD RAM Address=0)
  delay_ms(1);			        // Wait Initial Complete
  return;
}
/******************/
/* Set LCD Cursor */
/******************/
void gotolcd(unsigned char i)
{
  i |= 0x80;				// Set DD-RAM Address Command
  write_ins(i);  
  return;
}
/****************************/
/* Write Instruction to LCD */
/****************************/
void write_ins(unsigned char i)
{
  PORTB &= 0b10111111;			// Instruction Select RS=0(PB6)
  PORTB &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  PORTB |= (i>>4) & 0x0F;        	// Strobe High Nibble Command
  PORTB |= 0b00010000;    		// Enable ON    EN=1(PB4)
  delay_ms(1);
  PORTB &= 0b11101111;   		// Enable OFF   EN=0(PB4)
    PORTB &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  PORTB |=  i & 0x0F;    		// Strobe Low Nibble Command
  PORTB |= 0b00010000;    		// Enable ON    EN=1(PB4)
  delay_ms(1);
  PORTB &= 0b11101111;   		// Enable OFF   EN=0(PB4)
  delay_ms(1);				// Wait LCD Busy
  return;
}
/****************************/
/* Write Data(ASCII) to LCD */
/****************************/
void write_data(unsigned char i)
{
  PORTB |= 0b01000000;			// Instruction Select  RS=1(PB6)
  PORTB &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  PORTB |= (i>>4) & 0x0F;         	// Strobe High Nibble Command
  PORTB |= 0b00010000;    		// Enable ON    EN=1(PB4)
  delay_ms(1);
  PORTB &= 0b11101111;   		// Enable OFF   EN=0(PB4)
    PORTB &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  PORTB |= i & 0x0F;   			// Strobe Low Nibble Command
  PORTB |= 0b00010000;    		// Enable ON    EN=1(PB4)
  delay_ms(1);
  PORTB &= 0b11101111;   		// Enable OFF   EN=0(PB4) 
  delay_ms(1);				// Wait LCD Busy
  return;						
}
/****************************/
/* Print Data(ASCII) to LCD */
/****************************/
void printlcd(void)
{
  char *p;
   p = lcdbuf;
   do 					// Get ASCII & Write to LCD Until null
  {
    write_data(*p); 			// Write ASCII to LCD
    p++;				// Next ASCII
  }
  while(*p != '\0');		        // End of ASCII (null)
   return;
}
