;*****************************************************************
; Program		:Main program for test ET-BASE TINY2313          *
; Description   :You can select 1-5                              *
;               :for testing program                             *
; CPU Control	:ATTINY2313                                      *
; Frequency     :X-TAL : 7.3728 MHz                              *
; File name		:QC_Test.asm                                     *
; Assembler		:AVR Assembler 2                                 *
; 4 bit LCD pin information:RESET=PB6,ENABLE=PB4,DATA = PB0-PB4  * 
; I2C bus : SCL = PB7, SDA = PB5                                 *
;*****************************************************************

.include "tn2313def.inc"


		   .ORG	$0000                ;Reset Handle 
    	    RJMP  	RESET			
		   .ORG $0007                ;RX Complete  
		    RJMP    EXIT_TEST       

;/************************
; Define Register
;/************************

.def		COUNTER1  	= R16
.def		COUNTER2	= R17
.def		COUNTER3	= R18
.def		TEMP		= R19
.def		COLUMN		= R20
.def		DATA		= R21
.def		TEMP1		= R22
.def		ADDRESS		= R23
.def		FLAG		= R24
.def        TEMP2       = R25


;/***********************
; Main Program
;/***********************
RESET:      
			LDI		TEMP,LOW(RAMEND)
            OUT		SPL,TEMP            ;init Stack Pointer     
            LDI		TEMP,HIGH(RAMEND)
            OUT		SPL+1,TEMP        	
main:		
			SBI		UCR,TXEN
            SBI		UCR,RXEN
            LDI 	TEMP,47             ;Baud 9600 at 7.3728 MHz X-TAL
		    OUT		UBRR,TEMP
Loop:   	
			LDI 	ZH,high(2*TEXT1)
			LDI		ZL,low(2*TEXT1)
			RCALL	Intro		     
			RCALL	RX_Byte
			RCALL	TX_Byte
			PUSH	DATA
			LDI		DATA,0x0d
			RCALL	TX_Byte
			LDI		TEMP,2
Line_Feed:	
			LDI		DATA,0x0a
			RCALL	TX_byte
			DEC		TEMP
			BRNE	Line_Feed
			POP		DATA

    		CPI 	DATA,'1'
    		BREQ  	RS232_
    		CPI 	DATA,'2'
    		BREQ 	TEST_EEPROM_
    		CPI 	DATA,'3'
   			BREQ 	TEST_1307_
  			CPI 	DATA,'4'
    		BREQ 	TEST_LCD_
			CPI 	DATA,'5'
    		BREQ 	TEST_PORT_
    		RJMP  	Loop    


RS232_:		
			RJMP    RS232
TEST_EEPROM_: 
			RJMP    TEST_EEP
TEST_1307_: 	
			RJMP    TEST_DS1307
TEST_LCD_: 			
			RJMP    TEST_LCD
TEST_PORT_:
			RJMP    TEST_PORT


EXIT_TEST:	
		    IN 	    TEMP1,UDR
		    SET
		    CPI	    TEMP1,27
		    BREQ	EXIT_TEST_
		    CLT
EXIT_TEST_:	
            RETI

TX232_JR:   
			SBIS    USR,UDRE           ;wait until UDRE = 1
            RJMP    TX232_JR
            OUT     UDR,TEMP           ;send DATA to serial  
            RET

;***************************************************************************
;*                     TEST PORTB , PORTD                                  *
;***************************************************************************

TEST_PORT:    
			LDI     ZH,HIGH(PORT_TB*2)
            LDI     ZL,LOW(PORT_TB*2)
            RCALL   Intro
            SBI     UCR,RXCIE
            SEI
            SER     TEMP

            LDI     R17,0xFF          
            OUT     DDRB,R17
			OUT     DDRD,R17


loop_IO:    
			LDI     R17,0b11111110
			SEC
loop1_IO:   
			OUT     PORTB,R17
			OUT     PORTD,R17
            RCALL   delay_IO
            ROL     R17
            BRTS	END_LOOP_PORT
            RJMP    loop1_IO

END_LOOP_PORT:
         	CBI	    UCR,RXCIE
		    CLI
		    CLT
		    RJMP	main

;**********************************         
;*         Delay program          *  
;**********************************   
delay_IO:   
			LDI     R22,0x05
dly1_IO:    
			LDI     R23,0xFF
dly2_IO:    
			LDI     R24,0xFF
dly3_IO:    
			DEC     R24
           	BRNE    dly3_IO
           	DEC     R23
           	BRNE    dly2_IO
           	DEC     R22
           	BRNE    dly1_IO
           	RET   

;***************************************

RS232:          
			LDI    ZH,high(2*RS232TB)
            LDI    ZL,low(2*RS232TB)
            RCALL  Intro
            CBI    UCR,RXCIE
            CLI
sub_RS232:      
			RCALL  RX_Byte
            CPI    DATA,27
            BREQ   end_loop_RS232
            RCALL  TX_Byte
            RJMP   sub_RS232
end_loop_RS232: 
			RJMP   main

;*****************************************


;/*************
; Send Text
;/*************
Intro:      
			LPM
            TST     R0
            BREQ    end_sub
TX232:      
			SBIS    USR,UDRE
            RJMP    TX232
            OUT     UDR,r0
            ADIW    ZL,1
            RJMP    Intro
end_sub:    
			RET

;/*************
; Send a byte
;/*************
TX_Byte:	
			SBIS	USR,UDRE
		    RJMP	TX_Byte
	     	OUT	    UDR,DATA
		    RET
;/****************
; Receive a byte
;/****************
RX_Byte:	
			SBIS	USR,RXC
		    RJMP	RX_Byte
		    IN	    DATA,UDR
		    RET

;***************************************************************************

HEXASC:           
			LDI     COLUMN,0X30
            CPI     TEMP,0X0A
            BRCS    HEXASC1
           	LDI     COLUMN,0X36
HEXASC1:     
	        ADD     TEMP,COLUMN
            RET
           
;***************************************************************************

;***************************************************************************
;*                     SERIAL EEPROM (24LC256) TO RS232                    *
;***************************************************************************


.EQU        PORT_EEP      =  PORTB
.EQU        DDR_EEP       =  DDRB
.EQU        PIN_EEP       =  PINB
.EQU        BIT_SCL_EEP   =  7
.EQU        BIT_SDA_EEP   =  5

TEST_EEP:         
			LDI      ZH,HIGH(T_EEP*2)
          	LDI      ZL,LOW(T_EEP*2)
            RCALL    Intro
            SBI      UCR,RXCIE
            SEI
            SER      TEMP
            SBI      DDR_EEP,BIT_SDA_EEP
            SBI      DDR_EEP,BIT_SCL_EEP
            SBI      PORT_EEP,BIT_SDA_EEP
            SBI      PORT_EEP,BIT_SCL_EEP

YY_EEP:           
			LDI      TEMP,0x0A
            RCALL    TX232_JR    ;TX232_EEP
            LDI      TEMP,0X0D
            RCALL    TX232_JR    ;TX232_EEP

            LDI      ZH,high(TEXT_TAB1_EEP*2)
            LDI      ZL,low(TEXT_TAB1_EEP*2)
AA_EEP:           
			LPM
            LDI      R16,0x00
            CP       R0,R16
            BREQ     BB_EEP
            MOV      TEMP,R0
            RCALL    TX232_JR    ;TX232_EEP
            ADIW     ZL,1
            RJMP     AA_EEP

BB_EEP:           
			LDI      TEMP,0x0A
            RCALL    TX232_JR    ;TX232_EEP
            LDI      TEMP,0x0D
            RCALL    TX232_JR    ;TX232_EEP

            LDI      ZH,high(TEXT_TAB2_EEP*2)
            LDI      ZL,low(TEXT_TAB2_EEP*2)
CC_EEP:           
			LPM
            LDI      R16,0x00
            CP       R0,R16
            BREQ     DD_EEP
            MOV      TEMP,R0
            RCALL    TX232_JR    ;TX232_EEP
            ADIW     ZL,1
            RJMP     CC_EEP

DD_EEP:           
			RCALL    WRITE_BYTE
            RCALL    RANDOM_READ
            PUSH     TEMP
            SWAP     TEMP
            ANDI     TEMP,0x0F
            RCALL    HEXASC_EEP
            RCALL    TX232_JR    ;TX232_EEP
            POP      TEMP
            ANDI     TEMP,0x0F
            RCALL    HEXASC_EEP
            RCALL    TX232_JR    ;TX232_EEP
WAIT_EEP:   
            BRTS	 END_LOOP_EEP      
			RJMP     WAIT_EEP

END_LOOP_EEP:
         	CBI	    UCR,RXCIE
		    CLI
		    CLT
		    RJMP	main

;****************************************************
;*                  WRITE BYTE                      *
;****************************************************
WRITE_BYTE:       
			CBI      PORT_EEP,BIT_SDA_EEP      	;start condition
            CBI      PORT_EEP,BIT_SCL_EEP
            LDI      TEMP,0xA8                	;write command
            RCALL    TX_BYTE_EEP
            RCALL    ACKNOW                   	;wait acknowledge
            BRCS     WRITE_BYTE 

            LDI      TEMP,0x00                 	;Address high
            RCALL    TX_BYTE_EEP
            RCALL    ACKNOW               		;wait acknowledge
            BRCS     WRITE_BYTE

            LDI      TEMP,0x00            		;Address low
            RCALL    TX_BYTE_EEP
            RCALL    ACKNOW
            BRCS     WRITE_BYTE

            LDI      TEMP,0x55            		;DATA
            RCALL    TX_BYTE_EEP
            RCALL    ACKNOW               		;wait acknowledge
            BRCS     WRITE_BYTE

            CBI      PORT_EEP,BIT_SDA_EEP

            SBI      PORT_EEP,BIT_SCL_EEP      	;stop condition
            SBI      PORT_EEP,BIT_SDA_EEP
            RCALL    DELAY_EEP
            RET

;*****************************************************
;*                   RANDOM READ                     *
;*****************************************************
RANDOM_READ:      
			CBI      PORT_EEP,BIT_SDA_EEP      	;start condition
            CBI      PORT_EEP,BIT_SCL_EEP
            LDI      TEMP,0xA8            		;write command
            RCALL    TX_BYTE_EEP
            RCALL    ACKNOW               		;wait acknowledge
            BRCS     RANDOM_READ 

            LDI      TEMP,0x00            		;Address high
            RCALL    TX_BYTE_EEP
            RCALL    ACKNOW               		;wait acknowledge
            BRCS     RANDOM_READ

            LDI      TEMP,0x00            		;Address low
            RCALL    TX_BYTE_EEP
            RCALL    ACKNOW
            BRCS     RANDOM_READ

            SBI      PORT_EEP,BIT_SCL_EEP       ;stop condition
            SBI      PORT_EEP,BIT_SDA_EEP

            CBI      PORT_EEP,BIT_SDA_EEP
            CBI      PORT_EEP,BIT_SCL_EEP

            LDI      TEMP,0xA9
            RCALL    TX_BYTE_EEP
            RCALL    ACKNOW
            BRCS     RANDOM_READ

            RCALL    RX_BYTE_EEP

            SBI      PORT_EEP,BIT_SCL_EEP
            SBI      PORT_EEP,BIT_SCL_EEP

            CBI      PORT_EEP,BIT_SDA_EEP

            SBI      PORT_EEP,BIT_SCL_EEP        ;stop condition
            SBI      PORT_EEP,BIT_SDA_EEP
            RET

;*****************************************************
;*             CHECK BIT ACKNOWLEDGE BIT             *
;CARRY = 1: FAIL                                     *
;CARRY = 0: PASS                                     *
;*****************************************************
ACKNOW:           
			CBI      DDR_EEP,BIT_SDA_EEP         ;BIT_SDA input port
            SBI      PORT_EEP,BIT_SDA_EEP
            SEC
            SBI      PORT_EEP,BIT_SCL_EEP
            SBIS     PIN_EEP,BIT_SDA_EEP
            CLC
            CBI      PORT_EEP,BIT_SCL_EEP
            SBI      DDR_EEP,BIT_SDA_EEP          ;BIT_SDA output port
            RET

;****************************************************
;*               TX 1 BITE TO 24256                 *
;****************************************************
TX_BYTE_EEP:      
			LDI      COLUMN,0x08
TX_BYTE1_EEP:     
			SBI      PORT_EEP,BIT_SDA_EEP
            LSL      TEMP
            BRCS     TX_BYTE2_EEP
            CBI      PORT_EEP,BIT_SDA_EEP
TX_BYTE2_EEP:     
			SBI      PORT_EEP,BIT_SCL_EEP
            CBI      PORT_EEP,BIT_SCL_EEP
            DEC      COLUMN
            BRNE     TX_BYTE1_EEP
            RET

;****************************************************
;*               RX 1 BYTE FROM 24256               *
;****************************************************
RX_BYTE_EEP:      
			CBI      DDR_EEP,BIT_SDA_EEP
            SBI      PORT_EEP,BIT_SDA_EEP
            LDI      TEMP,0x00
            LDI      COLUMN,0x08
RX_BYTE1_EEP:     
			SBI      PORT_EEP,BIT_SCL_EEP
            SEC
            SBIS     PIN_EEP,BIT_SDA_EEP
            CLC
            ROL      TEMP
            CBI      PORT_EEP,BIT_SCL_EEP
            DEC      COLUMN
            BRNE     RX_BYTE1_EEP
            SBI      DDR_EEP,BIT_SDA_EEP
            RET

;***************************************************
;*                DELAY TIME FOR START             *
;***************************************************
DELAY_EEP:        
			PUSH     R26
            PUSH     R27
            LDI      R27,0xFF
DEL2_EEP:         
			LDI      R26,0xFF
DEL1_EEP:         
			DEC      R26
            BRNE     DEL1_EEP
            DEC      R27
            BRNE     DEL2_EEP
            POP      R27
            POP      R26
            RET

HEXASC_EEP:      
			LDI      ZL,LOW(ASC_TAB_EEP*2)
            LDI      ZH,HIGH(ASC_TAB_EEP*2)
            ADD      ZL,TEMP
            LDI      TEMP,0
            ADC      ZH,TEMP
            LPM
            MOV      TEMP,R0
            RET

ASC_TAB_EEP:      
			.DB     0x30,0x31,0x32,0x33,0x34,0x35
            .DB     0x36,0x37,0x38,0x39,0x41,0x42
            .DB     0x43,0x44,0x45,0x46
                       

;***************************************************************************
;*                       RTC(DS1307) TO RS232                              *
;***************************************************************************

.EQU        PORT_EE_DS1307   =  PORTB
.EQU        DDR_EE_DS1307    =  DDRB
.EQU        PIN_EE_DS1307    =  PINB
.EQU        BIT_SCL_DS1307   =  7
.EQU        BIT_SDA_DS1307   =  5
       

TEST_DS1307:      
			LDI     ZH,HIGH(2*TEXT_TAB_DS1307)
            LDI     ZL,LOW(2*TEXT_TAB_DS1307)
            RCALL   Intro
            SBI     UCR,RXCIE
            SEI
            SER     TEMP

            SBI     DDR_EE_DS1307,BIT_SDA_DS1307
            SBI     DDR_EE_DS1307,BIT_SCL_DS1307
            SBI     PORT_EE_DS1307,BIT_SDA_DS1307
            SBI     PORT_EE_DS1307,BIT_SCL_DS1307

YY_DS1307:  
			LDI     ADDRESS,0	      
			RCALL   WRITE_BYTE_DS1307
			LDI     ADDRESS,1	      
			RCALL   WRITE_BYTE_DS1307
			LDI     ADDRESS,2	      
			RCALL   WRITE_BYTE_DS1307
			

WAIT_DS1307:      
			LDI     TEMP2,2
WAIT1_DS1307:     
			RCALL   RANDOM_READ_DS1307
            PUSH    TEMP
            SWAP    TEMP
            ANDI    TEMP,0x0F
            RCALL   HEXASC_DS1307
            RCALL   TX232_JR    ;TX232_DS1307

            POP     TEMP
            ANDI    TEMP,0x0F
            RCALL   HEXASC
            RCALL   TX232_JR    ;TX232_DS1307
            LDI     TEMP,0x3A
            RCALL   TX232_JR    ;TX232_DS1307
            DEC     TEMP2
            BRNE    WAIT1_DS1307

            RCALL   RANDOM_READ_DS1307
            PUSH    TEMP
            SWAP    TEMP
            ANDI    TEMP,0x0F
            RCALL   HEXASC_DS1307
            RCALL   TX232_JR    ;TX232_DS1307
            POP     TEMP
            ANDI    TEMP,0x0F
            RCALL   HEXASC_DS1307
            RCALL   TX232_JR    ;TX232_DS1307
            LDI     TEMP,0x0D
            RCALL   TX232_JR    ;TX232_DS1307
			BRTS	END_LOOP_DS1307
            RJMP    WAIT_DS1307

END_LOOP_DS1307:
         	CBI	    UCR,RXCIE
		    CLI
		    CLT
			RCALL   delay_IO
		    RJMP	main

;****************************************************
;*                  WRITE BYTE                      *
;****************************************************
WRITE_BYTE_DS1307:
			CBI      PORT_EE_DS1307,BIT_SDA_DS1307      ;start condition
            CBI      PORT_EE_DS1307,BIT_SCL_DS1307
            LDI      TEMP,0xD0                          ;write command
            RCALL    TX_BYTE_DS1307
            RCALL    ACKNOW_DS1307                      ;wait acknowledge
            BRCS     WRITE_BYTE_DS1307

            MOV      TEMP,ADDRESS                       ;Address high
            RCALL    TX_BYTE_DS1307
            RCALL    ACKNOW_DS1307                      ;wait acknowledge
            BRCS     WRITE_BYTE_DS1307

            LDI      TEMP,00                            ;DATA
            RCALL    TX_BYTE_DS1307
            RCALL    ACKNOW_DS1307                      ;wait acknowledge
            BRCS     WRITE_BYTE_DS1307

            CBI      PORT_EE_DS1307,BIT_SDA_DS1307

            SBI      PORT_EE_DS1307,BIT_SCL_DS1307      ;stop condition
            SBI      PORT_EE_DS1307,BIT_SDA_DS1307
            RET

;*****************************************************
;*                   RANDOM READ                     *
;*****************************************************
RANDOM_READ_DS1307:  
			CBI      PORT_EE_DS1307,BIT_SDA_DS1307      ;start condition
            CBI      PORT_EE_DS1307,BIT_SCL_DS1307
            LDI      TEMP,0XD0                          ;write command
            RCALL    TX_BYTE_DS1307
            RCALL    ACKNOW_DS1307                      ;wait acknowledge
            BRCS     RANDOM_READ_DS1307

            MOV      TEMP,TEMP2                         ;Address high
            RCALL    TX_BYTE_DS1307
            RCALL    ACKNOW_DS1307                      ;wait acknowledge
            BRCS     RANDOM_READ_DS1307

            SBI      PORT_EE_DS1307,BIT_SCL_DS1307      ;stop condition
            SBI      PORT_EE_DS1307,BIT_SDA_DS1307

            CBI      PORT_EE_DS1307,BIT_SDA_DS1307
            CBI      PORT_EE_DS1307,BIT_SCL_DS1307

            LDI      TEMP,0XD1
            RCALL    TX_BYTE_DS1307
            RCALL    ACKNOW_DS1307
            BRCS     RANDOM_READ_DS1307

            RCALL    RX_BYTE_DS1307

            SBI      PORT_EE_DS1307,BIT_SCL_DS1307
            SBI      PORT_EE_DS1307,BIT_SCL_DS1307

            CBI      PORT_EE_DS1307,BIT_SDA_dS1307

            SBI      PORT_EE_DS1307,BIT_SCL_dS1307        ;stop condition
            SBI      PORT_EE_DS1307,BIT_SDA_DS1307
            RET

;*****************************************************
;*              CHECK BIT ACKNOWLEDGE BIT            *
;* CARRY = 1: FAIL                                   *
;* CARRY = 0: PASS                                   *
;*****************************************************
ACKNOW_DS1307:    
			CBI      DDR_EE_DS1307,BIT_SDA_DS1307         ;BIT_SDA input port
            SBI      PORT_EE_DS1307,BIT_SDA_DS1307
            SEC
            SBI      PORT_EE_DS1307,BIT_SCL_DS1307
            SBIS     PIN_EE_DS1307,BIT_SDA_DS1307
            CLC
            CBI      PORT_EE_DS1307,BIT_SCL_DS1307
            SBI      DDR_EE_DS1307,BIT_SDA_DS1307         ;BIT_SDA output port
            RET

;****************************************************
;*               TX 1 BITE TO 24256                 *
;****************************************************
TX_BYTE_DS1307:   
			LDI      COLUMN,0X08
TX_BYTE1_DS1307:  
			SBI      PORT_EE_DS1307,BIT_SDA_DS1307
            LSL      TEMP
            BRCS     TX_BYTE2_DS1307
            CBI      PORT_EE_DS1307,BIT_SDA_DS1307
TX_BYTE2_DS1307:  
			SBI      PORT_EE_DS1307,BIT_SCL_DS1307
            CBI      PORT_EE_DS1307,BIT_SCL_DS1307
            DEC      COLUMN
            BRNE     TX_BYTE1_DS1307
            RET

;****************************************************
;*               RX 1 BYTE FROM 24256               *
;****************************************************
RX_BYTE_DS1307:   
			CBI      DDR_EE_DS1307,BIT_SDA_DS1307
            SBI      PORT_EE_DS1307,BIT_SDA_DS1307
            LDI      TEMP,0X00
            LDI      COLUMN,0X08
RX_BYTE1_DS1307:  
			SBI      PORT_EE_DS1307,BIT_SCL_dS1307
            SEC
            NOP
            NOP
            NOP
            NOP
            NOP
            SBIS     PIN_EE_DS1307,BIT_SDA_DS1307
            CLC
            ROL      TEMP
            CBI      PORT_EE_DS1307,BIT_SCL_DS1307
            NOP
            NOP
            NOP
            NOP
            NOP
            DEC      COLUMN
            BRNE     RX_BYTE1_DS1307
            SBI      DDR_EE_DS1307,BIT_SDA_DS1307
            RET

HEXASC_DS1307:    
			LDI      ZL,LOW(ASC_TAB_DS1307*2)
            LDI      ZH,HIGH(ASC_TAB_DS1307*2)
            ADD      ZL,TEMP
            LDI      TEMP,0
            ADC      ZH,TEMP
            LPM
            MOV      TEMP,R0
            RET

ASC_TAB_DS1307:   
			.DB     0x30,0x31,0x32,0x33,0x34,0x35
            .DB     0x36,0x37,0x38,0x39,0x41,0x42
            .DB     0x43,0x44,0x45,0x46

;***************************************************************************
;*                  LCD Test                                               *
;***************************************************************************

.EQU        PORT_LCD  =  PORTB
.EQU        PIN_LCD   =  PINB
.EQU        DDR_LCD   =  DDRB 
.EQU        BIT_RS_LCD    =  6
.EQU        BIT_CS_LCD    =  4

TEST_LCD:         
			LDI      ZH,high(TEXT_TB_LCD*2)
            LDI      ZL,low(TEXT_TB_LCD*2)
            RCALL    Intro
            SBI      UCR,RXCIE
            SEI
            SER      TEMP

            LDI      TEMP,0x5F           
            OUT      DDR_LCD,TEMP

            SBI      PORT_LCD,BIT_RS_LCD
            SBI      PORT_LCD,BIT_CS_LCD

            RCALL    INIT_LCD            ;initial  LCD

            LDI      ZL,LOW(LCD_LINE1*2)
            LDI      ZH,HIGH(LCD_LINE1*2)
		    RCALL    SHOW_LCD

		    LDI      TEMP,0xC0            ;Line 2     
            RCALL    WR_INS_LCD
            LDI      ZH,high(LCD_LINE2*2)
            LDI      ZL,low(LCD_LINE2*2) 
		    RCALL    SHOW_LCD
WAIT:       BRTS	END_LOOP_LCD
            RJMP     WAIT

END_LOOP_LCD:
         	CBI	    UCR,RXCIE
		    CLI
		    CLT
		    RJMP	main

                  
SHOW_LCD:         
			LPM
            TST      R0
            BREQ     END
            MOV      TEMP,R0
            RCALL    WR_LCD
            ADIW     ZL,0x01
            RJMP     SHOW_LCD
END:              
			RET

;****************************************************
;*          SUBRUTINE FOR WRITE DATA TO LCD         *
;****************************************************
WR_LCD:           
			PUSH     TEMP
		    SWAP     TEMP
            ANDI     TEMP,0x0F
            SBI      PORT_LCD,BIT_RS_LCD

			CLR		 TEMP1
			OUT 	 DDR_LCD,TEMP1 
            IN       COLUMN,PORT_LCD
			SER		 TEMP1
			OUT 	 DDR_LCD,TEMP1 
            ANDI     COLUMN,0xF0
            OR       TEMP,COLUMN
            OUT      PORT_LCD,TEMP


            RCALL    EN_LCD
            POP      TEMP
            ANDI     TEMP,0x0F
            IN       COLUMN,PORT_LCD
            ANDI     COLUMN,0xF0
            OR       TEMP,COLUMN
            OUT      PORT_LCD,TEMP
            RCALL    EN_LCD
            RET

;****************************************************
;*               WRITE INSTRUCTION LCD              *
;****************************************************
WR_INS_LCD:       
			PUSH     TEMP
            SWAP     TEMP
            ANDI     TEMP,0x0F
            CBI      PORT_LCD,BIT_RS_LCD
             
            CLR		 TEMP1
			OUT 	 DDR_LCD,TEMP1 
            IN       COLUMN,PORT_LCD
			SER		 TEMP1
			OUT 	 DDR_LCD,TEMP1
            ANDI     COLUMN,0xF0
            OR       TEMP,COLUMN
            OUT      PORT_LCD,TEMP

            RCALL    EN_LCD
            POP      TEMP
            ANDI     TEMP,0x0F
            IN       COLUMN,PORT_LCD
            ANDI     COLUMN,0xF0
            OR       TEMP,COLUMN
            OUT      PORT_LCD,TEMP 
            RCALL    EN_LCD
            RET

;***************************************************
;*                  Initial LCD                    *
;***************************************************
INIT_LCD:         
			LDI      TEMP,0x33       
            RCALL    WR_INS_LCD
            LDI      TEMP,0x32
            RCALL    WR_INS_LCD
            LDI      TEMP,0x28
            RCALL    WR_INS_LCD
            LDI      TEMP,0x0C
            RCALL    WR_INS_LCD
            LDI      TEMP,0x06
            RCALL    WR_INS_LCD
            LDI      TEMP,0x01
            RCALL    WR_INS_LCD
            RET

;****************************************************
;*                   ENABLE LCD                     *
;****************************************************
EN_LCD:           
			SBI      PORT_LCD,BIT_CS_LCD
            RCALL    BUSY_LCD
            CBI      PORT_LCD,BIT_CS_LCD
            RET

;****************************************************
;*               DELAY TIME FOR BUSY                *
;****************************************************
BUSY_LCD:         
			PUSH     R26
            PUSH     R27
            LDI      R27,0x10
BUSY2_LCD:        
			LDI      R26,0xFF
BUSY1_LCD:        
			DEC      R26
            BRNE     BUSY1_LCD
            DEC      R27
            BRNE     BUSY2_LCD
            POP      R27
            POP      R26 
            RET 

;******************************************************************

TEXT_TAB_DS1307: 
            .DB   0x0C,"TEST RTC (DS1307) ",0x0A,0x0D,0x0A,0x0D,0

PORT_TB: 
            .DB   0x0C,"TEST PORTB [0..7] & PORTD [2..6]",0

LCD_LINE1:       
			.DB   "ET-BASE AVR TINY ",0
LCD_LINE2:       
			.DB   "Test LCD 16x2... ",0

TEXT_TAB_EEP:    
			.DB   "TEST SERIAL EEPROM (24XX)",0
TEXT_TAB1_EEP:   
			.DB   "WRITE = 55 ",0
TEXT_TAB2_EEP:   
			.DB   "READ  =  ",0

TEXT_TB_LCD:     
			.DB   0X0C,"TEST LCD BY ET-BASE AVR TINY2313",0

T_EEP:           
            .DB   0x0C,"TEST EEPROM (24XX)",0X0A,0X0D,0

TEXT1:           
			.DB  0x0C,"***** ET-BASE AVR TINY2313 QC Test ***** ",0x0A,0x0D,0x0A,0x0D
            .DB "       1: Test RS232",0x0A,0x0D
            .DB "       2: Test EEPROM (24XX)",0x0A,0x0D
            .DB "       3: Test RTC (DS1307) ",0x0A,0x0D
            .DB "       4: Test LCD Character 16x2 ",0x0A,0x0D
		    .DB "       5: Test PORTB [0..7] & PORTD [2..6]",0x0A,0x0D,0X0A,0X0D
            .DB "Press ESC if you want to exit any test program or ",0x0A,0x0D
            .DB "hit '1 - 5' for select one test program :",0

RS232TB:     
			.DB  0x0C,"Test RS232 ",0x0A,0x0D
            .DB  "Please enter any key to echo.",0x0A,0x0D,0x0A,0x0D,0


