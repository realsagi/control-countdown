'***********************************************************
'* QC test Program For "ET-BASE AVR MEGA64/128" Board      *
'* Target MCU  : Atmel ATmega128                           *
'* Frequency   : X-TAL : 16 MHz                            *
'* Compiler    : BASCOM-AVR 1.11.7.9                       *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)           *
'* Last Update : 14/March/2006                             *
'* Description : For QC test                               *
'*             : Setup RS232 = 9600,N,8,1                  *
'***********************************************************
'Display Result to Serial Port UART0(9600 bps)
'PORTA-PORTE connect ET-TEST 10P/OUT
'PORTF connect to ET-TEST 10P/ADC
'RS232-CH0 connect to Serial Port of computer
'RS232-CH1 connect to another Serial Port
'ET-LCD connect LCD Char16X2


$regfile = "m128def.dat"                                    'ATmega128
$baud = 9600                                                'Serial Port 9600 bps
$baud1 = 9600                                               'Serial Port ch1 9600 bps
$crystal = 16000000                                         'XTAL = 16 MHz


Dim Value As Byte , Dat As Byte , Choice As Byte , Channel As Byte , Key As Byte , L As Byte , I As Byte
Dim W As Word

Declare Sub Lcd_shift_right(byval A As Byte)    00
Declare Sub Lcd_shift_left(byval A As Byte)
Declare Sub Lcd_on_off(byval A As Byte)

Start:

Printbin $0c                                                'Clear Display

Print "********************************"
Print "     ET-BASE AVR MEGA64/128     "
Print "********************************"
Print
Print "     -> SYSTEM SELF TEST <-     "
Print "         ATMEGA128 MCU          "
Print
Print "1. Test ADC(CH0-CH7)            "
Print "2. Test PORTA-PORTE             "
Print "3. Test LCD(Char16X2)           "
Print "4. Test RS232 CH1               "
Print
Print "********************************"
Print
Print "Please select choice(1..4)      "


'Wait Choice Select
Do
  Do
     Choice = Waitkey()                                     'Select Choice
     Choice = Choice - &H30
  Loop Until Choice >= 0
Loop Until Choice < 8

Select Case Choice
   Case 1
      Print
      Print "Test ADC(CH0-CH7)"
      Print
      Goto Test_adc

   Case 2
      Print
      Print "Test PORTA-PORTE"
      Print
      Goto Test_port

   Case 3
      Print
      Print "Test LCD (Char16X2)"
      Print
      Goto Test_lcd

   Case 4
      Print
      Print "Test RS232 CH1"
      Print
      Goto Test_rs232_ch1

End Select

Test_adc:
      Config Adc = Single , Prescaler = Auto
      Start Adc
      Channel = 0
   Do
      W = Getadc(channel) : Print Hex(w);
      Incr Channel
      If Channel > 7 Then
         Channel = 0
      Printbin $0d                                          'Send Cariage Return
      End If
      If Channel > 0 Then
         Printbin $20                                       'Send Space
      End If
   Loop
End

Test_lcd:
      Config Lcdbus = 4                                     '4-bit Mode
      Config Lcd = 16 * 2                                   '16x2 LCD display
      Config Lcdpin = Pin , Db4 = Portg.0 , Db5 = Portg.1 , Db6 = Portg.2 , Db7 = Portg.3 , E = Portd.7 , Rs = Portg.4

      Cls
      Cursor Off
      Lcd "ET-BASE AVR....."
      Lowerline
      Lcd "BY..ETT CO.,LTD."
      Wait 1

   Do                                                       'Configure LCD Screen
      Call Lcd_on_off(4)
      Wait 1
      Call Lcd_shift_right(16)
      Call Lcd_shift_left(16)
      Call Lcd_on_off(4)
      Wait 1
      Call Lcd_shift_left(16)
      Call Lcd_shift_right(16)
   Loop
End

Test_rs232_ch1:
'The M64 has an extended UART.
'when CONFIG COMx is not used, the default N,8,1 will be used
Config Com2 = Dummy , Synchrone = 0 , Parity = None , Stopbits = 1 , Databits = 8 , Clockpol = 0
'try the second hardware UART
Open "com2:" For Binary As #1
   Printbin #1 , $0c                                        'Clear Display
   Print #1 , "Hello this is RS232 Channel 1"
   Print #1 , "Please enter any key to echo data"
   Do
      I = Waitkey(#1)
      Print #1 , Chr(i);
   Loop
End


Test_port:
'Reserved Pins
'PB1,(ISP LOAD)
'PD2(RXD1),PD3(TXD1)
'PE0(RXD0),PE1(TXD0)
'PF0-PF7(ADC)


      Ddra = &HFF


      Ddrb.0 = 1
      Ddrb.2 = 1
      Ddrb.3 = 1
      Ddrb.4 = 1
      Ddrb.5 = 1
      Ddrb.6 = 1
      Ddrb.7 = 1

      Ddrc = &HFF

      Ddrd.0 = 1
      Ddrd.1 = 1
      Ddrd.4 = 1
      Ddrd.5 = 1
      Ddrd.6 = 1
      Ddrd.7 = 1

      Ddre.2 = 1
      Ddre.3 = 1
      Ddre.4 = 1
      Ddre.5 = 1
      Ddre.6 = 1
      Ddre.7 = 1


      Print "Reserved Pins !!!"
      Print "PB1->SPI LOAD"
      Print "PD2->RXD1,PD3->TXD1"
      Print "PE0->RXD0,PE1->TXD0"
      Print "[PF0-PF7]->ADC"

   Do
      Porta = &HFF
      Portb = &HFF
      Portc = &HFF
      Portd = &HFF
      Porte = &HFF
      Waitms 200
      Porta = 0
      Portb = 0
      Portc = 0
      Portd = 0
      Porte = 0
      Waitms 200
   Loop
End

Sub Lcd_shift_right(a As Byte)
For L = 1 To A
Shiftlcd Right                                              'Right Shift LCD Display
Waitms 250                                                  'Wait a moment
Next L
End Sub

Sub Lcd_shift_left(a As Byte)
For L = 1 To A
Shiftlcd Left                                               'Right Shift LCD Display
Waitms 250                                                  'Wait a moment
Next L
End Sub

Sub Lcd_on_off(a As Byte)
For L = 1 To A
Display Off                                                 'Turn Display off
Waitms 200                                                  'Wait a moment
Display On                                                  'Turn Display on
Waitms 200
Next L
End Sub