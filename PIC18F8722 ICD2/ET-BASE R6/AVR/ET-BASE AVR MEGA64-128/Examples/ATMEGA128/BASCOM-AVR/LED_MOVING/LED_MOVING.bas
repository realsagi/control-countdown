'*********************************************************
'* Examples Program For "ET-BASE AVR MEGA64/128" Board   *
'* Target MCU  : Atmel ATmega128                         *
'* Frequency   : X-TAL : 16 MHz                          *
'* Compiler    : BASCOM-AVR 1.11.7.9                     *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)         *
'* Last Update : 9/March/2006                            *
'* Description : Example LED Moving on PA,PB,PC,PD,PE,PF *
'*********************************************************
'Used ET-TEST 10P/OUT
'Connect PORT to LED

$regfile = "m128def.dat"                                    'ATmega128
$crystal = 16000000                                         'X-TAL = 16 MHz

Config Porta = Output                                       'Config Porta as output
Config Portb = Output                                       'Config Portb as output
Config Portc = Output                                       'Config Portc as output
Config Portd = Output                                       'Config Portd as output
Config Porte = Output                                       'Config Porte as output
Config Portf = Output                                       'Config Portf as output

Dim B As Byte                                               'Declare Variable

B = &B11111110
While B > 0
   Do
      Porta = B                                             'Out Porta
      Portb = B                                             'Out Portb
      Portc = B                                             'Out Portc
      Portd = B                                             'Out Portd
      Porte = B                                             'Out Porte
      Portf = B                                             'Out Portf
      Waitms 200                                            'Delay 200ms
      Rotate B , Left , 1                                   'Rotate Left
   Loop Until B = &B01111111

   Do
      Porta = B                                             'Out Porta
      Portb = B                                             'Out Portb
      Portc = B                                             'Out Portc
      Portd = B                                             'Out Portd
      Porte = B                                             'Out Porte
      Portf = B                                             'Out Portf
      Waitms 200                                            'Delay 200ms
      Rotate B , Right , 1                                  'Rotate Right
   Loop Until B = &B11111110
Wend
End                                                         'End Program