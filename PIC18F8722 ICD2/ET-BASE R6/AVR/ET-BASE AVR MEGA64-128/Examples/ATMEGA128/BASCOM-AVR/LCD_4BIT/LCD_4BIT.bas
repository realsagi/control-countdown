'*******************************************************
'* Examples Program For "ET-BASE AVR MEGA64/128" Board *
'* Target MCU  : Atmel ATmega128                       *
'* Frequency   : X-TAL : 16 MHz                        *
'* Compiler    : BASCOM-AVR 1.11.7.9                   *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)       *
'* Last Update : 9/March/2006                          *
'* Description : Example Use Character LCD 16x2        *
'*             : 4-bit Mode                            *
'*******************************************************

'Connect  LCD Module to ET-CLCD PORT

$regfile = "m128def.dat"                                    'ATmega128
$crystal = 16000000                                         'X-TAL = 16 MHz


Declare Sub Lcd_shift_right(byval A As Byte)
Declare Sub Lcd_shift_left(byval A As Byte)
Declare Sub Lcd_on_off(byval A As Byte)

Dim L As Byte

'Configure LCD Screen
Config Lcdbus = 4                                           '4-bit Mode
Config Lcd = 16 * 2                                         '16x2 LCD display
Config Lcdpin = Pin , Db4 = Portg.0 , Db5 = Portg.1 , Db6 = Portg.2 , Db7 = Portg.3 , E = Portd.7 , Rs = Portg.4

       Cls
       Cursor Off
       Lcd "ET-BASE AVR....."
       Lowerline
       Lcd "BASE ON MEGA128."
       Wait 1

   Do
      Call Lcd_on_off(4)
      Wait 1
      Call Lcd_shift_right(16)
      Call Lcd_shift_left(16)
      Call Lcd_on_off(4)
      Wait 1
      Call Lcd_shift_left(16)
      Call Lcd_shift_right(16)
   Loop
End                                                         'End Program

Sub Lcd_shift_right(a As Byte)
      For L = 1 To A
      Shiftlcd Right                                        'Right Shift LCD Display
      Waitms 250                                            'Wait a moment
      Next L
End Sub

Sub Lcd_shift_left(a As Byte)
      For L = 1 To A
      Shiftlcd Left                                         'Right Shift LCD Display
      Waitms 250                                            'Wait a moment
      Next L
End Sub

Sub Lcd_on_off(a As Byte)
      For L = 1 To A
      Display Off                                           'Turn Display off
      Waitms 200                                            'Wait a moment
      Display On                                            'Turn Display on
      Waitms 200
      Next L
End Sub