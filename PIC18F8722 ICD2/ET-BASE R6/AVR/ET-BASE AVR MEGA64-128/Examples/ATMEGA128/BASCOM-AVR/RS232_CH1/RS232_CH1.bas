'*******************************************************
'* Examples Program For "ET-BASE AVR MEGA64/128" Board *
'* Target MCU  : Atmel ATmega128                       *
'* Frequency   : X-TAL : 16 MHz                        *
'* Compiler    : BASCOM-AVR 1.11.7.9                   *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)       *
'* Last Update : 9/March/2006                          *
'* Description : RS232 Channel 1 Demo Send/Receive     *
'*             : Setup RS232 = 9600,N,8,1              *
'*******************************************************
'Display Result to Serial Port UART1(9600 bps)

$regfile = "m128def.dat"                                    'ATmega128
$baud1 = 9600                                               'Serial Port 9600 bps
$crystal = 16000000                                         'X-TAL = 16 MHz
Open "com2:" For Binary As #1                               'Open Second Hardware UART

Printbin #1 , $0c                                           'Clear Display
Print #1 , "ET-BASE AVR MEGA64/128 Test RS232 Channel 1"
Print #1 , "Please enter any key to echo data"

Dim I As Byte
   Do
      I = Waitkey(#1)
      Print #1 , Chr(i);
   Loop
End