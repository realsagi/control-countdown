//***************************************************/;
//* Hardware    : ET-BASE AVR MEGA64/128            */;
//* CPU         : ATMEL-ATmega128                   */;
//* X-TAL       : 16.00 MHz                         */;
//* Complier    : CodeVisionAVR V1.24.7e            */;
//* Last Update : 9-03-2006 (ETT CO.,LTD)           */;
//*             : WWW.ETT.CO.TH                     */;
//* Description : RS232 Channel 0 Demo Send/Receive */;
//*             : Setup RS232 = 9600,N,8,1          */;
//***************************************************/;
//* CodeVisionAVR Complier Option Setting           */;
//* Chip type           : ATmega128                 */;
//* Program type        : Application               */;
//* Clock frequency     : 16.000000 MHz             */;
//* Memory model        : Small                     */;
//* External SRAM size  : 0                         */;
//* Data Stack size     : 1024                      */;
//***************************************************/;
//Display Result to Serial Port UART0(9600 bps)

#include <mega128.h>                                                // ATmega128 MCU
#include <stdio.h>                                                  // Standard Input/Output functions

void main(void)
{
  char uart_data;                                                   // RS232 Buffer

  // USART initialization
  // Communication Parameters: 8 Data, 1 Stop, No Parity
  // USART Receiver: On
  // USART Transmitter: On
  // USART Mode: Asynchronous
  // USART Baud rate: 9600
  UCSR0A=0x00;
  UCSR0B=0x18;
  UCSR0C=0x86;
  UBRR0H=0x00;
  UBRR0L=0x67;

  printf("\fHello ET-BASE AVR MEGA64/128...TEST RS232 CH0\n\r");    // Call printf Function
  printf("Please enter any key to echo data\n\n\r");
  while (1)                                                         // Loop Continue
  {
    uart_data = getchar();                                          // Wait RS232 Receive
    putchar(uart_data);                                             // Echo data
  }
}
