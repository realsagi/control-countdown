//**********************************************/;
//* Hardware    : ET-BASE AVR MEGA64/128       */;
//* CPU         : ATMEL-ATmega128              */;
//* X-TAL       : 16.00 MHz                    */;
//* Complier    : CodeVisionAVR V1.24.7e       */;
//* Last Update : 9-03-2006 (ETT CO.,LTD)      */;
//*             : WWW.ETT.CO.TH                */;
//* Description : Example LCD CHAR 16X2        */;
//**********************************************/;
//* CodeVisionAVR Complier Option Setting      */;
//* Chip type           : ATmega128            */;
//* Program type        : Application          */;
//* Clock frequency     : 16.000000 MHz        */;
//* Memory model        : Small                */;
//* External SRAM size  : 0                    */;
//* Data Stack size     : 1024                 */;
//**********************************************/;
//*Connect LCD Module to ET-CLCD PORT  
//*Lcdpin : D4 = PG0 , D5 = PG1 , D6 = PG2 , D7 = PG3 , EN = PD7 , RS = PG4

#include <mega128.h>                    // ATmega128 MCU
#include <delay.h>                      // Delay functions
#include <stdio.h>                      // Standard Input/Output functions


char lcdbuf[16+1];			// LCD Display Buffer

/* pototype  section */
void init_lcd(void);			// Initial Character LCD(4-Bit Interface)
void gotolcd(unsigned char);		// Set Cursor LCD
void write_ins(unsigned char);      	// Write Instruction LCD
void write_data(unsigned char);		// Write Data LCD
void printlcd(void);			// Display Message LCD


/*--------------------------------------------
The main C function.  Program execution Here 
---------------------------------------------*/
void main(void)
{
  DDRG=0xFF;                                                    // PORTG as output
  DDRD=0xFF;                                                    // PORTD as output
  
  delay_ms(30);							// Power-on Delay
  init_lcd();							// Initial LCD
   
  while(1)
  {
    gotolcd(0);							// Set Cursor Line-1
    sprintf(lcdbuf,"ET-BASE AVR....."); // Display Line-1
    printlcd();
    gotolcd(0x40);						// Set Cursor Line-2
    sprintf(lcdbuf,"BASE ON MEGA128."); // Display Line-2
    printlcd();	
    delay_ms(1000);						// Display Delay

    gotolcd(0);							// Set Cursor Line-1
    sprintf(lcdbuf,"16 MIPS Execute."); // Display Line-1  
    printlcd();
    gotolcd(0x40);						// Set Cursor Line-2
    sprintf(lcdbuf,"BY..ETT CO.,LTD."); // Display Line-2
    printlcd();	
    delay_ms(1000);						// Display Delay
  }  
}

/*******************************/
/* Initial LCD 4-Bit Interface */
/*******************************/
void init_lcd(void)
{
  PORTD &= 0b01111111;			// Start LCD Control   EN=0  (PD7)
  delay_ms(1);				// Wait LCD Ready
  
  write_ins(0x33);			// Initial (Set DL=1 3 Time, Reset DL=0 1 Time)
  write_ins(0x32);  
  write_ins(0x28);  			// Function Set (DL=0 4-Bit,N=1 2 Line,F=0 5X7)
  write_ins(0x0C);  			// Display on/off Control (Entry Display,Cursor off,Cursor not Blink)
  write_ins(0x06);  			// Entry Mode Set (I/D=1 Increment,S=0 Cursor Shift)
  write_ins(0x01);  			// Clear Display  (Clear Display,Set DD RAM Address=0)
  delay_ms(1);			        // Wait Initial Complete
  return;
}

/******************/
/* Set LCD Cursor */
/******************/
void gotolcd(unsigned char i)
{
  i |= 0x80;				// Set DD-RAM Address Command
  write_ins(i);  
  return;
}

/****************************/
/* Write Instruction to LCD */
/****************************/
void write_ins(unsigned char i)
{
  PORTG  &= 0b11101111;			// Instruction Select RS=0(PG4)

  PORTG &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  PORTG |= (i>>4) & 0x0F;       	// Strobe High Nibble Command
  PORTD |= 0b10000000;    		// Enable ON    EN=1(PD7)
  delay_ms(1);
  PORTD &= 0b01111111;   		// Enable OFF   EN=0(PD7)
  
  PORTG &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  PORTG |= i & 0x0F;  			// Strobe Low Nibble Command
  PORTD |= 0b10000000;    		// Enable ON    EN=1(PD7)
  delay_ms(1);
  PORTD &= 0b01111111;   		// Enable OFF   EN=0(PD7)
  delay_ms(1);				// Wait LCD Busy

  return;
}


/****************************/
/* Write Data(ASCII) to LCD */
/****************************/
void write_data(unsigned char i)
{
  PORTG  |= 0b00010000;			// Instruction Select  RS=1(PG4)

  PORTG &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  PORTG |= (i>>4) & 0x0F;       	// Strobe High Nibble Command
  PORTD |= 0b10000000;    		// Enable ON    EN=1(PD7)
  delay_ms(1);
  PORTD &= 0b01111111;   		// Enable OFF   EN=0(PD7)
  
  PORTG &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  PORTG |= i & 0x0F;  			// Strobe Low Nibble Command
  PORTD |= 0b10000000;    		// Enable ON    EN=1(PD7)
  delay_ms(1);
  PORTD &= 0b01111111;   		// Enable OFF   EN=0(PD7) 
  delay_ms(1);				// Wait LCD Busy

  return;						

}

/****************************/
/* Print Data(ASCII) to LCD */
/****************************/
void printlcd(void)
{
  char *p;
 
  p = lcdbuf;
 
  do 					// Get ASCII & Write to LCD Until null
  {
    write_data(*p); 			// Write ASCII to LCD
    p++;				// Next ASCII
  }
  while(*p != '\0');		        // End of ASCII (null)
 
  return;
}



