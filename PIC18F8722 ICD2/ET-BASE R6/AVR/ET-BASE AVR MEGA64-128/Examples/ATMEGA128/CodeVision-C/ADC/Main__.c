//************************************************/;
//* Hardware    : ET-BASE AVR MEGA64/128         */;
//* CPU         : ATMEL-ATmega128                */;
//* X-TAL       : 16.00 MHz                      */;
//* Complier    : CodeVisionAVR V1.24.7e         */;
//* Last Update : 9-03-2006 (ETT CO.,LTD)        */;
//*             : WWW.ETT.CO.TH                  */;
//* Description : Demo ADC Channel 0             */;
//*             : Setup RS232 = 9600,N,8,1       */;
//************************************************/;
//* CodeVisionAVR Complier Option Setting        */;
//* Chip type           : ATmega128              */;
//* Program type        : Application            */;
//* Clock frequency     : 16.000000 MHz          */;
//* Memory model        : Small                  */;
//* External SRAM size  : 0                      */;
//* Data Stack size     : 1024                   */;
//************************************************/;
//Display Result to Serial Port UART0(9600 bps)

#include <mega128.h>                                            // ATmega128 MCU
#include <stdio.h>                                              // Standard Input/Output functions
#define ADC_VREF_TYPE 0xC0                                      // ADC Used Internal Reference

/* pototype  section */
unsigned int read_adc(unsigned char adc_input);                 // Read ADC Result

void main(void)
{
  unsigned int val;                                             // ADC Result
  // USART initialization
  // Communication Parameters: 8 Data, 1 Stop, No Parity
  // USART Receiver: On
  // USART Transmitter: On
  // USART Mode: Asynchronous
  // USART Baud rate: 9600
  UCSR0A=0x00;
  UCSR0B=0x18;
  UCSR0C=0x86;
  UBRR0H=0x00;
  UBRR0L=0x67;

  // ADC initialization
  // ADC Clock frequency: 125 kHz
  // ADC Voltage Reference: Int., cap. on AREF
  // ADC High Speed Mode: Off
  // ADC Auto Trigger Source: None
  ADMUX=ADC_VREF_TYPE;
  ADCSRA=0x87;
  SFIOR&=0xEF;
  
  printf("\fHello ET-BASE AVR MEGA64/128....ADC0\n\r");  
  printf("Input DC Voltage 0-5V to PF0 For Test\n\n\r");
  
 while (1)                                                      // Loop Continue
  {
    val = read_adc(0);                                          // Read ADC0 Result
    printf("\rResult of ADC[0] = %X ",val);                     // Dispaly Result
  }
}

//*********************************/;
//* Read the AD conversion result */;
//*********************************/;

unsigned int read_adc(unsigned char adc_input)                  // Read Result ADC
{
  ADMUX=adc_input|ADC_VREF_TYPE;

  ADCSRA|=0x40;                                                 // Start the AD conversion
  
  while ((ADCSRA & 0x10)==0);                                   // Wait for the AD conversion to complete
  ADCSRA|=0x10;
  
  return ADCW;
}
