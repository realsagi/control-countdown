//************************************************/;
//* Hardware    : ET-BASE AVR MEGA64/128         */;
//* CPU         : ATMEL-ATmega128                */;
//* X-TAL       : 16.00 MHz                      */;
//* Complier    : CodeVisionAVR V1.24.7e         */;
//* Last Update : 9-03-2006 (ETT CO.,LTD)        */;
//*             : WWW.ETT.CO.TH                  */;
//* Description : Demo ADC Channel 0             */;
//*             : Setup RS232 = 9600,N,8,1       */;
//************************************************/;
//* CodeVisionAVR Complier Option Setting        */;
//* Chip type           : ATmega128              */;
//* Program type        : Application            */;
//* Clock frequency     : 16.000000 MHz          */;
//* Memory model        : Small                  */;
//* External SRAM size  : 0                      */;
//* Data Stack size     : 1024                   */;
//************************************************/;
//Display Result to Serial Port UART0(9600 bps)
// CodeVisionAVR C Compiler
// (C) 1998-2004 Pavel Haiduc, HP InfoTech S.R.L.
// I/O registers definitions for the ATmega128
#pragma used+
sfrb PINF=0;
sfrb PINE=1;
sfrb DDRE=2;
sfrb PORTE=3;
sfrb ADCL=4;
sfrb ADCH=5;
sfrw ADCW=4;      // 16 bit access
sfrb ADCSRA=6;
sfrb ADMUX=7;
sfrb ACSR=8;
sfrb UBRR0L=9;
sfrb UCSR0B=0xa;
sfrb UCSR0A=0xb;
sfrb UDR0=0xc;
sfrb SPCR=0xd;
sfrb SPSR=0xe;
sfrb SPDR=0xf;
sfrb PIND=0x10;
sfrb DDRD=0x11;
sfrb PORTD=0x12;
sfrb PINC=0x13;
sfrb DDRC=0x14;
sfrb PORTC=0x15;
sfrb PINB=0x16;
sfrb DDRB=0x17;
sfrb PORTB=0x18;
sfrb PINA=0x19;
sfrb DDRA=0x1a;
sfrb PORTA=0x1b;
sfrb EECR=0x1c;
sfrb EEDR=0x1d;
sfrb EEARL=0x1e;
sfrb EEARH=0x1f;
sfrw EEAR=0x1e;   // 16 bit access
sfrb SFIOR=0x20;
sfrb WDTCR=0x21;
sfrb OCDR=0x22;
sfrb OCR2=0x23;
sfrb TCNT2=0x24;
sfrb TCCR2=0x25;
sfrb ICR1L=0x26;
sfrb ICR1H=0x27;
sfrw ICR1=0x26;   // 16 bit access
sfrb OCR1BL=0x28;
sfrb OCR1BH=0x29;
sfrw OCR1B=0x28;  // 16 bit access
sfrb OCR1AL=0x2a;
sfrb OCR1AH=0x2b;
sfrw OCR1A=0x2a;  // 16 bit access
sfrb TCNT1L=0x2c;
sfrb TCNT1H=0x2d;
sfrw TCNT1=0x2c;  // 16 bit access
sfrb TCCR1B=0x2e;
sfrb TCCR1A=0x2f;
sfrb ASSR=0x30;
sfrb OCR0=0x31;
sfrb TCNT0=0x32;
sfrb TCCR0=0x33;
sfrb MCUCSR=0x34;
sfrb MCUCR=0x35;
sfrb TIFR=0x36;
sfrb TIMSK=0x37;
sfrb EIFR=0x38;
sfrb EIMSK=0x39;
sfrb EICRB=0x3a;
sfrb RAMPZ=0x3b;
sfrb XDIV=0x3c;
sfrb SPL=0x3d;
sfrb SPH=0x3e;
sfrb SREG=0x3f;
#pragma used-
// Interrupt vectors definitions
// CodeVisionAVR C Compiler
// (C) 1998-2003 Pavel Haiduc, HP InfoTech S.R.L.
// Prototypes for standard I/O functions
// CodeVisionAVR C Compiler
// (C) 1998-2002 Pavel Haiduc, HP InfoTech S.R.L.
// Variable length argument list macros
typedef char *va_list;
#pragma used+
char getchar(void);
void putchar(char c);
void puts(char *str);
void putsf(char flash *str);
char *gets(char *str,unsigned int len);
void printf(char flash *fmtstr,...);
void sprintf(char *str, char flash *fmtstr,...);
void vprintf (char flash * fmtstr, va_list argptr);
void vsprintf (char *str, char flash * fmtstr, va_list argptr);
signed char scanf(char flash *fmtstr,...);
signed char sscanf(char *str, char flash *fmtstr,...);
                                               #pragma used-
#pragma library stdio.lib
/* pototype  section */
unsigned int read_adc(unsigned char adc_input);                 // Read ADC Result
void main(void)
{
  unsigned int val;                                             // ADC Result
  // USART initialization
  // Communication Parameters: 8 Data, 1 Stop, No Parity
  // USART Receiver: On
  // USART Transmitter: On
  // USART Mode: Asynchronous
  // USART Baud rate: 9600
  UCSR0A=0x00;
  UCSR0B=0x18;
  (*(unsigned char *) 0x95)=0x86;
  (*(unsigned char *) 0x90)=0x00;
  UBRR0L=0x67;
  // ADC initialization
  // ADC Clock frequency: 125 kHz
  // ADC Voltage Reference: Int., cap. on AREF
  // ADC High Speed Mode: Off
  // ADC Auto Trigger Source: None
  ADMUX=0xC0                                      ;
  ADCSRA=0x87;
  SFIOR&=0xEF;
    printf("\fHello ET-BASE AVR MEGA64/128....ADC0\n\r");  
  printf("Input DC Voltage 0-5V to PF0 For Test\n\n\r");
   while (1)                                                      // Loop Continue
  {
    val = read_adc(0);                                          // Read ADC0 Result
    printf("\rResult of ADC[0] = %X ",val);                     // Dispaly Result
  }
}
//*********************************/;
//* Read the AD conversion result */;
//*********************************/;
unsigned int read_adc(unsigned char adc_input)                  // Read Result ADC
{
  ADMUX=adc_input|0xC0                                      ;
  ADCSRA|=0x40;                                                 // Start the AD conversion
    while ((ADCSRA & 0x10)==0);                                   // Wait for the AD conversion to complete
  ADCSRA|=0x10;
    return ADCW;
}
