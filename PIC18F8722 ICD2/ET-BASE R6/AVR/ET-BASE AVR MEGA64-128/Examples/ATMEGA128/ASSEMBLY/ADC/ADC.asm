;*******************************************************
;* Examples Program For "ET-BASE AVR MEGA64/128" Board *
;* Target MCU  : Atmel ATmega128                       *
;* Frequency   : X-TAL : 16 MHz                        *
;* Compiler    : AVR Studio 4.12 (AVR Assembler 2)     *
;* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)       *
;* Last Update : 9/March/2006                          *
;* Description : Demo ADC Channel 0 - Channel 7        *
;*             : Setup RS232 = 9600,N,8,1              *
;*******************************************************
;Display Result to Serial Port UART0(9600 bps)

.include "m128def.inc"                             ;ATmega128 MCU

;*******************
; Define Register
;*******************
.def		TEMP		= R16
.def		TEMP1		= R17
.def		DATA		= R18
.def		NUM_ADC		= R19

;*******************
; Main Program
;*******************
.CSEG			
			.ORG  	0 
    		RJMP  	RESET							;Reset Handle

RESET:      LDI		TEMP,LOW(RAMEND)				;Init Stack Pointer
            OUT		SPL,TEMP          	     
            LDI		TEMP,HIGH(RAMEND)
            OUT		SPH,TEMP
			
			;IN     TEMP,MCUCSR         			;Disable JTAG
		  	;ORI    TEMP,0x80
		  	;OUT    MCUCSR,TEMP         			;Write twice , Disable JTAG 
		  	;OUT    MCUCSR,TEMP  
			      	
INIT_USART:		
			LDI		TEMP,0							;Set baud rate 9600 bps					
			LDI		TEMP1,103
			STS     UBRR0H,TEMP
			OUT     UBRR0L,TEMP1 

			LDI     TEMP,(1<<RXEN0)|(1<<TXEN0)		;Enable receiver and transmitter
			OUT		UCSR0B,TEMP

			LDI     TEMP,(1<<UCSZ01)|(1<<UCSZ00)	;Set frame format : 8data,1stop bit
			STS		UCSR0C,TEMP

			LDI		TEMP,(1<<ADEN)|(7<<ADPS0)		;Enable ADC
			OUT		ADCSR,TEMP

			LDI		DATA,0X0C				    	;Clear Display
			RCALL	TX_BYTE

MAIN:		LDI		ZH,HIGH(2*ADC_TB)
			LDI		ZL,LOW(2*ADC_TB)
			RCALL	INTRO

SEND_DATA:	LDI		DATA,0X0D
			RCALL	TX_BYTE
			
			LDI		NUM_ADC,0						;Start Read Channel 0
ADC1:
			OUT		ADMUX,NUM_ADC
			SBI		ADCSR,ADSC         				;Start Conversion
			           	
			
ADC_LOOP:   SBIC    ADCSR,ADSC         				;Wait for Complete
			RJMP    ADC_LOOP	

ATOD:       IN 		TEMP1,ADCL          			;Read Low byte
            IN 		DATA,ADCH           			;Read High byte
			RCALL 	ASCII
            RCALL	TX_BYTE
			LDI		TEMP,2
			MOV		DATA,TEMP1
			SWAP	DATA
ATOD1:		ANDI	DATA,0x0F
			RCALL	ASCII
			RCALL	TX_Byte
			MOV	    DATA,TEMP1
			DEC		TEMP
			BRNE	ATOD1

			LDI     DATA,0x20						;Send space
			RCALL	TX_BYTE

			INC		NUM_ADC							;Next Channel
			CPI		NUM_ADC,8
			BRNE	ADC1
			RJMP	SEND_DATA

ASCII:		CPI		DATA,$0A
			BRLO	ASC_NUM
			SUBI	DATA,-$37
			RET

ASC_NUM:	SUBI	DATA,-$30
			RET  		

;********************
; Send text
;********************
INTRO:		LPM
			TST		R0
			BREQ	END_SUB
TX232:		SBIS	UCSR0A,UDRE0
			RJMP	TX232
			OUT		UDR0,R0
			ADIW	ZL,1
			RJMP	INTRO
END_SUB:	RET		

;********************
; Send a byte
;********************

TX_BYTE:	SBIS	UCSR0A,UDRE0
			RJMP	TX_BYTE
			OUT		UDR0,DATA
			RET

;*******************
; Get a byte
;*******************
RX_BYTE:	SBIS	UCSR0A,RXC0
			RJMP	RX_BYTE
			IN		DATA,UDR0
			RET

;*******************
; Code segment
;*******************
ADC_TB:		.DB		"ET-BASE AVR MEGA64/128 A/D Demo",0x0d,0x0a
			.DB		"Test 8 Channel Internal A/D (PF0-PF7)",0x0d,0x0a,0x0a,0x00
			
			
