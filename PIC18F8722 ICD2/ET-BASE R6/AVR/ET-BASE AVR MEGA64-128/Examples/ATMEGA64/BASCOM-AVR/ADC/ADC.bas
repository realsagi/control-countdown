'*******************************************************
'* Examples Program For "ET-BASE AVR MEGA64/128" Board *
'* Target MCU  : Atmel ATmega64                        *
'* Frequency   : X-TAL : 16 MHz                        *
'* Compiler    : BASCOM-AVR 1.11.7.9                   *
'* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)       *
'* Last Update : 9/March/2006                          *
'* Description : Demo ADC Channel 0 - Channel 7        *
'*             : Setup RS232 = 9600,N,8,1              *
'*******************************************************
'Display Result to Serial Port UART0(9600 bps)

$regfile = "m64def.dat"                                     'ATmega64
$baud = 9600                                                'Serial Port 9600 bps
$crystal = 16000000                                         'XTAL = 16 MHz

'Mcucsr.7 = 1                                               'Disable JTAG Interface
'Mcucsr.7 = 1

Config Adc = Single , Prescaler = Auto
Start Adc                                                   'Now give power to the chip
Dim W As Word , Channel As Byte

Printbin $0c                                                'Clear Display
Print "ET-BASE AVR MEGA64/128 A/D Demo"                     'Start Menu Test
Print "Test 8 Channel Internal A/D (PF0-PF7)"
Print                                                       'Blank Line

'Start Read ADC Channel
Channel = 0
Do
  W = Getadc(channel) : Print Hex(w);
  Incr Channel
   If Channel > 7 Then
      Channel = 0
      Printbin $0d                                          'Send Cariage Return
   End If
   If Channel > 0 Then
      Printbin $20                                          'Send Space
   End If
Loop
End