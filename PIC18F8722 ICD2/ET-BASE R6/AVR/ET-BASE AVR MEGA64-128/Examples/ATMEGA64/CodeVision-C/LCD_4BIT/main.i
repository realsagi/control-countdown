//**********************************************/;
//* Hardware    : ET-BASE AVR MEGA64/128       */;
//* CPU         : ATMEL-ATmega64               */;
//* X-TAL       : 16.00 MHz                    */;
//* Complier    : CodeVisionAVR V1.24.7e       */;
//* Last Update : 9-03-2006 (ETT CO.,LTD)      */;
//*             : WWW.ETT.CO.TH                */;
//* Description : Example LCD CHAR 16X2        */;
//**********************************************/;
//* CodeVisionAVR Complier Option Setting      */;
//* Chip type           : ATmega64             */;
//* Program type        : Application          */;
//* Clock frequency     : 16.000000 MHz        */;
//* Memory model        : Small                */;
//* External SRAM size  : 0                    */;
//* Data Stack size     : 1024                 */;
//**********************************************/;
//*Connect LCD Module to ET-CLCD PORT  
//*Lcdpin : D4 = PG0 , D5 = PG1 , D6 = PG2 , D7 = PG3 , EN = PD7 , RS = PG4
// CodeVisionAVR C Compiler
// (C) 1998-2004 Pavel Haiduc, HP InfoTech S.R.L.
// I/O registers definitions for the ATmega64
#pragma used+
sfrb PINF=0;
sfrb PINE=1;
sfrb DDRE=2;
sfrb PORTE=3;
sfrb ADCL=4;
sfrb ADCH=5;
sfrw ADCW=4;      // 16 bit access
sfrb ADCSRA=6;
sfrb ADMUX=7;
sfrb ACSR=8;
sfrb UBRR0L=9;
sfrb UCSR0B=0xa;
sfrb UCSR0A=0xb;
sfrb UDR0=0xc;
sfrb SPCR=0xd;
sfrb SPSR=0xe;
sfrb SPDR=0xf;
sfrb PIND=0x10;
sfrb DDRD=0x11;
sfrb PORTD=0x12;
sfrb PINC=0x13;
sfrb DDRC=0x14;
sfrb PORTC=0x15;
sfrb PINB=0x16;
sfrb DDRB=0x17;
sfrb PORTB=0x18;
sfrb PINA=0x19;
sfrb DDRA=0x1a;
sfrb PORTA=0x1b;
sfrb EECR=0x1c;
sfrb EEDR=0x1d;
sfrb EEARL=0x1e;
sfrb EEARH=0x1f;
sfrw EEAR=0x1e;   // 16 bit access
sfrb SFIOR=0x20;
sfrb WDTCR=0x21;
sfrb OCDR=0x22;
sfrb OCR2=0x23;
sfrb TCNT2=0x24;
sfrb TCCR2=0x25;
sfrb ICR1L=0x26;
sfrb ICR1H=0x27;
sfrw ICR1=0x26;   // 16 bit access
sfrb OCR1BL=0x28;
sfrb OCR1BH=0x29;
sfrw OCR1B=0x28;  // 16 bit access
sfrb OCR1AL=0x2a;
sfrb OCR1AH=0x2b;
sfrw OCR1A=0x2a;  // 16 bit access
sfrb TCNT1L=0x2c;
sfrb TCNT1H=0x2d;
sfrw TCNT1=0x2c;  // 16 bit access
sfrb TCCR1B=0x2e;
sfrb TCCR1A=0x2f;
sfrb ASSR=0x30;
sfrb OCR0=0x31;
sfrb TCNT0=0x32;
sfrb TCCR0=0x33;
sfrb MCUCSR=0x34;
sfrb MCUCR=0x35;
sfrb TIFR=0x36;
sfrb TIMSK=0x37;
sfrb EIFR=0x38;
sfrb EIMSK=0x39;
sfrb EICRB=0x3a;
sfrb XDIV=0x3c;
sfrb SPL=0x3d;
sfrb SPH=0x3e;
sfrb SREG=0x3f;
#pragma used-
// Interrupt vectors definitions
// CodeVisionAVR C Compiler
// (C) 1998-2000 Pavel Haiduc, HP InfoTech S.R.L.
#pragma used+
void delay_us(unsigned int n);
void delay_ms(unsigned int n);
#pragma used-
// CodeVisionAVR C Compiler
// (C) 1998-2003 Pavel Haiduc, HP InfoTech S.R.L.
// Prototypes for standard I/O functions
// CodeVisionAVR C Compiler
// (C) 1998-2002 Pavel Haiduc, HP InfoTech S.R.L.
// Variable length argument list macros
typedef char *va_list;
#pragma used+
char getchar(void);
void putchar(char c);
void puts(char *str);
void putsf(char flash *str);
char *gets(char *str,unsigned int len);
void printf(char flash *fmtstr,...);
void sprintf(char *str, char flash *fmtstr,...);
void vprintf (char flash * fmtstr, va_list argptr);
void vsprintf (char *str, char flash * fmtstr, va_list argptr);
signed char scanf(char flash *fmtstr,...);
signed char sscanf(char *str, char flash *fmtstr,...);
                                               #pragma used-
#pragma library stdio.lib
char lcdbuf[16+1];			// LCD Display Buffer
/* pototype  section */
void init_lcd(void);			// Initial Character LCD(4-Bit Interface)
void gotolcd(unsigned char);		// Set Cursor LCD
void write_ins(unsigned char);      	// Write Instruction LCD
void write_data(unsigned char);		// Write Data LCD
void printlcd(void);			// Display Message LCD
/*--------------------------------------------
The main C function.  Program execution Here 
---------------------------------------------*/
void main(void)
{
  (*(unsigned char *) 0x64)=0xFF;                                                    // PORTG as output
  DDRD=0xFF;                                                    // PORTD as output
    delay_ms(30);							// Power-on Delay
  init_lcd();							// Initial LCD
     while(1)
  {
    gotolcd(0);							// Set Cursor Line-1
    sprintf(lcdbuf,"ET-BASE AVR....."); // Display Line-1
    printlcd();
    gotolcd(0x40);						// Set Cursor Line-2
    sprintf(lcdbuf,"BASE ON ATMEGA64"); // Display Line-2
    printlcd();	
    delay_ms(1000);						// Display Delay
    gotolcd(0);							// Set Cursor Line-1
    sprintf(lcdbuf,"16 MIPS Execute."); // Display Line-1  
    printlcd();
    gotolcd(0x40);						// Set Cursor Line-2
    sprintf(lcdbuf,"BY..ETT CO.,LTD."); // Display Line-2
    printlcd();	
    delay_ms(1000);						// Display Delay
  }  
}
/*******************************/
/* Initial LCD 4-Bit Interface */
/*******************************/
void init_lcd(void)
{
  PORTD &= 0b01111111;			// Start LCD Control   EN=0  (PD7)
  delay_ms(1);				// Wait LCD Ready
    write_ins(0x33);			// Initial (Set DL=1 3 Time, Reset DL=0 1 Time)
  write_ins(0x32);  
  write_ins(0x28);  			// Function Set (DL=0 4-Bit,N=1 2 Line,F=0 5X7)
  write_ins(0x0C);  			// Display on/off Control (Entry Display,Cursor off,Cursor not Blink)
  write_ins(0x06);  			// Entry Mode Set (I/D=1 Increment,S=0 Cursor Shift)
  write_ins(0x01);  			// Clear Display  (Clear Display,Set DD RAM Address=0)
  delay_ms(1);			        // Wait Initial Complete
  return;
}
/******************/
/* Set LCD Cursor */
/******************/
void gotolcd(unsigned char i)
{
  i |= 0x80;				// Set DD-RAM Address Command
  write_ins(i);  
  return;
}
/****************************/
/* Write Instruction to LCD */
/****************************/
void write_ins(unsigned char i)
{
  (*(unsigned char *) 0x65)  &= 0b11101111;			// Instruction Select RS=0(PG4)
  (*(unsigned char *) 0x65) &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  (*(unsigned char *) 0x65) |= (i>>4) & 0x0F;       	// Strobe High Nibble Command
  PORTD |= 0b10000000;    		// Enable ON    EN=1(PD7)
  delay_ms(1);
  PORTD &= 0b01111111;   		// Enable OFF   EN=0(PD7)
    (*(unsigned char *) 0x65) &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  (*(unsigned char *) 0x65) |= i & 0x0F;  			// Strobe Low Nibble Command
  PORTD |= 0b10000000;    		// Enable ON    EN=1(PD7)
  delay_ms(1);
  PORTD &= 0b01111111;   		// Enable OFF   EN=0(PD7)
  delay_ms(1);				// Wait LCD Busy
  return;
}
/****************************/
/* Write Data(ASCII) to LCD */
/****************************/
void write_data(unsigned char i)
{
  (*(unsigned char *) 0x65)  |= 0b00010000;			// Instruction Select  RS=1(PG4)
  (*(unsigned char *) 0x65) &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  (*(unsigned char *) 0x65) |= (i>>4) & 0x0F;       	// Strobe High Nibble Command
  PORTD |= 0b10000000;    		// Enable ON    EN=1(PD7)
  delay_ms(1);
  PORTD &= 0b01111111;   		// Enable OFF   EN=0(PD7)
    (*(unsigned char *) 0x65) &= 0xF0;           		// Clear old LCD Data (Bit[3..0])
  (*(unsigned char *) 0x65) |= i & 0x0F;  			// Strobe Low Nibble Command
  PORTD |= 0b10000000;    		// Enable ON    EN=1(PD7)
  delay_ms(1);
  PORTD &= 0b01111111;   		// Enable OFF   EN=0(PD7) 
  delay_ms(1);				// Wait LCD Busy
  return;						
}
/****************************/
/* Print Data(ASCII) to LCD */
/****************************/
void printlcd(void)
{
  char *p;
   p = lcdbuf;
   do 					// Get ASCII & Write to LCD Until null
  {
    write_data(*p); 			// Write ASCII to LCD
    p++;				// Next ASCII
  }
  while(*p != '\0');		        // End of ASCII (null)
   return;
}
