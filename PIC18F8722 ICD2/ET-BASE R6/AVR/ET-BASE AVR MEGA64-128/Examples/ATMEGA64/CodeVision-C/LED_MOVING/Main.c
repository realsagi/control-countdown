//*********************************************************/;
//* Hardware    : ET-BASE AVR MEGA64/128                  */;
//* CPU         : ATMEL-ATmega64                          */;
//* X-TAL       : 16.00 MHz                               */;
//* Complier    : CodeVisionAVR V1.24.7e                  */;
//* Last Update : 9-03-2006 (ETT CO.,LTD)                 */;
//*             : WWW.ETT.CO.TH                           */;
//* Description : Example LED Moving on PA,PB,PC,PD,PE,PF */;
//*********************************************************/;
//* CodeVisionAVR Complier Option Setting                 */;
//* Chip type           : ATmega64                        */;
//* Program type        : Application                     */;
//* Clock frequency     : 16.000000 MHz                   */;
//* Memory model        : Small                           */;
//* External SRAM size  : 0                               */;
//* Data Stack size     : 1024                            */;
//*********************************************************/;
//Used ET-TEST 10P/OUT
//Connect PORT to LED

#include <mega64.h>                                 // ATmega64 MCU
#include <delay.h>                                  // Delay functions

void main(void)
{
  unsigned char LED; 	   		            // LED Output Status Buffer
  
  DDRA = 0xFF; 		   		            // PortA = Output
  DDRB = 0xFF; 		   		            // PortB = Output
  DDRC = 0xFF; 		   		            // PortC = Output
  DDRD = 0xFF; 		   		            // PortD = Output
  DDRE = 0xFF; 		   		            // PortE = Output
  DDRF = 0xFF; 		   		            // PortF = Output
  
  // Loop Test Output Port 
  while(1)													// Loop Continue
  {    	
    // Shift Left
    for (LED = 0x01; LED < 0x80; LED <<= 1)	    // Shift Left (Right <- Left)
    { 
      				 
      PORTA = ~LED;                                 // Active Output (Toggle ON LED)
      PORTB = ~LED;
      PORTC = ~LED;
      PORTD = ~LED;
      PORTE = ~LED;
      PORTF = ~LED;  
      delay_ms(200);				    // Display Delay	    
    }  	 
    
    // Shift Right
    for (LED = 0x80; LED > 0x01; LED >>= 1)	    // Shift Right (Right -> Left)
    { 
      PORTA = ~LED;                                 // Active Output (Toggle ON LED)
      PORTB = ~LED;
      PORTC = ~LED;
      PORTD = ~LED;
      PORTE = ~LED;
      PORTF = ~LED;    
      delay_ms(200);				    // Display Delay
    }  	  
	    
  } 
   
}	

