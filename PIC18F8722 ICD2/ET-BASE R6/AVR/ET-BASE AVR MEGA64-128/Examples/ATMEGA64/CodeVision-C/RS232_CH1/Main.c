//***************************************************/;
//* Hardware    : ET-BASE AVR MEGA64/128            */;
//* CPU         : ATMEL-ATmega64                    */;
//* X-TAL       : 16.00 MHz                         */;
//* Complier    : CodeVisionAVR V1.24.7e            */;
//* Last Update : 9-03-2006 (ETT CO.,LTD)           */;
//*             : WWW.ETT.CO.TH                     */;
//* Description : RS232 Channel 1 Demo Send/Receive */;
//*             : Setup RS232 = 9600,N,8,1          */;
//***************************************************/;
//* CodeVisionAVR Complier Option Setting           */;
//* Chip type           : ATmega64                  */;
//* Program type        : Application               */;
//* Clock frequency     : 16.000000 MHz             */;
//* Memory model        : Small                     */;
//* External SRAM size  : 0                         */;
//* Data Stack size     : 1024                      */;
//***************************************************/;
//Display Result to Serial Port UART1(9600 bps) 

#include <mega64.h>                                             // ATmega64 MCU
#include <stdio.h>                                              // Standard Input/Output functions

#define RXB8 1
#define TXB8 0
#define UPE 2
#define OVR 3
#define FE 4
#define UDRE 5
#define RXC 7

#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<UPE)
#define DATA_OVERRUN (1<<OVR)
#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)

// Get a character from the USART1 Receiver
#pragma used+
char getchar1(void)
{
char status,data;
while (1)
      {
      while (((status=UCSR1A) & RX_COMPLETE)==0);
      data=UDR1;
      if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
         return data;
      };
}
#pragma used-

// Write a character to the USART1 Transmitter
#pragma used+
void putchar1(char c)
{
while ((UCSR1A & DATA_REGISTER_EMPTY)==0);
UDR1=c;
}
#pragma used-


void main(void)
{
  char uart_data;                                                // RS232 Buffer

// USART1 initialization
// Communication Parameters: 8 Data, 1 Stop, No Parity
// USART1 Receiver: On
// USART1 Transmitter: On
// USART1 Mode: Asynchronous
// USART1 Baud rate: 9600
UCSR1A=0x00;
UCSR1B=0x18;
UCSR1C=0x06;
UBRR1H=0x00;
UBRR1L=0x67;

  while (1)                                                      // Loop Continue
  { 
    uart_data = getchar1();                                      // Wait RS232 Receive
    putchar1(uart_data);                                         // Echo data
  }
}
