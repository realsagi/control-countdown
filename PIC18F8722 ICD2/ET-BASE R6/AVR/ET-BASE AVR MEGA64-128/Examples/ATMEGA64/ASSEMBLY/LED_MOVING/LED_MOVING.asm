;*********************************************************
;* Examples Program For "ET-BASE AVR MEGA64/128" Board   *
;* Target MCU  : Atmel ATmega64                          *
;* Frequency   : X-TAL : 16 MHz                          *
;* Compiler    : AVR Studio 4.12 (AVR Assembler 2)       *
;* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)         *
;* Last Update : 9/March/2006                            *
;* Description : Example LED Moving on PA,PB,PC,PD,PE,PF *
;*********************************************************
;Used ET-TEST 10P/OUT
;Connect PORT to LED


.include "m64def.inc"			               ;ATmega64 MCU

;************************
; Define Register
;************************
.def		TEMP		= R16
.def		COUNTER1  	= R17
.def		COUNTER2	= R18
.def		COUNTER3	= R19

;*******************
; Main Program
;*******************
.CSEG
            	.ORG   	0 
    			RJMP  	RESET					;Reset Handle

RESET:      	LDI		TEMP,LOW(RAMEND)		;Initial Stack Pointer 
            	OUT		SPL,TEMP          		    
            	LDI		TEMP,HIGH(RAMEND)
            	OUT		SPH,TEMP
			   
				SER		TEMP
				OUT		DDRA,TEMP				;Config Porta as output
				OUT		DDRB,TEMP				;Config Portb as output
                OUT		DDRC,TEMP				;Config Portc as output
				OUT		DDRD,TEMP				;Config Portd as output
				OUT		DDRE,TEMP				;Config Porte as output
				STS		DDRF,TEMP				;Config Portf as output

				LDI		TEMP,0XFE				; 11111110

ROTATE_LEFT:	OUT	    PORTA,TEMP              ; Out Port
                OUT	    PORTB,TEMP
				OUT	    PORTC,TEMP
				OUT	    PORTD,TEMP
				OUT	    PORTE,TEMP
                STS	    PORTF,TEMP
				RCALL	DELAY_200ms
				CPI		TEMP,0x7F				; 01111111
				BREQ	ROTATE_RIGHT
				SEC								; cy = 1
				ROL		TEMP					; cy <- bit7...bit0 <- cy
				RJMP	ROTATE_LEFT
					 
ROTATE_RIGHT:	OUT		PORTA,TEMP              ; Out Port
				OUT	    PORTB,TEMP
				OUT	    PORTC,TEMP
				OUT	    PORTD,TEMP
				OUT	    PORTE,TEMP
                STS	    PORTF,TEMP
				RCALL	DELAY_200mS					
				CPI		TEMP,0xFE				; 11111110
				BREQ	ROTATE_LEFT
				SEC								; cy = 1
				ROR		TEMP				    ; cy -> cit7...bit0 -> cy
				RJMP	ROTATE_RIGHT

;*********************************
; Delay time
;*********************************
DELAY_1ms:		PUSH	COUNTER1
				PUSH	COUNTER2
				LDI		COUNTER1,16
DELAY_1ms_1: 
				LDI 	COUNTER2,250
DELAY_1ms_2: 
				NOP
				DEC 	COUNTER2
            	BRNE   	DELAY_1ms_2
            	DEC    	COUNTER1
            	BRNE   	DELAY_1ms_1
            	POP		COUNTER2
				POP		COUNTER1
				RET          

DELAY_15ms:		PUSH	COUNTER1
				PUSH	COUNTER2
				LDI		COUNTER1,240
DELAY_15ms_1: 
				LDI 	COUNTER2,250
DELAY_15ms_2: 
				NOP
				DEC 	COUNTER2
            	BRNE   	DELAY_15ms_2
            	DEC    	COUNTER1
            	BRNE   	DELAY_15ms_1
            	POP		COUNTER2
				POP		COUNTER1
				RET          

DELAY_200ms:	
				PUSH	COUNTER3
				LDI		COUNTER3,200
DELAY_200ms_1:	
				RCALL	DELAY_1ms
				DEC		COUNTER3
				BRNE	DELAY_200ms_1
				POP		COUNTER3
				RET

DELAY_1s:		PUSH	COUNTER3
				LDI		COUNTER3,66
DELAY_1s_1:		RCALL	DELAY_15ms
				DEC		COUNTER3
				BRNE	DELAY_1s_1
				POP		COUNTER3
				RET
           	 
           	 
