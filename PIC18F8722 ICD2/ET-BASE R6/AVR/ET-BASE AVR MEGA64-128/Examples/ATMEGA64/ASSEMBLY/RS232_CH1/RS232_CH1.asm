;*******************************************************
;* Examples Program For "ET-BASE AVR MEGA64/128" Board *
;* Target MCU  : Atmel ATmega64                        *
;* Frequency   : X-TAL : 16 MHz                        *
;* Compiler    : AVR Studio 4.12 (AVR Assembler 2)     *
;* Create By   : ADISAK CHOOCHAN (WWW.ETT.CO.TH)       *
;* Last Update : 9/March/2006                          *
;* Description : RS232 Channel 1 Demo Send/Receive     *
;*             : Setup RS232 = 9600,N,8,1              *
;*******************************************************
;Display Result to Serial Port UART1(9600 bps)

.include "m64def.inc"                               ;ATmega64 MCU

;*******************
; Define Register
;*******************
.def		TEMP		= R16
.def		DATA		= R17

;*******************
; Main Program
;*******************
.CSEG			
			.ORG  	0 
    		RJMP  	RESET							;Reset Handle

RESET:      LDI		TEMP,LOW(RAMEND)				;Init Stack Pointer
            OUT		SPL,TEMP          	     
            LDI		TEMP,HIGH(RAMEND)
            OUT		SPH,TEMP  
			      	
INIT_USART:		
			LDI		TEMP,0							;Set baud rate 9600 bps	
			STS     UBRR1H,TEMP			
			LDI		TEMP,103
			STS     UBRR1L,TEMP 

			LDI     TEMP,(1<<RXEN1)|(1<<TXEN1)		;Enable receiver and transmitter	
			STS		UCSR1B,TEMP

			LDI     TEMP,(1<<UCSZ11)|(1<<UCSZ10)	;Set frame format : 8data,1stop bit
			STS		UCSR1C,TEMP

			LDI		DATA,0X0C						;Clear Display
			RCALL	TX_BYTE

RS232:		LDI		ZH,HIGH(2*RS232TB)
			LDI		ZL,LOW(2*RS232TB)
			RCALL	INTRO

SUB_RS232:	RCALL	RX_BYTE
			RCALL	TX_BYTE
			RJMP	SUB_RS232

;*******************
; Send text
;*******************
INTRO:		LPM
			TST		R0
			BREQ	END_SUB
TX232:		LDS		TEMP,UCSR1A
			SBRS	TEMP,UDRE1
			RJMP	TX232
			STS		UDR1,R0
			ADIW	ZL,1
			RJMP	INTRO
END_SUB:	RET		

;*******************
; Send a byte
;*******************

TX_BYTE:	LDS     TEMP,UCSR1A						;Wait for empty transmit buffer
			SBRS	TEMP,UDRE1
			RJMP	TX_BYTE
			STS		UDR1,DATA						;Put data into buffer,sends the data
			RET

;*******************
; Get a byte
;*******************
RX_BYTE:	LDS		TEMP,UCSR1A						;Wait for data to be received
			SBRS	TEMP,RXC1
			RJMP	RX_BYTE
			LDS		DATA,UDR1						;Get and return received data from buffer
			RET

;*******************
; Text segment
;*******************
RS232TB:	.DB	"ET-BASE AVR MEGA64/128 Test RS232 Channel 1 ",0x0a,0x0d
			.DB	"Please enter any key to echo.",0x0a,0x0d,0




