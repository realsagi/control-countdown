;/******************************************/;
;/* Hardware    : ET-BASE AVR (ATmega8535) */;
;/* CPU 		: ATMEL-ATMEGA8535         */;
;/* X-TAL       : 8.00 MHz                 */;
;/* Filename    : TEST_LED.ASM             */;
;/* Assembler	: AVR Studio 4.12          */;
;/* Last Update : 9-12-2005 (ETT CO.,LTD)  */;
;/*             : WWW.ETT.CO.TH            */;
;/* Description	: Selftest LED (PB0) Blink */;
;/*             : Set Jumper PB0/LED = LED */;
;*******************************************/;
;
.include 		"m8535def.inc"				; ATmega8535

				.org   	$000 
    			rjmp  reset					; Reset Handle

;/************************
; Define Register
;/************************
.def			counter1  	= r16
.def			counter2	= r17
.def			counter3	= r18
.def			temp		= r19
.def			temp1		= r22

;/********************
; Define I/O Port,Pin
;/********************
.equ			LED			= 0b00000001	; Bit LED (PB0)

;/*************
; Main Program
;/*************
reset:      	ldi		temp,low(RAMEND)	;Init Stack Pointer 
            	out		SPL,temp          		    
            	ldi		temp,high(RAMEND)
            	out		SPH,temp   

Test_LED:		ser		temp				; Port Direction = Output
				out 	DDRB,temp			; PB0 = Output				

Test_L:			sbr		temp,LED			; OFF LED				
				out 	PORTB,temp			
				rcall	delay250ms

				cbr		temp,LED			; ON LED				
				out		PORTB,temp			
				rcall	delay250ms
				rjmp	Test_L		


;/******************/;
;/* Delay time 1mS */;
;/******************/;
;
delay1ms:		ldi    	counter1,8			; Delay 1mS Counter
delay1ms_1: 	ldi    	counter2,250
delay1ms_2: 	nop
				dec		counter2
            	brne   	delay1ms_2
            	dec 	counter1
            	brne   	delay1ms_1
				ret          

;/********************/;
;/* Delay time 250mS */;
;/********************/;
;
delay250ms:		push	counter3			; Delay 250mS
				ldi		counter3,250
delay250ms_1:	rcall	delay1ms
				dec		counter3
				brne	delay250ms_1
				pop		counter3
				ret	
           	 
