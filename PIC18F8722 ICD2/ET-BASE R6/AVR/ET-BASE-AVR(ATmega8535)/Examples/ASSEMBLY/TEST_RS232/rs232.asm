;/******************************************/;
;/* Hardware    : ET-BASE AVR (ATmega8535) */;
;/* CPU 		: ATMEL-ATMEGA8535         */;
;/* X-TAL       : 8.00 MHz                 */;
;/* Filename    : RS232.ASM                */;
;/* Assembler	: AVR Studio 4.12          */;
;/* Last Update : 9-12-2005 (ETT CO.,LTD)  */;
;/*             : WWW.ETT.CO.TH            */;
;/* Description	: Test Send/Receive RS232  */;
;/*             : Setup RS232 = 9600,N,8,1 */;
;*******************************************/;
;
.include 			"m8535def.inc"								; ATmega8535

					.org   	$000 
    				rjmp  	reset								; Reset Handle

;/*******************/;
;/* Define Register */;
;/*******************/;
.def				temp_byte		= r16
.def				rs232_data_byte	= r17

;/********************************/;
;/* Define New I/O Register Name */;
;/* I/O 90S8535 = I/O ATMEGA8535 */;
;/********************************/;		
.equ				usr		 		=	ucsra			
.equ				ucr				=	ucsrb

;/****************/;
;/* Main Program */;
;/****************/;
;
reset:      		ldi		temp_byte,low(RAMEND)				; Init Stack Pointer 
            		out		spl,temp_byte          				    
            		ldi		temp_byte,high(RAMEND)
            		out		sph,temp_byte
					;																			
					;
					;/* initial rs232 */;					
					sbi		ucr,txen							; Enable transmit
					sbi		ucr,rxen							; Enable receive 
					ldi		temp_byte,51						; Baudrate = 9600 bps
					out		ubrrl,temp_byte
					;
new_line:			ldi		ZH,high(2*message1)					; Display Start Messsge
					ldi		ZL,low(2*message1)
					rcall	display_string												
					;
					;/* receive & echo rs232 data */;								
loop:				rcall	rx_byte 							; Wait RS232 Data
					rcall	tx_byte								; Echo RS232 Data
					cpi		rs232_data_byte,0x0d				; Enter Check
					brne	loop
					ldi		rs232_data_byte,0x0a				; Add Line Feed
					rcall	tx_byte
					rjmp	loop
				    
	
;/***************/;
;/* Send string */;
;/***************/;
;
display_string:		lpm											; Display String Until 0x00
					tst		r0
					breq	display_st1
					mov		rs232_data_byte,r0					
					rcall	tx_byte
					adiw	ZL,1
					rjmp	display_string
display_st1:		ret		

;/*********************/;
;/* Send rs232 1 Byte */;
;/*********************/;
;
tx_byte:			sbis	usr,udre							; Send 1 Byte
					rjmp	tx_byte
					out		udr,rs232_data_byte
					ret

;/*********************/;
;/* Wait rs232 1 Byte */;
;/*********************/;
;
rx_byte:			sbis	usr,rxc								; Wait 1 Byte
					rjmp	rx_byte
					in		rs232_data_byte,udr
					ret


;/******************/;
;/* String Display */;
;/******************/;
message1:			.db		"Hello From ET-BASE AVR(ATmega8535)",0x0d,0x0a
					.db		"Test RS232 Receive  & Transmit",0x0d,0x0a
					.db		"Press Anykey For Test =  ",0x00




 	 
