;/*******************************************/;
;/* Hardware    : ET-BASE AVR (ATmega8535)  */;
;/* CPU 		: ATMEL-ATMEGA8535          */;
;/* X-TAL       : 8.00 MHz                  */;
;/* Filename    : TESTPORT.ASM              */;
;/* Assembler	: AVR Studio 4.12           */;
;/* Last Update : 9-12-2005 (ETT CO.,LTD)   */;
;/*             : WWW.ETT.CO.TH             */;
;/* Description	: Test Output Port Sink LED */;
;/*             : Moving LED on PA,PB,PC,PD */;
;********************************************/;
;
.include 		"m8535def.inc"				; ATmega8535

				.org   	$000 
    			rjmp  reset					; Reset Handle

;/************************
; Define Register
;/************************
.def			counter1  	= r16
.def			counter2	= r17
.def			counter3	= r18
.def			temp		= r19
.def			temp1		= r22

;/********************
; Define I/O Port,Pin
;/********************
.equ		LED		= 1

;/*************
; Main Program
;/*************
reset:      	ldi	temp,low(RAMEND)		; Init Stack Pointer 
            	out	SPL,temp          		    
            	ldi	temp,high(RAMEND)
            	out	SPH,temp   

Test_LED:		ser		temp				; 0xFF (Output)
				out		DDRA,temp			; PA = Output
				out 	DDRB,temp			; PB = Output
				out 	DDRC,temp			; PC = Output
				out 	DDRD,temp			; PD = Output

loop_main:		clc                         ; Carry = 0
				ser		temp				; Data Output = 11111111
                ;
loop_out:		out		PORTA,temp
				out 	PORTB,temp
				out 	PORTC,temp
				out 	PORTD,temp
				rcall	delay250ms
				rol     temp				; Rotate Left				
				rjmp    loop_out
			
;/******************/;
;/* Delay time 1mS */;
;/******************/;
;
delay1ms:		ldi    	counter1,8			; Delay 1mS Counter
delay1ms_1: 	ldi    	counter2,250
delay1ms_2: 	nop
				dec		counter2
            	brne   	delay1ms_2
            	dec 	counter1
            	brne   	delay1ms_1
				ret          

;/********************/;
;/* Delay time 250mS */;
;/********************/;
;
delay250ms:		push	counter3			; Delay 250mS
				ldi		counter3,250
delay250ms_1:	rcall	delay1ms
				dec		counter3
				brne	delay250ms_1
				pop		counter3
				ret	
           	 
