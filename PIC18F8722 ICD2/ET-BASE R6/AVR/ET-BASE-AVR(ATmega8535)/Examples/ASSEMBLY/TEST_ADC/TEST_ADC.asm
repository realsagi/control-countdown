;/******************************************/;
;/* Hardware    : ET-BASE AVR (ATmega8535) */;
;/* CPU 		: ATMEL-ATMEGA8535         */;
;/* X-TAL       : 8.00 MHz                 */;
;/* Filename    : TEST_ADC.ASM             */;
;/* Assembler	: AVR Studio 4.12          */;
;/* Last Update : 9-12-2005 (ETT CO.,LTD)  */;
;/*             : WWW.ETT.CO.TH            */;
;/* Description	: Read ADC & Show on RS232 */;
;/*             : Setup Rs232 = 9600,N,8,1 */;
;*******************************************/;
;
.include 			"m8535def.inc"								; ATmega8535

					.org   	$000 
    				rjmp  	reset								; Reset Handle

;/*******************/;
;/* Define Register */;
;/*******************/;
.def				temp	= r16
.def				data	= r17

;/********************************/;
;/* Define New I/O Register Name */;
;/* I/O 90S8535 = I/O ATMEGA8535 */;
;/********************************/;		
.equ				usr		=	ucsra			
.equ				ucr		=	ucsrb

;/****************/;
;/* Main Program */;
;/****************/;
;
reset:      		ldi		temp,low(RAMEND)					; Init Stack Pointer 
            		out		spl,temp          				    
            		ldi		temp,high(RAMEND)
            		out		sph,temp
					;																			
					;
					;/* initial rs232 9600,N,8,1 */;					
					sbi		ucr,txen							; Enable transmit
					sbi		ucr,rxen							; Enable receive 
					ldi		temp,51	 							; Baudrate = 9600 bps
					out		ubrrl,temp
					;
new_line:			ldi		ZH,high(2*message1)					; Display Start Messsge
					ldi		ZL,low(2*message1)
					rcall	display_string												
					;
					clr		temp								; Port Direction = Input
					out		DDRA,temp								

					in		temp,SFIOR							; ADC Mode = Free Running
					andi	temp,0b00011111						
					out		SFIOR,temp
					;
					;/* Read ADC & Display to RS232 */;								
loop_adc:			ldi		ZH,high(2*message2)					; Display Start Messsge
					ldi		ZL,low(2*message2)
					rcall	display_string	
					;
					;/* Initial ADC0 = 10 Bit Result Continue */;					
					;/* Internal Vref=2.56V,Shift Left Result */					
					clr		temp								; Stop ADC (Reset ADC Select)
					out		ADCSR,temp
					ldi		temp,0b11000000						; ADC0 Single End(110+00000)
					out     ADMUX,temp
					;																						
					ldi 	temp,(1<<ADPS2)|(ADPS1)|(ADPS0) 	
            		out 	ADCSR,temp 							; Set Sampling Rate
            		sbi 	ADCSR,ADEN         					; Enable ADC	    			
   					sbi 	ADCSR,ADSC         					; Start Conversion 
					;
wait_ad0:			sbic	ADCSR,ADSC
					rjmp    wait_ad0
  					;
					rcall	show_adc							; Read & Show ADC Result				
					ldi		data,0x20							; Send Space
					rcall	tx_byte					
					;
					;/* Initial ADC1 = 10 Bit Result Continue */;					
					;/* Internal Vref=2.56V,Shift Left Result */
					clr		temp								; Stop ADC (Reset ADC Select)
					out		ADCSR,temp
					ldi		temp,0b11000001						; ADC1 Single End(110+00001)
					out     ADMUX,temp
					;																	
					ldi 	temp,(1<<ADPS2)|(ADPS1)|(ADPS0) 	
            		out 	ADCSR,temp 							; Set Sampling Rate
            		sbi 	ADCSR,ADEN         					; Enable ADC	    			
   					sbi 	ADCSR,ADSC         					; Start Conversion 
					;
wait_ad1:			sbic	ADCSR,ADSC
					rjmp    wait_ad1
  					;
					rcall	show_adc							; Read & Show ADC Result				
					ldi		data,0x20							; Send Space
					rcall	tx_byte								
					;
					;/* Initial ADC2 = 10 Bit Result Continue */;					
					;/* Internal Vref=2.56V,Shift Left Result */
					clr		temp								; Stop ADC (Reset ADC Select)
					out		ADCSR,temp
					ldi		temp,0b11000010						; ADC2 Single End(110+00010)
					out     ADMUX,temp
					;																	
					ldi 	temp,(1<<ADPS2)|(ADPS1)|(ADPS0) 	
            		out 	ADCSR,temp 							; Set Sampling Rate
            		sbi 	ADCSR,ADEN         					; Enable ADC	    			
   					sbi 	ADCSR,ADSC         					; Start Conversion 
					;
wait_ad2:			sbic	ADCSR,ADSC
					rjmp    wait_ad2
  					;
					rcall	show_adc							; Read & Show ADC Result				
					ldi		data,0x20							; Send Space
					rcall	tx_byte								
					;
					;/* Initial ADC3 = 10 Bit Result Continue */;					
					;/* Internal Vref=2.56V,Shift Left Result */
					clr		temp								; Stop ADC (Reset ADC Select)
					out		ADCSR,temp
					ldi		temp,0b11000011						; ADC3 Single End(110+00011)
					out     ADMUX,temp
					;																	
					ldi 	temp,(1<<ADPS2)|(ADPS1)|(ADPS0) 	
            		out 	ADCSR,temp 							; Set Sampling Rate
            		sbi 	ADCSR,ADEN         					; Enable ADC	    			
   					sbi 	ADCSR,ADSC         					; Start Conversion 
					;
wait_ad3:			sbic	ADCSR,ADSC
					rjmp    wait_ad3
  					;
					rcall	show_adc							; Read & Show ADC Result				
					ldi		data,0x20							; Send Space
					rcall	tx_byte								
					;
					;/* Initial ADC4 = 10 Bit Result Continue */;					
					;/* Internal Vref=2.56V,Shift Left Result */
					clr		temp								; Stop ADC (Reset ADC Select)
					out		ADCSR,temp
					ldi		temp,0b11000100						; ADC4 Single End(110+00100)
					out     ADMUX,temp
					;																	
					ldi 	temp,(1<<ADPS2)|(ADPS1)|(ADPS0) 	
            		out 	ADCSR,temp 							; Set Sampling Rate
            		sbi 	ADCSR,ADEN         					; Enable ADC	    			
   					sbi 	ADCSR,ADSC         					; Start Conversion 
					;
wait_ad4:			sbic	ADCSR,ADSC
					rjmp    wait_ad4
  					;
					rcall	show_adc							; Read & Show ADC Result				
					ldi		data,0x20							; Send Space
					rcall	tx_byte								
					;
					;/* Initial ADC5 = 10 Bit Result Continue */;
					;/* Internal Vref=2.56V,Shift Left Result */	
					clr		temp								; Stop ADC (Reset ADC Select)
					out		ADCSR,temp				
					ldi		temp,0b11000101						; ADC5 Single End(110+00101)
					out     ADMUX,temp
					;																	
					ldi 	temp,(1<<ADPS2)|(ADPS1)|(ADPS0) 	
            		out 	ADCSR,temp 							; Set Sampling Rate
            		sbi 	ADCSR,ADEN         					; Enable ADC	    			
   					sbi 	ADCSR,ADSC         					; Start Conversion 
					;
wait_ad5:			sbic	ADCSR,ADSC
					rjmp    wait_ad5
  					;
					rcall	show_adc							; Read & Show ADC Result				
					ldi		data,0x20							; Send Space
					rcall	tx_byte								
					;
					;/* Initial ADC6 = 10 Bit Result Continue */;
					;/* Internal Vref=2.56V,Shift Left Result */
					clr		temp								; Stop ADC (Reset ADC Select)
					out		ADCSR,temp					
					ldi		temp,0b11000110						; ADC3 Single End(110+00110)
					out     ADMUX,temp
					;																	
					ldi 	temp,(1<<ADPS2)|(ADPS1)|(ADPS0) 	
            		out 	ADCSR,temp 							; Set Sampling Rate
            		sbi 	ADCSR,ADEN         					; Enable ADC	    			
   					sbi 	ADCSR,ADSC         					; Start Conversion 
					;
wait_ad6:			sbic	ADCSR,ADSC
					rjmp    wait_ad6
  					;
					rcall	show_adc							; Read & Show ADC Result				
					ldi		data,0x20							; Send Space
					rcall	tx_byte								
					;
					;/* Initial ADC7 = 10 Bit Result Continue */;
					;/* Internal Vref=2.56V,Shift Left Result */;					
					clr		temp								; Stop ADC (Reset ADC Select)
					out		ADCSR,temp
					ldi		temp,0b11000111						; ADC7 Single End(110+00111)
					out     ADMUX,temp
					;																	
					ldi 	temp,(1<<ADPS2)|(ADPS1)|(ADPS0) 	
            		out 	ADCSR,temp 							; Set Sampling Rate
            		sbi 	ADCSR,ADEN         					; Enable ADC	    			
   					sbi 	ADCSR,ADSC         					; Start Conversion 
					;
wait_ad7:			sbic	ADCSR,ADSC
					rjmp    wait_ad7
  					;
					rcall	show_adc							; Read & Show ADC Result				
					ldi		data,0x0d							; Send Enter
					rcall	tx_byte								
					rjmp	loop_adc


;/**************************/;
;/* Read & Show ADC Result */;
;/**************************/;
;
show_adc:			in      temp,ADCL          					; Read ADC Low Byte
            		in      data,ADCH           				; Read ADC High Byte
					andi	data,0x03							; High Byte Result (D9,D8)
					rcall 	hex_ascii							; Convert to ASCII		
            		rcall	tx_byte								; Send High Byte
					;
					mov		data,temp							; ADC Low Byte
					swap	data
					andi	data,0x0F
					rcall	hex_ascii			
					rcall	tx_byte								; Send Result (D7..D4)
					;
					mov		data,temp
					andi	data,0x0F
					rcall	hex_ascii			
					rcall	tx_byte								; Send Result (D3..D0)
					ret
													
;/************************/;
;/* Convert HEX to ASCII */;
;/* Input  : data(HEX)   */;
;/* Output : data(ASCII) */;
;/************************/;
;
hex_ascii:			cpi		data,0x0a							; Convero HEX to ASCII
					brlo	hex_asc1
					;
					subi	data,-0x37							; Convert A-F to 'A'-'F'
					rjmp	hex_asc2
					;
hex_asc1:			subi	data,-0x30							; Convert 0-9 to '0'-'9'
hex_asc2:			ret 
					     
				  	
;/***************/;
;/* Send string */;
;/***************/;
;
display_string:		lpm											; Display String Until 0x00
					tst		r0
					breq	display_st1
					mov		data,r0					
					rcall	tx_byte
					adiw	ZL,1
					rjmp	display_string
display_st1:		ret		

;/*********************/;
;/* Send rs232 1 Byte */;
;/* Input : data      */;
;/*********************/;
;
tx_byte:			sbis	usr,udre							; Send 1 Byte
					rjmp	tx_byte
					out		udr,data
					ret

;/*********************/;
;/* Wait rs232 1 Byte */;
;/* Output : data     */;
;/*********************/;
;
rx_byte:			sbis	usr,rxc								; Wait 1 Byte
					rjmp	rx_byte
					in		data,udr
					ret


;/******************/;
;/* String Display */;
;/******************/;
message1:			.db		"ET-BASE AVR(ATmega8535) A/D Demo",0x0d,0x0a
					.db		"Test 8 Channel Internal A/D (PA)",0x0d,0x0a,0x0a,0x00

message2:			.db		"ADC[0-7] = ",0x00


