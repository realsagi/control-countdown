'/*******************************************/;
'/* Hardware    : ET-BASE AVR (ATmega8535)  */;
'/* CPU         : ATMEL-ATMEGA8535          */;
'/* X-TAL       : 8.00 MHz                  */;
'/* Filename    : TESTPORT.BAS              */;
'/* Complier    : BASCOM-AVR : V1.11.7.9    */;
'/* Last Update : 9-12-2005 (ETT CO.,LTD)   */;
'/*             : WWW.ETT.CO.TH             */;
'/* Description : Test Output Port Sink LED */;
'/*             : Moving LED on PA,PB,PC,PD */;
'********************************************/;
'
$regfile = "8535def.dat"                                    ' ATmega8535
$baud = 9600                                                ' Baudrate = 9600 bps
$crystal = 8000000                                          ' Frequency = 8 MHz

Dim Count As Byte , Outled As Byte                          'General Variable

Config Porta = Output                                       'PortA = Output
Config Portb = Output                                       'PortB = Output
Config Portc = Output                                       'PortC = Output
Config Portd = Output                                       'PortD = Output

Do
  Restore Tab_led                                           'Get Table
  For Count = 1 To 8                                        '8 Byte Get Data

    Read Outled                                             'Get Data
    Porta = Outled                                          'Active PortA
    Portb = Outled                                          'Active PortB
    Portc = Outled                                          'Active PortC
    Portd = Outled                                          'Active PortD
    Wait 1                                                  'Delay Display LED
  Next
Loop                                                        'Loop Continue


Tab_led:
Data &B11111110                                             '1st Data Active Port
Data &B11111101
Data &B11111011
Data &B11110111
Data &B11101111
Data &B11011111
Data &B10111111
Data &B01111111                                             'Last Data Active Port