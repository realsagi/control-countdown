'/*******************************************/;
'/* Hardware    : ET-BASE AVR (ATmega8535)  */;
'/* CPU         : ATMEL-ATMEGA8535          */;
'/* X-TAL       : 8.00 MHz                  */;
'/* Filename    : TEST_ADC.BAS              */;
'/* Complier    : BASCOM-AVR : V1.11.7.9    */;
'/* Last Update : 9-12-2005 (ETT CO.,LTD)   */;
'/*             : WWW.ETT.CO.TH             */;
'/* Description : Read ADC0 & Display RS232 */;
'/*             : Use AD0(PA0) Read Voltage */;
'********************************************/;
'
$regfile = "8535def.dat"                                    ' ATmega8535
$baud = 9600                                                ' Baudrate = 9600 bps
$crystal = 8000000                                          ' Frequency = 8 MHz

Config Adc = Single , Prescaler = Auto                      'Initial ADC
Start Adc                                                   'Now give power to the chip

Dim Result_word As Word
Dim Result_volt As Single
Dim Result_string As String * 4

Print
Print
Print "ET-BASE AVR(ATmega8535) Demo A/D Read"
Print "Input DC Voltage 0-5V to PA0 For Test"
Print

Do
  Result_word = Getadc(0)                                   ' Read ADC0 = 0..1023 to Result_Word
  Result_volt = 5 / 1024                                    ' 1 Step Voltage
  Result_volt = Result_volt * Result_word                   ' ADC0 Result Voltage
  Result_string = Fusing(result_volt , "#.#")
  Print "Result of ADC0 = " ; Result_string ; " Volt" ; Chr(13);
Loop

End