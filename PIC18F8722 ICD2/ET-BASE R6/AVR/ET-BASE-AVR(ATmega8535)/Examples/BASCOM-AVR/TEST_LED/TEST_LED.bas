'/******************************************/;
'/* Hardware    : ET-BASE AVR (ATmega8535) */;
'/* CPU         : ATMEL-ATMEGA8535         */;
'/* X-TAL       : 8.00 MHz                 */;
'/* Filename    : TEST_LED.BAS             */;
'/* Complier    : BASCOM-AVR : V1.11.7.9   */;
'/* Last Update : 9-12-2005 (ETT CO.,LTD)  */;
'/*             : WWW.ETT.CO.TH            */;
'/* Description : Selftest LED (PB0) Blink */;
'/*             : Set Jumper PB0/LED = LED */;
'*******************************************/;
'
$regfile = "8535def.dat"                                    ' ATmega8535
$baud = 9600                                                ' Baudrate = 9600 bps
$crystal = 8000000                                          ' Frequency = 8 MHz

Config Portb = Output                                       'PortB = Output

Dim Led As Bit                                              'LED Status

Led = 0                                                     'Reset Status
Do
  Portb.0 = Led                                             ' Update LED
  Wait 1                                                    ' Delay ON/OFF
  Led = Not Led                                             ' Toggle LED Status
Loop

End