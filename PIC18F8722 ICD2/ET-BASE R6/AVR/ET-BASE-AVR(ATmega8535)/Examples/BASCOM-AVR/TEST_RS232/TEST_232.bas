'/******************************************/;
'/* Hardware    : ET-BASE AVR (ATmega8535) */;
'/* CPU         : ATMEL-ATMEGA8535         */;
'/* X-TAL       : 8.00 MHz                 */;
'/* Filename    : TEST_232.BAS              */;
'/* Complier    : BASCOM-AVR : V1.11.7.9   */;
'/* Last Update : 9-12-2005 (ETT CO.,LTD)  */;
'/*             : WWW.ETT.CO.TH            */;
'/* Description : Test Send/Receive RS232  */;
'/*             : Setup RS232 = 9600,N,8,1 */;
'*******************************************/;
'
$regfile = "8535def.dat"                                    ' ATmega8535
$baud = 9600                                                ' Baudrate = 9600 bps
$crystal = 8000000                                          ' Frequency = 8 MHz

Dim I As Byte
Print "Hello From ET-BASE AVR(ATmega8535)"
Print "Test RS232 Receive  & Transmit"
Print "Press Anykey For Test = ";

Do
   I = Waitkey()                                            'Wait RS232 Receive
   Print Chr(i);                                            'Echo Data
   If I = 13 Then Print Chr(10);                            'Auto Line Feed if Enter
Loop

End                                                         ' End Program