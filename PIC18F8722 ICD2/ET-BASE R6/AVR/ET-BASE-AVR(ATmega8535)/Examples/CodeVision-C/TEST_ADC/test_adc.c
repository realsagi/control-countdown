//******************************************/;
//* Hardware    : ET-BASE AVR (ATmega8535) */;
//* CPU         : ATMEL-ATMEGA8535         */;
//* X-TAL       : 8.00 MHz                 */;
//* Filename    : TEST_ADC.C               */;
//* Complier    : CodeVisionAVR V1.24.7d   */;
//* Last Update : 9-12-2005 (ETT CO.,LTD)  */;
//*             : WWW.ETT.CO.TH            */;
//* Description : Read ADC0 & Display RS232*/;
//*             : Use AD0(PA0) Read Voltage*/;
//******************************************/;
//* CodeVisionAVR Complier Option Setting  */;
//* Chip type           : ATmega8535       */;
//* Program type        : Application      */;
//* Clock frequency     : 8.000000 MHz     */;
//* Memory model        : Small            */;
//* External SRAM size  : 0                */;
//* Data Stack size     : 128              */;
//******************************************/;

#include <mega8535.h>                                           // ATmega8535 MCU
#include <stdio.h>                                              // Standard Input/Output functions
#define ADC_VREF_TYPE 0xC0                                      // ADC Used Internal Reference

/* pototype  section */
unsigned int read_adc(unsigned char adc_input);                 // Read ADC Result

void main(void)
{
  unsigned int val;                                             // ADC Result
  // USART initialization
  // Communication Parameters: 8 Data, 1 Stop, No Parity
  // USART Receiver: On
  // USART Transmitter: On
  // USART Mode: Asynchronous
  // USART Baud rate: 9600
  UCSRA=0x00;
  UCSRB=0x18;
  UCSRC=0x86;
  UBRRH=0x00;
  UBRRL=0x33;

  // ADC initialization
  // ADC Clock frequency: 62.500 kHz
  // ADC Voltage Reference: Int., cap. on AREF
  // ADC High Speed Mode: Off
  // ADC Auto Trigger Source: None
  ADMUX=ADC_VREF_TYPE;
  ADCSRA=0x87;
  SFIOR&=0xEF;
  
  printf("Hello ET-BASE AVR(ATmega8535)....ADC0\n\r");  
  printf("Input DC Voltage 0-5V to PA0 For Test\n\n\r");
  
  while (1)                                                     // Loop Continue
  {
    val = read_adc(0);                                          // Read ADC0 Result
    printf("\rResult of ADC[0] = %x ",val);                     // Dispaly Result
  }
}

//*********************************/;
//* Read the AD conversion result */;
//*********************************/;
//
unsigned int read_adc(unsigned char adc_input)                  // Read Result ADC
{
  ADMUX=adc_input|ADC_VREF_TYPE;

  ADCSRA|=0x40;                                                 // Start the AD conversion
  
  while ((ADCSRA & 0x10)==0);                                   // Wait for the AD conversion to complete
  ADCSRA|=0x10;
  
  return ADCW;
}
