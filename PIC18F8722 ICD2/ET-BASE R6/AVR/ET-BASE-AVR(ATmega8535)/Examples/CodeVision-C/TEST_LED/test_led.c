//******************************************/;
//* Hardware    : ET-BASE AVR (ATmega8535) */;
//* CPU         : ATMEL-ATMEGA8535         */;
//* X-TAL       : 8.00 MHz                 */;
//* Filename    : TEST_LED.C               */;
//* Complier    : CodeVisionAVR V1.24.7d   */;
//* Last Update : 9-12-2005 (ETT CO.,LTD)  */;
//*             : WWW.ETT.CO.TH            */;
//* Description : Selftest LED (PB0) Blink */;
//*             : Set Jumper PB0/LED = LED */;
//******************************************/;
//* CodeVisionAVR Complier Option Setting  */;
//* Chip type           : ATmega8535       */;
//* Program type        : Application      */;
//* Clock frequency     : 8.000000 MHz     */;
//* Memory model        : Small            */;
//* External SRAM size  : 0                */;
//* Data Stack size     : 128              */;
//******************************************/;

#include <mega8535.h>                                   // ATmega8535 MCU

/* pototype  section */
void delay_led(unsigned long int);                      // Delay LED

void main(void)
{  
  PORTB=0x00;                                           // PB7..0 = 0
  DDRB=0x01;                                            // PB0    = Output

  // Loop Blink LED on PB0 //
  while (1)
  {
    PORTB |= 0x01;			                // PB0 = 1 (OFF LED)
    delay_led(100000);	   		                // Display LED Delay
    PORTB &= 0xFE;                                      // PB0 = 0 (ON LED)	
    delay_led(100000);                                  // Display LED Delay                                             
  }
}

/***********************/
/* Delay Time Function */
/*    1-4294967296     */
/***********************/
void delay_led(unsigned long int count1)
{
  while(count1 > 0) {count1--;}		                // Loop Decrease Counter	
}
