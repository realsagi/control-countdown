//******************************************/;
//* Hardware    : ET-BASE AVR (ATmega8535) */;
//* CPU         : ATMEL-ATMEGA8535         */;
//* X-TAL       : 8.00 MHz                 */;
//* Filename    : TEST_RS232.C             */;
//* Complier    : CodeVisionAVR V1.24.7d   */;
//* Last Update : 9-12-2005 (ETT CO.,LTD)  */;
//*             : WWW.ETT.CO.TH            */;
//* Description : Test Send/Receive RS232  */;
//*             : Setup RS232 = 9600,N,8,1 */;
//******************************************/;
//* CodeVisionAVR Complier Option Setting  */;
//* Chip type           : ATmega8535       */;
//* Program type        : Application      */;
//* Clock frequency     : 8.000000 MHz     */;
//* Memory model        : Small            */;
//* External SRAM size  : 0                */;
//* Data Stack size     : 128              */;
//******************************************/;

#include <mega8535.h>                                           // ATmega8535 MCU
#include <stdio.h>                                              // Standard Input/Output functions

void main(void)
{
  char uart_data;                                               // RS232 Buffer

  // USART initialization
  // Communication Parameters: 8 Data, 1 Stop, No Parity
  // USART Receiver: On
  // USART Transmitter: On
  // USART Mode: Asynchronous
  // USART Baud rate: 9600
  UCSRA=0x00;
  UCSRB=0x18;
  UCSRC=0x86;
  UBRRH=0x00;
  UBRRL=0x33;

  printf("Hello ET-BASE AVR(ATmega8535)...TEST RS232\n\r");     // Call printf Function

  while (1)                                                     // Loop Continue
  {
    uart_data = getchar();                                      // Wait RS232 Receive
    putchar(uart_data);                                         // Echo data
  }
}
