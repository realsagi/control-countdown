//******************************************/;
//* Hardware    : ET-BASE AVR (ATmega8535) */;
//* CPU         : ATMEL-ATMEGA8535         */;
//* X-TAL       : 8.00 MHz                 */;
//* Filename    : TEST_PORT.C              */;
//* Complier    : CodeVisionAVR V1.24.7d   */;
//* Last Update : 9-12-2005 (ETT CO.,LTD)  */;
//*             : WWW.ETT.CO.TH            */;
//* Description : Test Output Port Sink LED*/;
//*             : Moving LED on PA,PB,PC,PD*/;
//******************************************/;
//* CodeVisionAVR Complier Option Setting  */;
//* Chip type           : ATmega8535       */;
//* Program type        : Application      */;
//* Clock frequency     : 8.000000 MHz     */;
//* Memory model        : Small            */;
//* External SRAM size  : 0                */;
//* Data Stack size     : 128              */;
//******************************************/;

#include <mega8535.h>                                   // ATmega8535 MCU

/* pototype  section */
void delay_led(unsigned long int);                      // Delay LED

void main(void)
{
  unsigned char LED; 	   		                // LED Output Status Buffer
  
  DDRA = 0xFF;		   		                // PortA = Output
  DDRB = 0xFF;		   		                // PortB = Output
  DDRC = 0xFF; 		   		                // PortC = Output
  DDRD = 0xFF;             	                        // PortD = Output 
 
  // Loop Test Output Port //
  while(1)													// Loop Continue
  {    	
    // Shift Left
    for (LED = 0x01; LED < 0x80; LED <<= 1)		// Shift Left (Right <- Left)
    { 
      PORTA = 0xFF - LED;				// Active Output (Toggle ON LED)
      PORTB = 0xFF - LED;
      PORTC = 0xFF - LED;
      PORTD = 0xFF - LED;
      delay_led(100000);				// Display Delay	    
    }  	 
    
    // Shift Right
    for (LED = 0x80; LED > 0x01; LED >>= 1)		// Shift Right (Right -> Left)
    { 
      PORTA = 0xFF - LED;				// Active Output (Toggle ON LED)
      PORTB = 0xFF - LED;
      PORTC = 0xFF - LED;
      PORTD = 0xFF - LED;
      delay_led(100000);				// Display Delay
    }  	  
	    
  } 
   
}	
/***********************/
/* Delay Time Function */
/*    1-4294967296     */
/***********************/
void delay_led(unsigned long int count1)
{
  while(count1 > 0) {count1--;}		                // Loop Decrease Counter	
}

