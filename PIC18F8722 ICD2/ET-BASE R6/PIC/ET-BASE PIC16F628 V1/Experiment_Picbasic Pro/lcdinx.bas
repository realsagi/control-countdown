' PicBasic Pro program to display "hello world" on LCD,
' then use LCDIN to read the first letter of each line and
' change it to upper case.

' Define LOADER_USED to allow use of the boot loader.
' This will not affect normal program operation.
DEFINE	LOADER_USED	1

' Define LCD registers and bits
DEFINE  LCD_DREG        PORTD
DEFINE  LCD_DBIT        4
DEFINE  LCD_RSREG       PORTE
DEFINE  LCD_RSBIT       0
DEFINE  LCD_EREG        PORTE
DEFINE  LCD_EBIT        1
DEFINE	LCD_RWREG		PORTE
DEFINE	LCD_RWBIT		2

character	VAR		BYTE


        ADCON1 = 7      ' Set PORTA and PORTE to digital
        Low PORTE.2     ' LCD R/W line low (W)
        Pause 100       ' Wait for LCD to start up


loop:   LCDOut $FE, 1   ' Clear screen
        Pause 500       ' Wait .5 second

        LCDOut "hello"  ' Display "hello"
        Pause 500       ' Wait .5 second

        LCDOut $fe, $C0, "world"	' Move to line 2  and display "world"
        Pause 500       ' Wait .5 second

		LCDIN $80, [character]	' Read value of first character, first line
		LCDOut $FE, $80, (character - $20)	' Change character to upper case
		Pause 500		' Wait .5 second

		LCDIN $C0, [character]	' Read value of first character, second line
		LCDOut $FE, $C0, (character - $20)	' Change character to upper case
		Pause 500		' Wait .5 second
		
        GoTo loop       ' Do it forever
        End
