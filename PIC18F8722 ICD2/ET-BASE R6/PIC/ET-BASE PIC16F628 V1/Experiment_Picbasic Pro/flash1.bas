' Flash LED (Sample 1)
'
' Blinks LED using HIGH and LOW commands to control specified pin.

Symbol	LED = 2				' LED Pin

Loop:	High LED			' LED On
	Pause 1000			' Delay 1 Second
	Low LED				' LED Off
	Pause 1000			' Delay 1 Second
	goto Loop			' Forever
