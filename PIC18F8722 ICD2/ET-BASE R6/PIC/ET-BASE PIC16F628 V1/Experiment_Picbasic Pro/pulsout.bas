' PULSOUT Command
'
' Variable Pulse Generator. Two buttons adjust from off to 10mSec in
' 10 uSec steps.

        Include "bs1defs.bas"           ' Include BS1 variables and serial modes

DN      con     5                       ' Define frequency down button
UP      con     4                       ' Define frequency up button
PO      con     3                       ' Define pulse output pin

        W0 = 0
        Low PO                                  ' Initialize output polarity
inc:    If (Pin4 = 1) Or (W0 = 1000) Then decr  ' Increment freq on button
	W0 = W0 + 1
decr:   If (Pin5 = 1) Or (W0 = 0) Then pulse    ' Decrement freq on button
	W0 = W0 - 1
pulse:  Pulsout 3,W0                            ' Generate pulse
        Pause 10                                ' Intra-pulse delay
        Goto inc                                ' Forever
