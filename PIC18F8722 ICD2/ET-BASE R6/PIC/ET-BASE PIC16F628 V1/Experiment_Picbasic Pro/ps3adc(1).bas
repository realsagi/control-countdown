' PicStic-3 ADC Test - Requires Micromint PicStic-3 with LTC1298
'
'       W10     Conversion Result [0 = 0V,4095 = 5V]

Symbol  SO = 0                                  ' Serial Output Channel

Loop:   Call AD0 : W0 = W10                     ' Read Input 0
	Call AD1 : W1 = W10                     ' Read Input 1
	Call AD  : W2 = W10                     ' Read Diff (0 = + : 1 = -)
	Serout SO,N2400,(#W0," ",#W1," ",#W2,13,10)
	Goto Loop

