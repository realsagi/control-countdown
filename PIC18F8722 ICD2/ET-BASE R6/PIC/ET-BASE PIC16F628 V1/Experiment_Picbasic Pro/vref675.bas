' PicBasic Pro program to display result of 
' 10-bit A/D conversion using the Vref pin
' on a 12F675. 
'
' Connect analog input to GP0
' Connect reference voltage to GP1


adval  VAR WORD					'Create adval to store result

ANSEL = %00110001 	' Set ADC clock to Frc and GP0 to analog mode

ADCON0 = %11000001	' Configure and turn on A/D Module:
					' Right justify result, use Vref pin, channel 0
						

loop:

	ADCON0.1 = 1					'Start Conversion

notdone:
	
	IF ADCON0.1 = 1 Then notdone	'wait for low on bit-1 of ADCON0, conversion finished
	
	adval.highbyte = ADRESH			'move HIGH byte of result to adval
	adval.lowbyte = ADRESL			'move LOW byte of result to adval
	
	SerOut2 GPIO.5,396, ["Value: ", DEC adval, 10, 13]		'Display the decimal value  
	
	Pause 100       				'Wait .1 second
	
	GoTo loop       				'Do it forever
	
