' PULSOUT Command
'
' Variable Pulse Generator. Two buttons adjust from off to 10mSec in
' 10 uSec steps.

Symbol	DN = 5				' Frequency Down Button
Symbol	UP = 4				' Frequency Up Button
Symbol	PO = 3				' Pulse Output

	Low PO					' Initialize Output Polarity
Inc:	If Pin4 = 1 Or W0 = 1000 Then Dec	' Increment Freq on Button
	W0 = W0 + 1
Dec:	If Pin5 = 1 Or W0 = 0 Then Pulse	' Decrement Freq on Button
	W0 = W0 - 1
Pulse:	Pulsout 3,W0				' Generate Pulse
	Pause 10				' Intra-Pulse Delay
	Goto Inc				' Forever
	
