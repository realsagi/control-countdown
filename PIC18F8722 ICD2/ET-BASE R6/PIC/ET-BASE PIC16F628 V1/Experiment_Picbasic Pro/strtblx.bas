' PicBasic Pro program to demonstrate a method of storing
' text strings in a table.  The strings can be called by 
' storing the location of the first character in a variable
' and calling the subroutine.  The string will be sent to both
' LCD and Serial pin and the main program will resume when
' the null terminating character is encountered in the table.

' PicBasic Pro 2.43, LAB-X1 Experimenter Board, PIC16F877A

' Define LOADER_USED to allow use of the boot loader.
' This will not affect normal program operation.
DEFINE	LOADER_USED	1

' Define LCD registers and bits
DEFINE  LCD_DREG        PORTD
DEFINE  LCD_DBIT        0
DEFINE  LCD_RSREG       PORTE
DEFINE  LCD_RSBIT       0
DEFINE  LCD_EREG        PORTE
DEFINE  LCD_EBIT        1
DEFINE  LCD_BITS        8

strloc		VAR	BYTE	' Store the numerical location of the first character in the string
nextchar	VAR	BYTE	' Stores each charater retrieved from the table

' Use constants to hold the locations of your strings.  This will make
' modifications easier and it doesn't use any RAM or code space.
hello	CON	0			' "Hello" is at location 0
world	CON	6			' "World" is at location 6


ADCON1 = 7      ' Set PORTA and PORTE to digital
Low PORTE.2     ' LCD R/W line low (W)
Pause 200       ' Wait for LCD to start up



loop:			' Main program starts here

	LCDOut $fe, 1   ' Clear LCD screen
	SerOut2 PORTC.6, 396, [10,13,10,13]		' Send linefeeds on serial
	Pause 500       ' Wait .5 second

	
	strloc = hello	' Prepare to get string "Hello" (strloc = 0)

	GoSub strout	' Display the string
	Pause 500       ' Wait .5 second
	
	LCDOut $fe, $c0	' Move LCD cursor to second line
	SerOut2 PORTC.6, 396, [10,13]		' Send linefeed on serial
	

	strloc = world	' Prepare to get string "World" (strloc = 6)

	GoSub strout	' Make it so
	Pause 500       ' Wait .5 second

GoTo loop       ' Do it forever





strout:		' Beginning of string-display routine
		
	' Put strings in this table, terminating each with a 0 (null value)
	LookUp strloc, ["Hello",0,"World",0], nextchar	' get character in specified location
	
	IF nextchar != 0 Then		' Check for null value (end of string), if not null:
	
		LCDOut nextchar			' Put the character on the display

		SerOut2 PORTC.6, 396, [nextchar]	' Send the character on the serial port.

		strloc = strloc + 1		' Increment position in table
		GoTo strout				' Jump to get another character

	EndIF
	
Return						' Return when the string is complete
		


End
