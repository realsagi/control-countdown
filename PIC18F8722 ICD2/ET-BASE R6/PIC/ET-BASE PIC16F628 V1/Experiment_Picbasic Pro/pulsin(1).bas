' PULSIN Command
'
' Serial Pulse Width Meter

Symbol	SO = 0				' Serial Out
Symbol	FI = 4				' Frequency Input

Loop:	Pulsin FI,0,W0				' Measure Pulse (in 10 uSec)
	If W0 = 0 Then Disp			' If Non-Zero, Display
	Serout SO,N2400,(#W0)
Disp:	SerOut SO,N2400,("0 uSec",13,10)	' Display Trailer
	Goto Loop				' Forever

