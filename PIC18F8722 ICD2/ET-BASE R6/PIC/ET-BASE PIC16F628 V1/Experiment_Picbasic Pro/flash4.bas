' Flash LED (Sample 4)
'
' Blinks LED using For..Next Loops. Uses Nap for Low Power & Good Measure!!!

Symbol  LED1_DIR = DIR2                 ' LED Direction Bit (1=Out, 0=In)
Symbol  LED2_DIR = DIR3
Symbol  LED1_PIN = PIN2                 ' Data Bit
Symbol  LED2_PIN = PIN3

	LED1_DIR = 1                    ' Make Pins Output
	LED2_DIR = 1
Loop:   For LED2_PIN = 0 To 1           ' Toggle LED 1
		For LED1_PIN = 0 To 1   ' Toggle LED 2
			Nap 6           ' Delay Approx 1 Second (Low Power)
		Next LED1_PIN
	Next                            ' NOTE : No NEXT Variable
	goto Loop                       ' Forever
