' PicBasic Pro program to demonstrate the Div32 command.
' Div32 must be used immediately after a multiply statement
' in order to retain the state of the internal registers of
' the device.
' This program must be compiled with PBP version 2.40 or later


' Define Debug pin

DEFINE DEBUG_REG	PORTC
DEFINE DEBUG_BIT	6
DEFINE DEBUG_BAUD	9600
DEFINE DEBUG_MODE	1


' Define variables for testing the command
WRESULT		VAR 	WORD	' Used to store results to a word
EXPECTED	VAR 	WORD	' Used to display the expected result
BRESULT		VAR		BYTE	' Used to store results to a byte
bogus 		VAR		BYTE	' Used to store intermediate results of the multiply

BB0			VAR		BYTE	' Data in byte form
BB1			VAR		BYTE	' Data in byte form
WW0			VAR		WORD	' Data in word form
WW1			VAR		WORD	' Data in word form


' Set values for testing
BB0=25
BB1=250
WW0=255
WW1=10052

Debug 10,13				' Send new line

bogus = WW0 * WW1		' Multiply to load internal registers with 32-bit value
WRESULT = Div32 BB1		' Divide 32-bit value by byte and store in word
EXPECTED = 10253		' Expected result for this calculation
GoSub resultword		' Send the result with Debug

bogus = WW0 * WW1		' Multiply to load internal registers with 32-bit value
WRESULT = Div32 WW0		' Divide 32-bit value by word and store in word
EXPECTED = 10052		' Expected result for this calculation
GoSub resultword		' Send the result with Debug

bogus = WW0 * WW1		' Multiply to load internal registers with 32-bit value
WRESULT = Div32 1000	' Divide 32-bit value by contant and store in word
EXPECTED = 2563			' Expected result for this calculation
GoSub resultword		' Send the result with Debug


bogus = BB0 * BB1		' Multiply to load internal registers with 32-bit value
BRESULT = Div32 BB0		' Divide 32-bit value by byte and store in byte
EXPECTED = 250			' Expected result for this calculation
GoSub resultbyte		' Send the result with Debug

bogus = BB0 * BB1		' Multiply to load internal registers with 32-bit value
BRESULT = Div32 WW0		' Divide 32-bit value by word and store in byte
EXPECTED = 24			' Expected result for this calculation
GoSub resultbyte		' Send the result with Debug

bogus = BB0 * BB1		' Multiply to load internal registers with 32-bit value
BRESULT = Div32 100		' Divide 32-bit value by contant and store in byte
EXPECTED = 62			' Expected result for this calculation
GoSub resultbyte		' Send the result with Debug


Debug 10,13				' Send new line

End						' Stop


resultbyte:				' Send byte value
		
	Debug "Byte Result = ",#BRESULT,"   ",#EXPECTED," Expected",10,13
	Pause 500
	Return
	
	
resultword:				' Send word value

	Debug "Word Result = ",#WRESULT,"   ",#EXPECTED," Expected",10,13
	Pause 500
	Return
	
