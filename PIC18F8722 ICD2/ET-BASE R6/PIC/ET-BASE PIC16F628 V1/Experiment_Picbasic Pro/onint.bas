' On Interrupt - Interrupts in BASIC
'  Turn LED on.  Interrupt on PORTB.0 (INTE) turns LED off.
'  Program waits .5 seconds and turns LED back on.

led     var     PORTB.7


        OPTION_REG = $7f        ' Enable PORTB pullups

        On Interrupt Goto myint ' Define interrupt handler
        INTCON = $90            ' Enable INTE interrupt

loop:   High led                ' Turn LED on
        Goto loop               ' Do it forever


' Interrupt handler
        Disable                 ' No interrupts past this point
myint:  Low led                 ' If we get here, turn LED off
        Pause   500             ' Wait .5 seconds
        INTCON.1 = 0            ' Clear interrupt flag
        Resume                  ' Return to main program
        Enable
