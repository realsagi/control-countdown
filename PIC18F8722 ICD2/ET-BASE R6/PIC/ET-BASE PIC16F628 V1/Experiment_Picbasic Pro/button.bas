' BUTTON Command
'
' Demonstate BUTTON command. Prints working variable (in this case B0) to
' show how auto-repeat works. Prints "PRESS" when button is pressed or
' repeated.

        Include "modedefs.bas"          ' Include serial modes

SO      con     0                       ' Define serial output pin
B       con     5                       ' Define Button input pin

B0      var     byte

	B0 = 0				' Zero Button Working Buffer
loop:   Button B,1,10,5,B0,0,notp       ' Check Button (Skip if Not Pressed)
        Serout SO,N2400,["Press",13,10] ' Indicate Button Pressed
notp:   Serout SO,N2400,[#B0,13,10]     ' Show Working Variable
	Pause 100			' Visual Pause
        Goto loop                       ' Forever
