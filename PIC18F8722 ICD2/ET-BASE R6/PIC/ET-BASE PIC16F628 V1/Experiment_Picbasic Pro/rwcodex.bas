' PicBasic Pro program to read and write to code space
' Flash Program Write must be enabled on your programmer
'
' Write to 8 locations of code space beginning at $1800
' Read 8 locations back and send to LCD repeatedly

' Define LCD registers and bits
Define  LCD_DREG        PORTD
Define  LCD_DBIT        4
Define  LCD_RSREG       PORTE
Define  LCD_RSBIT       0
Define  LCD_EREG        PORTE
Define  LCD_EBIT        1


I    var     byte                ' Loop count
D    var     byte                ' Data
A    var     word                ' Address

        ADCON1 = 7                      ' Set PORTA and PORTE to digital
        Low PORTE.2                     ' LCD R/W line low (W)
        Pause 100                       ' Wait for LCD to start up

        For I = 0 To 7         			' Loop 8 times, once for each address $1800 to $1807
				A = $1800 + I			' Increment Address
                D = I + $A0          	' Change Data
				Writecode A,D		 	' Send value in D to code space location A
        Next I

loop:   For I = 0 To 7          		' Loop 8 times, once for each address $1800 to $1807
				A = $1800 + I			' Increment Address
				Readcode A,D			' Get data in location A

                Lcdout $fe,1,hex A,": ",hex D   ' Display the location and data
                Pause 1000
        Next I

        Goto loop

        End
