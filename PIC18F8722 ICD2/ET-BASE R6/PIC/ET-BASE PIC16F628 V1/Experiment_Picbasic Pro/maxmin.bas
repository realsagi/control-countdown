' MAX/MIN Operators
'
' Use MAX and MIN operators to Bound [0..9] to [3..7]

        Include "modedefs.bas"          ' Include serial modes

SO      con     0                       ' Define serial out pins
B0      var     byte
B1      var     byte

loop:   For B0 = 0 To 9                 ' B0 = 0..9
                B1 = B0 Max 3 Min 7     ' B1 = B0 Bounded to [3..7]
					' Display Results
                Serout 0,N2400,[#B0," ",#B1,10,13]
	Next B0
        Serout 0,N2400,[10]             ' Line Break
        Goto loop                       ' Forever
