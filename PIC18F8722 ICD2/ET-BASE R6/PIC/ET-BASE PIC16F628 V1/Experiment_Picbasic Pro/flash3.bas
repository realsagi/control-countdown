' Flash LED (Sample 3)
'
' Blinks LED using Bit Math

Symbol  LED_DIR = DIR2                  ' LED Direction Bit (1=Out, 0=In)
Symbol  LED_PIN = PIN2                  ' Data Bit

	LED_DIR = 1                     ' Make Pin Output
Loop:   LED_PIN = LED_PIN + 1           ' Toggle LED
	Pause 1000                      ' Delay 1 Second
	goto Loop                       ' Forever

