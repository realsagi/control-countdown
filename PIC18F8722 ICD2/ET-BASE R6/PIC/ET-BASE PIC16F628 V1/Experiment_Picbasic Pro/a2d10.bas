' PicBasic Pro program to display result of 
' 10-bit A/D conversion on LCD
'
' Connect analog input to channel-0 (RA0)

' Define LCD registers and bits
Define  LCD_DREG        PORTD
Define  LCD_DBIT        4
Define  LCD_RSREG       PORTE
Define  LCD_RSBIT       0
Define  LCD_EREG        PORTE
Define  LCD_EBIT        1

adval  var word					'Create adval to store result


	TRISA = %11111111  	' Set PORTA to all input
    ADCON1 = %10000010 	' Set PORTA analog and RIGHT justify result
	ADCON0 = %11000001	' Configure and turn on A/D Module
    Pause 500       	' Wait .5 second


loop: 		ADCON0.2 = 1					'Start Conversion

notdone:	pause 5
			if ADCON0.2 = 1 Then notdone	'wait for low on bit-2 of ADCON0, conversion finished

			adval.highbyte = ADRESH			'move HIGH byte of result to adval
			adval.lowbyte = ADRESL			'move LOW byte of result to adval

	    	Lcdout $fe, 1   				'Clear screen
        	Lcdout "Value: ", DEC adval		'Display the decimal value  

        	Pause 100       				'Wait .1 second

       		Goto loop       				'Do it forever
        	End
