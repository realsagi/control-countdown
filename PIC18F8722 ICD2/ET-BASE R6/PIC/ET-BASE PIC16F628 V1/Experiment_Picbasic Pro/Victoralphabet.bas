'****************************************************************
'*  Name    : UNTITLED.BAS                                      *
'*  Author  : Victor Faria                                      *
'*  Notice  : Copyright (c) 2004 ASK if for more then hobby use *
'*          : All Rights Reserved                               *
'*  Date    : 6/23/2004                                         *
'*  Version : 1.0                                               *
'*  Notes   :
' PicBasic Pro program to display result odf an ADC reading to 
'the alphabet on lcd using a pot conected to  an 16F877 aDC CHANEL 0
' FIRST WE DO AN 8-bit A/D conversion 
'MAKE SURE THAT YOUR POT IS CONECTED TO  CHANEL 0 (PORTA.0) or adjust code
' set up your LCD  how ever you like it I'm using the MeLabs Lab X1 board
'watch as you turn the pot the alphabet will show on the lcd
' if you don't have an LCd you can use serout command and a terminal software
'Just ' out the LCD code
' now this is onlly a sample use your imagination think perhaps an enter button or 2 !!
' perhaps store the letters to a 2nd variable then load to pic/memory////
'perhaps write information to memory using write command.
'I'm Using A bootloader                                                  

'****************************************************************
 Define	LOADER_USED	1
define osc 20       'Oscilator 

' Define LCD registers and bits
Define	LCD_DREG	PORTD
Define	LCD_DBIT	4
Define	LCD_RSREG	PORTE
Define	LCD_RSBIT	0
Define	LCD_EREG	PORTE
Define	LCD_EBIT	1

' Define ADCIN parameters
Define	ADC_BITS	8	' Set number of bits in result
Define	ADC_CLOCK	3	' Set clock source (3=rc)
Define	ADC_SAMPLEUS	50	' Set sampling time in uS

adval	var	byte		' Create adval to store the result  of ADC value
letter var byte          'variable that will hold the letter after LOOKUP


	TRISA = %11111111	' Set PORTA to all input
	ADCON1 = %00000010	' Set PORTA analog
	Low PORTE.2		' LCD R/W line low (W)

	Pause 500		' Wait .5 second

loop: 
        lcdout $fe, 1      'clear LCD
     	ADCIN 0, adval		' Read channel 0 to adval  8 bit value from 0 to 255
        pause 10         
      adval= adval / 10 'devide by 10 so pot will be less sinsative
           
     lookup adval, ["ABCDEFGHIJKLMNOPQRSTUVWXYZ"],letter 'convert ADC result  to letter
     Lcdout $fe, 1		' Clear LCD
	 Lcdout "letter: ", letter	' Display the character/letter
    

	Pause 150		' Wait .150 second    'addjust this delay for refresh rate depending on your osc and lcd

	Goto loop		' Do it forever
	End


