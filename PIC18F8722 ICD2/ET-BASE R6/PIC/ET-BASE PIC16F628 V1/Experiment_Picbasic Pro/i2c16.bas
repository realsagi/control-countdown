' I2CIN and I2COUT Commands
'
' Write address to the first 16 locations of an external serial EEPROM
' Read first 16 locations back and send to serial out repeatedly
' The control byte and address variable are selected for EEPROM's with 
' 2-byte addressing, such as 24LC32 and 24LC65

Symbol	SO = 0				' Serial Output

        For W2 = 0 To 15                ' Loop 16 times
                I2Cout $D0,W2,(W2)      ' Write each location's address to itself
                Pause 10                ' Delay 10ms after each write
        Next W2

Loop:   For W2 = 0 To 15 step 2         ' Loop 8 times
                I2Cin $D0,W2,B1,B2      ' Read 2 locations in a row
                Serout SO,N2400,(#B1," ",#B2," ")       ' Print 2 locations
        Next W2

        Serout SO,N2400,(10)            ' Print linefeed

        Goto Loop
