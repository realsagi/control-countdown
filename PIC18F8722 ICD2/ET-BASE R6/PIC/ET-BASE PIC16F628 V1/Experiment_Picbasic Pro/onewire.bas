' One-wire temperature for LAB-X1 and DS1820

' This program must be compiled with PBP version 2.40 or later


temperature VAR	WORD			' Temperature storage
count_remain VAR BYTE			' Count remaining
count_per_c VAR	BYTE			' Count per degree C

DQ	VAR	PORTC.0			' One-wire data pin


' Define LCD registers and bits
DEFINE	LCD_DREG	PORTD
DEFINE	LCD_DBIT	4
DEFINE	LCD_RSREG	PORTE
DEFINE	LCD_RSBIT	0
DEFINE	LCD_EREG	PORTE
DEFINE	LCD_EBIT	1


	ADCON1 = 7			' Set PORTA and PORTE to digital
	Low PORTE.2			' LCD R/W line low (W)


mainloop: OWOut DQ, 1, [$CC, $44]       ' Start temperature conversion

waitloop: OWIn DQ, 4, [count_remain]	' Check for still busy converting
	IF count_remain = 0 Then waitloop

	OWOut DQ, 1, [$CC, $BE]		' Read the temperature
        OWIn DQ, 0, [temperature.LOWBYTE, temperature.HIGHBYTE, Skip 4, count_remain, count_per_c]

	' Calculate temperature in degrees C to 2 decimal places (not valid for negative temperature)
	temperature = (((temperature >> 1) * 100) - 25) + (((count_per_c - count_remain) * 100) / count_per_c)
	LCDOut $fe, 1, DEC (temperature / 100), ".", DEC2 temperature, " C"

	' Calculate temperature in degrees F to 2 decimal places (not valid for negative temperature)
	temperature = (temperature */ 461) + 3200
	LCDOut $fe, $c0, DEC (temperature / 100), ".", DEC2 temperature, " F"

        Pause 1000                      ' Display about once a second

	GoTo mainloop			' Do it forever
