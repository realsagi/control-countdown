' PEEK and POKE Commands
'
' Access 16C71 A/D using Peek and Poke

Symbol  ADCON0 = 8                      ' A/D Configuration Register 0
Symbol  ADRES = 9                       ' A/D Result
Symbol  ADCON1 = $88                    ' A/D Configuration Register 1
Symbol  SO = 0                          ' Serial Output

	poke ADCON1, 0                  ' Set PortA 0-3 to analog inputs
	poke ADCON0, $41                ' Set A/D to Fosc/8, Channel 0, On

Loop:   poke ADCON0, $45                ' Start Conversion
	pause 1                         ' Wait 1ms for conversion
	peek ADRES, B0                  ' Get Result to variable B0

	serout SO,N2400,(#B0,10)        ' Send variable to serial out

	goto Loop
