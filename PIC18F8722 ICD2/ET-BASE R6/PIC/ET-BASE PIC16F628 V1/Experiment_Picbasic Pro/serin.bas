' SERIN & SEROUT Commands
'
' Upper case serial filter.

        Include "modedefs.bas"          ' Include serial modes

SO      con     0                       ' Define serial out pin
SI      con     1                       ' Define serial in pin
B0      var     byte

loop:   Serin SI,N2400,B0                       ' B0 = input character
        If (B0 < "a") or (B0 > "z") Then print  ' If lower case, convert to upper
	B0 = B0 - $20
print:  Serout SO,N2400,[B0]                    ' Send character
        Goto loop                               ' Forever
