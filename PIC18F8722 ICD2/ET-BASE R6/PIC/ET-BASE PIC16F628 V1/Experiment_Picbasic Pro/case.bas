'PicBasic Pro program to demonstrate the Select Case...Case...End Select structure.
'Each loop results in the same output using slightly different conditions.
' This program must be compiled with PBP version 2.40 or later

' Define Debug pin

DEFINE DEBUG_REG	PORTC
DEFINE DEBUG_BIT	6
DEFINE DEBUG_BAUD	9600
DEFINE DEBUG_MODE	1


B10     VAR     BYTE		' Used to count and specify the case


loop1:							' First example

		Pause 250
		B10 = B10 + 1			' Increment B10

		Select Case B10			' Begin Select Case - use B10

			Case 1				' Execute if B10=1
				Debug "loop1 case 1",10,13		' Send string and new line
				
			Case 2				' Execute if B10=2
				Debug "loop1 case 2",10,13

			Case 4				' Execute if B10=4
				Debug "loop1 case 4",10,13

			Case Is >5			' Execute if B10 > 5
				Clear			' Set B10 to 0
				Debug 10,13		' Send new line
				GoTo loop2		' Finished - jump to second example

			Case Is >2			' Execute if B10>2 (but not >5 or =4)
				Debug "loop1 case 3,5",10,13

		End Select				' Resume here after case is executed

		GoTo loop1				' Continue first example



loop2:	Pause 250				' Second Example
		B10 = B10 + 1			' Increment B10
		Select Case B10			' Begin Select Case - use B10

			Case 1				' Execute if B10=1
				Debug "loop2 case 1",10,13

			Case 2				' Execute if B10=2
				Debug "loop2 case 2",10,13

			Case 4				' Execute if B10=4
				Debug "loop2 case 4",10,13

			Case Is <6			' Execute if B10<6 (but not = 1,2, or 4)
				Debug "loop2 case 3,5",10,13

			Case Else			' Execute if no other conditions met (B10>5)
				Clear			' Set B10 to 0
				Debug 10,13		' Send new line
				GoTo loop1		' Finished - jump back to first example

		End Select				' Resume here after case is executed
			
		GoTo loop2				' Continue second example
