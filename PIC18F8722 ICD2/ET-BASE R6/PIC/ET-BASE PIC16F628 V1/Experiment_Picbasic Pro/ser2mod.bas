' PicBasic Pro Code that demonstrates the use of modifiers
' with the Serin2 and Serout2 commands.


testword		VAR	WORD		' Define word variable
testbyte		VAR	BYTE		' Define byte variable
test8			VAR BYTE[8]		' Define array variable with 8 locations
pinin			VAR	PORTC.7		' Define pinin as PORTC.7
pinout			VAR	PORTC.6		' Define pinout as PORTC.6
		
' For these examples, assume that the following string
' is being received continuously at 2400 baud on PORTC.7:
' "X-1011Y-546Z-F7ZZ-0001"


loop:

	SerIn2 pinin,396,[WAIT("X"),testbyte]
	
	SerOut2 pinout,396,["1: ",testbyte,13,10]				' 1: -
	
' Waits for ascii "X", then reads the next byte without a modifier.
' Numeric value of testbyte is 45, the ascii code for "-".
	

	SerIn2 pinin,396,[WAIT("Y"),DEC testword]
	
	SerOut2 pinout,396,["2: ",SDEC testword,13,10]			' 2: -546
	
' Waits for ascii "Y", then looks for an ascii string that could represent
' a decimal number. It finds the string "-546", which it converts to a
' signed integer.  Since the value of testword is signed, we use SDEC to
' send it.
	

	SerIn2 pinin,396,[WAIT("Y-"),DEC testword]
	
	SerOut2 pinout,396,["3: ",DEC testword,13,10]			' 3: 546
	
' Same as the above example, but we change the WAIT parameter so that the
' ascii "-" is ignored.  The DEC modifier then finds the first decimal 
' sting, "546".
	
	
	SerIn2 pinin,396,[WAIT("X"),BIN testbyte]

	SerOut2 pinout,396,["4: ",ISBIN testbyte,13,10]			' 4: %-1011
	
' Waits for ascii "X", then looks for an ascii string that could represent
' a binary number.  It finds the string "-1011", which it converts to a
' signed integer.  Since the value of testbyte is signed, we use the S
' prefix to send it.  The I prefix inserts the ascii "%" to denote binary.
	
	
	SerIn2 pinin,396,[WAIT("X"),BIN testbyte]

	SerOut2 pinout,396,["5: ",BIN testbyte,13,10]			' 5: 11110101
	
' Same as example 4, but shows what is sent when the I and S prefixes
' are omitted from the SerOut2.
	
	
	SerIn2 pinin,396,[WAIT("ZZ-"),BIN testbyte]

	SerOut2 pinout,396,["6: ",IBIN4 testbyte,13,10]			' 6: %0001
	
' Waits for ascii string "ZZ-", then looks for a string that could represent
' a binary number.  It finds "0001", which it converts to a signed integer.
' We use the IBIN4 modifier, which inserts the "%" denoting binary, and 
' sends 4 digits.  With only IBIN, the result is: 6: %1
	
	
	SerIn2 pinin,396,[WAIT("X-"),HEX testbyte]
	
	SerOut2 pinout,396,["7: ",IHEX testbyte,13,10]			' 7: $11
	
' Waits for ascii string "X-", then looks for a string that could represent
' a hexadecimal number. It finds "1011" which it tries to store in the 
' testbyte variable.  Since the value of $1011 is too large for a single 
' byte, it only stores the least significant 8 bits.
	
	
	SerIn2 pinin,396,[WAIT("X-"),HEX2 testbyte]
	
	SerOut2 pinout,396,["8: ",IHEX testbyte,13,10]			' 8: $10
	
' Same as example 7, but we have used HEX2 in the SerIn2 statement.
' This causes the compiler to collect the string "10" and store it
' in testbyte.
	
	
	SerIn2 pinin,396,[WAIT("Z-"),HEX testbyte]
	
	SerOut2 pinout,396,["9: ",IHEX testbyte,13,10]			' 9: $F7
	
' Since this example waits for the string "Z-", it ignores the "1011"
' string.  The first string it finds that could be hex data is "F7".
	

	SerIn2 pinin,396,[WAIT("Y-"),DEC testword,testbyte]
	
	SerOut2 pinout,396,["10: ",DEC testword,",",testbyte,13,10]		' 10: 546,-
	
' Waits for ascii string "Y-", then looks for a string that could represent a
' decimal number.  It finds "546" and stores the value in testword.  Since we
' have a second item after testword, it stores the next charater "-" in 
' testbyte.
	

	SerIn2 pinin,396,[WAIT("Y-"),DEC testword,WAIT("-"),HEX testbyte]
	
	SerOut2 pinout,396,["11: ",IDEC testword,",",IHEX testbyte,13,10]	' 11: #546,$F7
	
' Waits for the string "Y-", then collects a string that could represent
' a decimal number ("546").  It then waits again for the string "-".  After
' that it collects the next string that looks like hex data, "F7".  We use
' the I prefix to send both numbers to differentiate between decimal and hex.
	
	
	SerIn2 pinin,396,[WAIT("Y-"),DEC2 testword,testbyte]
	
	SerOut2 pinout,396,["12: ",DEC testword,",",testbyte,13,10]		' 12: 54,6
	
' In this example, we use the DEC2 modifier to collect the only 2 decimal
' digits after the wait-string "Y-" is received.  This results in "54" being
' stored to testword.  The next character is "6", which is stored as ascii 
' in testbyte.
	
	
	SerIn2 pinin,396,[WAIT("Y"),SKIP 2,DEC2 testword,testbyte]
	
	SerOut2 pinout,396,["13: ",DEC testword,",",testbyte,13,10]		' 13: 46,Z
	
' Waits for the string "Y", then skips the next 2 characters "-5". It then
' collects a 2-digit decimal number ("46") and stores it to testword.  The
' next byte received is "Z", which is stored as ascii to testbyte.
	

	SerIn2 pinin,396,[WAIT("F"), STR test8\8]
	
	SerOut2 pinout,396,["14: ",STR test8\8,13,10]			' 15: 7ZZ-0001
	
' Waits for the string "F", then collects the next 8 characters.  These are
' stored as ascii in 8 locations of the array variable test8.  The SerOut2 
' statement uses the same modifier to send all 8 locations of the array 
' variable as an ascii string.
	
	
	SerIn2 pinin,396,[WAIT("Z",45), STR test8\8\"0"]
	
	SerOut2 pinout,396,["15: ",STR test8\8,13,10]			' 16: F7ZZ-
	
' This example demonstrates how you can put multiple characters in the
' WAIT.  It waits for the string "Z-", since the ascii code for "-" is
' 45.  The STR item is the same as above except we have added the stop
' character "0".  When it encounters "0" at the sixth character, it 
' replaces it and fills the rest of the test8 array with null characters.

	
	Pause 2000
	GoTo loop


	
	
	
	

