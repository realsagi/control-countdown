' Display Truth Table for Binary Logical Operators

        Include "modedefs.bas"  ' Include serial modes

SO      con     0               ' Define serial output pin
B0      var     byte
B1      var     byte
B2      var     byte

loop:   Serout SO,N2400,["      &  &/ |  |/ ^  ^/",10,13]
	For B0 = 0 To 1
		For B1 = 0 To 1
                        Serout SO,N2400,[#B1," ",#B0," : "]
                        B2 = B1 & B0 : Gosub disp
                        B2 = B1 &/ B0 : Gosub disp
                        B2 = B1 | B0 : Gosub disp
                        B2 = B1 |/ B0 : Gosub disp
                        B2 = B1 ^ B0 : Gosub disp
                        B2 = B1 ^/ B0 : Gosub disp
                        Serout SO,N2400,[10,13]
		Next B1
	Next B0
        Serout SO,N2400,[10,13]
        Goto loop
	
disp:   B2 = B2 & 1
        Serout SO,N2400,[#B2,"  "]
	Return
