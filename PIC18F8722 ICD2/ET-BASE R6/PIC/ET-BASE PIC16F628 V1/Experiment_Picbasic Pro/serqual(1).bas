' SERIN Command w/ Qualifiers
'
' "Crude" serial filter for C++ Style Comments

Symbol	SO = 0				' Serial Out
Symbol	SI = 1				' Serial In

Wait:	Serin SI,N2400,("//"),B0		' B0 = First Char on Comment
Loop:	Serout SO,N2400,(B0)			' Print Character
	If B0 = 13 Then Wait			' Continue til CR
	Serin SI,N2400,B0			' Get Next Character
	Goto Loop				' Forever
