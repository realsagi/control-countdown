' PicStic-3 ADC Test - Requires Micromint PicStic-3 with LTC1298
'
'       W10     Conversion Result [0 = 0V,4095 = 5V]
'
' Note: Must define processor type on command line as -pps3

        Include "modedefs.bas"          ' Include serial modes

W0      var     word
W1      var     word
W2      var     word
W10     var     word
SO      con     0                       ' Define serial output pin

loop:   Call AD0 : W0 = W10                     ' Read input 0
        Call AD1 : W1 = W10                     ' Read input 1
        Call AD  : W2 = W10                     ' Read diff (0 = + : 1 = -)
        Serout SO,N2400,[#W0," ",#W1," ",#W2,13,10]
        Goto loop
