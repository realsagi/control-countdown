' Flash LED (Sample 2)
'
' Blinks LED using Direction and Data bit variables.

Symbol  LED_DIR = DIR2                  ' LED Direction Bit (1=Out, 0=In)
Symbol  LED_PIN = PIN2                  ' Data Bit

	LED_DIR = 1                     ' Make Pin Output
Loop:   LED_PIN = 1                     ' LED On
	Pause 1000                      ' Delay 1 Second
	LED_PIN = 0                     ' LED Off
	Pause 1000                      ' Delay 1 Second
	goto Loop                       ' Forever
