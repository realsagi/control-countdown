' SERIN Command w/ Qualifiers
'
' "Crude" serial filter for C++ Style Comments

        Include "modedefs.bas"          ' Include serial modes

SO      con     0                       ' Define serial out pin
SI      con     1                       ' Define serial in pin
B0      var     byte

wait1:  Serin SI,N2400,["//"],B0                ' B0 = first char on comment
loop:   Serout SO,N2400,[B0]                    ' Print character
        If B0 = 13 Then wait1                   ' Continue til CR
        Serin SI,N2400,B0                       ' Get next character
        Goto loop                               ' Forever
