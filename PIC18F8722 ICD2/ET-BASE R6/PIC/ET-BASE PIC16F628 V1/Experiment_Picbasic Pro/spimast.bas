' PicBasic Pro program to read and write to SPI slave
' using the synchronous serial port
'
' Connect SDI(master) to SDO(slave), SDO(master) to
' SDI(slave), AND SCK(master) to SCK(slave).
' Common Ground is required.
'
' Sends ascii "?" to request data, waits for a "!" to 
' begin receiving data.  Expects to find the
' ADC conversion value in the 6th position of
' the received string.

 
' Define LCD registers and bits
DEFINE  LCD_DREG        PORTD
DEFINE  LCD_DBIT        4
DEFINE  LCD_RSREG       PORTE
DEFINE  LCD_RSBIT       0
DEFINE  LCD_EREG        PORTE
DEFINE  LCD_EBIT        1

SSPEN	VAR		SSPCON.5	'SSP Enable bit
CKP		VAR		SSPCON.4	'Clock Polarity Select
SMP		VAR		SSPSTAT.7	'Data input sample phase
CKE		VAR		SSPSTAT.6	'Clock Edge Select bit
SSPIF	VAR		PIR1.3		'SPI interrupt flag

i		VAR		BYTE		'loop counter
a		VAR		BYTE[6]		'Holds 6 characters read from slave


        ADCON1 = 7			'Set PORTA and PORTE to digital
        Low PORTE.2			'LCD R/W line low (W)
        Pause 100			'Wait for LCD to start up

		TRISC = 0			'set PORTC I/O
		
		SSPEN = 1			'enable SPI pins
		CKP = 0				'clock idle low
		CKE = 0				'transmit on idle to active transition
		SSPIF = 0			'clear buffer full status
		SMP = 0				'sample in middle of data
		
		
loop:
		GoSub getdata		'initiate conversion and receive data

		
		LCDOut $fe, 1, STR a\5, DEC a[5]	'display received string
		Pause 100
		
		GoTo loop			'do it forever
		

getdata:					
		
		SSPBUF = "?"					'send ? to start conversion
		GoSub letclear					'wait for buffer to clear
		IF SSPBUF<>"!" Then getdata		'wait for reply (!)

		For i = 0 to 5		'loop for 6 characters
			SSPBUF = 0		'write to SSPBUF to start clock
			GoSub letclear	'wait for receipt
			a[i] = SSPBUF	'store received character in array
		Next i				'get next character

		Return
		

letclear:

		IF SSPIF = 0 Then letclear	'wait for SPI interupt flag
		PauseUs 25					'25uS fudge factor
		SSPIF = 0					'reset flag
		Return		
