' SLEEP Command
'
' Slowly Blink LED Using Low Power Mode Delay

LED     con     2                       ' LED Pin

loop:   Toggle LED                      ' Toggle LED
	Sleep 10			' Sleep for 10 Seconds (or so)
        Goto loop                       ' Forever
