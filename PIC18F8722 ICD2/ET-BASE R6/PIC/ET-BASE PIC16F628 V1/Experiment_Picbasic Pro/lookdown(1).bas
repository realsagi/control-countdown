' LOOKDOWN Command
'
' Convert ASCII Hexadecimal characters to numeric equivalents.

Symbol	SO = 0				' Serial Out
Symbol	SI = 1				' Serial In

Loop:	Serin SI,N2400,B0		' B0 = Next Character
	B1 = 255			' B1 = Convert to Hex (255 if Fail)
	Lookdown B0,("0123456789ABCDEF"),B1
	If B1 = 255 Then Loop		' Skip Non-Hex Characters
	Serout SO,N2400,(#B1,13,10)	' Output Decimal Equivalent
	Goto Loop			' Forever

