'**********************************************************************************
'*  Name    : debugger.BAS                                                        *                             
'*  Author  : Victor Faria                                                        *
'*  Notice  : Copyright (c) 2001 [set under view...options]                       * 
'*          : All Rights Reserved                               
'*  Date    : 10/3/01                                           
'*  Version : 1.0                                               
'*  Notes  this is a modyfied vertion of the debugmonitor at  
'*  the me labs site
'*  use this with a terminal program if you don't need all the 
'*  info. that's provided at the check label just use the '
'*  to comment it out that way you just see what you need.
'*  I'm sure some people will have many ways to improve this.
'*  if you do please share with all at the list.
'*  I hope this ilistrates the flexability of the onthebug command
'*  you can even use it in your code as a switch monitor look 
'*  for a switch to change state or if it did or???? any thing
'*  when I first started playing with this I did have some trouble
'*  but thanks to Jeff and charles ,it was an easy fix (GREAT SUPPORT)
'*  thanks guy's the problem !!!! disable debug as you enter the check label  
'*  NOTICE THIS WAS WRITTEN FOR AN 16F877                                                 
'*                                                 
'****************************************************************
define onint_used 1    'I'm using the melabs boot loader
DEBUG_STACK     var     byte bank0 system       ' Stack level
DEBUG_ADDRESS   var     word bank0 system       ' Program address
vara var byte         'vara stands for variable a wich will be used for porta
varb var byte         'so on and so on
varc var byte
vard var byte
vare var byte
stk var word        'a variable to put the value of the stack
bnk var word
result var word      'same as above but for bank
trisa=0
trisb=0              'tris all registers
porta=0
portb=0
trise=0
porte=0
trisc=0
portc=0
trisd=0
portd=0
adcon1=7            'porta and e digital
ON DEBUG goto check 'you can use this label to look at switches or registers or?????
                    'what ondebug does is go to the check label after each instuction
                    'and executes the code in that label
                    'be sure to use disable debug command at the check label              
top:
    high portd.4 :pause 500     'turn on an led on portd.4
    disable debug                ' should disable debug befor the hserout command
    Hserout ["now at line2" , 10, 13] 'just using this hserout as marker so i know my place
    pause 200
    enable debug                     'enable the debug command again
    vara=porta                       'in the next 5 lines we read the ports and load values to variables
    varb=portb
    varc=portc
    vard=portd
    vare=porte
    low portd.4 :pause 500          'led off on portd.4    
    high portd.5 :pause 500         'turn on led on portd.5
    disable debug                ' should disable debug befor the hserout command
    Hserout ["now at line5" , 10, 13] 'same reason as above for this hserout
    pause 200
    enable debug                     'enable the debug command again
    low portd.5 :pause 500             'turn led off at portd.5
    goto top



check:
       disable debug                          'must diable in order to hseroout
       Hserout ["now at check" , 10, 13]   'this is so I know I'm at the check label
       vara=porta                              'next 5 lines read ports to variables
       varb=portb
       varc=portc
       vard=portd
       vare=porte
       stk=debug_stack
       Hserout [" This is the value of bank 0"] 'marker so I know what I'm reading
       for bnk = $0 to $7f
       if bnk // 16 =0 then                     'send out the value of the bank in hex format
       Hserout [13, 10, hex2 bnk, ":"]
       Hserout [ 13, 10]
         endif
       peek bnk, result
       Hserout [" ", hex2 result]  
       next bnk
       Hserout [13, 10]
       Hserout ["stack level=" ,  #bnk, 10, 13]
       pause 1000
       Hserout ["This is port values", 10, 13]   'again another marker
       pause 1000
       Hserout ["porta var-va=", bin  vara, 10, 13]      'next five lines send out the value of the port  
       pause 1000                                       'pins in binary format
       Hserout ["portb var-vb=", bin  varb, 10, 13]
       pause 1000
       Hserout ["portc var-vc=", bin  varc, 10, 13]
       pause 1000
       Hserout ["portd var-vd=", bin  vard, 10, 13]
       pause 1000
       Hserout ["porte var-ve=", bin  vare, 10, 13]
       pause 1000
       porte=0 :pause 3000                    'reason for long pauses is so you have time to see 
                                               'results on lcd or terminal while making changes. 
                                               'reset port
       goto run                                'reset variable 
       'this i have tested and works good
       'maybe use as an include file.
       'to debug programs



run:
     
  asm
        movf    DEBUG_ADDRESS + 1, W    ; Set PCLATH with top byte of return address
        movwf   PCLATH
        movf    DEBUG_ADDRESS, W        ; Go back to main program (just continues from last debug)
        movwf   PCL
  endasm
         
     
     
