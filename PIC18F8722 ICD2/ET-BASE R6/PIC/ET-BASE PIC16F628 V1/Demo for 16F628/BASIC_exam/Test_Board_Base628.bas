'******************************************************
'  Hard Ware: ET-BASE PIC16F628A V1
'  Freq :   10MHz
'  OSC mode:    extclk 
'******************************************************

        INCLUDE "modedefs.bas"          ' Include serial modes

SI      VAR     PORTB.1        ' Serial Input pin
SO      VAR     PORTB.2        ' Serial output pin          


Define  LCD_DREG    PORTA      ' RA0 to RA3 is data pin for LCD
Define  LCD_DBIT    0
Define  LCD_RSREG   PORTA      ' RA6
Define  LCD_RSBIT   6
Define  LCD_EREG    PORTA      ' RA4
Define  LCD_EBIT    4
Define  LCD_BITS    4

B0      VAR     BYTE
B1      VAR     BYTE

DEFINE  OSC     10

     CMCON =7       ' Comparator off
     TRISB = 2      ' RB1 is input
     TRISA = 0
     
main:               
                SerOut SO,T9600,[12,"PROGRAM TEST BOARD ET-BASE PIC/40",10,13]
                
                SerOut SO,T9600,["PRESS SELECT CHICE TEST  ",10,10,13]
                
                SerOut SO,T9600,["1.TEST RS232",10,13]

                SerOut SO,T9600,["2.TEST IO",10,13]
                
                SerOut SO,T9600,["3.TEST LCD",10,13]
                                                                                                                                                                                                    
        SerIn SI,T9600,B0
        Pause  500
   
Wait_CMD:
        IF B0 = "1" Then  T_RS232
            
        IF B0 = "2" Then  T_IO
        
        IF B0 = "3" THEN  T_LCD
        
        GoTo  Wait_CMD        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    

'*****************************************************************************
T_RS232:   SerOut SO,T9600,[12,"PROGRAM TEST RS232 PORT",10,13] 
          
           Pause  10
AAA:       SerIn SI,T9600,B0              ' receive data from serial port
           SerOut SO,T9600,[B0]           ' send data to serial port
           GoTo   AAA

'******************************************************************************           
T_IO:     SerOut SO,T9600,[12,"TEST I/O PIN",10,13]
          SerOut SO,T9600,["  RB4 RESERVED for PGM",10,13]
          SerOut SO,T9600,["  RB1 RESERVED for RX Pin",10,13]
          SerOut SO,T9600,["  RB2 RESERVED for TX Pin",10,13]
          SerOut SO,T9600,["  RA7 RESERVED for OSC Pin",10,13]
          SerOut SO,T9600,["  RA5 RESERVED for MCLR Pin",10,13]
          
          TRISA = $00
          TRISB = $00
       
start_IO:   B0 = %00000001       ' Moving LED of port_A
          for  B1 = 0 to 7
            PORTA = B0
            B0 = B0 << 1
            pause 250
          next B1
       
        B0 = %00000001           ' Moving LED of port_B
       for  B1 = 0 to 8
           PORTB = B0
            B0 = B0 << 1
            pause 250
       next B1
       goto start_IO
     
'*******************************************************************************
T_LCD:
        LCDOut $FE,1                     ' Clear LCD screen
        LCDOut "TEST LCD LINE1"          ' Display line 1
        LCDOut $FE,$C0,"TEST LCD LINE2"  ' Display line 2
       
        END                 
       
        
        
        
        
        






           
           

        
