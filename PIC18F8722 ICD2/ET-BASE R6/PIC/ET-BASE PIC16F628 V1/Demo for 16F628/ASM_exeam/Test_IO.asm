;**************************************
; Test I/O of PORTA and PORTB
; Hardware  : ET-BASE PIC16F628 v1
; OSC 	    : 10 MHZ [ExtClk MODE]
; Assembler : mpasm.exe
; Programmer: WATCHARIN KAOROP
; Company   : ETT  CO.,LTD.
; Date      : 9/02/2006
;**************************************

     list p=16f628A                 ; list directive to define processor
     #include <p16f628A.inc>        ; processor specific variable definitions
      
    	DT0       EQU       0X20
        DT1       EQU       0x21
        DT2       EQU       0x22

	   ORG       0x0000

		  MOVLW     0x07
          MOVWF     CMCON
MAIN      bsf       STATUS,RP0         ; select bank 1
          clrf      TRISA              ; All PORTA is output
          clrf      TRISB              ; port B is output
          bcf       STATUS,RP0         ; select bank 0

LOOP     movlw      0x55 
         movwf      PORTA          ; Out data to PORTA
	     movwf      PORTB          ; Out data to PORTB
         CALL       DELAY

         movlw      0xAA
	     movwf      PORTA          ; Out data to PORTA
	 	 movwf      PORTB          ; Out data to PORTB
	     CALL       DELAY
	      
	     GOTO       LOOP

;********* DELAY LOOP ********

DELAY     MOVLW     0X05
	      MOVWF     DT0
SDEL      CLRF      DT1
SD2       CLRF      DT2
SD1       DECFSZ    DT2
          GOTO      SD1
          DECFSZ    DT1
          GOTO      SD2
	      DECFSZ    DT0
	      GOTO      SDEL
          RETURN 

	 END    
