;**********************************************
; test internal ADC   of PIC18F8720
; Hardware  : ET-BASE PIC8720
; OSC  24MHz  (PLL Enable Mode)
; Assembler : mpasm.exe
; Programmer: Watcharin Kaorop
; Company   : ETT  CO.,LTD.
;**********************************************

     list p=18f8720                 ; list directive to define processor
     #include <p18f8720.inc>        ; processor specific variable definitions
;---------------------------------------------------------------------------------------
;  Device Configuration
;---------------------------------------------------------------------------------------
;code protect disabled
	CONFIG     CP0=OFF
;Oscillator H4 PLL.
	CONFIG     OSC=HSPLL
;Power Up Timer Enabled
	CONFIG     PWRT = ON            
;Brown-OutReset enabled, BOR Voltage is 2.5v
	CONFIG     BOR=ON, BORV=25
;Watch Dog Timer disable
	CONFIG     WDT=OFF
;Low Voltage ICSP  Enabled
   	CONFIG     LVP = ON            
;Stack over/underflow Reset enabled
	CONFIG     STVR=ON
;External Bus Data Wait Disabled
    CONFIG     WAIT = OFF           
; CCP2 Mux Disabled
    CONFIG     CCP2MUX = OFF      
;---------------------------------------------------------------------------------------


dt1       EQU       0x20
dt2       EQU       0x21
dt3       EQU       0x22
B1        EQU       0x31
BB        EQU       0x32
;***********************************
          ORG  0x0000          

		  movlw     0x0E
          movwf     ADCON1         ; AN0 is analog input
		  movlw     0x02           ; Clock FOSC/32
          movwf     ADCON2         ; ADC Conversion 
          bsf       ADCON2,7       ; ADFM = 1 Right justified 
		  clrf      ADCON0
          bsf       ADCON0,0       ; A/D converter module is enabled
          clrf      TRISD          ; port d is output
	      clrf	    TRISE          ; port e is output
          movlw     .155           ; BAUD rate 9600
          movwf     SPBRG1
          clrf      TXSTA1          ; 8 bits data ,no,1 stop
          bsf       RCSTA1,SPEN     ; Asynchronous serial port enable
          bsf       RCSTA1,CREN     ; continuous receive
          bsf       TXSTA1,TXEN     ; Transmit enable
          BSF	    TXSTA1,BRGH	   ; HI SPEED
        
;************ start conversion  **************

bbb       bsf       ADCON0,1       ; start conversion
Loop      btfsc     ADCON0,1       
          goto      Loop           ; DONE = 1
                                   ; DONE = 0
	
NEW       movlw     0x0C           ; Clear display
          call      SEND
          movlw     "D"
          call      SEND
          movlw     "A"
          call      SEND
          movlw     "T"
          call      SEND
          movlw     "A"
          call      SEND
          movlw     " "
          call      SEND
          movlw     "I"
          call      SEND        
	      movlw     "N"
          call      SEND
          movlw     " "
          call      SEND
                          
          movlw     0xA             ; new line
          call      SEND
          movlw     0xD
          call      SEND
	      movlw     "="
          call      SEND
 	      movlw     " "
	      call      SEND

	      movf	    ADRESH,w
		  movwf     PORTE			; Out data Higher Byte to PORTE
	      call      ADJT
	      movf      ADRESL,w        ; read lower 8 bit
		  movwf     PORTD			; Out data Lower Byte to PORTD
	      call      ADJT
	      call      delay			; Display to RS232 terminal 
	      goto      bbb

  
;**********************************************************
; Convert time 1 byte to ASCII 2 bytes and send to display
; Input  : W
; Output : -
;**********************************************************
ADJT      movwf     BB             ; B1 = HHHH LLLL
          swapf     BB,W           ; W  = LLLL HHHH
	      call      CHECK          ; Check data and convert to Ascii Hi Byte
          movf      BB,W
          call      CHECK	   ; Check data and convert to Ascii Lo Byte
          return

CHECK     andlw     0X0F
	      movwf     B1
	      sublw     0X0A
	      bz        CC            ; = 0XA JUMP TO CC 
	      bc        CONV          ; < 0XA JUMP TO CONV
CC	      movf      B1,W          ; A -> F
	      addlw     0X37
	      call      SEND           ; SEND FIRST DIGIT
	      return
CONV      movf      B1,W          ; 0 -> 9
          addlw     0X30
	      call      SEND	   ; SEND LAST DIGIT
          return

;********* SEND DATA TO RS-232 PORT ******************

SEND	  movwf     TXREG1          ; Send recent data to TX 
wait1     lfsr      0,TXSTA1        ; 
          btfss     INDF0,1        ; check TRMT bit in TXSTA (FSR)
          goto      wait1   
	      return

;********* DELAY LOOP ************
	
delay     movlw	    5
	      movwf	    dt1
sd3	      clrf      dt2
sd2       clrf      dt3
sd1       decfsz    dt3
          goto      sd1
          decfsz    dt2
          goto      sd2
	      decfsz    dt1
	      goto      sd3
          return



          END            ; directive 'end of program'



