/*
Code Support     : Board ET-BASE PIC8720
Compiler         : PIC C Compiler Version 3.249
Micro Controller : PIC18F8720
OSC              : 24MHz
*/


#include <18F8720.h>


#define TX1   PIN_C6
#define RX1   PIN_C7

#fuses H4,LVP,NOWDT,NOPROTECT,NOSTVREN,NOOSCSEN
#use delay (clock = 24000000)
#use rs232(baud = 9600, xmit = TX1, rcv = RX1)

#use fast_io(A)
#use fast_io(B)
#use fast_io(C)
#use fast_io(D)
#use fast_io(E)
#use fast_io(F)
#use fast_io(G)
#use fast_io(H)
#use fast_io(J)

void main() {

  int i,Dat;

      set_tris_a(0B00000000);
      set_tris_b(0B00000000);
      set_tris_c(0B10000000);
      set_tris_d(0B00000000);
      set_tris_e(0B00000000);
      set_tris_f(0B00000000);
      set_tris_g(0B00000100);
      set_tris_h(0B00000000);
      set_tris_j(0B00000000);

      setup_comparator(NC_NC_NC_NC);
      SETUP_ADC_PORTS(NO_ANALOGS);

     printf("\f\n\rPROGRAM TEST IO BOARD ET-BASE PIC8720\n\r");
     printf("Connect LED to any I/O PORT you will see LED blink \n\r");
     printf("\n\r RB5 is reserved for PGM (Programming Mode)\n\r");
     printf(" RC6 is reserved for TX-RS232 pin \n\r");
     printf(" RC7 is reserved for RX-RS232 pin \n\r");

   While (1)
   {
     Dat = 0B00000001;
     for (i=0;i<8;i++)
     {
         output_a(Dat);
         output_b(Dat);
         output_c(Dat);
         output_d(Dat);
         output_e(Dat);
         output_f(Dat);
         output_g(Dat);
         output_h(Dat);
         output_j(Dat);

         delay_ms(500);
         Dat = Dat<<1;
     }

   }
}
