/*
Code Support     : Board ET-BASE PIC8720
Compiler         : PIC C Compiler Version 3.249
Micro Controller : PIC18F8720
OSC              : 24MHz
*/


#include <18F8720.h>

#device ADC=10
#define TX1   PIN_C6
#define RX1   PIN_C7



#fuses H4,LVP,NOWDT,NOPROTECT,NOSTVREN,NOOSCSEN
#use delay (clock = 24000000)
#use rs232(baud = 9600, xmit = TX1, rcv = RX1)


void main() {

  int16 Dat;


      set_tris_a(0B00000001);
      SETUP_ADC_PORTS(AN0);
      SETUP_ADC(ADC_CLOCK_DIV_32);

   While (1)
   {

   Dat= Read_ADC();
   printf("\f\n\rValue(AN0) = %lu",Dat);
   delay_ms(100);


   }
}
