/*
Code Support     : Board ET-BASE PIC8720
Compiler         : PIC C Compiler Version 3.249
Micro Controller : PIC18F8720
OSC              : 24MHz
*/


#include <18F8720.h>


#define TX2   PIN_G1
#define RX2   PIN_G2

#fuses H4,LVP,NOWDT,NOPROTECT,NOSTVREN,NOOSCSEN
#use delay (clock = 24000000)
#use rs232(baud = 9600, xmit = TX2, rcv = RX2)


#use fast_io(C)

void main() {

  char Dat;
  
  
      set_tris_g(0B00000100);
      
      
     printf("\f\n\rPROGRAM TEST RS232 Channel-2 of ET-BASE PIC8720\n\r");  
     printf("\n\rPlease press any key on keyboard you will see that key on screen \n\r");


   While (1)
   {
     Dat = getc();     // Receive Data from RS232 RX
     putc(Dat);        // Send Data to RS232 TX

   }
}
