/*
Code Support     : Board ET-BASE PIC8628
Compiler         : PIC C Compiler Version 4.069
Micro Controller : PIC18F8628
OSC              : 40MHz
Programmer       : WATCHARIN KAOROP
*/


#include <18F8628.h>
#device   ADC=12
#define TX1   PIN_C6
#define RX1   PIN_C7

#fuses   H4,NOLVP,NOWDT,NOPROTECT,NOSTVREN

#use delay (clock = 40000000)
#use rs232(baud = 9600, xmit = TX1, rcv = RX1)


void main() {

  int16 Dat;
  int16 AN0_Value,AN1_Value,AN2_Value,AN3_Value;


      set_tris_a(0B00001111);
      set_tris_c(0B10111111);
      setup_adc_ports(AN0_TO_AN3);                                        // Asign pin A/D 
      setup_adc(ADC_CLOCK_INTERNAL);                                      // Asigna clock

      printf("\fTest 12-bit Analog to Digital Module");


   While (1)
   {
                    set_adc_channel(0);                                 // Canal A0
                    delay_us(10);                                       // wait for conversion completed
                    AN0_Value = read_adc();                             // Lee el valor

                    set_adc_channel(1);                                 // Canal A1
                    delay_us(10);                                       // wait for conversion completed
                    AN1_Value = read_adc();                             // Lee el valor

                    set_adc_channel(2);                                 // Canal A2
                    delay_us(10);                                       // wait for conversion completed
                    AN2_Value = read_adc();                             // Lee el valor

                    set_adc_channel(3);                                 // Canal A3
                    delay_us(10);                                       // wait for conversion completed
                    AN3_Value = read_adc();                             // 

                    printf("\r\nAN0 = %lu      AN1 = %lu      AN2 = %lu      AN3 = %lu",AN0_Value,AN1_Value,AN2_Value,AN3_Value);
                    delay_ms(100);


   }
}

