/*
Code Support     : Board ET-BASE PIC8628
Compiler         : PIC C Compiler Version 4.069
Micro Controller : PIC18F8628
OSC              : 40MHz
Programmer       : WATCHARIN KAOROP
*/

#include <18F8628.h>

#define TX1   PIN_C6
#define RX1   PIN_C7
#define TX2   PIN_G1
#define RX2   PIN_G2

#fuses   H4,NOLVP,NOWDT,NOPROTECT,NOSTVREN

#use delay (clock = 40000000)
#use rs232(baud = 9600, xmit = TX1, rcv = RX1)

char Data1;

#use fast_io(C)


#INT_RDA
void IntRDA_isr(void)
{
   #use rs232(baud = 9600, xmit = TX1, rcv = RX1)
   Data1 = getc();
   putc (Data1);
}


#INT_RDA2
void IntRDA_isr2(void)
{
   #use rs232(baud = 9600, xmit = TX2, rcv = RX2)
   Data1 = getc();
   putc (Data1);
}


void main() {

  char Dat;

       set_tris_c(0B10000000);
       set_tris_g(0B10000100);

      enable_interrupts(GLOBAL);
      enable_interrupts(INT_RDA);
      enable_interrupts(INT_RDA2);

     #use rs232(baud = 9600, xmit = TX1, rcv = RX1)
     printf("\f\n\rPROGRAM TEST Interrupt UART-1 and UART-2 of ET-BASE PIC8628\n\r");

     #use rs232(baud = 9600, xmit = TX2, rcv = RX2)
     printf("\f\n\rPROGRAM TEST Interrupt UART-1 and UART-2 of ET-BASE PIC8628\n\r");

   While (1)
   {
      //  nothing

   }
}
