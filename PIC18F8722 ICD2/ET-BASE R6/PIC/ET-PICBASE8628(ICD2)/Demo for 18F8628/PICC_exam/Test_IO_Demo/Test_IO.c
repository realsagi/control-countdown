/*
Code Support     : Board ET-BASE PIC8628
Compiler         : PIC C Compiler Version 4.069
Micro Controller : PIC18F8628
OSC              : 40MHz
Programmer       : WATCHARIN KAOROP
*/

#include <18F8628.h>


#define TX1   PIN_C6
#define RX1   PIN_C7
#define TX2   PIN_G1
#define RX2   PIN_G2


#fuses   H4,NOLVP,NOWDT,NOPROTECT,NOSTVREN

#use delay (clock = 40000000)
//#use rs232(baud = 9600, xmit = TX1, rcv = RX1)

#use fast_io(A)
#use fast_io(B)
#use fast_io(C)
#use fast_io(D)
#use fast_io(E)
#use fast_io(F)
#use fast_io(G)
#use fast_io(H)
#use fast_io(J)

void main() {

  int i;
  unsigned char Dat;
  
      set_tris_a(0B00000000);
      set_tris_b(0B00000000);
      set_tris_c(0B00000000);
      set_tris_d(0B00000000);
      set_tris_e(0B00000000);
      set_tris_f(0B00000000);
      set_tris_g(0B00000100);
      set_tris_h(0B00000000);
      set_tris_j(0B00000000);
      
      

      setup_comparator(NC_NC_NC_NC);
      SETUP_ADC_PORTS(NO_ANALOGS);
      
      
      output_a(0xFF);
      output_b(0xFF);
      output_c(0xFF);
      output_d(0xFF);
      output_e(0xFF);
      output_f(0xFF);
      output_g(0xFF);
      output_h(0xFF);
      output_j(0xFF);      
      



   While (1)
   {
   
  
     Dat = 0B00000001;
     for (i=0;i<6;i++)
     {
         output_a(Dat);
         delay_ms(1000);
         Dat = Dat<<1;
     }
     
      output_a(0xFF);
     
     
      Dat = 0B00000001;
     for (i=0;i<10;i++)
     {
         output_b(Dat);
         delay_ms(1000);
         Dat = Dat<<1;
     }    
     
      output_b(0xFF);
 
      Dat = 0B00000001;
     for (i=0;i<10;i++)
     {
         output_c(Dat);
         delay_ms(1000);
         Dat = Dat<<1;
     }
 
  output_c(0xFF);
 
      Dat = 0B00000001;
     for (i=0;i<8;i++)
     {
         output_d(Dat);       
         delay_ms(1000);
         Dat = Dat<<1;
     }
 
 output_d(0xFF);

     Dat = 0B00000001;
     for (i=0;i<8;i++)
     {

         output_e(Dat);
         delay_ms(1000);
         Dat = Dat<<1;
     }

 output_e(0xFF);

     Dat = 0B00000001;
     for (i=0;i<8;i++)
     {
         output_f(Dat);       
         delay_ms(1000);
         Dat = Dat<<1;
     }

 output_f(0xFF);

     Dat = 0B00000001;
     for (i=0;i<5;i++)
     {     
         output_g(Dat);     
         delay_ms(1000);
         Dat = Dat<<1;
     }
     
      output_g(0xFF);
     
         Dat = 0B00000001;
     for (i=0;i<8;i++)
     {   
         output_h(~Dat);        
         delay_ms(1000);
         Dat = Dat<<1;
     } 
     
 output_h(0xFF);

     Dat = 0B00000001;
     for (i=0;i<8;i++)
     {   
         output_j(~Dat);
         delay_ms(1000);
         Dat = Dat<<1;
     }

 output_j(0xFF);

   }
}
