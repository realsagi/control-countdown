;**********************************************
; Test RS232 or RS422(Single node) 
; Hardware  : ET-BASE PIC40(18F458) 
; Oscillator 10 MHz
; file name : 232.ASM
; Assembler : mpasm.exe 
; Programmer: Watcharin Kaolop
; Company   : ETT  CO.,LTD.
; Date      : 09/11/2005
;**********************************************
; STEP to follow when test this program
;    1.Connect RS232 or RS422 cable between board and PC.
;    2.Download this program to CPU.
;    3.Run terminal program such as Procom plus,XTALK etc.
;    4.Set parameter Procom plus to 9600 baud,No parity,8 bits data and 1 bit. stop
;    5.Reset board ,after reset board you will see this message on PC.
;
;         "TEST PIC18F458 ON RS-232 By ETT CO.,LTD."
;       
;    6.Press any key on keyboard, you will see the key that you pressed.
;**********************************************


     list p=18f458                 ; list directive to define processor
     #include <p18f458.inc>        ; processor specific variable definitions

offset    EQU       0x20
temp      EQU       0x21
TEST	  EQU	    0x22
dt1       EQU       0x23
dt2       EQU       0x24
dt3       EQU       0x25

          ORG       0x0000

;************ initial *******************
init      clrf      TRISD
          movlw     0x40           ; BAUD rate 9600
          movwf     SPBRG
          clrf      TXSTA          ; 8 bits data ,no,1 stop
          bsf       RCSTA,SPEN     ; Asynchronous serial port enable
          bsf       RCSTA,CREN     ; continuous receive
          bsf       TXSTA,TXEN     ; Transmit enable
          bsf	    TXSTA,BRGH	   ; HI SPEED
	  	  
;********** start to send **********************

new       clrf      offset         ; load offset of character table
	  call	    delay
start     movf      offset,w       
          call      TAB
          addlw     0              ; Character = 00 ?
          btfsc     STATUS,Z       ; Character = 00 ?
          goto      wait2          ; Yes , Z = 1
                                   ; No  , Z = 0
          movwf     TXREG          ; Send recent data to TX 
wait1     lfsr      0,TXSTA        ; 
          btfss     INDF0,1        ; check TRMT bit in TXSTA (FSR)
          goto      wait1          ; TXREG full  or TRMT = 0
          incf      offset,f       ; TXREG empty  or TRMT = 1
	  incf	    offset,f
          goto      start          ; Send again

;********** start to receive *******************          
wait2     btfss     PIR1,RCIF      ; Check RCIF  bit in PIR1 register
          goto      wait2          ; RCREG empty or RCIF = 0
          movf      RCREG,w        ; RCREG full or RCIF = 1
          movwf     TXREG
          goto      wait2

;********* Tebel of message ********************  
TAB       addwf     PCL            ; Move offset to PC lower

	  DT	    0x0C,"TEST PIC18F458 ON RS-232 By ETT CO.,LTD.",0XA,0XD,0X0

  
delay     movlw	    5
	  movwf	    dt1
sd3	  clrf      dt2
sd2       clrf      dt3
sd1       decfsz    dt3
          goto      sd1
          decfsz    dt2
          goto      sd2
	  decfsz    dt1
	  goto      sd3
          return

        END
