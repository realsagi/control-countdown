'*********************************************
'Test read data from internal ADC
'Hardware   : ET-BASE PIC40 (18F4620)
'file name  : PicBasic Pro compiler 
'Programmer : Watcharin Kaorop
'Company    : ETT  CO.,LTD.
' Date      : 9/11/2005
'*********************************************
         INCLUDE "modedefs.bas"          ' Include serial modes

SI      VAR     PORTC.7
SO      VAR     PORTC.6           ' Define serial output pin

Result  var     word

       define OSC 10
       
      DEFINE ADC_BITS 10       ' Set number of bits in result
      DEFINE ADC_CLOCK 3       ' Set clock source (rc = 3)
      DEFINE ADC_SAMPLEUS 50   ' Set sampling time in microseconds 

      TRISA  = 255             ' Set PORTA to all input
      ADCON1 = %00000000       ' PORTA is analog
      ADCON2.7 =  1            ' Right justified
      
	
	     Serout2  SO,84,[12,"Test Analog to Digital converter of PIC MCU",10,13]
	     Serout2  SO,84,["Connect analog voltage to RA0(AN0)",10,13]
	     Serout2  SO,84,["Result of ADC will display on Screen(RS232)",10,13]
	
loop:    
         ADCIN   0,Result             ' Read channel 0 to Result

         Serout2  SO,84,[dec Result]  ' Display in Decimal format.
         Serout2  SO,84,[10,13]       ' Line feed
         pause   100
        
         goto    loop
    END
