;********************************************                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      ;********************************************
; Test  LED On Board
; Hardware  : ET-BASE PIC40/18F4620 
; OSC   10MHz
; Assembler : mpasm.exe
; Programmer: Watcharin Kaorop
; Company   : ETT  CO.,LTD.
; Date      : 9/11/2005
;********************************************

     LIST P=18f4620
     include <p18f4620.inc>

dt1         EQU       0x23
dt2         EQU       0x24
dt3         EQU       0x25

           ORG       0x0000

	    clrf	 TRISA	        ; port A is output
		clrf     TRISB          ; port B is output
	    clrf	 TRISC	        ; port C is output
		clrf     TRISD          ; port D is output
	    clrf	 TRISE	        ; port E is output
	
	    movlw    0x07
		movwf    ADCON1			; PORTA is digital I/O
		movwf    CMCON			; Comparator off

loop    movlw    0x55
		movwf    PORTA          ; Out data to PORTA
		movwf    PORTB          ; Out data to PORTB
		movwf    PORTC          ; Out data to PORTC
		movwf    PORTD          ; Out data to PORTD
		movwf    PORTE          ; Out data to PORTE
        call     delay

        movlw    0xAA
		movwf    PORTA          ; Out data to PORTA
		movwf    PORTB          ; Out data to PORTB
		movwf    PORTC          ; Out data to PORTC
		movwf    PORTD          ; Out data to PORTD
		movwf    PORTE          ; Out data to PORTE
        call     delay

        goto     loop

;***************************************
	
delay   movlw	  5
	    movwf	  dt1
sd3	    clrf      dt2
sd2     clrf      dt3
sd1     decfsz    dt3
        goto      sd1
        decfsz    dt2
        goto      sd2
	    decfsz    dt1
	    goto      sd3
        return

     end

