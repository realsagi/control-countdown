'*********************************************
'Test RS-232 Port
'Hardware   : ET-BASE PIC40 (18F4620)
'file name  : PicBasic Pro compiler 
'Programmer : Watcharin Kaorop
'Company    : ETT  CO.,LTD.
' Date      : 9/11/2005
'*********************************************

           INCLUDE "modedefs.bas"          ' Include serial modes

SI      VAR     PORTC.7
SO      VAR     PORTC.6                       ' Define serial output pin

B0      VAR     BYTE
B1      VAR     BYTE

DEFINE  OSC     10


        SerOut SO,T9600,[12,"PROGRAM TEST RS232 PORT",10,13] 
          
LOOP:   SerIn  SI,T9600,B0
        SerOut SO,T9600,[B0]
        GoTo   LOOP

