' PEEK and POKE Commands
'
' Read and write PortA using Peek and Poke

Symbol  PortA = 5                       ' PortA address
Symbol  TrisA = $85                     ' PortA data direction register
Symbol  SO = 0                          ' Serial Output

	poke TrisA, 0                   ' Set PortA to all output
	poke PortA, 31                  ' Send a value to PortA

	poke TrisA, 255                 ' Set PortA to all input
	peek PortA, B0                  ' Get PortA inputs to variable B0

	serout SO,N2400,(#B0,10)        ' Send variable to serial out
