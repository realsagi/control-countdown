' EEPROM, READ and WRITE Commands
'
' Demonstate commands for EEPROM. Works on PIC16F(C)84 targets only!!!
' Initialized address 0..5 and 9. Writes 10..63. This leaves addesses
' 6..8 undefined (assuming your programmer doesn't unconditionally
' program all EEPROM locations).

        Include "modedefs.bas"          ' Include serial modes

SO      con     0                       ' Define serial output pin
B0      var     byte
B1      var     byte
B2      var     byte

EEPROM  ["vwxyz"]                       ' EEPROM[0..4] = 118..122
EEPROM  9,[100]                         ' EEPROM[9] = 100

loop:   B0 = 63                         ' Set Size of EEPROM

	For B1 = 10 To B0		' Check WRITE Command
                B2 = B1 + 100           ' EEPROM[10..63] = 110..163
                Write B1,B2
	Next B1

	For B1 = 0 To B0		' Check READ Command
                Read B1,B2              ' Dump EEPROM Contents
                Serout SO,N2400,[#B2," "]
	Next B1
        Serout SO,N2400,[10,10]         ' Skip 2 Lines

        Goto loop                       ' Forever
