' READ and WRITE word variables to on-board EEPROM
'
' Word is used to add 1000 to each location address and store the
' result.  The word data must be stored as 2 separate bytes.


        INCLUDE "modedefs.bas"          ' Include serial modes

SO      VAR		PORTC.6					' Define serial output pin

B1      VAR     BYTE					' Address variable
W2      VAR     WORD					' Data variable


loop:   

		For B1 = 0 To 12 step 2			' Step 2 because each word requires 2 bytes of memory
        	W2 = B1 + 1000          	' Add 1000 to address
        	Write B1, W2.HIGHBYTE		' Write high byte of word
        	Write B1+1, W2.LOWBYTE		' Write low byte of word to next address
		Next B1

		For B1 = 0 To 12 step 2			' Step 2 to get word size data
        	Read B1, W2.HIGHBYTE		' Read high byte
        	Read B1+1, W2.LOWBYTE		' Read low byte
        	SerOut SO,T2400,[#W2," ",10,13]	' Display the word data
		Next B1
		
        SerOut SO,T2400,[10,13,10,13] 	' Skip 2 Lines

        GoTo loop                       ' Forever

