' SHIFTIN and SHIFTOUT Commands

        Include "modedefs.bas"  ' Include shift modes

D1PIN   var     PORTA.0         ' Shift data pin 1
C1PIN   var     PORTA.1         ' Shift clock pin 1
D2PIN   var     PORTB.0         ' Shift data pin 2
C2PIN   var     PORTB.1         ' Shift clock pin 2

bvar    var     byte
wvar    var     word


' Shift in 10 bits of data
        Shiftin D1PIN, C1PIN, MSBPRE, [wvar\10]

        bvar = wvar

' Shift out 8 bits of data onto other pins
        Shiftout D2PIN, C2PIN, MSBFIRST,[bvar]

        End
