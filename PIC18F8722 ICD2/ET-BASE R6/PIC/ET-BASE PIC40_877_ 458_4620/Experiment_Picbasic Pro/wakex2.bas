' PicBasic Pro program to demonstrate wake on interrupt.

' You should DISABLE THE WATCHDOG TIMER when programming the
' PICmicro device.

' The program outputs the condition of the switches on the
' LEDs, then immediately goes into power-down mode.  When
' the condition on any switch changes, the RB port change
' interrupt occurs, waking the part.
' 	Since the global interrupt enable bit is not set, no 
' jump to the interrupt vector occurs.  Program execution 
' is resumed, the LEDs are updated, and the part goes back
' to sleep.
' 	To further reduce the power consumption, all unused
' hardware peripherals should be disabled.  The PORTB
' pullups should be replaced with external resistors, and
' the internal pullups disabled.


' Define ONINT_USED to allow use of the boot loader.
' This will not affect normal program operation.
DEFINE	ONINT_USED	1

' Define the pins that are connected to pushbuttons.
' The switches must be connected to PORTB, pins 4,5,6,
' or 7 in order to use the RB port change interrupt.
sw1	VAR	PORTB.4
sw2	VAR PORTB.5
sw3	VAR	PORTB.6

' Define the pins that are connected to LEDs
led1	VAR	PORTB.0
led2	VAR	PORTB.1
led3	VAR	PORTB.2

INTCON.3 = 1		' Enable the RB port change interrupt
OPTION_REG = $7f	' Enable PORTB pull-ups
TRISB = %11111000	' Set PORTB.0-2 (LEDs) to output, 3-7 to input

main:	' main program begins here

	PORTB = 0		' Turn off all LEDs

	' Check any button pressed to toggle on LED
	IF sw1 = 0 Then led1 = 1
	IF sw2 = 0 Then led2 = 1
	IF sw3 = 0 Then led3 = 1
	
	INTCON.0 = 0	' Clear the RB port change flag bit
	
	NAP 7			' Go to sleep.  When the watchdog is
					' disabled, NAP won't wake up until
					' an interrupt occurs.

	GoTo main		' Do it again upon waking

End
