'Example program from manual to blink an LED connected to RB0 about once a second

loop:   High 0          'Turn on LED connected to RB0
        Pause 500       'Delay for .5 seconds

        Low 0           'Turn off LED connected to RB0
        Pause 500       'Delay for .5 seconds

        Goto loop       'Go back to loop and blink LED forever
        End
