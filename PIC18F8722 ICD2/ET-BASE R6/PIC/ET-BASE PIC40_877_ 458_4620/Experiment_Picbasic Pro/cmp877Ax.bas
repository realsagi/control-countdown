
' 08/06/02, PicBasic Pro Compiler 2.42, 16F877A, LAB-X1 Experimenter Board

' PicBasic Pro program to demonstrate the setup and use of analog comparators.
' This program will not compile for the 16F877, as it has no comparators.  You
' must use the 16F877A, or another PICmicro MCU with comparators.  Inputs are
' analog voltages on RA0 and RA3.  The program will compare the voltages and 
' tell you which is greater.  On the LAB-X1, POT1 and POT3 adjust the respective
' inputs.

' Define LCD connections
DEFINE  LCD_DREG        PORTD
DEFINE  LCD_DBIT        4
DEFINE  LCD_RSREG       PORTE
DEFINE  LCD_RSBIT       0
DEFINE  LCD_EREG        PORTE
DEFINE  LCD_EBIT        1

DEFINE	LOADER_USED		1	' Required only for use with melabs Loader


C1OUT VAR CMCON.6	' Alias C1OUT to output bit in CMCON

ADCON1 = 7			' Make all PORTA and PORTE pins digital
CMCON = 2			' Set comparators to mode 010 (see datasheet)

Low PORTE.2			' Set LCD to write mode
Pause 150			' Pause to let LCD power up

loop:

	IF C1OUT Then	' Check comparator output
	
		' Display if C1OUT = 1
		LCDOut $fe,1, "C1OUT = 1"
		LCDOut $fe,$C0, "PORTA.3 GREATER"
	Else
		
		' Display if C1OUT = 0
		LCDOut $fe,1, "C1OUT = 0"
		LCDOut $fe,$C0, "PORTA.0 GREATER"
		
	EndIF
	
	Pause 100		' Pause 100mS to reduce LCD flicker
	
GoTo loop			' Do it forever


	