' PULSIN Command
'
' Serial Pulse Width Meter

        Include "modedefs.bas"          ' Include serial modes

SO      con     0                       ' Define serial out pin
FI      con     4                       ' Define frequency input pin
W0      var     word

loop:   Pulsin FI,0,W0                  ' Measure pulse (in 10 uSec)
        If W0 = 0 Then disp             ' If non-zero, display
        Serout SO,N2400,[#W0]
disp:   Serout SO,N2400,["0 uSec",13,10]        ' Display trailer
        Goto loop                       ' Forever
