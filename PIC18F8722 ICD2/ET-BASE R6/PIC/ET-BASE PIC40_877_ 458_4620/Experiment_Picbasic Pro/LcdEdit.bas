' LcdEdit.BAS      PicBasic Pro 2.4 using 24 char Lcd Display and 4 buttons  6 Jul 2002
' Editing and saving Numbers and Alfa Characters on Lcd
' by Steve Collins  SteveColln@cs.com Oceanport NJ USA 732 571 4183


W0 var word          : W1 var word         : W2 var word        : W3 var word
b0  var W0.LowByte   : b1  var W0.HighByte : b2  var W1.LowByte : b3  var W1.HighByte
b4  var W2.LowByte   : b5  var W2.HighByte : b6  var W3.LowByte : b7  var W3.HighByte

     clear           : goto EndStuff   : Pause 200              'put the ugly stuff at the end
here:More var PortB.2: Less var PortB.5: Right var PortB.4: Left var PortB.3: OnOff var PortB.6: Reset var PortB.7
     Lc var byte [15]: i var byte      : VarEd var byte   : flag var bit

     LcdOut $FE, 1, "Customer's Name Here Inc." 'this does not change

     flag = 1                                   'flag to return, initialize if blank chip
     For i = 0 to 15 : read i, b0:if b0 = 0 then lookup i,[1,2,3,11,4,5,6,11,7,8,9,10,11,12,13,14], b0
     Lc(i)=b0        : goto LL
L:   next i
     flag = 0                                   'return flag off

     i = 0   : LcdOut $FE, $0E                  'turn underline cursor on
LL:  B0=Lc(i): if B0=10 then B0=0               'encode a zero as a ten
               if B0=11 then B0=239             'rolls over to give 32 or $20 for space
     LcdOut $FE, $C5+i,(B0+$30),$FE,$C5+i       'print the variable and move the cursor back where it was it was
     if flag = 1 then goto L

     b2 = 0
LLL: pause 200
     if Lc(i) > 128 then Lc(i)=Lc(i) - 128      'exclude the oriental characters
     if Right = 0 then i = i + 1: goto LL       'mover cursor to right
     if Left  = 0 then i = i - 1: goto LL
     if More  = 0 then Lc(i) = Lc(i) + 1: write i,Lc(i):goto LL  'increase value
     if Less  = 0 then Lc(i) = Lc(i) - 1: write i,Lc(i):goto LL
     B2 = B2 + 1:if b2< 200 then LLL

Loop:  LcdOut $FE,$0C                                            'cursor off cleasr screen
       goto Loop                                                 'Rest of program goes here

EndStuff: Option_Reg.7 = 0  : trisB = %11111110
          ADCON1=132                       ' 128 + 4  'Left just 3 analog inputs rest digital
          Define LCD_DREG  PORTC
          Define LCD_DBIT  4
          Define LCD_RSREG PORTC
          Define LCD_RSBIT 3               
          Define LCD_EREG  PORTC
          Define LCD_EBIT  0               
          Define DEBUG_REG PORTB
          Define DEBUG_BIT 0               ' is 7 on other units so it is on end
          T2CON=%00000111:PR2=190:CCP1CON=%00001100:CCP2CON=%00001100   '%00000110=244hz and pre=190 gives 328 hz  %00000100 = 3.9khz
          high portC.1: high PortC.2        'make Ccp Lines outputs
          goto here



