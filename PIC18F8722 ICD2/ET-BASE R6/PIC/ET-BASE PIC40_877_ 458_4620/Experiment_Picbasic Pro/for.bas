' FOR..NEXT Command
'
' Prints series of numbers showing STEP facility.

        Include "modedefs.bas"          ' Include serial modes

SO      con     0                       ' Define serial output pin
B0      var     byte
B1      var     byte

loop:   For B0 = 3 To 1 Step -1         ' Countdown step size
                For B1 = 0 To 10 Step B0        ' Count to 10 (or so)
                        Serout SO,N2400,[#B1," "]
		Next B1
                Serout SO,N2400,[13,10] ' End of line
                Pause 100               ' Delay
	Next B0
        Goto loop
