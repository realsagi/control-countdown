' EEPROM, READ and WRITE Commands
'
' Demonstate commands for EEPROM. Works on 16C84 and 16Fxxx targets only!!!
' Initialized address 0..5 and 9. Writes 10..63. This leaves addesses
' 6..8 undefined (assuming your programmer doesn't unconditionally
' program all EEPROM locations).

Symbol	SO = 0				' Serial Output

EEPROM	("vwxyz")			' EEPROM[0..4] = 118..122
EEPROM	9,(100)				' EEPROM[9] = 100


Loop:   For B1 = 10 To 63               ' Check WRITE Command
		B2 = B1 + 100			' EEPROM[10..63] = 110..163
		write B1,B2
	Next B1

        For B1 = 0 To 63                ' Check READ Command
		Read B1,B2			' Dump EEPROM Contents
		serout SO,N2400,(#B2," ")
	Next B1
	serout SO,N2400,(10,10)

	goto Loop			' Forever
