' BUTTON Command
'
' Demonstate BUTTON command. Prints working variable (in this case B0) to
' show how auto-repeat works. Prints "PRESS" when button is pressed or
' repeated.

Symbol	SO = 0				' Serial Output
Symbol	B = 5				' Button Input

	B0 = 0				' Zero Button Working Buffer
Loop:	Button B,1,10,5,B0,0,Skip	' Check Button (Skip if Not Pressed)
	Serout 0,N2400,("Press",13,10)	' Indicate Button Pressed
Skip:	Serout 0,N2400,(#B0,13,10)	' Show Working Variable
	Pause 100			' Visual Pause
	Goto Loop			' Forever
