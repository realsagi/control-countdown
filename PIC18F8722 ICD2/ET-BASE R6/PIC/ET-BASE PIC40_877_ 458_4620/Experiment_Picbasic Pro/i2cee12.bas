' I2CREAD and I2WRITE Commands with internal EEPROM on 12-bit core
'
' Write to the first 16 locations of internal I2C EEPROM
' Read first 16 locations back and send to serial out repeatedly
' Tested on 12CE519
' Output: A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P,


' Define debug and I2C pins
DEFINE DEBUG_REG	GPIO
DEFINE DEBUG_BIT	1
DEFINE DEBUGIN_BIT	2
DEFINE DEBUG_BAUD	9600
DEFINE DEBUG_MODE	0

DEFINE	I2C_INTERNAL	1
DEFINE	I2C_SCL	GPIO,7
DEFINE	I2C_SDA	GPIO,6

' Declare variables
B1	VAR	BYTE
B2	VAR	BYTE
B3	VAR	BYTE

' calibrate the internal oscillator
OSCCAL = $c4

loop:

	For B3 = 0 to 15	' Loop 16 times for data write
	
		B1 = B3 + 65	' Add 65 to loop counter to create ASCII alphabet
		I2CWrite GPIO.6,GPIO.7,$A0,B3,[B1]  ' Write data to EEPROM
		Pause 10                ' Delay 10ms after each write

	Next B3	
	
	For B3 = 0 to 15 step 2	' loop again but step 2 to read 2 locations each loop

	    I2CRead GPIO.6,GPIO.7,$A0,B3,[B1,B2]	' Read 2 locations
		Debug B1,", ",B2,", "   ' Print 2 locations

	Next B3	


	Debug 10,13            ' Print linefeed
	
GoTo loop
