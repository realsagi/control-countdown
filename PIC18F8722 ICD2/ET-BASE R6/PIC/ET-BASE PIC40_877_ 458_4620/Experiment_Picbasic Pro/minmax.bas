' MIN/MAX Operators
'
' Use MIN and MAX operators to Bound [0..9] to [3..7]

Symbol	SO = 0				' Serial Out

Loop:	For B0 = 0 To 9			' B0 = 0..9
		B1 = B0 Min 3 Max 7	' B1 = B0 Bounded to [3..7]
					' Display Results
		Serout 0,N2400,(#B0," ",#B1,10,13)
	Next B0
	SerOut 0,N2400,(10)		' Line Break
	Goto Loop			' Forever
