'Using PIC 84 to: 
'Get Speed in feet/minute by parsing the $GPRMC sentence 
'Use NONE/NMEA protocol on Garmin12 or similar units 
 
'pinouts: 
      
GPSin VAR PORTB.2 	'pin 8 
 
'Allocate Variables for GPS: 
TimeOut CON 3000 
baudGPS CON 24764 	'16572 + 8192 (bit 13) 
tens VAR BYTE 		'GPS results 
digits VAR BYTE 
tenth VAR BYTE 
knots VAR WORD 
fps VAR WORD 
fpm VAR WORD 
 
    
GPS: 
SerIn2 GPSin,baudGPS,Timeout,Nogps,[wait("$GPRMC"),skip 34,DEC1 tens,DEC1 digits,skip 1,DEC1 tenth] 
knots =  tens *100 + digits * 10 + tenth 	'tenths of knots 
fps = knots * 81 		'knots * 1.689 = fps (times 60 to get fpm) 
fpm = fps /8 			'81/48 = 1.688 *6, 6/48 = 1/8 
'using 6 instead of 60 takes care of the tenths of knots 
'necessary to read GPS in integer form. 
 
Nogps: 
LCDOut $FE, 1 		'Clear Screen 
LCDOut "No GPS" 	'LCD section not shown 
Pause  2000 
 
