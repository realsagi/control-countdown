' I2CREAD and I2WRITE Commands
'
' Write to the first 16 locations of an external serial EEPROM
' Read first 16 locations back and send to serial out repeatedly
' Note: for EEPROMS with byte-sized address

        Include "modedefs.bas"          ' Include serial modes

SO      con     0                       ' Define serial output pin
DPIN    var     PORTA.0                 ' I2C data pin
CPIN    var     PORTA.1                 ' I2C clock pin
B0      var     byte
B1      var     byte
B2      var     byte

        For B0 = 0 To 15                ' Loop 16 times
                I2CWRITE DPIN,CPIN,$A0,B0,[B0]  ' Write each location's address to itself
                Pause 10                ' Delay 10ms after each write
        Next B0

loop:   For B0 = 0 To 15 step 2         ' Loop 8 times
                I2CREAD DPIN,CPIN,$A0,B0,[B1,B2]        ' Read 2 locations in a row
                Serout SO,N2400,[#B1," ",#B2," "]       ' Print 2 locations
        Next B0

        Serout SO,N2400,[10]            ' Print linefeed

        Goto loop
