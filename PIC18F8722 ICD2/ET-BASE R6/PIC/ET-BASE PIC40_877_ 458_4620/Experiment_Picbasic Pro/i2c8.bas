' I2CIN and I2COUT Commands
'
' Write address to the first 16 locations of an external serial EEPROM
' Read first 16 locations back and send to serial out repeatedly
' The control byte and address variable are selected for EEPROM's with 
' 1-byte addressing, such as 24LC01, 24LC02, etc.

Symbol	SO = 0				' Serial Output

        For B0 = 0 To 15                ' Loop 16 times
                I2Cout $50,B0,(B0)      ' Write each location's address to itself
                Pause 10                ' Delay 10ms after each write
        Next B0

Loop:   For B0 = 0 To 15 step 2         ' Loop 8 times
                I2Cin $50,B0,B1,B2      ' Read 2 locations in a row
                Serout SO,N2400,(#B1," ",#B2," ")       ' Print 2 locations
        Next B0

        Serout SO,N2400,(10)            ' Print linefeed

        Goto Loop
