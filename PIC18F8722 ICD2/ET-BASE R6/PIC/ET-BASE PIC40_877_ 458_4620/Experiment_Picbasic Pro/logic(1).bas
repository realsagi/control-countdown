' Logical Operators
'
' Display Truth Table for Binary Logical Operators

Symbol	SO = 0				' Serial Output

Loop:	Serout SO,N2400,("      &  &/ |  |/ ^  ^/",10,13)
	For B0 = 0 To 1
		For B1 = 0 To 1
			Serout SO,N2400,(#B1," ",#B0," : " )
			B2 = B1 & B0 : Gosub Disp
			B2 = B1 &/ B0 : Gosub Disp
			B2 = B1 | B0 : Gosub Disp
			B2 = B1 |/ B0 : Gosub Disp
			B2 = B1 ^ B0 : Gosub Disp
			B2 = B1 ^/ B0 : Gosub Disp
			Serout SO,N2400,(10,13)
		Next B1
	Next B0
	Serout SO,N2400,(10,13)
	Goto Loop
	
Disp:	B2 = B2 & 1
	Serout SO,N2400,(#B2,"  ")
	Return

