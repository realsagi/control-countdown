' SOUND Command
'
' Make random computer-like noises. More refinement might make sound effects
' realistic enough to convince your boss you're working when you're really
' just playing Doom!!!

        Include "bs1defs.bas"           ' Include BS1 variables

SND     con     0                       ' Define speaker pin

loop:   Random W0                       ' Randomize W0
        B2 = (B0 & 31) + 64             ' Generate notes [64..95]
        If B2 >= 68 Then beep           ' Make [64..68] silence
	B2 = 0
beep:   Sound SND,[B2,4]                ' Generate sound
        Goto loop                       ' Forever
