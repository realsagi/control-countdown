' SLEEP Command
'
' Slowly Blink LED Using Low Power Mode Delay

Symbol	LED = 2				' LED Pin

Loop:	Toggle LED			' Toggle LED
	Sleep 10			' Sleep for 10 Seconds (or so)
	Goto Loop			' Forever

