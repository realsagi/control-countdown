' LOOKUP & RANDOM Commands
'
' Convert numeric value to ASCII Hexadecimal equivalents.

Symbol	SO = 0				' Serial Out

Loop:	W6 = W0 : Gosub Hex		' Print W0
	Serout SO,N2400,(13,10)		' Print End of Line
	Random W0			' Randomize W0
	Goto Loop			' Do This Forever!!!

						' Send W6 as XXXX (Uses W5)
Hex:	Gosub HexB13				' Print MSB
	B13 = B12				' Print LSB
HexB13:	B11 = B13 / 16				' Print MSN
	Gosub HexB11
	B11 = B13 & 15
HexB11:	Lookup B11,("0123456789ABCDEF"),B10	' B10 = HEX$(B11)
	Serout SO,N2400,(B10)
	Return
