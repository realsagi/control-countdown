' Simulate BS2 Shiftin and Shiftout

Symbol  DDIR = Dir0     ' Shift data pin direction is Dir0
Symbol  DPIN = Pin0     ' Shift data pin is 0
Symbol  CPIN = 1        ' Shift clock pin is 1

Symbol  I = B2          ' Loop counter


' Shift in some data
        Low CPIN        ' Start shift clock low

        Gosub shiftin   ' Shift in some data

' Shift out some data
        Low CPIN        ' Start shift clock low

        B0 = 100        ' Data to shift out
        Gosub shiftout  ' Go do it

        End


' Subroutine to synchronously shift in one byte
shiftin: DDIR = 0       ' Set data pin direction to input

        For I = 1 to 8  ' 8 bits to a byte
        B0 = B0 * 2     ' Shift result 1 bit to the left
        Toggle CPIN     ' Toggle shift clock
        Bit0 = DPIN     ' Move data into LSB
        Toggle CPIN     ' Toggle shift clock once more
        Next I          ' Loop

        Return          ' Go back to caller


' Subroutine to synchronously shift out one byte
shiftout: DDIR = 1      ' Set data pin direction to output

        For I = 1 to 8  ' 8 bits to a byte
        DPIN = Bit0     ' Data out is LSB
        Toggle CPIN     ' Toggle shift clock
        B0 = B0 / 2     ' Shift byte 1 bit to the right
        Toggle CPIN     ' Toggle shift clock once more
        Next I          ' Loop

        Return          ' Go back to caller
