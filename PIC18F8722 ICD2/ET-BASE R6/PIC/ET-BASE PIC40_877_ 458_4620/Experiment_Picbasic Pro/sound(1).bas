' SOUND Command
'
' Make random computer-like noises. More refinement might make sound effects
' realistic enough to convince your boss you're working when you're really
' just playing Doom!!!

Symbol	SND = 0				' Speaker Pin

Loop:	Random W0			' Randomize W0
	B2 = B0 & 31 + 64		' Generate Notes [64..95]
	If B2 >= 68 Then Beep		' Make [64..68) Silence
	B2 = 0
Beep:	Sound SND,(B2,4)		' Generate Sound
	Goto Loop			' Forever

