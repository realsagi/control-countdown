' SERIN & SEROUT Commands
'
' Upper case serial filter.

Symbol	SO = 0				' Serial Out
Symbol	SI = 1				' Serial In

Loop:	Serin SI,N2400,B0			' B0 = Next Character
	If B0 < "a" or B0 > "z" Then Print	' If Lower Case, Convert
	B0 = B0 - $20
Print:	Serout SO,N2400,(B0)			' Send Character
	Goto Loop				' Forever
