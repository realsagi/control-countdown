'
' Six Pics.bas  Six Pic microprocessors on one serial Port!         15 May 2001
'     Single ended; talk only operation. Same software in all pics
'     Tested with six but should work with 20 or more Pics
'     also drives 7 seg display LED's with no transistors
'     compiled in PicBasicPro ver 2.32 / Microengineering Labs 
'
'                 by Stephen R. Collins               
'                  Collins & Associates
'          Custom Pic Microprocessor Instrumentation
'              Custom Visual Basic Applications
'                   design and manufacturing
'                    stevecolln@cs.com
'           
'
' 7 Segment Common Cathode High Eff Displays 470 ohm resistors on each segment
' Channel 0 is reading from Transducer Ch1 is Setpoint
' charge pump on second analog output using PWM output produces negative supply
' show the status of the setpoint if it is right PortA.2 is from jumper to show sp



    OPTION_REG.7 = 0                    'enable portb pullups    
    ADCON1=133  ' 128 + 5 = %10000101   'A0 and A1 analog inputs A2 digital A3 ref A4 digital

    TRISA = 255                         'all inputs for the a to d
    TRISB = 0                           'Port B0, B1, B2, B3 = 1000, 100, 10, 1 Comm Cathode
    TRISC = 0                           'Port C all outputs driving segments and dp.

    DEFINE DEBUG_REG PORTC
    DEFINE DEBUG_BIT 6

    INCLUDE "scdefs.bas"                'this has the Adc stuff in it. APPENDED	BELOW
    			'YOU MUST CREATE THIS INCLUDE FILE BEFORE COMPILING!!
        
        
    Torr VAR WORD    : timr1 VAR BYTE   : WhichOne VAR BYTE : StartCh VAR BYTE : Endch VAR BYTE
    Qtime VAR WORD   : MyQuiet VAR WORD : Shutup VAR WORD
    DigWdp VAR BYTE  : ud VAR BYTE      : dd VAR BYTE     :cd VAR BYTE    :kd VAR BYTE
    CountsCt VAR WORD: CountSp1 VAR WORD:CountSp2 VAR WORD:a4image VAR BIT

    PR2 = $FF: T2CON = %00000100 : CCP1CON = %00001100 : CCP2CON = %00001100 
    CCPR2L = 128                        'Pwm for voltage booster
    DigWdp = 3                          'Digit with decimal point


Loop: ' Debug "W11=",#W11," counts " ' --------------Loop starts; reads A to D  64 times while strobing result in W11---------------------

      IF W11 =< 4096 Then W11 = 0                       'remove zero offset from transducer amp
      IF W11  > 4096 Then W11 = W11 - 4096
      Torr = W11 / 19 : W11 = 0                         'the 19 scales the result

      kd = Torr DIG 3: cd = Torr DIG 2:  dd = Torr DIG 1: ud = Torr DIG 0

      ADCIN 1, W0  : WHichOne = ( W0 + 10 ) / 25        ' Units set .1v .2v .3v .4v .5v .6v 'Adc in aprox 25 per.1volt
      MyQuiet = 15 + WhichOne * 5                       '    20  25  30  35  40  45         'How many millisec This waits to talk
      StartCh = 64 + Whichone : Endch = WhichOne + 96   '    Aa  Bb  Cc  Dd  Ee  Ff         'Start and finish char : C1234c 
      IF PORTA.2=0 Then W0=W0*4:kd=W0 DIG 3:cd=W0 DIG 2:dd=W0 DIG 1: ud=W0 DIG 0            'if the jumper is in place, show them which one

sppp: 
Loopx: GoSub Strobe: GoTo loop  '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Loop  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


       '-----Strobe digits 256 times; dwell on digits for 3 milliseconds-----

Strobe: b7=b7+1: IF b7 =1 Then b0 = ud  'b7 =1 is ones'digit 2 is 10's etc                        
                 IF b7 =2 Then b0 = dd  
                 IF b7 =3 Then b0 = cd  
                 IF b7 =4 Then b0 = kd                'This never quite worked right; b1=0 a few lines down
                 IF b7 =4 AND  b0  = 0 Then b0 = 10   'Make this a 10 and it blanks the display 0 is leading zero

        LookUp B0,[63,6,91,79,102,109,125,7,127,103, 0 ],B1  'segment codes and blank

        IF b7 = DigWdp Then b1 = b1 + 128             'Light the decimal point if necessary

        PORTC = 0 : Low PORTB.4:Low PORTB.5:Low PORTB.6:Low PORTB.7     'turn all segs off

        IF b7=1 Then PORTB.0 = 1:PORTB.3 = 0   'PortB.0 was the last one on so make it high and turn it off
        IF b7=2 Then PORTB.3 = 1:PORTB.2 = 0   'This section enables the digits by pulling them low
        IF b7=3 Then PORTB.2 = 1:PORTB.1 = 0   'hundreds digit if PortB.1
        IF b7=4 Then PORTB.1 = 1:PORTB.0 = 0:b7 = 0  'Thousands digit is PortB.0

        IF B1.0=1 Then High PORTC.0 ' Light segments as rqd
        IF B1.1=1 Then High PORTB.4 '   segments are on 
        IF B1.2=1 Then High PORTB.5 '    various lines    
        IF B1.3=1 Then High PORTC.3
        IF B1.4=1 Then High PORTC.4
        IF B1.5=1 Then High PORTC.5
        IF B1.6=1 Then High PORTB.6
        IF B1.7=1 Then High PORTB.7

        b24 = b24 + 1:ADCIN 0, W0:GoSub MaybeTalk : IF b7 = 0 Then W11=W11+W0

        IF b24 <> 0   Then Strobe
        Poke  7, 0   :Low PORTA.5: Return' Make C all low and make the line on PortA low ......end strobe...................


MaybeTalk: timr1=0 : shutup = shutup + 1                        'this is the 3 millisec delay for the display; shutup prevents talking too much

Maybe2: timr1 = timr1 + 1 :PauseUs 300                          '
        Input PORTC.6 : IF PORTC.6 = 1 Then  Qtime = 0          'make the serial line an input and see if it is quiet.
        IF timr1 < 10 Then GoTo Maybe2                          'stay here 300 usec x 10 = 3 millisec for display
        Qtime = Qtime + 1 : IF Qtime > MyQuiet Then GoSub talk  'qtime is how long the line has been quiet
        Return

Talk:   IF Shutup < 250 Then Return                             'you still don't talk unless it has been one second
        Shutup = 0:IF WhichOne =< 1 Then Debug 13,10            'carriage return only on first unit

        Debug StartCh 
        Debug #kd,#cd,".", #dd' #ud
        Debug Endch," "                                         '"A1234a "  is what the data looks like
        Return


 '      lo    hi        variable usage 
 'W0    B0    B1     W0  used in A to D readings  B0 and B1 lookseg   temp use ok
 'W1    B2    B3     Copy of a2d W10 b2+b3 used to shift segcodes     kyho
 'W2    B4    B5     Copy of AtoD W10 in lineariz ------------------  kyho
 'W2    B4    B5     ------------------------------------------------ open       
 'W3    B6    B7     -----------------------B7 which dig strobed----- kyho         
 'W11   B22   B23    A to D x 64 Accumulator for averaging            kyho
 'W12   B24   B25    B24is display strobe counter                     kyho



'    Junkyard  extra code for analog output or controller


'       if Torr > 2000 then CCPR1L = 255: goto Loopx                   ' the output PWM
'       W4 = Microns: W5 = W4 / 40 : W4 = W4 + W5:W4 = W4 / 4          
'       Low CCP1CON.4 : If W4.0 = 1 then High CCP1CON.4                'set the LSB's
'       Low CCP1CON.5 : If W4.1 = 1 then High CCP1CON.5
'       W4=W4/4 : CCPR1L = W4.Lowbyte                                  'take off the last two bits then let W4.lowByte be the Pwm

'      If CountsCt < CountSp1      then Low  PortA.2:goto spp           'stuff for control
'      If CountsCt - CountSp1 > 10 then High PortA.2
'spp:  If CountsCt < CountSp2      then Low  PortA.4: a4image = 0 :goto sppp 'no pullup if no relay
'      If CountsCt - CountSp2 > 10 then High PortA.4: a4image = 1


'S.bat                          'batch file to compile and program
'pbp -p16f873 SixPix.bas
'pause
'epicc -dPIC16f873 -p SixPix.hex


' HERE IS THE DEFS FILE, YOU HAVE TO MAKE THIS A SEPARATE FILE
' Defs file appended here so you won't have to chase me for it.
'****************************************************************
'*  SCDEFS.BAS  copied from BS2DEFS.BAS AND MODEDEFS.BAS + LDD  *
'*          needed to run with PicBasicPro                      *
'*     also need -PPS3 in command line to read 1298 a to d      *
'*  Notes     : Compatability variable naming for BS2           *
'****************************************************************

'INCLUDE "MODEDEFS.BAS"  ' Include Mode definitions

'These Mode definitions are ridiculous; I have deleted most of them
'If you want to read up on them, look in the picbasicpro book.

Symbol N2400  = 4       ' Driven Inverted


DEFINE ADC_SAMPLEUS 50
DEFINE ADC_BITS 10
DEFINE ADC_CLOCK 3  ' Clock source (rc = 3)


' Shiftin Modes
Symbol MSBPRE = 0       ' MSB first, sample before clock
Symbol LSBPRE = 1       ' LSB first, sample before clock
Symbol MSBPOST = 2      ' MSB first, sample after clock
Symbol LSBPOST = 3      ' LSB first, sample after clock

' Shiftout Modes
Symbol LSBFIRST = 0     ' LSB first
Symbol MSBFIRST = 1     ' MSB first


' LCD Stuff to data on PortB lines 4 thru 7, 4 bit

DEFINE LCD_DREG PORTB
DEFINE LCD_DBIT 4
DEFINE LCD_RSREG PORTB
DEFINE LCD_RSBIT 2
DEFINE LCD_EREG PORTB
DEFINE LCD_EBIT  3
DEFINE LCD_BITS  4
DEFINE LCD_LINES 2


' original bs2defs

INS     VAR     PORTL
OUTS    VAR     PORTL

INL     VAR     PORTL
INH     VAR     PORTH

OUTL    VAR     PORTL
OUTH    VAR     PORTH

'IN0     VAR     INL.0
'IN1     VAR     INL.1
'IN2     VAR     INL.2
'IN3     VAR     INL.3
'IN4     VAR     INL.4
'IN5     VAR     INL.5
'IN6     VAR     INL.6
'IN7     VAR     INL.7
'IN8     VAR     INH.0
'IN9     VAR     INH.1
'IN10    VAR     INH.2
'IN11    VAR     INH.3
'IN12    VAR     INH.4
'IN13    VAR     INH.5
'IN14    VAR     INH.6
'IN15    VAR     INH.7

'OUT0    VAR     OUTL.0
'OUT1    VAR     OUTL.1
'OUT2    VAR     OUTL.2
'OUT3    VAR     OUTL.3
'OUT4    VAR     OUTL.4
'OUT5    VAR     OUTL.5
'OUT6    VAR     OUTL.6
'OUT7    VAR     OUTL.7
'OUT8    VAR     OUTH.0
'OUT9    VAR     OUTH.1
'OUT10   VAR     OUTH.2
'OUT11   VAR     OUTH.3
'OUT12   VAR     OUTH.4
'OUT13   VAR     OUTH.5
'OUT14   VAR     OUTH.6
'OUT15   VAR     OUTH.7

W0      VAR     WORD
W1      VAR     WORD
W2      VAR     WORD
W3      VAR     WORD
W4      VAR     WORD
W5      VAR     WORD
W6      VAR     WORD
W7      VAR     WORD
W8      VAR     WORD
W9      VAR     WORD
W10     VAR     WORD
W11     VAR     WORD
W12     VAR     WORD

B0      VAR     W0.LOWBYTE
B1      VAR     W0.HIGHBYTE
B2      VAR     W1.LOWBYTE
B3      VAR     W1.HIGHBYTE
B4      VAR     W2.LOWBYTE
B5      VAR     W2.HIGHBYTE
B6      VAR     W3.LOWBYTE
B7      VAR     W3.HIGHBYTE
B8      VAR     W4.LOWBYTE
B9      VAR     W4.HIGHBYTE
B10     VAR     W5.LOWBYTE
B11     VAR     W5.HIGHBYTE
B12     VAR     W6.LOWBYTE
B13     VAR     W6.HIGHBYTE
B14     VAR     W7.LOWBYTE
B15     VAR     W7.HIGHBYTE
B16     VAR     W8.LOWBYTE         'Look at these; I may want to use higher ones
B17     VAR     W8.HIGHBYTE        'maybe put back b16 to b21 or b25
B18     VAR     W9.LOWBYTE         'before I do it make sure it dosen't keep any programs from running
B19     VAR     W9.HIGHBYTE
B20     VAR     W10.LOWBYTE
B21     VAR     W10.HIGHBYTE
B22     VAR     W11.LOWBYTE
B23     VAR     W11.HIGHBYTE
B24     VAR     W12.LOWBYTE
B25     VAR     W12.HIGHBYTE

'BIT0    VAR     B0.0               'If I use these, I can shorten the d to a program
'BIT1    VAR     B0.1
'BIT2    VAR     B0.2
'BIT3    VAR     B0.3
'BIT4    VAR     B0.4 
'BIT5    VAR     B0.5
'BIT6    VAR     B0.6
'BIT7    VAR     B0.7
'BIT8    VAR     B1.0
'BIT9    VAR     B1.1
'BIT10   VAR     B1.2
'BIT11   VAR     B1.3
'BIT12   VAR     B1.4
'BIT13   VAR     B1.5
'BIT14   VAR     B1.6
'BIT15   VAR     B1.7

