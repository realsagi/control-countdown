' LOOKDOWN Command
'
' Convert ASCII Hexadecimal characters to numeric equivalents.

        Include "modedefs.bas"          ' Include serial modes

SO      con     0                       ' Define serial out pin
SI      con     1                       ' Define serial in pin
B0      var     byte
B1      var     byte

loop:   Serin SI,N2400,B0               ' B0 = Next Character
	B1 = 255			' B1 = Convert to Hex (255 if Fail)
        Lookdown B0,["0123456789ABCDEF"],B1
        If B1 = 255 Then loop           ' Skip Non-Hex Characters
        Serout SO,N2400,[#B1,13,10]     ' Output Decimal Equivalent
        Goto loop                       ' Forever
