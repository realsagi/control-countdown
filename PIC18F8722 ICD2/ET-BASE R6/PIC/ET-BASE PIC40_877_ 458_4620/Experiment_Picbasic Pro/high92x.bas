' Access high register banks in 16C92x

Symbol  STATUS = 3      ' Status register is register 3
Symbol  PORTF = $107    ' PortF is register hex 107
Symbol  TRISF = $187    ' PortF Data Direction register is hex 187

' Set PortF Data Direction to every other bit output and input
        Poke STATUS,$80         ' Set IRP in Status register to 1 to access high register banks
        Poke TRISF,%01010101    ' Set the Data Direction
        Poke STATUS,0           ' Reset IRP in Status register to 0

' Read PortF into B0
        Poke STATUS,$80         ' Set IRP in Status register to 1 to access high register banks
        Peek PORTF,B0           ' Read PortF into B0
        Poke STATUS,0           ' Reset IRP in Status register to 0

        End
