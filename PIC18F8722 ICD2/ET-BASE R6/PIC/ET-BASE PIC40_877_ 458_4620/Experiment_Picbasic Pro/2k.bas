' Demo program of table use past 2K
'  Must be done in 2 stages:
'  First compile using -s switch to suppress assembly.
'  Then delete second include "pbl.inc" from end of .src file.
'  Finally execute PM to finish the process.

Symbol  i = B2

loop:   For i = 0 to 10         ' 11 chars in this table
        Gosub gettable1         ' Get char i from table 1
        Serout 0, N2400, (B0)   ' Send char from table to serial pin
        Next i                  ' Move to next char
        Serout 0, N2400, (13)   ' Send carriage return to serial pin

        Goto loop               ' Do it forever

gettable1:
    Asm                         ' The following must be in assembler
        movlw   table1/256      ; Get page address of table
        movwf   PCLATH          ; Set pc latch high
        call    table1          ; Do long call
        clrf    PCLATH          ; Reset pc latch high to page 0
        movwf   _B0             ; Save char in B0
        goto    done            ; Return to caller via done

        include "pbl.inc"       ; Include the PBC library here

        LIST

        org     800H            ; Bump to next 2K

table1  movf    _i, W           ; Get char index to W
        addwf   PCL
        retw    "Hello world"   ; Return char i in W
    Endasm                      ' That's it for assembler
