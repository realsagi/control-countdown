'****************************************************************
'*  Name    : readconf.pbp                                      *
'*  Notice  : Copyright (c) 2005 microEngineering Labs, Inc.    *
'*          : All Rights Reserved                               *
'*  Date    : 5/9/2005                                          *
'*  Notes   : PBP 2.46, 18F452, LAB-X1 Experimenter Board       *
'*          :                                                   *
'****************************************************************
' PicBasic Pro program to display PIC18F configuration on LCD

' Define LOADER_USED to allow use of the boot loader.
' This will not affect normal program operation.
Define	LOADER_USED	1

' Define LCD registers and bits
Define  LCD_DREG        PORTD
Define  LCD_DBIT        4
Define  LCD_RSREG       PORTE
Define  LCD_RSBIT       0
Define  LCD_EREG        PORTE
Define  LCD_EBIT        1

Address	Var	Byte
Config	Var	Byte


        ADCON1 = 7      ' Set PORTA and PORTE to digital
        Low PORTE.2     ' LCD R/W line low (Write)
        Pause 100       ' Wait for LCD to start up

	For Address = 0 to 13	' 14 configuration addresses to read
		GoSub ReadConfig	' Do the read
		Lcdout $fe, 1	' Clear screen
		Lcdout "0x30000", Hex Address, ": 0x", Hex2 Config	' Display data
		Pause 2000	' Wait 2 seconds
	Next Address
	End


' Subroutine to read 1 configuration location to variable Config
ReadConfig
    asm
    	movlw	30h		; Set configuration address
	movwf	TBLPTRU
    	movlw	0
	movwf	TBLPTRH
    	movf	_Address, W
	movwf	TBLPTRL
	clrf	EECON1		; Read from code memory
	bsf	EECON1, EEPGD
	tblrd*			; Read a byte
	movff	TABLAT, _Config	; Save byte to Config
   endasm
   	Return
