
' PicBasic Pro Program to demonstrate the use of the hardware capture
' module to measure the period of a frequency input.  Written for the
' PIC16F877 and LAB-X2 Experimenter Board.
'
' Input signal should be connected to RC2/CCP1

DEFINE LOADER_USED 1		' Optional, allows use of melabs Loader

INCLUDE "modedefs.bas"		' Modes for SerOut

So	VAR PORTC.6				' Serial output pin

capture	VAR PIR1.2			' CCP1 capture flag
overflow	VAR	PIR1.0		' Timer1 overflow flag

period	VAR	WORD			' Word variable that stores the value


CCP1CON = %00000100			' Enable the CCP1 capture, falling edge
T1CON = %00000001			' TMR1 prescale=1, and turn it on (1uS per count)


loop:
	IF (capture = 0) Then loop	' Wait here until captured 

	period.lowbyte = CCPR1L		' Store the captured value in
	period.highbyte = CCPR1H	' period variable
	
	IF overflow = 0 Then		' Skip the output if the timer overflowed
		SerOut So, T2400, [ "Input Period: ",#period, "uS", 10,13]	' Output
	EndIF

	capture = 0					' Clear the capture flag
	
reset:
	IF (capture = 0) Then reset	' Wait for beginning of next period
	TMR1L = 0					' Clear Timer1 low register
	TMR1H = 0					' Clear Timer1 high register
	capture = 0					' Clear capture flag
	overflow = 0				' Clear overflow flag
	
	GoTo loop					' Do it forever
	

	
