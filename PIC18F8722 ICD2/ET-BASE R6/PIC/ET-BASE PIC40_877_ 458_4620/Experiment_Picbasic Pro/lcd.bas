' PicBasic program to demonstrate operation of an LCD in 4-bit mode
'
' LCD should be connected as follows:
'       LCD     PIC
'       DB4     PortA.0
'       DB5     PortA.1
'       DB6     PortA.2
'       DB7     PortA.3
'       RS      PortA.4 (add 4.7K pullup resistor to 5 volts)
'       E       PortB.3
'       RW      Ground
'       Vdd     5 volts
'       Vss     Ground
'       Vo      20K potentiometer (or ground)
'       DB0-3   No connect

        Pause 500       ' Wait for LCD to startup

loop:   Lcdout $fe, 1   ' Clear LCD screen
        Lcdout "Hello"  ' Display Hello
        Pause 500       ' Wait .5 second

        Lcdout $fe, 1   ' Clear LCD screen
        Lcdout "World"
        Pause 500       ' Wait .5 second

        Goto loop       ' Do it forever
