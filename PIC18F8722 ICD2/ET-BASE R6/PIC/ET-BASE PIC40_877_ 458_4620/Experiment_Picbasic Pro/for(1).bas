' FOR..NEXT Command
'
' Prints series of numbers showing STEP facility. Inner-loop locks up when
' step size reaches zero (because loop never increments).

Symbol	SO = 0				' Serial Output

	Pause 10000			' Wait 10 Seconds (Just for Fun!!!)
Loop:	For B0 = 3 To 0 Step -1		' Countdown Step Size
		For B1 = 0 To 10 Step B0	' Count To 10 (or So)
			Serout SO,N2400,(#B1," ")
		Next B1
		Serout SO,N2400,(13,10)		' End of Line
		Pause 100			' Delay
	Next B0
	Goto Loop
