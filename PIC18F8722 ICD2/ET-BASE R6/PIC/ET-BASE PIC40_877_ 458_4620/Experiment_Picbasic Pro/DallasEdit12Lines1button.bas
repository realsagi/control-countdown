'   Dallas Edit12Lines. bas Addapted from TerSmith.bas 				16 Mar 2004
'   Collins and Associates 83 Bridgewaters Rd. Oceanport, NJ, 07757  
'     SteveColln@cs.com 732 571 4183
'   20 x 4 big digit display Okaya  PicBasicPro 2.43
'   Terriss type board 252.pcb  Pic16F876 or Pic 16F876  128 or 256 bytes eeprom
'   Dallas Semiconductor Rtc with advanced edit back in
'   Based on another application.  Submitted for educational purposes
'   There are two include files which are appended to the end of this file. Makes separate files of them.


    clear
    Include "scdefs.bas" : Include "modedefs.bas" : goto endstuff           'then come back to LL

LL:       L1 con  $80: L2         con  $C0: L3         con  $94: L4      con   $D4: Cycles var word :rtccontrol var word
    SkipLcd var byte : A2DcT  var byte    : io    var   PortC.2: Sclk var  PortC.1: Rst  var PortC.3
    GoDown   var bit : GoUp       var  bit: GoBack     var byte: BeepStop var byte
    Mv       var Word: Amps       var Word: Mvz        var Word: Mva      var Word: Mvt   var Word: bogus  var byte:i      var byte
    RtcYr    var byte: RtcMo      var byte: rtcDate    var byte: rtcDay   var byte: rtcHr var byte: rtcmin var byte:rtcSec var byte
    rMode    var byte: WhichUpDn  var byte: ckBCD      var byte: cknum    var byte: j     var byte
    go2      var byte: pT         var Byte: sp         var byte: endflag  var  bit
    BinO     var byte: BinI       var Byte: BcdO       var Byte: BcdIn    var Byte:Total  var Word
    Hrs      var word: Mins       var byte: Sec100     var word: TotHr    var word: Totmin     var byte: totsec     var word 
    LStartHr var word: LStartMin  var byte: LStartSec  var word: LStopHr  var word: LStopMin   var byte: LStopSec   var word
    RunHr    var word: RunMin     var byte: RunSec     var word:   OffHr  var word: OffMin     var byte: OffSec     var word
    RunTotHr var word: RunTotMin  var byte: RunTotSec  var word: OffTotHr var word: OffTotMin  var byte: OffTotSec  var word
    GraTotHr var word: GraTotMin  var byte: GraTotSec  var word: m2       var byte: m5         var byte
    Rt       var byte[8]		  : Which      var byte

	LcdOut $FE,1, $FE,L2, "www.YourDomain .com"  : Sp = 8
        Lcdout        $FE,L3, " 800 nnn nnnn 4C16 "  			'characters
	Debug "Start",13,10      : endflag = 0
        debug 13,10:  Debug "        N N N Timer Start     ",13,10,13,10
	Pause  500 : LcdOut $FE,1: skipLcd=51				'FIXME Make it 5000 for ship

	If PortB.6=0 then Rmode = 2					' don't know if I need this.

Main:  Gosub Readd  : BeepStop=BeepStop+1:If BeepStop>50 then BeepStop=0:input PortA.4
       if A2DcT < 60  then Main2					' Read A2D and maybe skip
Main1: Gosub GetTime: Lcdout $fe, L4 : gosub Rtc2Lcd  			' Read and show time
       Bogus=Mva*100: Mv=div32  1309 : Amps=Mv/35:A2DcT=0:Mva=0		' Read A2D and calc amps

       If PortB.7 = 0 then goto ClockEd 				' otherwise you are in Run or Standby

       LcdOut $FE,L3+11,"Amps=": if Amps >= 4 then LcdOut "on "
				 if Amps <  4 then LcdOut "off"

Main2: SkipLcd = SkipLcd + 1 : if SkipLcd < 50 then Switch
								'takes you to 
       SkipLcd = 0 : LcdOut $FE,L1				'status at right side of top row
       if rMode=0 then LcdOut " Stby"				'show amps but don't start
       if rMode=1 then LcdOut " Wait"				'ready to run, looking for amps		
       if rMode=2 then LcdOut " Run "				'got amps, am timing
       if rMode=3 then LcdOut " Stop"				'no amps, but may restart
       LcdOut $FE,L3+1, "Cycles=",#Cycles

Switch:If PortB.6=0 then goto Run  				'Run  is switch up
       If PortB.6=1 then goto Stby				'Stby is switch down

Stby:  If rMode > 1  then gosub PrintStopTest           	'if was run but now stopped then...
       rMode    = 0
       If PortB.4=0  then LcdOut $FE,L2,"   Printer Test      " :SkipLcd=51
       if PortB.4=0  then Gosub PrinterTest: LcdOut $FE, 1:goto Main
       pause 300   : goto Main	

Run:   if rMode => 2 then gosub RunTimer
       If rMode=0             then rmode=1:gosub PrStarTst      ' wait4start zero cycle times
       If rMode=1 and Amps >3 then rMode=2:gosub PrFirstAmp     ' first start from wait
       If rMode=2 and Amps <4 then rMode=3:gosub PrStopMot	' stop the motor
       If rMode=3 and Amps >3 then rMode=2:gosub PrStarMot      ' re started print once

       w4 = W4 + 1 : if W4 < 17 then goto Main
       W4 = 0: W5 = Sec100/100
       LcdOut $Fe, L2," Runtime=", #Hrs,":",#Mins dig 1,#Mins dig 0,":",#Sec100 dig 3,#Sec100 dig 2,".",#Sec100 dig 1,#Sec100 dig 0
       goto main
	              	   '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ MAIN LOOP END ^^^^^^^^^^^^^^^^^                  

Readd:   for i=1 to 16:AdcIn 0,W0: Mva = Mva+ W0: next i:A2DcT=A2DcT+16:pauseus 4105: return  ' To correct multiply x ratio of Digi time to Clock time
				'4400 >>  indicated  58 gor rtc 60 3.4% slow
				'4100 >>  indicated 14:55 for 15:25 30 sec / 900 sec = 3.3%  slow
				'4020 >>  indicated 15 min 900 sec  8 sec fast                 		       .9% fast 
				'4035 >>  Proto       12:12:20.35 = 12:08:11  4 minutes fast in 12 hours 4/720 .6% fast
				'4035 >>  Final Unit  14:50:14.58 = 14:42:23  8 minutes fast in 15 hours 8/980 .8%
					'it looks like .3% = 15 on the variable so add 15 * 2 + 10 for diff from proto so add 40
				'4075 >>  was .6" fast with a route that did not print much.



RunTimer: Sec100 = Sec100 + 1 : if Sec100 < 6000 then return		' Hrs, Mins and Sec100  Counter type clock
  	  Sec100 = 0:Mins = Mins + 1					' Minutes Roll over
	  if Mins > 59 then Mins = 0 : hrs = hrs + 1			' Hours roll over
	  return


PrStarTst:   debug "     Start Test    ":gosub pRtcTime:Debug 13,10:Hrs=0:Mins=0:Sec100=0:cycles=0:return

PrFirstAmp:  debug "        Closed     ":LstartHr=0:LStartMin=0:LstartSec =0: Low PortA.4:BeepStop=0
				        Hrs=0     : Mins=0    :Sec100    =0
				        RunTotHr=0: RunTotMin=0:RunTotSec=0
				        OffTotHr=0: OffTotMin=0:OffTotSec=0 : return
'm2 var byte m5 var byte

PrStopMot:   W3=Hrs:LstopHr= W3: m2=Mins: LStopMin = m2 : W1=Sec100: LStopSec = W1	'motor stops first calculate

borrow1:     if W1 < LStartSec  then W1 = W1 + 6000     :     m2  = m2 -1		'seconds borrow from minutes
carry1:	     If m2 = 255        then m2 = 59	        :     W3  = W3 -1		'if mins neg borrow hours
rollover:    If m2 < LstartMin  then m2 = m2 + 60       :     W3  = W3 -1		'if mins too small borrow hours

   	     RunHr=W3-LstartHr :RunMin  = m2 - LstartMin: RunSec  = W1 -LStartSec       'calc RunHr with friendly numbers
   	     RunTotSec = RunTotsec + RunSec: if RunTotSec > 6000 then RunTotsec =Runtotsec-6000: Runtotmin = Runtotmin +1
   	     RunTotMin = RunTotMin + RunMin: if RunTotMin > 59   then RunTotMin =RunTotMin-  60: RuntotHr  = RuntotHr  +1
   	     RunTotHr  = RunTotHr  + RunHr 

	     gosub pRunTime               :if endflag = 1 then goto Stopp
	     debug "        Open       "  :cycles=cycles+1:	return


   	
PrStarMot:   W6=Hrs:LstartHr= W6: m5=Mins:LStartMin=m5:W4=Sec100:LStartSec=W4:If endflag=0 then Low PortA.4:BeepStop=0	
	     if W4 < LStopSec   then W4 = W4 	  + 6000 : m5 = m5 - 1			'if sec too small borrow from mins
	     if m5 = 255        then m5 = 59		 : W6 = W6 - 1			'if mins neg borrowfrom hrs
	     if m5 <   LStopMin then m5 = m5      + 60 	 : W6 = W6 - 1			'if mins too small borrow hours
   	     OffHr = W6-LStopHr :OffMin = m5 - LStopMin  : OffSec = W4 - LstopSec	'now an easy calc	

   	     OffTotSec = OffTotsec + OffSec: if OffTotSec > 6000 then OffTotsec =Offtotsec-6000: Offtotmin = Offtotmin +1
   	     OffTotMin = OffTotMin + OffMin: if OffTotMin > 59   then OffTotMin =OffTotMin-  60: OffTotHr  = OffTotHr  +1
   	     OffTotHr  = OffTotHr  + OffHr 

   	     TotSec = RunSec + OffSec    : i=0: if TotSec > 6000 then Totsec = totsec-6000: i = 1	'use i to increment
   	     TotMin = RunMin + OffMin + i: i=0: if TotMin > 59   then TotMin =TotMin-  60:  i = 1
   	     TotHr  = RunHr  + OffHr  + i

             gosub pOffTime: Gosub prCyc : gosub PrToTime  : if endflag = 1 then goto Stopp
  	     debug "        Closed     " : return

prCyc:	     debug "        Cycle ",#cycles dig 3,#cycles dig 2,#cycles dig 1,#cycles dig 0," ":   return


PrintStopTest: endflag = 1: if rmode = 2 then goto PrStopMot		'finish the phase by printing the end
			    if rmode = 3 then goto PrStarMot    	'   or either open or closed

Stopp:	     debug 13,10,  "     Stop  Test  " : gosub pRtcTime : endflag = 0

   	     GraTotSec = RunTotSec + OffTotSec   :i=0:if GraTotSec > 6000 then GraTotsec =Gratotsec-6000: i=1
   	     GraTotMin = RunTotMin + OffTotMin+i :i=0: if GraTotMin > 59  then GraTotMin =GraTotMin-  60: i=1
   	     GraTotHr  = RunTotHr  + OffTotHr +i

     	       	debug "        Closed Time= ": Gosub prRunTot
     	       	debug "        Open   Time= ": Gosub prOffTot
     	       	debug "        Total  Time= ": Gosub prGtTime
	       	debug "             Cycles=", #cycles
		debug 13,10,13,10,13,10,13,10,13,10,13,10,13,10,13,10: return	


pRunTime: debug #RunHr,":",#RunMin dig 1, #RunMin dig 0,":", #RunSec dig 3, #RunSec dig 2,".", #RunSec dig 1, #RunSec dig 0,13,10:return
pOffTime: debug #OffHr,":",#OffMin dig 1, #OffMin dig 0,":", #OffSec dig 3, #OffSec dig 2,".", #OffSec dig 1, #OffSec dig 0,13,10:return
PrToTime: debug #TotHr,":",#TotMin dig 1, #TotMin dig 0,":", #TotSec dig 3, #TotSec dig 2,".", #TotSec dig 1, #TotSec dig 0,13,10:return

prLstart: debug #LstartHr,":",#LstartMin dig 1,#LstartMin dig 0,":",#LstartSec dig 3,#LstartSec dig 2,".",#LstartSec dig 1,#LstartSec dig 0,13,10:return 
prLstop:  debug  #LstopHr,":",#LstopMin  dig 1,#LstopMin  dig 0,":",#LstopSec  dig 3,#LstopSec  dig 2,".",#LstopSec  dig 1,#LstopSec  dig 0,13,10:return 


PrGtTime: debug #GraTotHr dig 2,#GraTotHr dig 1,#GraTotHr dig 0,":",#GraTotMin dig 1,#GraTotMin dig 0,":",#GraTotSec dig 3,#GraTotSec dig 2,".",#GraTotSec dig 1,#GraTotSec dig 0,13,10: return
PrRunToT: debug #RunTotHr dig 2,#RunTotHr dig 1,#RunTotHr dig 0,":",#RunTotMin dig 1,#RunTotMin dig 0,":",#RunTotSec dig 3,#RunTotSec dig 2,".",#RunTotSec dig 1,#RunTotSec dig 0,13,10: return
PrOffToT: debug #OffTotHr dig 2,#OffTotHr dig 1,#OffTotHr dig 0,":",#OffTotMin dig 1,#OffTotMin dig 0,":",#OffTotSec dig 3,#OffTotSec dig 2,".",#OffTotSec dig 1,#OffTotSec dig 0,13,10: return

PrinterTest: debug 13,10: debug "   Printer Test  ": gosub pRtcTime:Pause 1000:debug 13,10:return
Clearr: LcdOut $FE,1:Pause 1:return


pRtcTime: debug hex2 RtcMo,"/",hex2 RtcDate,"/20",hex2 RtcYr,"  ",hex2 RtcHr,":",hex2 RtcMin,":" ,hex2 RtcSec,13,10: return
prHhTime: debug   #Hrs,":",#Mins   dig 1,   #Mins dig 0,":", #Sec100 dig 3, #Sec100 dig 2,".", #Sec100 dig 1, #Sec100 dig 0,13,10:return


SpLcd:   Sp=Sp max 1: for i = 1 to sp:LcdOut " ": next i : return


Rtc2Lcd:  LcdOut hex2 Rt(0),"/", hex2 Rt(1), "/20", hex2 Rt(2)," ", hex2 Rt(3),":",hex2 Rt(4),":",hex2 Rt(5)," ":return


GetTime:   RST = 1 :    Shiftout IO, SCLK, LSBFIRST, [$bf]      	' Read all 8 RTC registers in burst mode
           Shiftin  IO, SCLK, LSBPRE, [ rt(5),  rt(4), rt(3),   rt(1), rt(0),  rt(6), rt(2), rtccontrol]
           RST = 0 :    Return       '[ rtcsec, rtcmin, rtchr, rtcdate, RtcMo, rtcday, RtcYr,  0]

SetTime:   RST = 1  :  Shiftout IO, SCLK, LSBFIRST, [$8e, 0]		' Enable write
           RST = 0  :  RST = 1         					' Ready for transfer
           Shiftout IO, SCLK, LSBFIRST, [$be, rt(5),  rt(4),  rt(3),   rt(1), rt(0),  rt(6), rt(2),  0]
           RST = 0 :    Return         

'**************** HERE IS THE CLOCK EDIT PART ******************************************

ClockEd: LcdOut $FE,L3,"    Edit:" : if j = 1       then pause 500  	 ' Edit clock with 1 button and 13 lines of code!

        if WhichUpDn >    9 then WhichUpDn = 0
        Which=WhichUpDn / 2: goDown = WhichUpDn.0:goUp = goDown + 1       'goUp (bit) is 1 or rolls to zero

	b3=Which*6  :b4=b3+5:for i=b3 to b4:lookup i,["Month Date  Year  Hour  Min  "],b5:lcdout b5:next i

	if GoUp  =1 then Lcdout "+    " 
	if GoDown=1 then Lcdout "-    " 

	if PortB.4 = 1 then goto Clked2					'if no keypress then scram

	b7=1: b6=Rt(Which) : b6=b6 & $0F:if b6 = 9 then     b7=7	'if going up   and lsd = 9 then incr = 7 to bypass illegals
	if goUp = 1             then Rt(Which) = rt(Which)+ b7

        b7=1 : b6=Rt(Which): b6 = b6 & $0F: if b6=0 then      b7=7	'if going down and lsd = 0 then decr = 7
	if goDown=1 and Rt(Which) >0 then Rt(Which)=Rt(Which)-b7

	goBack = goDown * 2 : j=0 : Gosub SetTime	 	     	'if a keypress, write the time if Decrease, set goback

Clked2:	pause 20:j=j+1:if j>60 then j=2 :WhichUpDn= WhichUpDn+1-GoBack : GoBack = 0 ' increment which and up/down or go back
	goto Main1


EndStuff: Option_Reg.7 = 0
 Low RST: Low SCLK: gosub GetTime 					' If new battery, initailize Ds
 if RtcSec.7=1 then  RtcYr=$03:rtcday=$06:RtcMo=$06:rtcdate=$20:rtchr=$13:rtcmin=$25:rtcsec=0:Gosub settime

          AdCon1=128 + 8 + 1 : pause 100 
          Define Lcd_Dbit 0              
          Define Lcd_RsReg PORTC         'pin 4 of Lcd
          Define Lcd_RsBit 5
          Define Lcd_Ereg PORTB          'pin 6 of Lcd       
          Define Lcd_Ebit  5
          Define Debug_Reg PortC
          Define Debug_Bit 6
          Define Debug_Baud 9600
          High   PortC.6
	  goto LL

'junkyard		----------------------------------------------------------
'pbpw -p16f876 TerSmith.bas 		ts.bat
'pause
'epicwin /dpic16f876 /p /x TerSmith.hex



'****************************************************************
'*  SCDEFS.BAS  copied from BS2DEFS.BAS AND MODEDEFS.BAS + LDD  *
'*          needed to run with PicBasicPro                      *
'****************************************************************

Define ADC_SAMPLEUS 50
Define ADC_BITS 10
Define ADC_CLOCK 3  ' Clock source (rc = 3)

' LCD Stuff to data on PortB lines 4 thru 7, 4 bit

DEFINE LCD_DREG PORTB
DEFINE LCD_DBIT 4
DEFINE LCD_RSREG PORTB
DEFINE LCD_RSBIT 2
DEFINE LCD_EREG PORTB
DEFINE LCD_EBIT  3
DEFINE LCD_BITS  4
DEFINE LCD_LINES 2

W0 var word:W1 var word:W2 var word: W3 var word: W4 var word: W5 var word:W6 var word
W7 var word:W8 var word:W9 var word:W10 var word:W11 var word:W12 var word

b0  var W0.LowByte : b1  var W0.HighByte :b2  var W1.LowByte : b3  var W1.HighByte
b4  var W2.LowByte : b5  var W2.HighByte :b6  var W3.LowByte : b7  var W3.HighByte
b8  var W4.LowByte : b9  var W4.HighByte :b10 var W5.LowByte : b11 var W5.HighByte
b12 var W6.LowByte : b13 var W6.HighByte :b14 var W7.LowByte : b15 var W7.HighByte
b16 var W8.LowByte : b17 var W8.HighByte :b18 var W9.LowByte : b19 var W9.HighByte
b20 var W10.LowByte: b21 var W10.HighByte:b22 var W11.LowByte: b23 var W11.HighByte
b24 var W12.LowByte: b25 var W12.HighByte

'/*-----------------------* EOF SCDEFS.BAS *-------------------*/

'****************************************************************
'*  MODEDEFS.BAS                                                *
'*                                                              *
'*  By        : Leonard Zerman, Jeffrey Schmoyer                *
'*  Notice    : Copyright (c) 2001 microEngineering Labs, Inc.  *
'*              All Rights Reserved                             *
'*  Date      : 04/17/01                                        *
'*  Version   : 2.40                                            *
'*  Notes     : Mode definitions for Debug, Serin/out,          *
'*              Shiftin/out, Xin/out                            *
'****************************************************************

' Serin / Serout Modes (Do not use with Serin2 / Serout2)
Symbol T2400  = 0       ' Driven True
Symbol T1200  = 1       ' Driven True
Symbol T9600  = 2       ' Driven True
Symbol T300   = 3       ' Driven True

Symbol N2400  = 4       ' Driven Inverted
Symbol N1200  = 5       ' Driven Inverted
Symbol N9600  = 6       ' Driven Inverted
Symbol N300   = 7       ' Driven Inverted

Symbol OT2400 = 8       ' Open True
Symbol OT1200 = 9       ' Open True
Symbol OT9600 = 10      ' Open True
Symbol OT300  = 11      ' Open True

Symbol ON2400 = 12      ' Open Inverted
Symbol ON1200 = 13      ' Open Inverted
Symbol ON9600 = 14      ' Open Inverted
Symbol ON300  = 15      ' Open Inverted


' Shiftin Modes
Symbol MSBPRE = 0       ' MSB first, sample before clock
Symbol LSBPRE = 1       ' LSB first, sample before clock
Symbol MSBPOST = 2      ' MSB first, sample after clock
Symbol LSBPOST = 3      ' LSB first, sample after clock

' Shiftout Modes
Symbol LSBFIRST = 0     ' LSB first
Symbol MSBFIRST = 1     ' MSB first


' Debug / Serial Constants
Symbol CLS = 0
Symbol HOME = 1
Symbol BELL = 7
Symbol BKSP = 8
Symbol TAB = 9
Symbol CR = 13


' Xout Commands
Symbol UnitOn = %10010
Symbol UnitOff = %11010
Symbol UnitsOff = %11100
Symbol LightsOn = %10100
Symbol LightsOff = %10000
Symbol Dim = %11110
Symbol Bright = %10110


'*----------------------* EOF MODEDEFS.BAS *--------------------*

