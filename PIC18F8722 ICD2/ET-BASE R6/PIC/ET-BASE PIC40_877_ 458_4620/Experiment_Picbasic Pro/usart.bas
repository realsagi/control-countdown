' Read and write hardware USART

B1      var     byte

' Initialize USART
        TRISC = %10111111       ' Set TX (PortC.6) to out, rest in
        SPBRG = 25              ' Set baud rate to 2400
        RCSTA = %10010000       ' Enable serial port and continuous receive
        TXSTA = %00100000       ' Enable transmit and asynchronous mode


' Echo received characters in infinite loop
loop:   Gosub charin            ' Get a character from serial input, if any
        If B1 = 0 Then loop     ' No character yet

        Gosub charout           ' Send character to serial output
        Goto loop               ' Do it forever


' Subroutine to get a character from USART receiver
charin: B1 = 0                  ' Preset to no character received

        If PIR1.5 = 1 Then      ' If receive flag then...
                B1 = RCREG      ' ...get received character to B1
        Endif

ciret:  Return                  ' Go back to caller


' Subroutine to send a character to USART transmitter
charout: If PIR1.4 = 0 Then charout     ' Wait for transmit register empty

        TXREG = B1              ' Send character to transmit register

        Return                  ' Go back to caller
