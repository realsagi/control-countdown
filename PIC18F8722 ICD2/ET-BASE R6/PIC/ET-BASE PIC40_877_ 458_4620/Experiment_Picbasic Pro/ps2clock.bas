' PicStic-2 Clock Test - Requires Micromint PicStic-2 with JRC6355
'
'       B15     Year            00 - 99
'       B16     Month           01 - 12
'       B17     Day of Month    01 - 31
'       B18     Day of Week     01 - 07
'       B19     Hours           00 - 23
'       B20     Minutes         00 - 59
'       B21     Seconds         00 - 59
'
' Note : All values are stored in BCD. Print as Hex to Display.
' Must define command line processor type as -pps2

        Include "modedefs.bas"          ' Include serial modes

B12     var     byte
B13     var     byte
B14     var     byte
B15     var     byte
B16     var     byte
B17     var     byte
B18     var     byte
B19     var     byte
B20     var     byte
B21     var     byte
SO      con     0                       ' Define serial output pin

        B19 = $23 : B20 = $58 : B21 = $50       ' Set clock
	B18 = $07
	B16 = $12 : B17 = $31 : B15 = $99

	Call ClockSet

loop:   Call ClockGet                           ' Read clock
        B13 = "/" :     B14 = B16 : Gosub P2    ' Print date & time
			B14 = B17 : Gosub P02
	B13 = " " :     B14 = B15 : Gosub P02
	B13 = ":" :     B14 = B19 : Gosub P2
			B14 = B20 : Gosub P02
	B13 = 13 :      B14 = B21 : Gosub P02
        Pause 250                               ' Visual delay
        Goto loop                               ' Continue forever

P2:     If B14 >= 10 Then P02
        B12 = " " : Gosub PC : Goto P1
P02:    B12 = (B14 / 16) + "0" : Gosub PC
P1:     B12 = (B14 & $0F) + "0" : Gosub PC
	B12 = B13
PC:     Serout SO,N2400,[B12]
	Return
