'*************************************************** 
'  Hard Ware: ET-BASE PIC40
'  OSC : 10MHz (HS Mode)
'  " This Program it use test board ET-BASE PIC40."
'***************************************************

        INCLUDE "modedefs.bas"          ' Include serial modes

SI      VAR     PORTC.7
SO      VAR     PORTC.6                       ' Define serial output pin

B0      VAR     BYTE
B1      VAR     BYTE

DEFINE  OSC     10

ST:           
                SerOut SO,T9600,[12,"PROGRAM TEST BOARD ET-BASE PIC/40",10,13]
                
                SerOut SO,T9600,["PRESS SELECT CHICE TEST  ",10,10,13]
                
                SerOut SO,T9600,["1.TEST RS232",10,13]

                SerOut SO,T9600,["2.TEST IO",10,13]
                                                                                                                                                                                                    
        SerIn SI,T9600,B0
        Pause  500
   
NOW:    IF B0 = "1" Then  T_RS232
            
        IF B0 = "2" Then  T_IO
        
        GoTo  NOW        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    

'*****************************************************************************
T_RS232:   SerOut SO,T9600,[12,"PROGRAM TEST RS232 PORT",10,13] 
          


           Pause  10
AAA:       SerIn SI,T9600,B0
            IF B0 = 27 Then ST
           SerOut SO,T9600,[B0]
           GoTo   AAA

'******************************************************************************           
T_IO:     SerOut SO,T9600,[12,"TEST 34 PIN I/O",10,13]
          SerOut SO,T9600,["  PIC16Fxxx = RB3 RESERVED",10,13]
          SerOut SO,T9600,["  PIC18Fxxx = RB5 RESERVED",10,13]

          TRISA = $00
          TRISB = $00
          TRISC = $00
          TRISD = $00
          TRISE = $00
          ADCON1 = $07     ' portA is Digital I/O

start_IO:   B0 = %00000001
          for  B1 = 0 to 6
            PORTA = B0
            B0 = B0 << 1
            pause 250
          next B1
       
        B0 = %00000001
       for  B1 = 0 to 8
           PORTB = B0
            B0 = B0 << 1
            pause 250
       next B1
       
            B0 = %00000001
       for  B1 = 0 to 8
           PORTC = B0
            B0 = B0 << 1
            pause 250
       next B1
       
            B0 = %00000001
       for  B1 = 0 to 8
           PORTD = B0
            B0 = B0 << 1
            pause 250
       next B1
       
       
          B0 = %00000001
       for  B1 = 0 to 3
           PORTE = B0
            B0 = B0 << 1
            pause 250
       next B1
                     
       goto start_IO
        
        
        
        
        






           
           

        
