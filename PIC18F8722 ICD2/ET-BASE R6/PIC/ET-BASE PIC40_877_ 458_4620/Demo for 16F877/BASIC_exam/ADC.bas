'*********************************************
'Test read data from internal ADC
'Hardware   : ET-BASE PIC40
'file name  : PicBasic Pro compiler 
'Programmer : Watcharin Kaorop
'Company    : ETT  CO.,LTD.
' Date      : 9/11/2005
'*********************************************
         INCLUDE "modedefs.bas"          ' Include serial modes

SI      VAR     PORTC.7
SO      VAR     PORTC.6           ' Define serial output pin

       define OSC 10

         ADCON1 = %10000010 	' Set PORTA analog and RIGHT justify result
         ADCON0 = %11000001	' Configure and turn on A/D Module
	
	     Serout2  SO,84,[12,"Test Analog to Digital converter of PIC MCU",10,13]
	     Serout2  SO,84,["Connect analog voltage to RA0(AN0)",10,13]
	     Serout2  SO,84,["Result of ADC will display on Screen(RS232)",10,13]
	
START:   ADCON0.2 = 1           ' start conversion
        
loop:    if ADCON0.2 = 1 then loop

         Serout2  SO,84,[HEX2 ADRESH,HEX2 ADRESL,10,13]  ' Display in Hex format.
         pause  100
        
         goto start
    END
