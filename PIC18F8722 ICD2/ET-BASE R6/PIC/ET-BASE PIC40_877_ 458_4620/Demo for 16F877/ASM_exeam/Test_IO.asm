;**************************************
; Test LED ON & OFF PORTA.0
; Hardware  : ET-BASE PIC40
; OSC 	    : 10 MHZ [HS MODE]
; Assembler : mpasm.exe
; Programmer: WATCHARIN KAOROP
; Company   : ETT  CO.,LTD.
; Date      : 6/8/2002
;**************************************

     list p=16f877                 ; list directive to define processor
     #include <p16f877.inc>        ; processor specific variable definitions
    __CONFIG _CP_OFF & _WDT_OFF & _BODEN_ON & _PWRTE_ON & _HS_OSC & _WRT_ENABLE_ON & _LVP_ON & _DEBUG_OFF & _CPD_OFF
         
    	DT0       EQU       0X20
        DT1       EQU       0x21
        DT2       EQU       0x22

	   ORG       0x0000

		  MOVLW     0x07
          MOVWF     ADCON1
MAIN      bsf       STATUS,RP0         ; select bank 1
          clrf      TRISA              ; All PORTA is output
          clrf      TRISB              ; port B is output
	      clrf	    TRISC	           ; port C is output
		  clrf      TRISD              ; port D is output
	      clrf	    TRISE	           ; port E is output
          bcf       STATUS,RP0         ; select bank 0

LOOP     movlw      0x55 
         movwf      PORTA          ; Out data to PORTA
	     movwf      PORTB          ; Out data to PORTB
	 	 movwf      PORTC          ; Out data to PORTC
		 movwf      PORTD          ; Out data to PORTD
		 movwf      PORTE          ; Out data to PORTE 
         CALL       DELAY

         movlw      0xAA
	     movwf      PORTA          ; Out data to PORTA
	 	 movwf      PORTB          ; Out data to PORTB
		 movwf      PORTC          ; Out data to PORTC
		 movwf      PORTD          ; Out data to PORTD
		 movwf      PORTE          ; Out data to PORTE
	     CALL       DELAY
	      
	     GOTO       LOOP

;********* DELAY LOOP ********

DELAY     MOVLW     0X05
	      MOVWF     DT0
SDEL      CLRF      DT1
SD2       CLRF      DT2
SD1       DECFSZ    DT2
          GOTO      SD1
          DECFSZ    DT1
          GOTO      SD2
	      DECFSZ    DT0
	      GOTO      SDEL
          RETURN 

	 END    
