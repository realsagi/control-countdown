;**********************************************
; Test RS232 or RS422(Single node) 
; Hardware  : ET-BASE PIC40
; OSC	    : 10 MHz [HS mode]
; file name : 232.ASM
; Assembler : mpasm.exe
; Programmer: Watcharin Kaorop
; Company   : ETT  CO.,LTD.
; Date      : 9/11/2005
;**********************************************
; STEP to follow when test this program
;    1.Connect RS232 or RS422 cable between board and PC.
;    2.Download this program to CPU.
;    3.Run terminal program such as Procom plus,XTALK etc.
;    4.Set parameter Procom plus to 9600 baud,No parity,8 bits data and 1 bit. stop
;    5.Reset board ,after reset board you will see this message on PC.
;
;         "PROGRAM TEST RS232 PORT ON CPU PIC16F877 RUN FREQ 10MHz"
;
;    6.Press any key on keyboard, you will see the key that you pressed.
;
;**********************************************


     list p=16f877                 ; list directive to define processor
     #include <p16f877.inc>        ; processor specific variable definitions

offset    EQU       0x20
temp      EQU       0x21
          ORG       0x0000

;************ initial *******************
init      bsf       STATUS,RP0     ; select bank 1
          clrf      TRISD
          movlw     0x40           ; BAUD rate 9600
          movwf     SPBRG
          clrf      TXSTA          ; 8 bits data ,no,1 stop
          bcf       STATUS,RP0     ; select bank 0

          bsf       RCSTA,SPEN     ; Asynchronous serial port enable
          bsf       RCSTA,CREN     ; continuous receive

          bsf       STATUS,RP0     ; select bank 1
          bsf       TXSTA,TXEN     ; Transmit enable
	      bsf       TXSTA,BRGH     ; hi speed
          bcf       STATUS,RP0     ; select bank 0

;********** start to send **********************

          clrf      offset         ; load offset of character table
start     movf      offset,w       
          call      TAB
          addlw     0              ; Character = 00 ?
          btfsc     STATUS,Z       ; Character = 00 ?
          goto      wait2          ; Yes , Z = 1
                                   ; No  , Z = 0
          movwf     TXREG          ; Send recent data to TX 
wait1     movlw     TXSTA          ; 
          movwf     FSR            ; FSR <= TXSTA
          btfss     INDF,1         ; check TRMT bit in TXSTA (FSR)
          goto      wait1          ; TXREG full  or TRMT = 0
          incf      offset,f       ; TXREG empty  or TRMT = 1
          goto      start          ; Send again

;********** start to receive *******************          
wait2     btfss     PIR1,RCIF      ; Check RCIF  bit in PIR1 register
          goto      wait2          ; RCREG empty or RCIF = 0
          movf      RCREG,w        ; RCREG full or RCIF = 1
          movwf     TXREG

          goto      wait2

;********* Tebel of message ********************  
TAB       addwf     PCL,F            ; Move offset to PC lower
          
	  DT        0X0C,"PROGRAM TEST RS232 PORT ON CPU PIC16F877 RUN FREQ 10MHz",0X0A,0X0D,0X0



          END
