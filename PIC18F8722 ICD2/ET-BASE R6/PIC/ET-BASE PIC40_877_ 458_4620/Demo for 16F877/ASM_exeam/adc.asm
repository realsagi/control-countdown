;**********************************************
; test internal ADC of 16F877
; Hardware  : ET-BASE PIC40
; OSC       : 10 MHz [HS mode]
; Assembler : mpasm.exe
; Programmer: WATCHARIN KAOLOP
; Company   : ETT  CO.,LTD.
; Date      : 9/11/2005
;**********************************************

     list p=16f877                 ; list directive to define processor
     #include <p16f877.inc>        ; processor specific variable definitions

    __CONFIG _CP_OFF & _WDT_OFF & _BODEN_ON & _PWRTE_ON & _HS_OSC & _WRT_ENABLE_ON & _LVP_ON & _DEBUG_OFF & _CPD_OFF 

DT1       EQU       0x20
DT2       EQU       0x21
B1        EQU       0x22
BB        EQU       0x23

          ORG  0x0000   
     
;************** INITIAL ANALOG TO DIGITAL

  	      BCF          STATUS,RP1
          bSf          STATUS,RP0     ; select bank 1
          CLRF         ADCON1 
          BSF          ADCON1,7       ; ADFM = 1, AN0-AN7 is analog input
          BCF          STATUS,RP0     ; select bank 0
          MOVLW        10000001
          MOVWF        ADCON0         ; Clock=Fosc/32, ADC operate, Channel 0

;********** INITIAL USART ***********

          BSF	    STATUS,RP0     ; select bank 1
          BCF       TRISC,4
          BCF       TRISC,3
          MOVLW     0x40           ; BAUD rate 9600
          MOVWF     SPBRG
          CLRF      TXSTA          ; 8 bits data ,no,1 stop
          BSF       TXSTA,TXEN     ; Transmit enable
	      BSF	    TXSTA,BRGH     ; hi speed

	      BCF	    STATUS,RP0     ; select bank 0
          CLRF      RCSTA
          BSF       RCSTA,SPEN     ; Asynchronous serial port enable
          BSF       RCSTA,CREN     ; continuous receive
          
ST	  MOVLW     0X0D
	  CALL	    SEND	 
      MOVLW     "D"
	  CALL	    SEND
	  MOVLW     "A"
	  CALL	    SEND
	  MOVLW     "T"
	  CALL	    SEND
	  MOVLW     "A"
	  CALL	    SEND
	  MOVLW     " "
	  CALL	    SEND
	  MOVLW     "="	 
	  CALL	    SEND
	  MOVLW     " "
	  CALL	    SEND
	     
;************* ANALOG TO DIGITAL CONVERTOR **************

        
          BSF     ADCON0,2       ; start conversion
LOOP      BTFSC   ADCON0,2       
          GOTO    LOOP           ; DONE = 1
                                 ; DONE = 0
          MOVF    ADRESH,W       ; DATA HIGH
	      ANDLW   0X03
	      CALL    ADJT
	      BSF     STATUS,RP0     ; select  bank 1
          MOVF    ADRESL,W       ; DATA lower 8 bit
	      BCF     STATUS,RP0
	      CALL    ADJT
	      CALL    DELAY
	      GOTO    ST


;**********************************************************
; Convert time 1 byte to ASCII 2 bytes and send to display
; Input  : W
; Output : -
;**********************************************************
ADJT      MOVWF     BB             ; B1 = HHHH LLLL
          SWAPF     BB,W           ; W  = LLLL HHHH
	      CALL      CHECK           
          MOVF      BB,W
          CALL      CHECK
          RETURN

CHECK     ANDLW     0X0F
	      MOVWF     B1
	      CLRC                    ; CLEAR CARRY FLAG
	      SUBLW     0X0A
	      BZ        CC            ; = 0XA JUMP TO CC 
	      BC        CONV          ; < 0XA JUMP TO CONV
CC	      MOVF      B1,W          ; A - F
	      ADDLW     0X37
	      CALL      SEND           ; SEND FIRST DIGIT
	      RETURN
CONV      MOVF      B1,W          ; 0 - 9
          ADDLW     0X30
	      CALL      SEND	   ; SEND LAST DIGIT
          RETURN
	  
;********************************************************
; Send data to RS-232 9600 Buad,8 bits data, No parity, 1 stop
; Input  : W
; Output : Computer screen
;********************************************************
SEND      MOVWF     TXREG          ; Send recent data to TX 
WAIT1     MOVLW     TXSTA
	  MOVWF	    FSR
          BTFSS     INDF,1         ; check TRMT bit in TXSTA register
	  GOTO      WAIT1          ; TXREG full  or TRMT = 0
          RETURN

;******** DELAY TIME **************

DELAY     MOVLW     0x00
          MOVWF     DT1
SD2       MOVLW     0x00
          MOVWF     DT2
SD1       DECFSZ    DT2
          GOTO      SD1
          DECFSZ    DT1
          GOTO      SD2
          RETURN


          END            ; directive 'end of program'

