;**************************************
; Test LCD
; Hardware  : ET-BASE PIC8722
; Assembler : mpasm.exe
; Programmer: Watcharin Kaorop
; Company   : ETT  CO.,LTD.
;**************************************
;       PIC18F8722  pin                                 LCD pin
;        RH1           ->                                 RS
;  	     RH2           ->							      RW
;        RH3           ->                                 E
;        RH4           ->                                 D4
;        RH5           ->                                 D5
;        RH6           ->                                 D6
;        RH7           ->                                 D7


     list p=18f8722                 ; list directive to define processor
     #include <p18f8722.inc>        ; processor specific variable definitions
;---------------------------------------------------------------------------------------
;  Device Configuration
;---------------------------------------------------------------------------------------
;code protect disabled
	CONFIG     CP0=OFF
;Oscillator H4 PLL.
	CONFIG     OSC=HSPLL
;Power Up Timer Enabled
	CONFIG     PWRT = ON            
;Watch Dog Timer disable
	CONFIG     WDT=OFF
;Low Voltage ICSP  Enabled
   	CONFIG     LVP = OFF            

 
;---------------------------------------------------------------------------------------

#define   RS        PORTH,1         ; RS pin
#define   RW        PORTH,2         ; RS pin
#define   E         PORTH,3         ; Enable pin

com       EQU       0x20            ; buffer for Instruction
dat       EQU       0x21            ; buffer for data
count1    EQU       0x22
count2    EQU       0x23
count3    EQU       0x24
offset    EQU       0x25


          ORG       0x0000

;************ initial *******************

init      call      delay
          call      delay
          call 		delay		   ; wait for startup LCD	

		  movlw     0x0F           ;Turn Analog input off and
 	      movwf     ADCON1         ;enable pins for I/O functions

		  movlw     0x07           ;Turn comparators off and
 	      movwf     CMCON          ;enable pins for I/O functions
                                  
          clrf      TRISH          ; All port H is output
          clrf      PORTH

          bsf       E

          movlw     B'00110011'    ; 
          call      WR_INS
          movlw     B'00110010'
          call      WR_INS
          movlw     B'00101000'    ; 4 bits, 2 lines,5X7 dot 
          call      WR_INS
          movlw     B'00001100'    ; display on/off
          call      WR_INS
          movlw     B'00000110'    ; Entry mode
          call      WR_INS
          movlw     B'00000001'    ; Clear ram
          call      WR_INS
          movlw     B'00000001'    ; Clear ram
          call      WR_INS

          clrf      offset         ; load offset of character table
line1:    movf      offset,w       
          call      TAB1
          addlw     0              ; Character = 00 ?
          btfsc     STATUS,Z       ; Character = 00 ?
          goto      line2          ; Yes , Z = 1
                                   ; No  , Z = 0
 		  call      WR_DATA		   ; Write Data to Display LCD
          incf      offset,f       ; next character
          incf      offset,f       ; next character
          goto      line1          

line2:    movlw     0xC0		   ; Goto Line 2
		  call	    WR_INS

		  clrf      offset
dsp_l2:   movf      offset,w       
          call      TAB2
          addlw     0              ; Character = 00 ?
          btfsc     STATUS,Z       ; Character = 00 ?
          goto      endmass        ; Yes , Z = 1
                                   ; No  , Z = 0
 		  call      WR_DATA		   ; Write Data to Display LCD
          incf      offset,f       ; next character
          incf      offset,f       ; next character
          goto      dsp_l2   
		  

endmass   nop
          goto      endmass        ; end program 

;********* Tebel of message ********************  
TAB1       addwf     PCL,F            ; Move offset to PC lower
          
	  DT   "ET-BASE PIC8722",0X0

;********* Tebel of message ********************  
TAB2       addwf     PCL,F            ; Move offset to PC lower
          
	  DT   "ETT CO.,LTD",0X0

;****************************************
; Write command to LCD
; Input  : W
; output : -
;****************************************
WR_INS    bcf       RS        ; clear RS
          movwf     com       ; W --> com
          andlw     0xF0      ; mask 4 bits MSB  W = X0
          addlw     8		  ; Hold Enable state
          movwf     PORTH     ; Send 4 bits MSB
          bcf       E         ; 
          call      delay     ; __    __      
          bsf       E         ;   |__|
          swapf     com,w
          andlw     0xF0      ; 1111 1000
          addlw     8		  ; Hold Enable state
          movwf     PORTH     ; send 4 bits LSB
          bcf       E         ;
          call      delay     ; __    __      
          bsf       E         ;   |__|
          call      delay
          return

;***************************************
; Write data to LCD
; Input  : W
; Output : -
;***************************************
WR_DATA   bsf       RS
          movwf     dat
          movf      dat,w
          andlw     0xF0
          addlw     0x0A	  ; Hold E and RS state
          movwf     PORTH
          bcf       E         
          call      delay     ; __    __
          bsf       E         ;   |__|
          swapf     dat,w
          andlw     0xF0
          addlw     0x0A	  ; Hold E and RS state
          movwf     PORTH
          bcf       E         ; 
          call      delay     ; __    __
          bsf       E         ;   |__|
          return

;***************************************
; Delay 
;***************************************
delay     movlw     10
          movwf     count1
del1      clrf      count2
del2      decfsz    count2
          goto      del2
          decfsz    count1
          goto      del1
          return

          END
