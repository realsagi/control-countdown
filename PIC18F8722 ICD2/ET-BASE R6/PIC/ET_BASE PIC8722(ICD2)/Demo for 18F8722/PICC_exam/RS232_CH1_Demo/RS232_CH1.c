/*
Code Support     : Board ET-BASE PIC8722
Compiler         : PIC C Compiler Version 3.249
Micro Controller : PIC18F8722
OSC              : 40MHz (HSPLL)
*/


#include <18F8722.h>


#define TX1   PIN_C6
#define RX1   PIN_C7

#fuses   H4,NOLVP,NOWDT,NOPROTECT,NOSTVREN

#use delay (clock = 40000000)
#use rs232(baud = 9600, xmit = TX1, rcv = RX1)


#use fast_io(C)

void main() {

  char Dat;

      set_tris_c(0B10000000);

     printf("\f\n\rPROGRAM TEST RS232 Channel-1 of ET-BASE PIC8722\n\r");
     printf("\n\rPlease press any key on keyboard you will see that key on screen \n\r");


   While (1)
   {
     Dat = getc();     // Receive Data from RS232 RX
     putc(Dat);        // Send Data to RS232 TX
   }
}
