
OTP 8-Bit CMOS MCU with EEPROM Data Memory

 PIC16CE623
 PIC16CE624
 PIC16CE625

High Performance RISC CPU:

 Only 35 instructions to learn
 All single-cycle instructions (200 ns), except for
  program branches which are two-cycle
 Operating speed:
  - DC - 20 MHz clock input
  - DC - 200 ns instruction cycle
 Interrupt capability
 16 special function hardware registers
 8-level deep hardware stack
 Direct, Indirect and Relative addressing modes
 Interrupt capability
 16 special function hardware registers
 8-level deep hardware stack
 Direct, Indirect and Relative addressing modes

Peripheral Features:

 13 I/O pins with individual direction control
 High current sink/source for direct LED drive
 Analog comparator module with:
  - Two analog comparators
  - Programmable on-chip voltage reference
  (VREF) module
  - Programmable input multiplexing from device
    inputs and internal voltage reference
  - Comparator outputs can be output signals
 Timer0: 8-bit timer/counter with 8-bit
  programmable prescaler
  
Special Microcontroller Features:

 In-Circuit Serial Programming (ICSP) (via two
  pins)
 Power-on Reset (POR)
 Power-up Timer (PWRT) and Oscillator Start-up
  Timer (OST)
 Brown-out Reset
 Watchdog Timer (WDT) with its own on-chip RC
  oscillator for reliable operation
 1,000,000 erase/write cycle EEPROM data
  memory
 EEPROM data retention > 40 years
 Programmable code protection
 Power saving SLEEP mode
 Selectable oscillator options
 Four user programmable ID locations
  CMOS Technology:
 Low-power, high-speed CMOS EPROM/EEPROM
  technology
 Fully static design
 Wide operating voltage range
  - 2.5V to 5.5V
 Commercial, industrial and extended temperature
  range
 Low power consumption
  - < 2.0 mA @ 5.0V, 4.0 MHz
  - 15 mA typical @ 3.0V, 32 kHz
  - < 1.0 mA typical standby current @ 3.0V
