
8-Bit CMOS Microcontrollers with USB

 PIC16C745  PIC16C765

Microcontroller Core Features:

 High-performance RISC CPU
 Only 35 single word instructions
 All single cycle instructions except for program
  branches which are two cycle
 Interrupt capability (up to 12 internal/external
  interrupt sources)
 Eight level deep hardware stack
 Direct, indirect and relative addressing modes
 Power-on Reset (POR)
 Power-up Timer (PWRT) and Oscillator Start-up
  Timer (OST)
 Watchdog Timer (WDT) with its own on-chip RC
  oscillator for reliable operation
 Brown-out detection circuitry for
  Brown-out Reset (BOR)
 Programmable code-protection
 Power saving SLEEP mode
 Selectable oscillator options
  - EC - External clock (24 MHz)
  - E4 - External clock with PLL (6 MHz)
  - HS - Crystal/Resonator (24 MHz)
  - H4 - Crystal/Resonator with PLL (6 MHz)
 Processor clock of 24 MHz derived from 6 MHz
  crystal or resonator
 Fully static low-power, high-speed CMOS
 In-Circuit Serial Programming? (ICSP)
 Operating voltage range
  - 4.35 to 5.25V
 High Sink/Source Current 25/25 mA
 Wide temperature range
  - Industrial (-40°C - 85°C)
 Low-power consumption:
  - ~ 16 mA @ 5V, 24 MHz
  - 100 ľA typical standby current

Peripheral Features:

 Universal Serial Bus (USB 1.1)
  - Soft attach/detach
 64 bytes of USB dual port RAM
 22 (PIC16C745) or 33 (PIC16C765) I/O pins
  - Individual direction control
  - 1 high voltage open drain (RA4)
  - 8 PORTB pins with:
  - Interrupt-on-change control (RB<7:4> only)
  - Weak pull-up control
  - 3 pins dedicated to USB
 Timer0: 8-bit timer/counter with 8-bit prescaler
 Timer1: 16-bit timer/counter with prescaler
  can be incremented during SLEEP via external
  crystal/clock
 Timer2: 8-bit timer/counter with 8-bit period
  register, prescaler and postscaler
 2 Capture, Compare and PWM modules
  - Capture is 16-bit, max. resolution is 10.4 ns
  - Compare is 16-bit, max. resolution is 167 ns
  - PWM maximum resolution is 10-bit
 8-bit multi-channel Analog-to-Digital converter
 Universal Synchronous Asynchronous Receiver
  Transmitter (USART/SCI)
 Parallel Slave Port (PSP) 8-bits wide, with external
  RD, WR and CS controls (PIC16C765 only)

