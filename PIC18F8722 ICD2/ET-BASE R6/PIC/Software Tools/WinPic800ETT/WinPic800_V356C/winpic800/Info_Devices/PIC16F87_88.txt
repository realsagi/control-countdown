
PIC16F87/88

18/20/28-Pin Enhanced Flash MCUs with nanoWatt Technology

Low-Power Features:

 Power-Managed modes:
  - Primary Run: RC oscillator, 76 ΅A, 1 MHz, 2V
  - RC_RUN: 7 ΅A, 31.25 kHz, 2V
  - SEC_RUN: 9 ΅A, 32 kHz, 2V
  - Sleep: 0.1 ΅A, 2V
 Timer1 Oscillator: 1.8 ΅A, 32 kHz, 2V
 Watchdog Timer: 2.2 ΅A, 2V
 Two-Speed Oscillator Start-up
  Oscillators:
 Three Crystal modes:
  - LP, XT, HS: up to 20 MHz
 Two External RC modes
 One External Clock mode:
  - ECIO: up to 20 MHz
 Internal oscillator block:
  - 8 user selectable frequencies: 31 kHz,
    125 kHz, 250 kHz, 500 kHz, 1 MHz, 2 MHz,
    4 MHz, 8 MHz

Peripheral Features:

 Capture, Compare, PWM (CCP) module:
  - Capture is 16-bit, max. resolution is 12.5 ns
  - Compare is 16-bit, max. resolution is 200 ns
  - PWM max. resolution is 10-bit
 10-bit, 7-channel Analog-to-Digital Converter
 Synchronous Serial Port (SSP) with SPI
  (Master/Slave) and I2C (Slave)
 Addressable Universal Synchronous
  Asynchronous Receiver Transmitter
  (AUSART/SCI) with 9-bit address detection:
  - RS-232 operation using internal oscillator
    (no external crystal required)
 Dual Analog Comparator module:
  - Programmable on-chip voltage reference
  - Programmable input multiplexing from device
    inputs and internal voltage reference
  - Comparator outputs are externally accessible

Special Microcontroller Features:

 100,000 erase/write cycles Enhanced Flash
  program memory typical
 1,000,000 typical erase/write cycles EEPROM
  data memory typical
 EEPROM Data Retention: > 40 years
 In-Circuit Serial Programming (ICSP)
  via two pins
 Processor read/write access to program memory
 Low-Voltage Programming
 In-Circuit Debugging via two pins
 Extended Watchdog Timer (WDT):
  - Programmable period from 1 ms to 268s
 Wide operating voltage range: 2.0V to 5.5V
